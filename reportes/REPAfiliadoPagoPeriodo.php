<?php
/**
 * Created by PhpStorm.
 * User: faviofigueroa
 * Date: 10/10/16
 * Time: 9:42 AM
 */
class REPAfiliadoPagoPeriodo
{

    function __construct(CTParametro $objParam)
    {
        //$this->SetMargins(0,0,0);



        $this->objParam = $objParam;
        $this->url_archivo = "../../../reportes_generados/" . $this->objParam->getParametro('nombre_archivo');


    }



    var $url_archivo;



    function pagoAfiliadoPrint($afiliadoPagoPeriodoSel,$redPuntos){




        $porcentaje_array = explode(",",$afiliadoPagoPeriodoSel[0]["porcentajes_pagados"]);



        $html = '<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Example 1</title>
    <style>
    .clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  width: 21cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 90px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(dimension.png);
}

#project {
  float: left;
}

#project span {
  color: #5D6975;
  text-align: right;
  width: 52px;
  margin-right: 10px;
  display: inline-block;
  font-size: 0.8em;
}

#company {
  float: right;
  text-align: right;
}

#project div,
#company div {
  white-space: nowrap;        
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
}

table th {
  padding: 5px 20px;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 20px;
  text-align: right;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
</style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
      <!--  <img src="logo.png">-->
      </div>
      
      <h1>RED</h1>
      <div id="company" style="float: right;" class="clearfix">
        <div>GONET</div>
        <div>Tipo de Moneda: '.$afiliadoPagoPeriodoSel[0]["tipo_moneda"].'</div>
        <div>Multiplicador: '.$afiliadoPagoPeriodoSel[0]["monto_multiplicar"].'</div>
        <div>Niveles a cobrar: '.$afiliadoPagoPeriodoSel[0]["niveles_cobrados"].'</div>
        <div>Pago red: '.$afiliadoPagoPeriodoSel[0]["pago_de_red"].'</div>
        <div>Porcetajes pagados: '.$afiliadoPagoPeriodoSel[0]["porcentajes_pagados"].'</div>
       
      
      </div>
      <div id="project" style="float: left;">
        <div><span>Proceso de Pago</span></div>
         <div><span>Codigo Afil </span>'.$afiliadoPagoPeriodoSel[0]["codigo"].'</div>
         <div><span>Gestion</span>'.$afiliadoPagoPeriodoSel[0]["gestion"].' </div>
        <div><span>Periodo</span>'.$afiliadoPagoPeriodoSel[0]["periodo"].'/12.</div>
        <div><span>Habilitado</span>'.$afiliadoPagoPeriodoSel[0]["habilitado"].'</div>
        <div><span>Red Activos</span>'.$afiliadoPagoPeriodoSel[0]["afiliados_activos"].'</div>
       
        <div><span>Nombre</span>'.$afiliadoPagoPeriodoSel[0]["nombre_completo2"].' </div>
        <div><span>Estado</span>'.$afiliadoPagoPeriodoSel[0]["estado"].' </div>
        
      </div>
    </header>
    <main>
    
    
    <table>
    <theader>
    <tr>
    <th>PAGO RED</th>
    <th>BONUS DE REGISTRO</th>
    <th>TOTAL A COBRAR</th>
    </tr>
    </theader>
    <tbody>
    <tr>
    <td>
    '.$afiliadoPagoPeriodoSel[0]["pago_de_red"].'
    </td>
    <td>
    '.$afiliadoPagoPeriodoSel[0]["pago_bonus_registro"].'
    </td>
    <td style="font-size:20pt;">
    <b>'.$afiliadoPagoPeriodoSel[0]["pago_total"].'</b>
    </td>
    </tr>
    </tbody>
    </table>
    
    
    <table>
    <theader>
    <tr>
    <th>PV ACUMULADO</th>
    <th>SUBE NIVEL</th>
    </tr>
    </theader>
    <tbody>
    <tr>
    <td>
    '.$afiliadoPagoPeriodoSel[0]["pv_acumulado"].'
    </td>
   
    <td style="font-size:20pt;">
    <b>'.$afiliadoPagoPeriodoSel[0]["sube_nivel"].'</b>
    </td>
    </tr>
    </tbody>
    </table>
    
    
    <table>
    <theader>
    <tr>
    <th>CV PROPIO</th>
    <th>PV PROPIO</th>
    </tr>
    </theader>
    <tbody>
    <tr>
    <td>
    '.$afiliadoPagoPeriodoSel[0]["cv_propio"].'
    </td>
    <td>
    '.$afiliadoPagoPeriodoSel[0]["pv_propio"].'
    </td>
    </tr>
    </tbody>
    </table>
    
    <table>
    <thead>
    <th></th>
    <th>1 Generacion ('.$porcentaje_array[0].'%)</th>
    <th>2 Generacion ('.$porcentaje_array[1].'%)</th>
    <th>3 Generacion ('.$porcentaje_array[2].'%)</th>
    <th>4 Generacion (10%)</th>
    <th>5 Generacion (10%)</th>
   
    
    </thead>
    <tbody>
    <tr>
    <td>CV</td>
    <td>'.$afiliadoPagoPeriodoSel[0]["primera_gen_cv"].'</td>
    <td>'.$afiliadoPagoPeriodoSel[0]["segunda_gen_cv"].'</td>
    <td>'.$afiliadoPagoPeriodoSel[0]["tercera_gen_cv"].'</td>
    <td>'.$afiliadoPagoPeriodoSel[0]["cuarta_gen_cv"].'</td>
    <td>'.$afiliadoPagoPeriodoSel[0]["quinta_gen_cv"].'</td>
    </tr>
    
    <tr>
    <td>(CV * %)*'.$afiliadoPagoPeriodoSel[0]["monto_multiplicar"].'</td>
    <td>'.($afiliadoPagoPeriodoSel[0]["primera_gen_cv"] * ($porcentaje_array[0]/100)) * $afiliadoPagoPeriodoSel[0]["monto_multiplicar"].' '.$afiliadoPagoPeriodoSel[0]["tipo_moneda"].'</td>
    <td>'.($afiliadoPagoPeriodoSel[0]["segunda_gen_cv"] * ($porcentaje_array[1]/100)) * $afiliadoPagoPeriodoSel[0]["monto_multiplicar"].' '.$afiliadoPagoPeriodoSel[0]["tipo_moneda"].'</td>
    <td>'.($afiliadoPagoPeriodoSel[0]["tercera_gen_cv"] * ($porcentaje_array[2]/100)) * $afiliadoPagoPeriodoSel[0]["monto_multiplicar"].' '.$afiliadoPagoPeriodoSel[0]["tipo_moneda"].'</td>
    <td>'.($afiliadoPagoPeriodoSel[0]["cuarta_gen_cv"] * 0.10) * $afiliadoPagoPeriodoSel[0]["monto_multiplicar"].' '.$afiliadoPagoPeriodoSel[0]["tipo_moneda"].'</td>
    <td>'.($afiliadoPagoPeriodoSel[0]["quinta_gen_cv"] * 0.10) *$afiliadoPagoPeriodoSel[0]["monto_multiplicar"].' '.$afiliadoPagoPeriodoSel[0]["tipo_moneda"].'</td>
    </tr>
    
    
    <tr>
    <td>PV</td>
    <td>'.$afiliadoPagoPeriodoSel[0]["primera_gen_pv"].'</td>
    <td>'.$afiliadoPagoPeriodoSel[0]["segunda_gen_pv"].'</td>
    <td>'.$afiliadoPagoPeriodoSel[0]["tercera_gen_pv"].'</td>
    <td>'.$afiliadoPagoPeriodoSel[0]["cuarta_gen_pv"].'</td>
    <td>'.$afiliadoPagoPeriodoSel[0]["quinta_gen_pv"].'</td>
    </tr>
    
    
   
    
    
    </tbody>
    </table>
    
      <table>
        <thead>
          <tr>
            <th>Generacion</th>
            <th class="service">Codigo</th>
            <th class="desc">Afiliado</th>
            <th>CV</th>
            <th>PV</th>
          </tr>
        </thead>
        <tbody>';
        foreach ($redPuntos as $punto) {
            $html .= '<tr>
            <td class="service">'.$punto["nivel"].'</td>
            <td class="desc">
            <span style="display:block;">'.$punto["codigo_patrocinador"].'</span>
            <span>'.$punto["nombre_completo2_patrocinador"].'</span>
            </td>
            <td class="unit">
             <span style="display:block;">'.$punto["codigo"].'</span>
             <span>'.$punto["nombre_completo2"].'</span>
            </td>
            <td class="qty">'.$punto["sum_cv"].'</td>
            <td class="qty">'.$punto["sum_pv"].'</td>
           
          </tr>';
        }

        $html.='</tbody>
      </table>
      <div id="notices">
        <div>NOTA:</div>
        <div class="notice">naturalgo.org.</div>
        

      </div>
    </main>
    <!--<footer>
PROFORMA - KAE ELECTRONICS    </footer>-->
  
</body></html>';



        return $html;

    }

    function SumPagoDetalle($sumPagoDetalleJson){

        $sumPagoDetalle = json_decode($sumPagoDetalleJson[0]["json"], true);




        $html = '<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Example 1</title>
    <style>
    .clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  width: 21cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 90px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(dimension.png);
}

#project {
  float: left;
}

#project span {
  color: #5D6975;
  text-align: right;
  width: 52px;
  margin-right: 10px;
  display: inline-block;
  font-size: 0.8em;
}

#company {
  float: right;
  text-align: right;
}

#project div,
#company div {
  white-space: nowrap;        
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
}

table th {
  padding: 5px 20px;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 20px;
  text-align: right;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
</style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
      <!--  <img src="logo.png">-->
      </div>
      
      <h1>Reporte Sum Total Detalle Pagos por Periodo</h1>
      <div id="company" style="float: right;" class="clearfix">
        <div>NATURALGO</div>
        <div>Sucursal: '.$sumPagoDetalle[0]["sucursal"]["sucursal"].'</div>
        <div>Mes: '.$sumPagoDetalle[0]["sucursal"]["f_literal_periodo"].'</div>
       
       
      
      </div>
      <div id="project" style="float: left;">
         <div><span>Fecha Inicial </span>'.$sumPagoDetalle[0]["fechas"]["fecha_ini"].'</div>
         <div><span>Fecha Fin </span>'.$sumPagoDetalle[0]["fechas"]["fecha_fin"].'</div>
      </div>
    </header>
    <main>
    
    
    
      <table>
        <thead>
          <tr>
            <th>Codigo</th>
            <th class="service">punto_de_venta</th>
            <th class="desc">pago bonus registro</th>
            <th>pago de red</th>
            <th>pago total</th>
          </tr>
        </thead>
        <tbody>';
        foreach ($sumPagoDetalle[0]["pago_periodo"] as $detalle) {
            $html .= '<tr>
            <td class="service">'.$detalle["codigo"].'</td>
            <td class="desc">
                <span style="display:block;">'.$detalle["punto_de_venta"].'</span>
            </td>
            <td class="qty">'.$detalle["pago_bonus_registro"].'</td>
            <td class="qty">'.$detalle["pago_de_red"].'</td>
            <td class="qty">'.$detalle["pago_total"].'</td>
           
          </tr>';
        }

        $html.='
<tr>
    
    <td colspan="2">Pagados</td>
    <td>'.$sumPagoDetalle[0]["totales_pagados"]["sum_pago_bonus_registro"].'</td>
    <td>'.$sumPagoDetalle[0]["totales_pagados"]["sum_pago_de_red"].'</td>
    <td>'.$sumPagoDetalle[0]["totales_pagados"]["sum_pago_total"].'</td>
</tr>
<tr>
    
    <td colspan="2">No Pagados</td>
    <td>'.$sumPagoDetalle[0]["totales_no_pagados"]["sum_pago_bonus_registro"].'</td>
    <td>'.$sumPagoDetalle[0]["totales_no_pagados"]["sum_pago_de_red"].'</td>
    <td>'.$sumPagoDetalle[0]["totales_no_pagados"]["sum_pago_total"].'</td>
   
</tr>
<tr>
    
    <td colspan="2">Total</td>
    <td>'.$sumPagoDetalle[0]["totales"]["sum_pago_bonus_registro"].'</td>
    <td>'.$sumPagoDetalle[0]["totales"]["sum_pago_de_red"].'</td>
    <td>'.$sumPagoDetalle[0]["totales"]["sum_pago_total"].'</td>
   
</tr>
</tbody>
      </table>
      <div id="notices">
        <div>NOTA:</div>
        <div class="notice">naturalgo.org.</div>
        

      </div>
    </main>
    <!--<footer>
PROFORMA - KAE ELECTRONICS    </footer>-->
  
</body></html>';



        return $html;

    }




    function afiliacion($sumPagoDetalleJson){

        $sumPagoDetalle = json_decode($sumPagoDetalleJson[0]["json"], true);




        $html = '<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Example 1</title>
    <style>
    .clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  width: 21cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 90px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(dimension.png);
}

#project {
  float: left;
}

#project span {
  color: #5D6975;
  text-align: right;
  width: 52px;
  margin-right: 10px;
  display: inline-block;
  font-size: 0.8em;
}

#company {
  float: right;
  text-align: right;
}

#project div,
#company div {
  white-space: nowrap;        
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
}

table th {
  padding: 5px 20px;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 20px;
  text-align: right;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
</style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
      <!--  <img src="logo.png">-->
      </div>
      
      <h1>Registro Afiliado</h1>
      <div id="company" style="float: right;" class="clearfix">
        <div>NATURALGO</div>
        <div>CODIGO AFILIADO: '.$sumPagoDetalle[0]["sucursal"]["sucursal"].'</div>
        <div>CODIGO PATROCINADOR: '.$sumPagoDetalle[0]["sucursal"]["f_literal_periodo"].'</div>
       
      </div>
      
      <h1>Datos Persona</h1>
      <div id="company" style="float: right;" class="clearfix">
        <div>Nombre Completo: '.$sumPagoDetalle[0]["sucursal"]["sucursal"].'</div>
        <div>CI: '.$sumPagoDetalle[0]["sucursal"]["f_literal_periodo"].'</div>
       
      </div>
      <div id="project" style="float: left;">
         <div><span>PAIS </span>'.$sumPagoDetalle[0]["fechas"]["fecha_ini"].'</div>
         <div><span>NUMERO DOCUMENTO </span>'.$sumPagoDetalle[0]["fechas"]["fecha_fin"].'</div>
      </div>
      
    </header>
    
  
</body></html>';



        return $html;

    }








}

?>