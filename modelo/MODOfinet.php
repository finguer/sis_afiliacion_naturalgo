<?php
/**
 *@package pXP
 *@file gen-MODAfiliado.php
 *@author  (admin)
 *@date 02-04-2015 01:06:18
 *@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
 */

class MODOfinet extends MODbase{

    function __construct(CTParametro $pParam){
        parent::__construct($pParam);
        $this->cone = new conexion();
        $this->link = $this->cone->conectarpdo(); //conexion a pxp(postgres)

    }


    function verCodigosIngreso(){



        //Definicion de variables para ejecucion del procedimientp
        $this->procedimiento='afi.ft_oficina_ime';
        $this->transaccion='OFI_IN_SEL';
        $this->tipo_procedimiento='IME';//tipo de transaccion

        //Definicion de la lista del resultado del query
        $this->setParametro('ci','ci','varchar');
        $this->setParametro('codigo','codigo','numeric');



        //Ejecuta la instruccion
        $this->armarConsulta();
        $this->ejecutarConsulta();

        //Devuelve la respuesta
        return $this->respuesta;
    }


    function verDatosAfiliado(){


        $this->setParametro('ci','ci','varchar');
        $this->setParametro('codigo','codigo','numeric');

        //Definicion de variables para ejecucion del procedimientp
        $this->procedimiento='afi.ft_oficina_sel';
        $this->transaccion='AFI_OFI_SEL';
        $this->tipo_procedimiento='SEL';//tipo de transaccion

        //Definicion de la lista del resultado del query


		$this->captura('direccion_principal','varchar');
		$this->captura('lugar_nacimiento','varchar');
		$this->captura('estado_reg','varchar');
		$this->captura('fecha_nacimiento','date');
        $this->captura('desc_person','text');
        $this->captura('ci','varchar');
        $this->captura('codigo','numeric');

        $this->captura('celular1','varchar');
        $this->captura('celular2','varchar');
        $this->captura('telefono1','varchar');
        $this->captura('telefono2','varchar');
        $this->captura('li','int4');
        $this->captura('codigo_patrocinador','numeric');
        $this->captura('desc_person_patrocinador','text');





        //Ejecuta la instruccion
        $this->armarConsulta();
        $this->ejecutarConsulta();

        //Devuelve la respuesta
        return $this->respuesta;
    }


}
?>