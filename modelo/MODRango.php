<?php
/**
*@package pXP
*@file gen-MODRango.php
*@author  (admin)
*@date 14-10-2016 13:55:26
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODRango extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarRango(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_rango_sel';
		$this->transaccion='AFI_RAN_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_rango','int4');
		$this->captura('porcentaje_de_pago','numeric');
		$this->captura('afiliado_activos','int4');
		$this->captura('estado_reg','varchar');
		$this->captura('volumen_organizacional','numeric');
		$this->captura('volumen_cobro_nivel','numeric');
		$this->captura('id_rango_fk','int4');
		$this->captura('volumen_personal','numeric');
		$this->captura('nombre','varchar');
		$this->captura('periodos_acumulados','int4');
		$this->captura('nivel','int4');
		$this->captura('usuario_ai','varchar');
		$this->captura('fecha_reg','timestamp');
		$this->captura('id_usuario_reg','int4');
		$this->captura('id_usuario_ai','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('id_usuario_mod','int4');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');
		$this->captura('desc_rango_fk','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarRango(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_rango_ime';
		$this->transaccion='AFI_RAN_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('porcentaje_de_pago','porcentaje_de_pago','numeric');
		$this->setParametro('afiliado_activos','afiliado_activos','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('volumen_organizacional','volumen_organizacional','numeric');
		$this->setParametro('volumen_cobro_nivel','volumen_cobro_nivel','numeric');
		$this->setParametro('id_rango_fk','id_rango_fk','int4');
		$this->setParametro('volumen_personal','volumen_personal','numeric');
		$this->setParametro('nombre','nombre','varchar');
		$this->setParametro('periodos_acumulados','periodos_acumulados','int4');
		$this->setParametro('nivel','nivel','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarRango(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_rango_ime';
		$this->transaccion='AFI_RAN_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_rango','id_rango','int4');
		$this->setParametro('porcentaje_de_pago','porcentaje_de_pago','numeric');
		$this->setParametro('afiliado_activos','afiliado_activos','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('volumen_organizacional','volumen_organizacional','numeric');
		$this->setParametro('volumen_cobro_nivel','volumen_cobro_nivel','numeric');
		$this->setParametro('id_rango_fk','id_rango_fk','int4');
		$this->setParametro('volumen_personal','volumen_personal','numeric');
		$this->setParametro('nombre','nombre','varchar');
		$this->setParametro('periodos_acumulados','periodos_acumulados','int4');
		$this->setParametro('nivel','nivel','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarRango(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_rango_ime';
		$this->transaccion='AFI_RAN_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_rango','id_rango','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
}
?>