<?php
/**
*@package pXP
*@file gen-MODPuntoItem.php
*@author  (admin)
*@date 18-12-2015 23:29:48
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODPuntoItem extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarPuntoItem(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_punto_item_sel';
		$this->transaccion='AFI_PUNTI_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_punto_item','int4');
		$this->captura('pv','numeric');
		$this->captura('estado_reg','varchar');
		$this->captura('cv','numeric');
		$this->captura('id_set','int4');
		$this->captura('id_item','int4');
		$this->captura('usuario_ai','varchar');
		$this->captura('fecha_reg','timestamp');
		$this->captura('id_usuario_reg','int4');
		$this->captura('id_usuario_ai','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('id_usuario_mod','int4');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');
		$this->captura('tipo','varchar');
		$this->captura('desc_item','varchar');
		$this->captura('desc_set','varchar');
		$this->captura('bonus_patrocinador','varchar');
		$this->captura('desc_bonus_config','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarPuntoItem(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_punto_item_ime';
		$this->transaccion='AFI_PUNTI_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('pv','pv','numeric');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('cv','cv','numeric');
		$this->setParametro('id_set','id_set','int4');
		$this->setParametro('id_item','id_item','int4');
		$this->setParametro('tipo','tipo','varchar');
		$this->setParametro('bonus_patrocinador','bonus_patrocinador','varchar');
		$this->setParametro('id_bonus_config','id_bonus_config','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarPuntoItem(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_punto_item_ime';
		$this->transaccion='AFI_PUNTI_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_punto_item','id_punto_item','int4');
		$this->setParametro('pv','pv','numeric');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('cv','cv','numeric');
		$this->setParametro('id_set','id_set','int4');
		$this->setParametro('id_item','id_item','int4');
		$this->setParametro('tipo','tipo','varchar');
		$this->setParametro('bonus_patrocinador','bonus_patrocinador','varchar');
        $this->setParametro('id_bonus_config','id_bonus_config','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarPuntoItem(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_punto_item_ime';
		$this->transaccion='AFI_PUNTI_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_punto_item','id_punto_item','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
}
?>