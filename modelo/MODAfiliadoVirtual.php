<?php
/**
*@package pXP
*@file gen-MODAfiliado.php
*@author  (admin)
*@date 02-04-2015 01:06:18
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODAfiliadoVirtual extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
		$this->cone = new conexion();
		$this->link = $this->cone->conectarpdo(); //conexion a pxp(postgres)

	}


	function listarAfiliadoOficinaVirtual(){
		$codigo = $this->aParam->getParametro('codigo');
		$ci = $this->aParam->getParametro('ci');

		try {
			//obtener sucursal del usuario
			$this->link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->link->beginTransaction();
			if($codigo == 5910001){
				$res = $this->link->prepare("select
										  afil.direccion_principal,
										  afil.lugar_nacimiento,
										  afil.estado_reg,
										  afil.fecha_nacimiento,
										  PERSON.nombre_completo2 as desc_person,
										  PERSON.ci,
										  afil.codigo as codigo,
										  afil.monto_registro,
										  afil.modalidad_de_registro,
										  afil.colocacion,
										  PERSON.celular1,
										  PERSON.celular2,
										  PERSON.telefono1,
										  PERSON.telefono2
										from afi.tafiliado afil
										  inner join segu.tusuario usu1 on usu1.id_usuario = afil.id_usuario_reg
										  INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
										  left join segu.tusuario usu2 on usu2.id_usuario = afil.id_usuario_mod
										  WHERE afil.codigo = ".$codigo." and PERSON.ci = ".$ci."::varchar ");
			}else{
				$res = $this->link->prepare("select
										  afil.direccion_principal,
										  afil.lugar_nacimiento,
										  afil.estado_reg,
										  afil.fecha_nacimiento,
										  PERSON.nombre_completo2 as desc_person,
										  PERSONPA.nombre_completo2 as desc_person_patro,
										  PERSONCO.nombre_completo2 as desc_person_colo,
										  PERSON.ci,

										  af.direccion_principal as desc_patrocinador,
										  af.codigo as codigo_patrocinador,
										  afco.codigo as codigo_colocacion,
										  afil.codigo as codigo,
										  afil.monto_registro,
										  afil.modalidad_de_registro,
										  afil.colocacion,
										  PERSON.celular1,
										  PERSON.celular2,
										  PERSON.telefono1,
										  PERSON.telefono2




										from afi.tafiliado afil
										  inner join segu.tusuario usu1 on usu1.id_usuario = afil.id_usuario_reg
										  INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona

										  LEFT join  afi.tafiliado af on af.id_afiliado = afil.id_patrocinador

										  LEFT join  afi.tafi_patro afipatro on afipatro.id_afiliado = afil.id_afiliado
										  LEFT join  afi.tafiliado afco on afco.id_afiliado = afipatro.id_colocacion

										  INNER JOIN segu.vpersona PERSONPA on PERSONPA.id_persona = af.id_persona
										  INNER JOIN segu.vpersona PERSONCO on PERSONCO.id_persona = afco.id_persona

										  left join segu.tusuario usu2 on usu2.id_usuario = afil.id_usuario_mod
										  WHERE afil.codigo = ".$codigo." and PERSON.ci = ".$ci."::varchar ");
			}

			$res->execute();
			$result = $res->fetchAll(PDO::FETCH_ASSOC);

			$this->link->commit();
			$this->respuesta = new Mensaje();
			$this->respuesta->setMensaje('EXITO', $this->nombre_archivo, 'La consulta se ejecuto con exito de insercion de nota', 'La consulta se ejecuto con exito', 'base', 'no tiene', 'no tiene', 'SEL', '$this->consulta', 'no tiene');
			$this->respuesta->setTotal(1);
			$this->respuesta->setDatos($result);
			return $this->respuesta;

		} catch (Exception $e) {
			$this->link->rollBack();
			$this->respuesta = new Mensaje();
			if ($e->getCode() == 3) {//es un error de un procedimiento almacenado de pxp
				$this->respuesta->setMensaje($resp_procedimiento['tipo_respuesta'], $this->nombre_archivo, $resp_procedimiento['mensaje'], $resp_procedimiento['mensaje_tec'], 'base', $this->procedimiento, $this->transaccion, $this->tipo_procedimiento, $this->consulta);
			} else if ($e->getCode() == 2) {//es un error en bd de una consulta
				$this->respuesta->setMensaje('ERROR', $this->nombre_archivo, $e->getMessage(), $e->getMessage(), 'modelo', '', '', '', '');
			} else {//es un error lanzado con throw exception
				throw new Exception($e->getMessage(), 2);
			}
		}
		return $this->respuesta;
	}




	function tablaBinario(){


		try {
			//obtener sucursal del usuario
			$this->link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->link->beginTransaction();

			$id_afiliado = $this->encontrarIdAfiliado();

			$colocaciones = $this->verColocacion($id_afiliado[0]["id_afiliado"]);


			$arra =  array();
			if($colocaciones[0]["izquierda"] != null){
				$disponibilidad_izquierda = $this->verDisponibilidadBinario($colocaciones[0]["izquierda"]);
				$arra[0] = $disponibilidad_izquierda;
			}
			else{
				$arra[0] = "izquierdo_cero";
			}
			if($colocaciones[0]["derecha"] != null){
				$disponibilidad_derecha = $this->verDisponibilidadBinario($colocaciones[0]["derecha"]);

				$arra[1] = $disponibilidad_derecha;
			}else{
				$arra[1] = "derecho_cero";
			}





			$this->link->commit();
			$this->respuesta = new Mensaje();
			$this->respuesta->setMensaje('EXITO', $this->nombre_archivo, 'La consulta se ejecuto con exito de insercion de nota', 'La consulta se ejecuto con exito', 'base', 'no tiene', 'no tiene', 'SEL', '$this->consulta', 'no tiene');
			$this->respuesta->setTotal(1);
			$this->respuesta->setDatos($arra);
			return $this->respuesta;

		} catch (Exception $e) {
			$this->link->rollBack();
			$this->respuesta = new Mensaje();
			if ($e->getCode() == 3) {//es un error de un procedimiento almacenado de pxp
				$this->respuesta->setMensaje($resp_procedimiento['tipo_respuesta'], $this->nombre_archivo, $resp_procedimiento['mensaje'], $resp_procedimiento['mensaje_tec'], 'base', $this->procedimiento, $this->transaccion, $this->tipo_procedimiento, $this->consulta);
			} else if ($e->getCode() == 2) {//es un error en bd de una consulta
				$this->respuesta->setMensaje('ERROR', $this->nombre_archivo, $e->getMessage(), $e->getMessage(), 'modelo', '', '', '', '');
			} else {//es un error lanzado con throw exception
				throw new Exception($e->getMessage(), 2);
			}
		}
		return $this->respuesta;

	}
	function verDisponibilidadBinario($id_posicion){

		$count = $this->link->prepare("with recursive binario(id_colocacion, izquierda, derecha,persona,codigo,codigo_izquierda,codigo_derecha)
									as(
									  select patro.id_colocacion as id_colocacion, patro.izquierda as izquierda ,
										patro.derecha as derecha , PERSON.nombre_completo2 as persona , af.codigo as codigo,
										'1'::numeric as codigo_izquierda ,'1'::NUMERIC as codigo_derecha
									  from afi.tpatro_colo patro
									  INNER join  afi.tafiliado af on af.id_afiliado = patro.id_colocacion
									  INNER JOIN segu.vpersona PERSON on PERSON.id_persona = af.id_persona
									  where patro.id_colocacion = ".$id_posicion."

									  union

									  select patro.id_colocacion as id_colocacion, patro.izquierda as izquierda ,
										patro.derecha as derecha , PERSON.nombre_completo2 as persona , af.codigo as codigo ,
										afiz.codigo as codigo_izquierda, afde.codigo as codigo_derecha
									  from binario , afi.tpatro_colo patro

										INNER join  afi.tafiliado af on af.id_afiliado = patro.id_colocacion
										INNER JOIN segu.vpersona PERSON on PERSON.id_persona = af.id_persona

										LEFT join  afi.tafiliado afiz on afiz.id_afiliado = izquierda
										LEFT join  afi.tafiliado afde on afde.id_afiliado = derecha


									  where patro.id_colocacion = binario.izquierda or  patro.id_colocacion = binario.derecha
									)
									select *
									from binario;");
		$count->execute();
		$count_result = $count->fetchAll(PDO::FETCH_ASSOC);

		return $count_result;
	}



	function conteoRamas(){

		try {
			//obtener sucursal del usuario
			$this->link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->link->beginTransaction();

			$count= $this->generarConteodeRamas();
			$this->link->commit();
			$this->respuesta = new Mensaje();
			$this->respuesta->setMensaje('EXITO', $this->nombre_archivo, 'La consulta se ejecuto con exito de insercion de nota', 'La consulta se ejecuto con exito', 'base', 'no tiene', 'no tiene', 'SEL', '$this->consulta', 'no tiene');
			$this->respuesta->setTotal(1);
			$this->respuesta->setDatos($count);
			return $this->respuesta;

		} catch (Exception $e) {
			$this->link->rollBack();
			$this->respuesta = new Mensaje();
			if ($e->getCode() == 3) {//es un error de un procedimiento almacenado de pxp
			$this->respuesta->setMensaje($resp_procedimiento['tipo_respuesta'], $this->nombre_archivo, $resp_procedimiento['mensaje'], $resp_procedimiento['mensaje_tec'], 'base', $this->procedimiento, $this->transaccion, $this->tipo_procedimiento, $this->consulta);
			} else if ($e->getCode() == 2) {//es un error en bd de una consulta
				$this->respuesta->setMensaje('ERROR', $this->nombre_archivo, $e->getMessage(), $e->getMessage(), 'modelo', '', '', '', '');
			} else {//es un error lanzado con throw exception
				throw new Exception($e->getMessage(), 2);
			}
		}
		return $this->respuesta;

	}
	function generarConteodeRamas(){
		$id_afiliado = $this->encontrarIdAfiliado();

		$colocaciones = $this->verColocacion($id_afiliado[0]["id_afiliado"]);


		$arra = array();
		if($colocaciones[0]['izquierda'] != NULL){
			$izquierda = $this->verCuantosHayenRama($colocaciones[0]['izquierda'],'izquierda');
			$arra[0]["conteo"] = $izquierda[0]["conteo"];
			$arra[0]["posicion"] = $izquierda[0]["posicion"];
		}else{
			$arra[0]["conteo"] = 0;
			$arra[0]["posicion"] = 'izquierda';
		}
		if($colocaciones[0]['derecha'] != NULL){
			$derecha = $this->verCuantosHayenRama($colocaciones[0]['derecha'],'derecha');
			$arra[1]["conteo"] = $derecha[0]["conteo"];
			$arra[1]["posicion"] = $derecha[0]["posicion"];
		}else{
			$arra[1]["conteo"] = 0;
			$arra[1]["posicion"] = 'derecha';
		}






		return $arra;

	}

	function encontrarIdAfiliado(){
		$codigo = $this->aParam->getParametro('codigo');
		$res = $this->link->prepare("select id_afiliado from afi.tafiliado where codigo = ".$codigo." ");
		$res->execute();
		$result = $res->fetchAll(PDO::FETCH_ASSOC);


		return $result;
	}
	function verColocacion($id_afiliado){
		$colocacion = $this->link->prepare("select * from afi.tpatro_colo where id_colocacion = ".$id_afiliado." ");
		$colocacion->execute();
		$colocacion_result = $colocacion->fetchAll(PDO::FETCH_ASSOC);
		return $colocacion_result;
	}

	function  verCuantosHayenRama($id_posicion,$posicion){
		$count = $this->link->prepare("with recursive binario(id_colocacion, izquierda, derecha)
										as(
										  select patro.id_colocacion as id_colocacion, patro.izquierda as izquierda , patro.derecha as derecha
										  from afi.tpatro_colo patro
										  where patro.id_colocacion = ".$id_posicion."

										  union

										  select patro.id_colocacion as id_colocacion, patro.izquierda as izquierda , patro.derecha as derecha
										  from binario, afi.tpatro_colo patro
										  where patro.id_colocacion = binario.izquierda or  patro.id_colocacion = binario.derecha
										)
										select count(*) as  conteo, '".$posicion."' as posicion
										from binario;");
		$count->execute();
		$count_result = $count->fetchAll(PDO::FETCH_ASSOC);
		return $count_result;
	}
	function encontrarCodigoAfiliado($id_afiliado){
		$codigo = $this->aParam->getParametro('codigo');
		$res = $this->link->prepare("select id_afiliado from afi.tafiliado where id_afiliado = ".$id_afiliado." ");
		$res->execute();
		$result = $res->fetchAll(PDO::FETCH_ASSOC);


		return $result;
	}

			
}
?>