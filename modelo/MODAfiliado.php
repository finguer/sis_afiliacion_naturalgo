<?php
/**
*@package pXP
*@file gen-MODAfiliado.php
*@author  (admin)
*@date 02-04-2015 01:06:18
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODAfiliado extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
		$this->cone = new conexion();
		$this->link = $this->cone->conectarpdo(); //conexion a pxp(postgres)

	}


	function listarAfiliado(){
		
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_afiliado_sel';
		$this->transaccion='AFI_AFIL_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion


		//Definicion de la lista del resultado del query
		$this->captura('id_afiliado','int4');
		$this->captura('direccion_principal','varchar');
		$this->captura('id_persona','int4');
		$this->captura('lugar_nacimiento','varchar');
		$this->captura('estado_reg','varchar');
		$this->captura('fecha_nacimiento','date');
		$this->captura('id_usuario_ai','int4');
		$this->captura('id_usuario_reg','int4');
		$this->captura('fecha_reg','timestamp');
		$this->captura('usuario_ai','varchar');
		$this->captura('id_usuario_mod','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');
		$this->captura('desc_person','text');
		$this->captura('ci','varchar');

		$this->captura('codigo_patrocinador','varchar');
		$this->captura('codigo','varchar');
		
		$this->captura('id_patrocinador','int4');

		$this->captura('monto_registro','numeric');
		$this->captura('modalidad_de_registro','varchar');
		$this->captura('id_set_item','int4');
		$this->captura('id_punto_venta','int4');
		$this->captura('id_almacen','int4');

        $this->setParametro('solo_registrados_por_usuario','solo_registrados_por_usuario','varchar');


		
		
		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarAfiliado(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afiliado_ime';
		$this->transaccion='AFI_AFIL_INS';
		$this->tipo_procedimiento='IME';


        //persona
        $this->setParametro('ap_paterno','ap_paterno','varchar');
        $this->setParametro('ap_materno','ap_materno','varchar');
        $this->setParametro('nombre','nombre','varchar');
        $this->setParametro('ci','ci','varchar');
        $this->setParametro('correo','correo','varchar');
        $this->setParametro('celular1','celular1','varchar');
        $this->setParametro('telefono1','telefono1','varchar');



        //Define los parametros para la funcion
		$this->setParametro('direccion_principal','direccion_principal','varchar');
		$this->setParametro('id_persona','id_persona','int4');
		$this->setParametro('lugar_nacimiento','lugar_nacimiento','varchar');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('fecha_nacimiento','fecha_nacimiento','date');

		$this->setParametro('id_patrocinador','id_patrocinador','int4');

		//$this->setParametro('modalidad_de_registro','modalidad_de_registro','varchar');

		$this->setParametro('codigo','codigo','varchar');

		$this->setParametro('id_set_item','id_set_item','int4');
		$this->setParametro('id_punto_venta','id_punto_venta','int4');

		$this->setParametro('monto_registro','monto_registro','numeric');
		/*

		if($this->aParam->getParametro('modalidad_de_registro') == 'Afiliado Directo'){

			$this->arreglo = array("estado" => 'activo');
		}else{
			$this->arreglo = array("estado" => 'inactivo');

		}*/
		$this->setParametro('estado','estado','varchar');
		$this->setParametro('id_moneda','id_moneda','int4');
		$this->setParametro('pais','pais','varchar');
		$this->setParametro('pre_registro','pre_registro','varchar');
		$this->setParametro('id_persona_pre_registro','id_persona_pre_registro','int4');




		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarAfiliado(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afiliado_ime';
		$this->transaccion='AFI_AFIL_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('direccion_principal','direccion_principal','varchar');
		$this->setParametro('id_persona','id_persona','int4');
		$this->setParametro('lugar_nacimiento','lugar_nacimiento','varchar');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('fecha_nacimiento','fecha_nacimiento','date');
		$this->setParametro('id_patrocinador','id_patrocinador','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarAfiliado(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afiliado_ime';
		$this->transaccion='AFI_AFIL_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_afiliado','id_afiliado','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}



	function conteoRamas(){

		try {
			//obtener sucursal del usuario
			$this->link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->link->beginTransaction();

			$count= $this->generarConteodeRamas();
			$this->link->commit();
			$this->respuesta = new Mensaje();
			$this->respuesta->setMensaje('EXITO', $this->nombre_archivo, 'La consulta se ejecuto con exito de insercion de nota', 'La consulta se ejecuto con exito', 'base', 'no tiene', 'no tiene', 'SEL', '$this->consulta', 'no tiene');
			$this->respuesta->setTotal(1);
			$this->respuesta->setDatos($count);
			return $this->respuesta;

		} catch (Exception $e) {
			$this->link->rollBack();
			$this->respuesta = new Mensaje();
			if ($e->getCode() == 3) {//es un error de un procedimiento almacenado de pxp
			$this->respuesta->setMensaje($resp_procedimiento['tipo_respuesta'], $this->nombre_archivo, $resp_procedimiento['mensaje'], $resp_procedimiento['mensaje_tec'], 'base', $this->procedimiento, $this->transaccion, $this->tipo_procedimiento, $this->consulta);
			} else if ($e->getCode() == 2) {//es un error en bd de una consulta
				$this->respuesta->setMensaje('ERROR', $this->nombre_archivo, $e->getMessage(), $e->getMessage(), 'modelo', '', '', '', '');
			} else {//es un error lanzado con throw exception
				throw new Exception($e->getMessage(), 2);
			}
		}
		return $this->respuesta;

	}
	function generarConteodeRamas(){
		$afi = $this->aParam->getParametro('afi'); // es el id del afiliado
		$colocaciones = $this->verColocacion();
		$izquierda = $this->verCuantosHayenRama($colocaciones[0]['izquierda'],'izquierda');
		$derecha = $this->verCuantosHayenRama($colocaciones[0]['derecha'],'derecha');
		$arra = array();
		$arra[0]["conteo"] = $izquierda[0]["conteo"];
		$arra[0]["posicion"] = $izquierda[0]["posicion"];
		$arra[1]["conteo"] = $derecha[0]["conteo"];
		$arra[1]["posicion"] = $derecha[0]["posicion"];

		return $arra;

	}

	function verColocacion(){
		$afi = $this->aParam->getParametro('afi');
		$colocacion = $this->link->prepare("select * from afi.tpatro_colo where id_colocacion = ".$afi." ");
		$colocacion->execute();
		$colocacion_result = $colocacion->fetchAll(PDO::FETCH_ASSOC);
		return $colocacion_result;
	}

	function  verCuantosHayenRama($id_posicion,$posicion){
		$count = $this->link->prepare("with recursive binario(id_colocacion, izquierda, derecha)
										as(
										  select patro.id_colocacion as id_colocacion, patro.izquierda as izquierda , patro.derecha as derecha
										  from afi.tpatro_colo patro
										  where patro.id_colocacion = ".$id_posicion."

										  union

										  select patro.id_colocacion as id_colocacion, patro.izquierda as izquierda , patro.derecha as derecha
										  from binario, afi.tpatro_colo patro
										  where patro.id_colocacion = binario.izquierda or  patro.id_colocacion = binario.derecha
										)
										select count(*) as  conteo, '".$posicion."' as posicion
										from binario;");
		$count->execute();
		$count_result = $count->fetchAll(PDO::FETCH_ASSOC);
		return $count_result;
	}



	function listarArbolAfilido(){

		$codigo = $this->aParam->getParametro('codigo');
		$inicio = $this->aParam->getParametro('inicio');

		$this->setParametro('codigo','codigo','varchar');
		$this->setParametro('inicio','inicio','varchar');




		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_afiliado_sel';
		$this->transaccion='AFI_AFIARB_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion

		//Definicion de la lista del resultado del query

		$this->captura('desc_person','text');

		$this->captura('codigo_patrocinador','varchar');
		$this->captura('codigo','varchar');




		$this->captura('celular1','varchar');
		$this->captura('telefono1','varchar');






		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();


		//Devuelve la respuesta
		return $this->respuesta;

	}


    function listarAfiliadoRedPersonal(){

        //Definicion de variables para ejecucion del procedimientp
        $this->procedimiento='afi.ft_afiliado_sel';
        $this->transaccion='AFI_AFIRED_SEL';
        $this->tipo_procedimiento='SEL';//tipo de transaccion
        $this->count = false;



        //Definicion de la lista del resultado del query
        $this->captura('nivel','int4');
        //$this->captura('id_afiliado','int4');
        //$this->captura('id_patrocinador','int4');
        $this->captura('nombre_afiliado','text');
        $this->captura('codigo','varchar');
        $this->captura('codigo_patrocinador','varchar');

        //Ejecuta la instruccion
        $this->armarConsulta();
        $this->ejecutarConsulta();


        //Devuelve la respuesta
        return $this->respuesta;
    }

    function listarAfiliadoOficina(){

        //Definicion de variables para ejecucion del procedimientp
        $this->procedimiento='afi.ft_afiliado_sel';
        $this->transaccion='AFI_OFINET_SEL';
        $this->tipo_procedimiento='SEL';//tipo de transaccion
        $this->count = false;


        $this->setParametro('id_periodo','id_periodo','int4');

        $this->captura('json','text');


        //Ejecuta la instruccion
        $this->armarConsulta();
        $this->ejecutarConsulta();


        //Devuelve la respuesta
        return $this->respuesta;
    }


    function compresionDinamica(){

        $this->procedimiento='afi.ft_afiliado_ime';
        $this->transaccion='AFI_AFIL_COMD';
        $this->tipo_procedimiento='IME';

        //Ejecuta la instruccion
        $this->armarConsulta();
        $this->ejecutarConsulta();

        //Devuelve la respuesta
        return $this->respuesta;
    }

    function insertarPreRegistro(){

        $this->procedimiento='afi.ft_afiliado_ime';
        $this->transaccion='AFI_AFIL_PREREG';
        $this->tipo_procedimiento='IME';

        $this->setParametro('personaJson','personaJson','text');


        //Ejecuta la instruccion
        $this->armarConsulta();
        $this->ejecutarConsulta();

        //Devuelve la respuesta
        return $this->respuesta;
    }


    function listarPreRegistroPersona(){

        //Definicion de variables para ejecucion del procedimientp
        $this->procedimiento='afi.ft_afiliado_sel';
        $this->transaccion='AFI_AFILPRE_SEL';
        $this->tipo_procedimiento='SEL';//tipo de transaccion

        //v_consulta:='select per.id_persona, afil.id_afiliado as id_patrocinador, vper.nombre_completo2, vper.ci

        //Definicion de la lista del resultado del query
        $this->captura('id_persona','int4');
        $this->captura('id_patrocinador','int4');
        $this->captura('nombre_completo1','text');
        $this->captura('ci','varchar');
        $this->captura('codigo_patrocinador','varchar');



        //Ejecuta la instruccion
        $this->armarConsulta();
        $this->ejecutarConsulta();


        //Devuelve la respuesta
        return $this->respuesta;
    }


			
}
?>