<?php
/**
*@package pXP
*@file gen-MODBonusproductopatrocinador.php
*@author  (admin)
*@date 06-01-2016 16:58:27
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODBonusproductopatrocinador extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarBonusproductopatrocinador(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_bonusproductopatrocinador_sel';
		$this->transaccion='AFI_BOPROPA_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_bonus_producto_patrocinador','int4');
		$this->captura('id_afiliado','int4');
		$this->captura('total_cv','numeric');
		$this->captura('id_patrocinador','int4');
		$this->captura('cv','numeric');
		$this->captura('observaciones','varchar');
		$this->captura('cantidad_pro','int4');
		$this->captura('estado_reg','varchar');
		$this->captura('id_usuario_ai','int4');
		$this->captura('usuario_ai','varchar');
		$this->captura('fecha_reg','timestamp');
		$this->captura('id_usuario_reg','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('id_usuario_mod','int4');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');
		
		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarBonusproductopatrocinador(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_bonusproductopatrocinador_ime';
		$this->transaccion='AFI_BOPROPA_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('total_cv','total_cv','numeric');
		$this->setParametro('id_patrocinador','id_patrocinador','int4');
		$this->setParametro('cv','cv','numeric');
		$this->setParametro('observaciones','observaciones','varchar');
		$this->setParametro('cantidad_pro','cantidad_pro','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarBonusproductopatrocinador(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_bonusproductopatrocinador_ime';
		$this->transaccion='AFI_BOPROPA_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_bonus_producto_patrocinador','id_bonus_producto_patrocinador','int4');
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('total_cv','total_cv','numeric');
		$this->setParametro('id_patrocinador','id_patrocinador','int4');
		$this->setParametro('cv','cv','numeric');
		$this->setParametro('observaciones','observaciones','varchar');
		$this->setParametro('cantidad_pro','cantidad_pro','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarBonusproductopatrocinador(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_bonusproductopatrocinador_ime';
		$this->transaccion='AFI_BOPROPA_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_bonus_producto_patrocinador','id_bonus_producto_patrocinador','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
}
?>