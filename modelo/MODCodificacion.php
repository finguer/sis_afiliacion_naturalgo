<?php
/**
*@package pXP
*@file gen-MODCodificacion.php
*@author  (admin)
*@date 04-04-2015 17:10:04
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODCodificacion extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarCodificacion(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_codificacion_sel';
		$this->transaccion='AFI_CODI_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_codificacion','int4');
		$this->captura('numero_inicio','numeric');
		$this->captura('estado_reg','varchar');
		$this->captura('numero_siguiente','numeric');
		$this->captura('numero_fin','numeric');
		$this->captura('id_usuario_reg','int4');
		$this->captura('fecha_reg','timestamp');
		$this->captura('usuario_ai','varchar');
		$this->captura('id_usuario_ai','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('id_usuario_mod','int4');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');
		
		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarCodificacion(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_codificacion_ime';
		$this->transaccion='AFI_CODI_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('numero_inicio','numero_inicio','numeric');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('numero_siguiente','numero_siguiente','numeric');
		$this->setParametro('numero_fin','numero_fin','numeric');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarCodificacion(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_codificacion_ime';
		$this->transaccion='AFI_CODI_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_codificacion','id_codificacion','int4');
		$this->setParametro('numero_inicio','numero_inicio','numeric');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('numero_siguiente','numero_siguiente','numeric');
		$this->setParametro('numero_fin','numero_fin','numeric');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarCodificacion(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_codificacion_ime';
		$this->transaccion='AFI_CODI_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_codificacion','id_codificacion','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
}
?>