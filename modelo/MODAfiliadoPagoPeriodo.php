<?php
/**
*@package pXP
*@file gen-MODAfiliadoPagoPeriodo.php
*@author  (admin)
*@date 02-04-2016 16:22:34
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODAfiliadoPagoPeriodo extends MODbase{

	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}

	function listarAfiliadoPagoPeriodo(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_afiliado_pago_periodo_sel';
		$this->transaccion='AFI_afpape_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion


        $this->setParametro('id_periodo','id_periodo','int4');


        //Definicion de la lista del resultado del query
		$this->captura('id_afiliado_pago_periodo','int4');
		$this->captura('id_afiliado','int4');
		$this->captura('pago_bonus_registro','numeric');
		$this->captura('afiliados_registrados','int4');
		$this->captura('habilitado','varchar');
		$this->captura('pago_de_red','numeric');
		$this->captura('estado','varchar');
		$this->captura('cv_propio','numeric');
		$this->captura('id_periodo','int4');
		$this->captura('pago_total','numeric');
		$this->captura('descripcion_pago_red','varchar');
		$this->captura('pv_propio','numeric');
		$this->captura('estado_reg','varchar');
		$this->captura('id_usuario_ai','int4');
		$this->captura('fecha_reg','timestamp');
		$this->captura('usuario_ai','varchar');
		$this->captura('id_usuario_reg','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('id_usuario_mod','int4');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');

		$this->captura('codigo','varchar');
		$this->captura('nombre_completo2','text');
		$this->captura('periodo','int4');
		$this->captura('gestion','int4');

		$this->captura('primera_gen_cv','numeric');
		$this->captura('primera_gen_pv','numeric');
		$this->captura('segunda_gen_cv','numeric');
		$this->captura('segunda_gen_pv','numeric');
		$this->captura('tercera_gen_cv','numeric');
		$this->captura('tercera_gen_pv','numeric');
		$this->captura('cuarta_gen_cv','numeric');
		$this->captura('cuarta_gen_pv','numeric');
		$this->captura('quinta_gen_cv','numeric');
		$this->captura('quinta_gen_pv','numeric');
		$this->captura('monto_multiplicar','numeric');
		$this->captura('tipo_moneda','varchar');
		$this->captura('afiliados_activos','int4');
		$this->captura('niveles_cobrados','varchar');
		$this->captura('sube_nivel','varchar');
		$this->captura('pv_acumulado','numeric');
		$this->captura('porcentajes_pagados','text');
		$this->captura('rango_actual','varchar');
		$this->captura('rango_siguiente','varchar');
		$this->captura('premio','text');
		$this->captura('kit_negocios','varchar');












		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}

	function insertarAfiliadoPagoPeriodo(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afiliado_pago_periodo_ime';
		$this->transaccion='AFI_afpape_INS';
		$this->tipo_procedimiento='IME';

		//Define los parametros para la funcion
		$this->setParametro('cv_hijos','cv_hijos','numeric');
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('pago_bonus_registro','pago_bonus_registro','numeric');
		$this->setParametro('habilitado','habilitado','varchar');
		$this->setParametro('pago_de_red','pago_de_red','numeric');
		$this->setParametro('estado','estado','varchar');
		$this->setParametro('cv_propio','cv_propio','numeric');
		$this->setParametro('id_periodo','id_periodo','int4');
		$this->setParametro('pago_total','pago_total','numeric');
		$this->setParametro('descripcion_pago_red','descripcion_pago_red','varchar');
		$this->setParametro('pv_hijos','pv_hijos','numeric');
		$this->setParametro('pv_propio','pv_propio','numeric');
		$this->setParametro('estado_reg','estado_reg','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}

	function modificarAfiliadoPagoPeriodo(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afiliado_pago_periodo_ime';
		$this->transaccion='AFI_afpape_MOD';
		$this->tipo_procedimiento='IME';

		//Define los parametros para la funcion
		$this->setParametro('id_afiliado_pago_periodo','id_afiliado_pago_periodo','int4');
		$this->setParametro('cv_hijos','cv_hijos','numeric');
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('pago_bonus_registro','pago_bonus_registro','numeric');
		$this->setParametro('habilitado','habilitado','varchar');
		$this->setParametro('pago_de_red','pago_de_red','numeric');
		$this->setParametro('estado','estado','varchar');
		$this->setParametro('cv_propio','cv_propio','numeric');
		$this->setParametro('id_periodo','id_periodo','int4');
		$this->setParametro('pago_total','pago_total','numeric');
		$this->setParametro('descripcion_pago_red','descripcion_pago_red','varchar');
		$this->setParametro('pv_hijos','pv_hijos','numeric');
		$this->setParametro('pv_propio','pv_propio','numeric');
		$this->setParametro('estado_reg','estado_reg','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}

	function eliminarAfiliadoPagoPeriodo(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afiliado_pago_periodo_ime';
		$this->transaccion='AFI_afpape_ELI';
		$this->tipo_procedimiento='IME';

		//Define los parametros para la funcion
		$this->setParametro('id_afiliado_pago_periodo','id_afiliado_pago_periodo','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}

	function generarPagoAfiliado(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afiliado_pago_periodo_ime';
		$this->transaccion='AFI_AFPAPE_GEPA';
		$this->tipo_procedimiento='IME';

		//Define los parametros para la funcion
		$this->setParametro('id_periodo','id_periodo','int4');
		$this->setParametro('id_afiliado','id_afiliado','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}

	function  PagoAfiliado(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afiliado_pago_periodo_ime';
		$this->transaccion='AFI_AFPAPE_PAG';
		$this->tipo_procedimiento='IME';

		//Define los parametros para la funcion
		$this->setParametro('id_afiliado_pago_periodo','id_afiliado_pago_periodo','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}

	function  PagarBonusPaquete(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afiliado_pago_periodo_ime';
		$this->transaccion='AFI_AFPAPE_PBON';
		$this->tipo_procedimiento='IME';

		//Define los parametros para la funcion
		$this->setParametro('id_afiliado_pago_periodo','id_afiliado_pago_periodo','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}


	function listarRedPuntos(){

		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_afiliado_pago_periodo_sel';
		$this->transaccion='AFI_AFPAPE_GEN';
		$this->tipo_procedimiento='SEL';//tipo de transaccion

		$this->setCount(false);
		//Definicion de la lista del resultado del query


		//Define los parametros para la funcion
		$this->setParametro('id_periodo','id_periodo','int4');
		$this->setParametro('id_afiliado','id_afiliado','int4');

		$this->captura('nivel','int4');
		//$this->captura('id_afiliado','int4');
		$this->captura('codigo','varchar');
		$this->captura('codigo_patrocinador','varchar');
		$this->captura('nombre_completo2','text');
		$this->captura('nombre_completo2_patrocinador','text');
		$this->captura('sum_cv','numeric');
		$this->captura('sum_pv','numeric');







		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;

	}

	function generarPagos(){


		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afiliado_pago_periodo_ime';
		$this->transaccion='AFI_AFPAPE_GENALL';
		$this->tipo_procedimiento='IME';

		//Define los parametros para la funcion
		$this->setParametro('id_periodo','id_periodo','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}


    function listarSumPagosDetalle(){

        //Definicion de variables para ejecucion del procedimientp
        $this->procedimiento='afi.ft_afiliado_pago_periodo_sel';
        $this->transaccion='AFI_AFPAPE_PAGO';
        $this->tipo_procedimiento='SEL';//tipo de transaccion

        $this->setCount(false);
        //Definicion de la lista del resultado del query


        //Define los parametros para la funcion
        $this->setParametro('id_periodo','id_periodo','int4');
        $this->setParametro('id_sucursal','id_sucursal','int4');
        $this->setParametro('fecha','fecha','date');
        $this->setParametro('fecha_fin','fecha_fin','date');

        $this->captura('json','text');


        //Ejecuta la instruccion
        $this->armarConsulta();
        $this->ejecutarConsulta();

        //Devuelve la respuesta
        return $this->respuesta;

    }
    function premiar(){

        //Definicion de variables para ejecucion del procedimiento
        $this->procedimiento='afi.ft_afiliado_pago_periodo_ime';
        $this->transaccion='AFI_AFPAPE_PREMI';
        $this->tipo_procedimiento='IME';

        //Define los parametros para la funcion
        $this->setParametro('id_periodo','id_periodo','int4');
        $this->setParametro('id_afiliado','id_afiliado','int4');
        $this->setParametro('descripcion','descripcion','text');

        //Ejecuta la instruccion
        $this->armarConsulta();
        $this->ejecutarConsulta();

        //Devuelve la respuesta
        return $this->respuesta;

    }



}
?>