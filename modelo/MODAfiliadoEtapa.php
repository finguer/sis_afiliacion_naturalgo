<?php
/**
*@package pXP
*@file gen-MODAfiliadoEtapa.php
*@author  (admin)
*@date 04-01-2016 16:42:12
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODAfiliadoEtapa extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarAfiliadoEtapa(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_afiliado_etapa_sel';
		$this->transaccion='AFI_AFET_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_afiliado_etapa','int4');
		$this->captura('id_afiliado','int4');
		$this->captura('fecha_fin','date');
		$this->captura('etapa','varchar');
		$this->captura('fecha_ini','date');
		$this->captura('estado_reg','varchar');
		$this->captura('id_usuario_ai','int4');
		$this->captura('usuario_ai','varchar');
		$this->captura('fecha_reg','timestamp');
		$this->captura('id_usuario_reg','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('id_usuario_mod','int4');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');
		
		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarAfiliadoEtapa(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afiliado_etapa_ime';
		$this->transaccion='AFI_AFET_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('fecha_fin','fecha_fin','date');
		$this->setParametro('etapa','etapa','varchar');
		$this->setParametro('fecha_ini','fecha_ini','date');
		$this->setParametro('estado_reg','estado_reg','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarAfiliadoEtapa(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afiliado_etapa_ime';
		$this->transaccion='AFI_AFET_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_afiliado_etapa','id_afiliado_etapa','int4');
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('fecha_fin','fecha_fin','date');
		$this->setParametro('etapa','etapa','varchar');
		$this->setParametro('fecha_ini','fecha_ini','date');
		$this->setParametro('estado_reg','estado_reg','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarAfiliadoEtapa(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afiliado_etapa_ime';
		$this->transaccion='AFI_AFET_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_afiliado_etapa','id_afiliado_etapa','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
}
?>