<?php
/**
*@package pXP
*@file gen-MODCotitular.php
*@author  (admin)
*@date 02-04-2015 01:19:24
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODCotitular extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarCotitular(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_cotitular_sel';
		$this->transaccion='AFI_TITU_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_cotitular','int4');
		$this->captura('nombre','varchar');
		$this->captura('id_afiliado','int4');
		$this->captura('domicilio','varchar');
		$this->captura('apellido1','varchar');
		$this->captura('apellido2','varchar');
		$this->captura('relacion','varchar');
		$this->captura('estado_reg','varchar');
		$this->captura('telefono','numeric');
		$this->captura('id_usuario_ai','int4');
		$this->captura('id_usuario_reg','int4');
		$this->captura('usuario_ai','varchar');
		$this->captura('fecha_reg','timestamp');
		$this->captura('fecha_mod','timestamp');
		$this->captura('id_usuario_mod','int4');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');
		$this->captura('desc_person','text');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarCotitular(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_cotitular_ime';
		$this->transaccion='AFI_TITU_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('nombre','nombre','varchar');
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('domicilio','domicilio','varchar');
		$this->setParametro('apellido1','apellido1','varchar');
		$this->setParametro('apellido2','apellido2','varchar');
		$this->setParametro('relacion','relacion','varchar');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('telefono','telefono','numeric');

		$this->setParametro('id_persona','id_persona','int4');


		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarCotitular(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_cotitular_ime';
		$this->transaccion='AFI_TITU_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_cotitular','id_cotitular','int4');
		$this->setParametro('nombre','nombre','varchar');
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('domicilio','domicilio','varchar');
		$this->setParametro('apellido1','apellido1','varchar');
		$this->setParametro('apellido2','apellido2','varchar');
		$this->setParametro('relacion','relacion','varchar');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('telefono','telefono','numeric');

		$this->setParametro('id_persona','id_persona','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarCotitular(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_cotitular_ime';
		$this->transaccion='AFI_TITU_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_cotitular','id_cotitular','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
}
?>