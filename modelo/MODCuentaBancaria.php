<?php
/**
*@package pXP
*@file gen-MODCuentaBancaria.php
*@author  (admin)
*@date 18-08-2019 17:44:41
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODCuentaBancaria extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarCuentaBancaria(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_cuenta_bancaria_sel';
		$this->transaccion='AFI_CUBAN_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_cuenta_bancaria','int4');
		$this->captura('id_afiliado','int4');
		$this->captura('estado_reg','varchar');
		$this->captura('tipo_cuenta','varchar');
		$this->captura('banco','varchar');
		$this->captura('nro_cuenta','varchar');
		$this->captura('fecha_reg','timestamp');
		$this->captura('usuario_ai','varchar');
		$this->captura('id_usuario_reg','int4');
		$this->captura('id_usuario_ai','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('id_usuario_mod','int4');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');
		$this->captura('codigo_desc','numeric');
		


		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarCuentaBancaria(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_cuenta_bancaria_ime';
		$this->transaccion='AFI_CUBAN_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('tipo_cuenta','tipo_cuenta','varchar');
		$this->setParametro('banco','banco','varchar');
		$this->setParametro('nro_cuenta','nro_cuenta','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarCuentaBancaria(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_cuenta_bancaria_ime';
		$this->transaccion='AFI_CUBAN_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_cuenta_bancaria','id_cuenta_bancaria','int4');
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('tipo_cuenta','tipo_cuenta','varchar');
		$this->setParametro('banco','banco','varchar');
		$this->setParametro('nro_cuenta','nro_cuenta','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarCuentaBancaria(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_cuenta_bancaria_ime';
		$this->transaccion='AFI_CUBAN_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_cuenta_bancaria','id_cuenta_bancaria','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
}
?>