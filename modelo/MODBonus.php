<?php
/**
*@package pXP
*@file gen-MODBonus.php
*@author  (admin)
*@date 11-04-2015 16:33:17
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODBonus extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);

		$this->cone = new conexion();
		$this->link = $this->cone->conectarpdo(); //conexion a pxp(postgres)

	}
			
	function listarBonus(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_bonus_sel';
		$this->transaccion='AFI_BON_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_bonus','int4');
		$this->captura('tipo','varchar');
		$this->captura('estado_reg','varchar');
		$this->captura('estado_pago','varchar');
		$this->captura('id_afiliado','int4');
		$this->captura('monto','numeric');
		$this->captura('id_patrocinador','int4');
		$this->captura('id_usuario_reg','int4');
		$this->captura('usuario_ai','varchar');
		$this->captura('fecha_reg','timestamp');
		$this->captura('id_usuario_ai','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('id_usuario_mod','int4');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');

		$this->captura('codigo_patrocinador','varchar');
		$this->captura('codigo_afiliado','varchar');
		$this->captura('desc_persona','text');
		$this->captura('id_periodo','int4');
		$this->captura('nivel_from','int4');
		$this->captura('codigo_afiliado_from','varchar');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarBonus(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_bonus_ime';
		$this->transaccion='AFI_BON_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('tipo','tipo','varchar');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('monto','monto','numeric');
		$this->setParametro('id_patrocinador','id_patrocinador','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarBonus(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_bonus_ime';
		$this->transaccion='AFI_BON_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_bonus','id_bonus','int4');
		$this->setParametro('tipo','tipo','varchar');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('id_afiliado','id_afiliado','int4');
		$this->setParametro('monto','monto','numeric');
		$this->setParametro('id_patrocinador','id_patrocinador','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarBonus(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_bonus_ime';
		$this->transaccion='AFI_BON_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_bonus','id_bonus','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}


	function pagarBonus(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_bonus_ime';
		$this->transaccion='AFI_BON_PAG';
		$this->tipo_procedimiento='IME';

		//Define los parametros para la funcion
		$this->setParametro('id_bonus','id_bonus','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}

	function generarPagoBonusDirectos(){

		$this->listarBonus();

	}
	function resetParametrosBonus(){
		$this->resetParametros();
		$this->resetCaptura();
	}

	function pagar($id_afiliado){

		try {

			$this->link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->link->beginTransaction();

			$this->modificarEstadoBonus($id_afiliado);
			$this->link->commit();
			$this->respuesta = new Mensaje();
			$this->respuesta->setMensaje('EXITO', $this->nombre_archivo, 'La consulta se ejecuto con exito de insercion de nota', 'La consulta se ejecuto con exito', 'base', 'no tiene', 'no tiene', 'SEL', '$this->consulta', 'no tiene');
			$this->respuesta->setTotal(1);
			$this->respuesta->setDatos("correcto");
			return "correcto";

		} catch (Exception $e) {
			$this->link->rollBack();
			$this->respuesta = new Mensaje();
			if ($e->getCode() == 3) {//es un error de un procedimiento almacenado de pxp
				$this->respuesta->setMensaje($resp_procedimiento['tipo_respuesta'], $this->nombre_archivo, $resp_procedimiento['mensaje'], $resp_procedimiento['mensaje_tec'], 'base', $this->procedimiento, $this->transaccion, $this->tipo_procedimiento, $this->consulta);
			} else if ($e->getCode() == 2) {//es un error en bd de una consulta
				$this->respuesta->setMensaje('ERROR', $this->nombre_archivo, $e->getMessage(), $e->getMessage(), 'modelo', '', '', '', '');
			} else {//es un error lanzado con throw exception
				throw new Exception($e->getMessage(), 2);
			}
		}
		return $this->respuesta;

	}

	function modificarEstadoBonus($id_afiliado){
		$res = $this->link->prepare("update afi.tbonus set estado_pago = 'pagado' where id_patrocinador = ".$id_afiliado." and estado_pago = 'debe' ");
		$res->execute();
		$respuesta = $res->fetchAll(PDO::FETCH_ASSOC);
		return $res;

	}
	function verSeDebe(){

		$id_patrocinador = $this->objParam->getParametro('id_patrocinador');
		$tipo = $this->objParam->getParametro('tipo');
		$res = $this->link->prepare("select sum(monto) from afi.tbonus where id_patrocinador = ".$id_patrocinador." and tipo = '".$tipo."' ");

		$res->execute();
		$respuesta = $res->fetchAll(PDO::FETCH_ASSOC);
		$this->respuesta = new Mensaje();
			$this->respuesta->setMensaje('EXITO', $this->nombre_archivo, 'La consulta se ejecuto con exito de insercion de nota', 'La consulta se ejecuto con exito', 'base', 'no tiene', 'no tiene', 'SEL', '$this->consulta', 'no tiene');
			$this->respuesta->setTotal(1);
			$this->respuesta->setDatos($respuesta);

		return $this->respuesta;
	}
			
}
?>