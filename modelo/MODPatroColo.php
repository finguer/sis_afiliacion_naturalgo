<?php
/**
*@package pXP
*@file gen-MODPatroColo.php
*@author  (admin)
*@date 04-04-2015 15:41:13
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODPatroColo extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarPatroColo(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_patro_colo_sel';
		$this->transaccion='AFI_COLO_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_patro_colo','int4');
		$this->captura('derecha','int4');
		$this->captura('izquierda','int4');
		$this->captura('estado_reg','varchar');
		$this->captura('id_colocacion','int4');
		$this->captura('id_usuario_ai','int4');
		$this->captura('id_usuario_reg','int4');
		$this->captura('fecha_reg','timestamp');
		$this->captura('usuario_ai','varchar');
		$this->captura('id_usuario_mod','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');

		$this->captura('desc_person','text');
		$this->captura('ci','varchar');
		$this->captura('codigo','numeric');

		$this->captura('codigo_izquierda','numeric');
		$this->captura('codigo_derecha','numeric');


		$this->captura('correo','varchar');
		$this->captura('telefono1','varchar');
		$this->captura('celular1','varchar');
		
		$this->captura('colocacion','varchar');
		
		$this->captura('id_patrocinador','int4');

		
		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarPatroColo(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_patro_colo_ime';
		$this->transaccion='AFI_COLO_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('derecha','derecha','int4');
		$this->setParametro('izquierda','izquierda','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('id_colocacion','id_colocacion','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarPatroColo(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_patro_colo_ime';
		$this->transaccion='AFI_COLO_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_patro_colo','id_patro_colo','int4');
		$this->setParametro('derecha','derecha','int4');
		$this->setParametro('izquierda','izquierda','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('id_colocacion','id_colocacion','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarPatroColo(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_patro_colo_ime';
		$this->transaccion='AFI_COLO_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_patro_colo','id_patro_colo','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
}
?>