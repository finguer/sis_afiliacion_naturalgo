<?php
/**
*@package pXP
*@file gen-MODAfiPatro.php
*@author  (admin)
*@date 04-04-2015 15:40:52
*@description Clase que envia los parametros requeridos a la Base de datos para la ejecucion de las funciones, y que recibe la respuesta del resultado de la ejecucion de las mismas
*/

class MODAfiPatro extends MODbase{
	
	function __construct(CTParametro $pParam){
		parent::__construct($pParam);
	}
			
	function listarAfiPatro(){
		//Definicion de variables para ejecucion del procedimientp
		$this->procedimiento='afi.ft_afi_patro_sel';
		$this->transaccion='AFI_PATRO_SEL';
		$this->tipo_procedimiento='SEL';//tipo de transaccion
				
		//Definicion de la lista del resultado del query
		$this->captura('id_afi_patro','int4');
		$this->captura('id_colocacion','int4');
		$this->captura('id_patrocinador','int4');
		$this->captura('estado_reg','varchar');
		$this->captura('id_afiliado','int4');
		$this->captura('id_usuario_ai','int4');
		$this->captura('id_usuario_reg','int4');
		$this->captura('fecha_reg','timestamp');
		$this->captura('usuario_ai','varchar');
		$this->captura('id_usuario_mod','int4');
		$this->captura('fecha_mod','timestamp');
		$this->captura('usr_reg','varchar');
		$this->captura('usr_mod','varchar');

		$this->captura('desc_person','text');
		$this->captura('ci','varchar');
		$this->captura('codigo','numeric');


		$this->captura('correo','varchar');
		$this->captura('telefono1','varchar');
		$this->captura('celular1','varchar');


		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();
		
		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function insertarAfiPatro(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afi_patro_ime';
		$this->transaccion='AFI_PATRO_INS';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_colocacion','id_colocacion','int4');
		$this->setParametro('id_patrocinador','id_patrocinador','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('id_afiliado','id_afiliado','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function modificarAfiPatro(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afi_patro_ime';
		$this->transaccion='AFI_PATRO_MOD';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_afi_patro','id_afi_patro','int4');
		$this->setParametro('id_colocacion','id_colocacion','int4');
		$this->setParametro('id_patrocinador','id_patrocinador','int4');
		$this->setParametro('estado_reg','estado_reg','varchar');
		$this->setParametro('id_afiliado','id_afiliado','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
	function eliminarAfiPatro(){
		//Definicion de variables para ejecucion del procedimiento
		$this->procedimiento='afi.ft_afi_patro_ime';
		$this->transaccion='AFI_PATRO_ELI';
		$this->tipo_procedimiento='IME';
				
		//Define los parametros para la funcion
		$this->setParametro('id_afi_patro','id_afi_patro','int4');

		//Ejecuta la instruccion
		$this->armarConsulta();
		$this->ejecutarConsulta();

		//Devuelve la respuesta
		return $this->respuesta;
	}
			
}
?>