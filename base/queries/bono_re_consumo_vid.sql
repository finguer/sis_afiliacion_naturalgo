WITH t_afiliados_productos as (
    select ta.id_afiliado, sum(tvd.cantidad_producto) cantidad_productos
    from vent.tventa_detalle tvd
             inner join vent.tventa tv on tv.id_venta = tvd.id_venta
             inner join vent.tcliente tc on tc.id_cliente = tv.id_cliente
             inner join afi.tafiliado ta on ta.id_persona = tc.id_persona
    where tv.id_periodo = 130 and tvd.id_item is not null --and ta.id_afiliado = 61491
    GROUP BY ta.id_afiliado
),t_afiliado_bono_multiplicador as (
    select id_afiliado, case WHEN cantidad_productos >= 7 then 5 else 4 end as bono_multiplicador
    from t_afiliados_productos
    where cantidad_productos >= 5
)select ta.id_afiliado,ta.codigo,tap.cantidad_productos, tabm.bono_multiplicador, (tap.cantidad_productos * tabm.bono_multiplicador) as bono_reconsumo
from t_afiliados_productos tap
         inner join t_afiliado_bono_multiplicador tabm on tabm.id_afiliado = tap.id_afiliado
         inner join afi.tafiliado ta on ta.id_afiliado = tap.id_afiliado;