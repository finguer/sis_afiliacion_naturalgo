DO
$$DECLARE r record;
          v_pv_acumulado numeric;
          v_fecha_1 date;
          v_fecha_2 date;
          v_record_periodo_1 record;
          v_record_periodo_2 record;
          v_record_periodo_current record;
          v_afiliado record;
          v_rango record;
          v_rango_afiliado record;
    v_array_puntos_imp integer[];
    v_codigo_afiliado varchar;
          v_count_aux INTEGER;
          v_sum_puntos INTEGER;
          v_rango_obtenido_con_sus_puntos record;
          v_ultimo_rango_obtenido_con_sus_puntos record;
          v_ultimo_rango_obtenido integer;
          v_id_rango_obtenido integer;

BEGIN
    v_fecha_1:= '01/11/2021'::date;
    v_fecha_2:= '01/12/2021'::date;
    v_record_periodo_1:= param.f_get_periodo_gestion(v_fecha_1::date);
    v_record_periodo_2:= param.f_get_periodo_gestion(v_fecha_2::date);
    v_record_periodo_current:= param.f_get_periodo_gestion(now()::date);
    --RAISE EXCEPTION '%',v_record_periodo_2.po_id_periodo;

    FOR r IN (
        select * from public.puntos_sumados_importar_vid)
        LOOP
            v_codigo_afiliado:='VIDBO'||r.codigo_vid;
            select * into v_afiliado from afi.tafiliado where codigo = v_codigo_afiliado;
            v_count_aux:=1;
            v_sum_puntos:=0;
            v_ultimo_rango_obtenido:=0;
            v_id_rango_obtenido:= 0;
            IF v_afiliado is null then
                RAISE NOTICE '%','NO EXISTE' || v_codigo_afiliado;
            else

                --RAISE NOTICE '%',v_afiliado.codigo;
                select ARRAY[cast(r.paquete_inicio as integer),r.enero,r.febrero,r.marzo,r.abril,r.mayo,r.junio] as meses_array
                into v_array_puntos_imp;

                --obtener rango actual


                WHILE v_count_aux <= ARRAY_LENGTH(v_array_puntos_imp,1)
                    LOOP
                        v_sum_puntos:= v_sum_puntos + v_array_puntos_imp[v_count_aux];

                        SELECT *
                        INTO v_rango_obtenido_con_sus_puntos
                        FROM afi.trango
                        WHERE volumen_organizacional <= v_sum_puntos and nivel > v_ultimo_rango_obtenido
                        ORDER BY nivel desc
                        LIMIT 1;

                        IF v_rango_obtenido_con_sus_puntos.id_rango is not null then
                            v_ultimo_rango_obtenido_con_sus_puntos:= v_rango_obtenido_con_sus_puntos;
                            v_id_rango_obtenido:= v_rango_obtenido_con_sus_puntos.id_rango;
                            v_ultimo_rango_obtenido:= v_rango_obtenido_con_sus_puntos.nivel;
                            v_sum_puntos:=v_sum_puntos/2;
                        END IF;


                    v_count_aux := v_count_aux + 1;
                END LOOP;

                --RAISE NOTICE '%',v_afiliado.codigo || '-' ||v_sum_puntos;
                INSERT INTO afi.tpunto_acumulado (id_usuario_reg, id_usuario_mod, fecha_reg,
                                                  fecha_mod, estado_reg, id_usuario_ai,
                                                  usuario_ai, obs_dba, id_afiliado,
                                                  id_periodo, pv)VALUES (
                                                                         1,null,now(),null,'activo',NULL,'null',
                                                                         null,v_afiliado.id_afiliado,127, v_sum_puntos
                                                                                        );

                if v_id_rango_obtenido > 0
                then
                    UPDATE afi.trango_afiliado set id_rango = v_id_rango_obtenido, obs_dba ='subido con la importacion de datos vid'
                    where id_afiliado = v_afiliado.id_afiliado and id_periodo = 127;
                    RAISE NOTICE '%', 'SUBE DE RANGO A '||v_afiliado.codigo || ' - '|| v_ultimo_rango_obtenido_con_sus_puntos.nombre || ' - '|| v_sum_puntos;

                END IF;

            END IF;

        END LOOP;
END$$;
select * from param.tperiodo;