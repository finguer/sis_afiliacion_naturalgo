WITH t_periodo_config AS (
    SELECT :p_id_periodo                                 AS id_periodo,
           afi.f_get_id_periodo_siguiente(:p_id_periodo) AS id_periodo_sig
)
SELECT tg.gestion,
       tp.periodo,
       ta.id_afiliado,
       ta.codigo,
       p.nombre_completo2,
       tapp.sube_nivel,
       rango_actual.nombre    AS de_rango,
       rango_siguiente.nombre AS a_rango
FROM afi.tafiliado_pago_periodo tapp
         INNER JOIN t_periodo_config tpc ON tpc.id_periodo = tapp.id_periodo
         INNER JOIN afi.tafiliado ta ON ta.id_afiliado = tapp.id_afiliado
         INNER JOIN segu.vpersona2 p ON p.id_persona = ta.id_persona
         INNER JOIN param.tperiodo tp ON tp.id_periodo = tapp.id_periodo
         INNER JOIN param.tgestion tg ON tg.id_gestion = tp.id_gestion
         INNER JOIN afi.trango_afiliado ran_afi_actual ON ran_afi_actual.id_afiliado = ta.id_afiliado
    AND ran_afi_actual.id_periodo = tpc.id_periodo
         INNER JOIN afi.trango rango_actual ON rango_actual.id_rango = ran_afi_actual.id_rango
         INNER JOIN afi.trango_afiliado ran_afi_siguiente ON ran_afi_siguiente.id_afiliado = ta.id_afiliado AND
                                                             ran_afi_siguiente.id_periodo = tpc.id_periodo_sig
         INNER JOIN afi.trango rango_siguiente ON rango_siguiente.id_rango = ran_afi_siguiente.id_rango
WHERE tapp.sube_nivel = 'si';


select ta.id_afiliado,ta.codigo, tr.nombre as de_rango , tr2.nombre as a_rango
from afi.tafiliado_pago_periodo tapp
         inner join afi.tafiliado ta on ta.id_afiliado = tapp.id_afiliado
         inner join afi.trango_afiliado tra on tra.id_afiliado = tapp.id_afiliado and tra.id_periodo = 116
         inner join afi.trango tr on tr.id_rango = tra.id_rango
         inner join afi.trango tr2 on tr2.id_rango = cast(tapp.data_json_aux->'sube_nivel'->'rango'->>'id_rango' as integer)
where tapp.id_periodo = 116 and tapp.sube_nivel = 'si'
  and ta.id_afiliado not in(657,646,630,1)
order by tr.nivel;
