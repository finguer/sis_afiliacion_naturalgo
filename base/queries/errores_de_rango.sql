WITH t_periodo_config AS (
    SELECT :p_id_periodo                                 AS id_periodo,
           afi.f_get_id_periodo_siguiente(:p_id_periodo) AS id_periodo_sig
)
SELECT
       tapp.id_afiliado,
       tapp.estado,
       tg.gestion,
       tp.periodo,
       ta.codigo,
       p.nombre_completo2,
       tapp.sube_nivel,
       rango_actual.nombre    AS de_rango,
       rango_siguiente.nombre AS a_rango,
       rango_siguiente.volumen_organizacional,
       tapp.pv_acumulado
FROM afi.tafiliado_pago_periodo tapp
         INNER JOIN t_periodo_config tpc ON tpc.id_periodo = tapp.id_periodo
         INNER JOIN afi.tafiliado ta ON ta.id_afiliado = tapp.id_afiliado
         INNER JOIN segu.vpersona2 p ON p.id_persona = ta.id_persona
         INNER JOIN param.tperiodo tp ON tp.id_periodo = tapp.id_periodo
         INNER JOIN param.tgestion tg ON tg.id_gestion = tp.id_gestion
         INNER JOIN afi.trango_afiliado ran_afi_actual ON ran_afi_actual.id_afiliado = ta.id_afiliado
    AND ran_afi_actual.id_periodo = tpc.id_periodo
         INNER JOIN afi.trango rango_actual ON rango_actual.id_rango = ran_afi_actual.id_rango

         INNER JOIN afi.trango rango_siguiente ON rango_siguiente.id_rango_fk = rango_actual.id_rango
WHERE tapp.pv_acumulado >= rango_siguiente.volumen_organizacional
and sube_nivel = 'no' and tapp.estado = 'registrado no pagado';


select * from afi.tafiliado_pago_periodo where id_afiliado = 17739 and id_periodo = 109;
select * from afi.trango_afiliado where id_afiliado = 17739;

17739
4872
27673
10410
25089
4159
19058



-- pagados

16974
25688
21509
27380
24185
21477
16791
26356
11927
25508
30329
36144
26833
30095
25070
4851
17972
13378
22500
31970
12975

UPDATE afi.tafiliado_pago_periodo set estado = 'pagado'
where id_afiliado in (

    16974,
        25688,
21509,
27380,
24185,
21477,
16791,
26356,
11927,
25508,
30329,
36144,
26833,
30095,
25070,
4851,
17972,
13378,
22500,
31970,
12975

    );


DO $$DECLARE r record;
             v_id_tipo_chat			   INTEGER;
             v_id_chat			       INTEGER;
             v_usuarios			       varchar;
             v_tabla			           varchar;
             v_json_params_to_send			           json;
             v_res json;
    v_id_periodo integer DEFAULT 109;
             v_id_afiliado_pago_periodo integer;
BEGIN


    FOR r IN (
        WITH t_periodo_config AS (
            SELECT v_id_periodo                                 AS id_periodo,
                   afi.f_get_id_periodo_siguiente(v_id_periodo) AS id_periodo_sig
        )
        SELECT
            tapp.id_afiliado,
            tapp.estado,
            tg.gestion,
            tp.periodo,
            ta.codigo,
            p.nombre_completo2,
            tapp.sube_nivel,
            rango_actual.nombre    AS de_rango,
            rango_siguiente.nombre AS a_rango,
            rango_siguiente.volumen_organizacional,
            tapp.pv_acumulado
        FROM afi.tafiliado_pago_periodo tapp
                 INNER JOIN t_periodo_config tpc ON tpc.id_periodo = tapp.id_periodo
                 INNER JOIN afi.tafiliado ta ON ta.id_afiliado = tapp.id_afiliado
                 INNER JOIN segu.vpersona2 p ON p.id_persona = ta.id_persona
                 INNER JOIN param.tperiodo tp ON tp.id_periodo = tapp.id_periodo
                 INNER JOIN param.tgestion tg ON tg.id_gestion = tp.id_gestion
                 INNER JOIN afi.trango_afiliado ran_afi_actual ON ran_afi_actual.id_afiliado = ta.id_afiliado
            AND ran_afi_actual.id_periodo = tpc.id_periodo
                 INNER JOIN afi.trango rango_actual ON rango_actual.id_rango = ran_afi_actual.id_rango

                 INNER JOIN afi.trango rango_siguiente ON rango_siguiente.id_rango_fk = rango_actual.id_rango
        WHERE tapp.pv_acumulado >= rango_siguiente.volumen_organizacional
          and sube_nivel = 'no' and tapp.estado = 'registrado no pagado'
    )
        LOOP


            v_tabla =  pxp.f_crear_parametro(ARRAY[
                                                 '_nombre_usuario_ai',
                                                 '_id_usuario_ai',
                                                 'tabla_origen',

                                                 'estado_reg',
                                                 'id_periodo',
                                                 'id_afiliado'
                                                 ],
                                             ARRAY[	'1',
                                                 '1',
                                                 'afi.tafiliado_pago_periodo',--'tabla_origen',
                                                 'activo'--'estado_reg',
                                                 ,v_id_periodo::varchar
                                                 ,r.id_afiliado::varchar
                                                 ],
                                             ARRAY['varchar',
                                                 'int4',
                                                 'varchar',
                                                 'varchar',
                                                 'int4',
                                                 'int4'
                                                 ]
                );

            v_id_afiliado_pago_periodo = afi.f_get_pago_periodo_afiliado(1, 1, v_tabla,r.id_afiliado, v_id_periodo );


        END LOOP;



END$$;