WITH t_sum_pv_cv_propios AS (
    SELECT sum(tpa.cv) AS sum_cv, sum(tpa.pv) sum_pv, min(tpa.fecha_reg) min_date, tpa.id_afiliado
    FROM afi.tpuntos_afiliado tpa
    WHERE tpa.id_periodo = 111
    GROUP BY tpa.id_afiliado
),
     t_activos AS (
         SELECT tspcp.*, ta.volumen_personal, ta.nombre AS rango
         FROM t_sum_pv_cv_propios tspcp
                  INNER JOIN afi.trango_afiliado tra ON tra.id_afiliado = tspcp.id_afiliado AND id_periodo = 111
                  INNER JOIN afi.trango ta ON ta.id_rango = tra.id_rango
         WHERE sum_pv >= ta.volumen_personal
     )
SELECT afil.codigo,
       p.nombre_completo2,
       ta.rango,
       ta.sum_cv,
       ta.sum_pv,
       ta.volumen_personal AS volumen_personal_de_rango
FROM t_activos ta
         INNER JOIN afi.tafiliado afil ON afil.id_afiliado = ta.id_afiliado
         INNER JOIN segu.vpersona2 p ON p.id_persona = afil.id_persona
ORDER BY min_date ASC;