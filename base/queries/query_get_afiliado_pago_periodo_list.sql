WITH datos_afiliado AS
         (
             select
                 afiliado.id_afiliado,
                 afiliado.direccion_principal,
                 afiliado.lugar_nacimiento,
                 afiliado.fecha_nacimiento,
                 usuario.cuenta,
                 persona.nombre_completo2 as desc_person,
                 persona.ci,
                 patrocinador.codigo as codigo_patrocinador,
                 afiliado.codigo as codigo
             from afi.tafiliado afiliado
                      inner join segu.tusuario usuario on usuario.id_persona = afiliado.id_persona
                      INNER JOIN segu.vpersona persona on persona.id_persona = afiliado.id_persona
                      INNER join  afi.tafiliado patrocinador on patrocinador.id_afiliado = afiliado.id_patrocinador
             where usuario.id_usuario = '||p_id_usuario||'
         ), pago_periodo AS
         (
             select
                 afpape.*,
                 afil.codigo,
                 person.nombre_completo2,
                 per.periodo,
                 ges.gestion,
                 array_to_string(afpape.porcentajes_pagados, '','', ''*'') as porcentajes_pagados
             from afi.tafiliado_pago_periodo afpape
                      inner join afi.tafiliado afil on afil.id_afiliado = afpape.id_afiliado
                      inner join segu.vpersona person on person.id_persona = afil.id_persona
                      inner join segu.tusuario usuario on usuario.id_persona = afil.id_persona
                      inner join param.tperiodo per on per.id_periodo = afpape.id_periodo
                      inner join param.tgestion ges on ges.id_gestion = per.id_gestion
             where usuario.id_usuario = '||p_id_usuario||' and per.id_periodo = '||v_rec_gestion_periodo.po_id_periodo||'
         ),rango as
         (
             select ran.nombre as rango,ranafi.id_afiliado FROM afi.trango_afiliado ranafi
                                                                    INNER JOIN afi.trango ran on ran.id_rango = ranafi.id_rango
             where ranafi.id_afiliado = '||v_id_afiliado||' and ranafi.id_periodo = '||v_rec_gestion_periodo.po_id_periodo||'
         ),
     periodos_disponibles as
         (
             select
                 per.id_periodo,
                 ges.gestion,
                 pxp.f_obtener_literal_periodo(per.periodo,0) as periodo
             from param.tperiodo per
                      INNER JOIN param.tgestion ges on ges.id_gestion = per.id_gestion
             WHERE per.fecha_ini >= (now() - INTERVAL ''4 MONTH'')::date
               and per.fecha_fin <= (date_trunc(''month'', now()::date) + interval ''1 month'' - interval ''1 day'')::date
             order by per.id_periodo desc

         ),
     habilitado_a_cobrar_paquete as (
         select tbc.bonus
         from afi.tpuntos_afiliado tpa
                  inner join afi.tpunto_item tpi on tpi.id_set = tpa.id_set_item
                  inner join afi.tbonus_config tbc on tbc.id_bonus_config = tpi.id_bonus_config
         where tpa.id_afiliado = '||v_id_afiliado||'
           and tbc.bono_patrocinio_por_nivel is not null
         GROUP BY tbc.bonus

     ),
     bonus_patrocinio_detalle as (
         select tb.tipo,
                tb.monto,
                tb.nivel_from,
                tb.validado_para_pagar_a_patrocinador,
                afiliado_hijo_directo.codigo as codigo_afiliado_hijo_directo,
                afiliado_from.codigo as codigo_from,
                persona_hijo_directo.nombre_completo2 as nombre_hijo_directo,
                persona_from.nombre_completo2 as nombre_hijo_from
         from afi.tbonus tb
                  inner join afi.tbonus_config tbc on tbc.bonus = tb.tipo
             and tbc.bonus != ''BONUS_REGISTRO''
                 inner join afi.tafiliado afiliado_hijo_directo on afiliado_hijo_directo.id_afiliado = tb.id_afiliado
             inner join afi.tafiliado afiliado_from on afiliado_from.id_afiliado = tb.id_afiliado_from --aca no sale el bono registro por que el id_afiliado_from tiene que ser not null
             inner join segu.vpersona persona_hijo_directo on persona_hijo_directo.id_persona = afiliado_hijo_directo.id_persona
             inner join segu.vpersona persona_from on persona_from.id_persona = afiliado_from.id_persona
         where tb.id_patrocinador = '||v_id_afiliado||'
           and id_periodo = '||v_rec_gestion_periodo.po_id_periodo||'
         ORDER BY tb.id_afiliado, tb.nivel_from ASC

     ),
     bonus_patrocinio_totales as (
         select bpd.tipo,bpd.validado_para_pagar_a_patrocinador, sum(bpd.monto)
         from bonus_patrocinio_detalle bpd
         GROUP BY bpd.tipo, bpd.validado_para_pagar_a_patrocinador

     ),
     bono_presentador as (
         select COALESCE(sum(monto), 0) as total
         from afi.tafiliado_presentador tap
         where tap.id_presentador = '||v_id_afiliado||' and tap.id_periodo = '||v_rec_gestion_periodo.po_id_periodo||'

     )
select to_json(array_to_json(array_agg(jsonData)) :: text) #>> ''{}'' as json
from
    (
    select
    da.direccion_principal,
    da.lugar_nacimiento,
    da.fecha_nacimiento,
    da.cuenta,
    da.desc_person,
    da.ci,
    da.codigo_patrocinador,
    da.codigo,
    ran.rango,
    (
    select array_to_json(array_agg(row_to_json(pp))) as pago_periodo
    from (
    select * from pago_periodo
    ) pp
    ),
    (
    select array_to_json(array_agg(row_to_json(pp))) as periodos_disponibles
    from (
    select * from periodos_disponibles
    ) pp
    ),
    (
    select array_to_json(array_agg(row_to_json(hacp))) as habilitado_a_cobrar_paquete
    from (
    select * from habilitado_a_cobrar_paquete
    ) hacp
    ),
    (
    select array_to_json(array_agg(row_to_json(bpd))) as bonus_patrocinio_detalle
    from (
    select * from bonus_patrocinio_detalle
    ) bpd
    ),
    (
    select array_to_json(array_agg(row_to_json(bpt))) as bonus_patrocinio_totales
    from (
    select * from bonus_patrocinio_totales
    ) bpt
    ),
    (
    select TO_JSON(bp) as bono_presentador
    from (
    select * from bono_presentador
    ) bp
    )
    FROM datos_afiliado da
    inner join rango ran on ran.id_afiliado = da.id_afiliado
    ) jsonData