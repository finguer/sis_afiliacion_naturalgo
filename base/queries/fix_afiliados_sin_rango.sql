DO
$$
    DECLARE
        r record;

    BEGIN

        FOR r IN (
        WITH t_afiliados_con_rango AS (
            SELECT tra.id_afiliado, tra.id_periodo
            FROM afi.trango_afiliado tra
                     INNER JOIN afi.tafiliado ta ON ta.id_afiliado = tra.id_afiliado
            WHERE tra.id_periodo = 113
        ),
             t_afiliados_sin_rango AS (
                 SELECT ta.*
                 FROM afi.tafiliado ta
                          LEFT JOIN t_afiliados_con_rango tasr ON tasr.id_afiliado = ta.id_afiliado
                 WHERE tasr.id_afiliado IS NULL
             ),
             t_ultimo_rango AS (
                 SELECT min(tra.id_rango) AS id_ultimo_rango, tra.id_afiliado
                 FROM afi.trango_afiliado tra
                          INNER JOIN t_afiliados_sin_rango tasr ON tasr.id_afiliado = tra.id_afiliado
                 WHERE tra.id_periodo < 114
                 GROUP BY tra.id_afiliado
             )
        SELECT *
        FROM t_ultimo_rango
            ) loop
            RAISE NOTICE '%',r;

            INSERT INTO afi.trango_afiliado (id_usuario_reg,
                                             id_usuario_mod,
                                             fecha_reg,
                                             fecha_mod,
                                             estado_reg,
                                             id_usuario_ai,
                                             usuario_ai,
                                             id_afiliado,
                                             id_rango,
                                             id_periodo)
            VALUES (1,
                    NULL,
                    now(),
                    NULL,
                    'activo',
                    1,
                    1,
                    r.id_afiliado,
                    r.id_ultimo_rango,
                    113
                   );

            END LOOP;

    END
$$;