WITH t_rango_anterior AS
         (
             select tra.id_afiliado, tra.id_rango, tra.id_periodo, tr.nivel, tr.volumen_organizacional
             from afi.trango_afiliado tra
                      inner join afi.trango tr on tr.id_rango = tra.id_rango
             where tra.id_periodo = 114
         ), t_rango_siguiente as
         (
             select tra.id_afiliado, tra.id_rango, tra.id_periodo, tr.nivel, tr.volumen_organizacional
             from afi.trango_afiliado tra
                      inner join afi.trango tr on tr.id_rango = tra.id_rango
             where tra.id_periodo = 115
         )
select tra.id_afiliado, tra.id_periodo, tra.nivel, trs.id_periodo, trs.nivel, tra.id_rango as id_rango_anterior, trs.id_rango as id_rango_siguiente
from t_rango_anterior tra
         inner join t_rango_siguiente trs on trs.id_afiliado = tra.id_afiliado
where trs.id_rango != tra.id_rango and tra.nivel > trs.nivel;

select * from afi.trango_afiliado where id_afiliado = 1117;

DO $$
    DECLARE r record;
BEGIN

    FOR r IN (

        WITH t_rango_anterior AS
                 (
                     select tra.id_afiliado, tra.id_rango, tra.id_periodo, tr.nivel, tr.volumen_organizacional
                     from afi.trango_afiliado tra
                              inner join afi.trango tr on tr.id_rango = tra.id_rango
                     where tra.id_periodo = 114
                 ), t_rango_siguiente as
                 (
                     select tra.id_afiliado, tra.id_rango, tra.id_periodo, tr.nivel, tr.volumen_organizacional
                     from afi.trango_afiliado tra
                              inner join afi.trango tr on tr.id_rango = tra.id_rango
                     where tra.id_periodo = 115
                 )
        select tra.id_afiliado, tra.id_periodo, tra.nivel, trs.id_periodo, trs.nivel, tra.id_rango as id_rango_anterior, trs.id_rango as id_rango_siguiente
        from t_rango_anterior tra
                 inner join t_rango_siguiente trs on trs.id_afiliado = tra.id_afiliado
        where trs.id_rango != tra.id_rango and tra.nivel > trs.nivel
    ) loop

            update
                afi.trango_afiliado set id_rango = r.id_rango_anterior
            where id_periodo in (114,115) and id_afiliado = r.id_afiliado;

        END LOOP;

END$$;

select * from afi.tafiliado where id_afiliado = 646;

select tapp.id_afiliado,
       tapp.sube_nivel,
       tapp.pv_acumulado
       ,tr_siguiente.volumen_organizacional,
       tr_siguiente.nombre,
       tr_siguiente.id_rango
from afi.tafiliado_pago_periodo tapp
inner join afi.trango_afiliado tra on tra.id_afiliado = tapp.id_afiliado and tra.id_periodo = tapp.id_periodo
inner join afi.trango tr on tr.id_rango = tra.id_rango
inner join afi.trango tr_siguiente on tr_siguiente.id_rango_fk = tra.id_rango
where tapp.id_periodo = 115
and  tapp.pv_acumulado >= tr_siguiente.volumen_organizacional and tapp.pv_acumulado > 0
and tr_siguiente.nombre != 'doble diamante'
--and tr_siguiente.id_rango = 2
ORDER BY tr_siguiente.id_rango asc;


/*
update afi.trango_afiliado
set id_rango = 6
where id_periodo = 115 and id_afiliado in (1333);
*/