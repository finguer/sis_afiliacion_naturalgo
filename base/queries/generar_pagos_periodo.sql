

--primero debes hacer esto antes de generar los pagos
/*update afi.tbonus set id_patrocinador = id_afiliado
where id_bonus_config = 4 and id_periodo = 121;*/

DO
$$
    DECLARE
        r                                        record;
        r_activos                                record;
        v_id_tipo_chat                           INTEGER;
        v_id_chat                                INTEGER;
        v_usuarios                               varchar;
        v_tabla                                  varchar;
        v_json_params_to_send                    json;
        v_res                                    json;
        v_id_afiliado                            integer;
        v_id_presentador                         integer;
        v_data_json                              json;
        v_activos_primer_nivel                   integer DEFAULT 0;
        v_niveles_a_cobrar_string                varchar;
        v_niveles_a_cobrar_array                 NUMERIC[];
        v_generaciones_cv                        numeric[];
        v_generaciones_pv                        numeric[];
        v_generaciones_cv_monto                  numeric[];
        v_generaciones_count_afiliados_100pts_pv integer[];
        v_generaciones_count_afiliados_activos   integer[];
        v_bonus_registro numeric;
        v_bonus_retorno numeric;
        v_id_periodo integer;
        v_id_rango integer;
        v_resp_json json;
        v_count numeric DEFAULT  0;
    BEGIN
        --24299,21641,18433,20139,21226,11071,21896,40372,41780,3976,45439
        --22016,18474,15605,19149,20139,10905,21122,2255,40372,1130,10721
        v_id_periodo := 128;
        --raise exception '%','eliminar esta parte para correr cada mes';
        FOR r IN (
            SELECT ta.id_afiliado
            FROM afi.tafiliado ta
            INNER JOIN afi.trango_afiliado ran ON ran.id_afiliado = ta.id_afiliado
            WHERE ran.id_periodo = v_id_periodo
            and ta.id_afiliado in (3210, 14)
            order by ta.id_afiliado asc
           --limit 10000 offset 30000



            --LIMIT 100
        )
            LOOP
            v_count := v_count + 1;
                v_id_rango:= null;
                v_bonus_registro:= null;
                v_activos_primer_nivel:= null;
                v_generaciones_count_afiliados_activos:= null;
                v_generaciones_count_afiliados_activos:= null;
                v_generaciones_count_afiliados_100pts_pv:= null;
                v_niveles_a_cobrar_string:= null;
                v_niveles_a_cobrar_array:= null;
                v_generaciones_pv:= null;
                v_generaciones_cv:= null;
                v_generaciones_cv_monto:= null;
                v_id_rango:= null;



                SELECT json_strip_nulls(json_build_object(
                        'id_afiliado', r.id_afiliado,
                        'id_periodo', v_id_periodo,
                        'from_gen', 'Y'
                    ))
                INTO v_json_params_to_send;

                v_resp_json:= afi.f_generar_afiliado_pago_periodo(v_json_params_to_send);

                RAISE NOTICE '%',v_count;



            END LOOP;

    END
$$;