/********************************************I-DEP-FFP-AFI-0-12/04/2015********************************************/


ALTER TABLE afi.tcotitular ALTER COLUMN relacion TYPE VARCHAR(255) USING relacion::VARCHAR(255);

ALTER TABLE afi.tcotitular ALTER COLUMN domicilio TYPE VARCHAR(255) USING domicilio::VARCHAR(255);

ALTER TABLE afi.tcotitular ALTER COLUMN telefono TYPE NUMERIC USING telefono::NUMERIC;

ALTER TABLE afi.tafiliado ADD codigo NUMERIC (16,0);

ALTER TABLE afi.tafiliado ADD estado VARCHAR(255) NULL;

ALTER TABLE afi.tafiliado ADD monto_registro numeric NULL;

ALTER TABLE afi.tafiliado ADD modalidad_de_registro VARCHAR (255)NULL;

/*********************************F-DEP-FFP-AFI-0-12/04/2015***********************************/


/********************************************I-DEP-FFP-AFI-0-29/03/2016********************************************/

select pxp.f_insert_testructura_gui ('PUNTITE', 'AFI');
/*********************************F-DEP-FFP-AFI-0-29/03/2016***********************************/



/********************************************I-DEP-FFP-AFI-0-04/04/2016********************************************/

select pxp.f_insert_testructura_gui ('AFILPAPE', 'AFI');
/*********************************F-DEP-FFP-AFI-0-04/04/2016***********************************/

/********************************************I-DEP-FFP-AFI-0-19/10/2016********************************************/

select pxp.f_insert_testructura_gui ('PARAFI', 'AFI');
select pxp.f_insert_testructura_gui ('RANG', 'PARAFI');
select pxp.f_insert_testructura_gui ('PAGOCON', 'PARAFI');


-----2022
ALTER TABLE ONLY afi.tpuntos_afiliado
    ADD CONSTRAINT fk_tpuntos_afiliado__id_periodo
        FOREIGN KEY (id_periodo) REFERENCES param.tperiodo(id_periodo);

CREATE INDEX idx_tpuntos_afiliado_id_periodo ON afi.tpuntos_afiliado (id_periodo);
CREATE INDEX idx_tafiliado_id_afiliado ON afi.tafiliado (id_afiliado);
CREATE INDEX idx_tafiliado_id_patrocinador ON afi.tafiliado (id_patrocinador);
CREATE INDEX idx_tbonus_id_patrocinador ON afi.tbonus (id_patrocinador);
CREATE INDEX idx_tbonus_id_periodo ON afi.tbonus (id_periodo);

---nuevos 2024
CREATE INDEX idx_tafiliado_pago_periodo_id_periodo ON afi.tafiliado_pago_periodo (id_periodo);
/*********************************F-DEP-FFP-AFI-0-19/10/2016***********************************/
