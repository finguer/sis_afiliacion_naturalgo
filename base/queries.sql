-----iniciar metodos-----

SELECT * FROM mer.titem WHERE codigo = 'aloe polvo';
update mer.titem
set codigo = id_item || 'NGO' || public.generate_uid(id_item, 8, 'salt')
WHERE id_item = 41;

--PARA VID
update mer.titem
set codigo =  'VID' ||id_item || UPPER(public.generate_uid(id_item, 3, 'salt'))
WHERE 1=1;


--obtener persona
select afil.id_afiliado, afil.codigo, p.nombre_completo2, p.ci
from afi.tafiliado afil
inner join segu.vpersona2 p on p.id_persona = afil.id_persona
where afil.id_afiliado = 18522;



select afil.id_afiliado, afil.codigo, p.nombre_completo2, p.ci
from afi.tafiliado afil
inner join segu.vpersona2 p on p.id_persona = afil.id_persona
where afil.codigo in (
'591016227'
    );

select count(*) from segu.tusuario;

select * from afi.tafiliado where id_afiliado = 25958;
SELECT * from param.tperiodo where id_periodo = 103;

select * from param.f_get_periodo_gestion('01/07/2021'::date);
--get the periodo with dates
--25958
DO
$$DECLARE r record;
          v_pv_acumulado numeric;
          v_fecha_1 date;
          v_fecha_2 date;
          v_record_periodo_1 record;
          v_record_periodo_2 record;
          v_record_periodo_current record;
          v_rango record;
          v_rango_afiliado record;

BEGIN
    v_fecha_1:= '01/11/2021'::date;
    v_fecha_2:= '01/12/2021'::date;
    v_record_periodo_1:= param.f_get_periodo_gestion(v_fecha_1::date);
    v_record_periodo_2:= param.f_get_periodo_gestion(v_fecha_2::date);
    v_record_periodo_current:= param.f_get_periodo_gestion(now()::date);
    --RAISE EXCEPTION '%',v_record_periodo_2.po_id_periodo;

    FOR r IN (
        SELECT *
        FROM afi.tafiliado ta
                 INNER JOIN segu.vpersona2 vp on vp.id_persona = ta.id_persona
            /*WHERE ta.codigo = 5910787*/ ORDER BY ta.id_afiliado ASC)
        LOOP

            SELECT sum(pv_acumulado)
            into v_pv_acumulado
            FROM afi.tafiliado_pago_periodo
            where id_afiliado = r.id_afiliado and id_periodo in ( v_record_periodo_1.po_id_periodo , v_record_periodo_2.po_id_periodo)
              and habilitado = 'si';

            --RAISE NOTICE '%',v_pv_acumulado;

            SELECT *
            INTO v_rango
            FROM afi.trango
            WHERE volumen_organizacional <= v_pv_acumulado
            ORDER BY nivel desc
            LIMIT 1;

            SELECT tr.nivel,tr.nombre
            INTO v_rango_afiliado
            FROM afi.trango_afiliado tra
                     inner join afi.trango tr on tr.id_rango = tra.id_rango
            WHERE tra.id_afiliado = r.id_afiliado
              AND tra.id_periodo = v_record_periodo_current.po_id_periodo;

            --RAISE NOTICE '%',v_rango.nivel;
            --RAISE NOTICE '%',v_rango_afiliado.nivel;

            IF v_rango.nivel > v_rango_afiliado.nivel THEN
                RAISE NOTICE '%','id afiliado '|| r.id_afiliado|| ' sube nivel de ' || v_rango_afiliado.nombre || ' a ' || v_rango.nombre || ' el codigo ' || r.codigo || '- ' || r.nombre_completo1;
               /* UPDATE afi.trango_afiliado set id_rango = v_rango.id_rango
                where id_afiliado = r.id_afiliado
                  and id_periodo > v_record_periodo_2.po_id_periodo;*/
            END IF;

        END LOOP;
END$$;


---------


-- funcionalidad para agregar los puntos de patrocinador
DO $$DECLARE r record;
             v_id_tipo_chat			   INTEGER;
             v_id_chat			       INTEGER;
             v_usuarios			       varchar;
             v_tabla			           varchar;
             v_json_params_to_send			           json;
             v_res json;
BEGIN


    FOR r IN (
        select tpa.*
        from afi.tpuntos_afiliado tpa
        where tpa.id_set_item = 48 and id_periodo = 109
    )
        LOOP
            SELECT json_strip_nulls(json_build_object(
                    'id_set_item', r.id_set_item,
                    'id_afiliado', r.id_afiliado,
                    'id_usuario', r.id_usuario_reg,
                    '_nombre_usuario_ai', 'NULL',
                    '_id_usuario_ai', '',
                    'po_id_periodo', r.id_periodo
                ))
            INTO v_json_params_to_send;


            --v_res:= afi.f_insertar_bonus_afiliado(v_json_params_to_send::json);
            RAISE NOTICE '%',v_res;


        END LOOP;



END$$;

-- funcionalidad para agregar bono presentados
DO $$DECLARE r record;
             v_id_tipo_chat			   INTEGER;
             v_id_chat			       INTEGER;
             v_usuarios			       varchar;
             v_tabla			           varchar;
             v_json_params_to_send			           json;
             v_res json;
            v_id_afiliado integer;
            v_id_presentador integer;
BEGIN

    FOR r IN (
        select *
        from public.temp_bono_presentador_feb_2022
    )
    LOOP

        select id_afiliado into v_id_afiliado from afi.tafiliado where codigo = UPPER(trim(r.codigo_afiliado));
        select id_afiliado into v_id_presentador from afi.tafiliado where codigo = UPPER(trim(r.codigo_patrocinador));



        IF v_id_afiliado is not null and v_id_presentador is not null then

            INSERT INTO afi.tafiliado_presentador (id_usuario_reg, id_usuario_mod, fecha_reg, fecha_mod, estado_reg,
                                                   id_usuario_ai, usuario_ai, obs_dba, id_presentador,
                                                   id_afiliado, id_periodo, monto)
            VALUES (1,
                    NULL,
                    now(),
                    NULL,
                    'activo',
                    null,
                    'NULL',
                    NULL,
                    v_id_presentador,
                    v_id_afiliado,
                    110,
                    r.monto);

            else
            RAISE NOTICE '%','algun error';
END IF;






    END LOOP;

END$$;