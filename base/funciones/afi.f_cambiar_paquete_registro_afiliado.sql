CREATE OR REPLACE FUNCTION afi.f_cambiar_paquete_registro_afiliado(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE


    v_nombre_funcion                varchar;
    p_id_afiliado                   integer DEFAULT p_params ->> 'id_afiliado';
    p_id_set_item_nuevo                  integer DEFAULT p_params ->> 'id_set_item_nuevo';
    p_id_usuario                    integer DEFAULT p_params ->> 'userId';
    p_items                         json DEFAULT p_params ->> 'items';
    v_records_items                 record;
    v_record_afiliado               record;
    v_res_regenerar_bono_patrocinio json;
    v_rec_periodo                   record;
    v_json_to_send                  json;
    v_resp                          json;
    v_return_movimiento_item_set    BOOL;
    v_id_almacen                    INTEGER;
    v_puntos_item                    record;


BEGIN


    v_nombre_funcion = 'afi.f_cambiar_paquete_registro_afiliado';

    SELECT *
    INTO v_record_afiliado
    FROM afi.tafiliado
    WHERE id_afiliado = p_id_afiliado;


    v_rec_periodo = param.f_get_periodo_gestion(to_char(v_record_afiliado.fecha_reg::date, 'YYYY-mm-dd')::DATE);

    UPDATE afi.tafiliado
    SET id_set_item = p_id_set_item_nuevo
    WHERE id_afiliado = v_record_afiliado.id_afiliado;


    SELECT id_almacen
    INTO v_id_almacen
    FROM su.tpunto_venta_almacen
    WHERE id_punto_venta = v_record_afiliado.id_punto_venta;

    -- necesitamos agregar el stock del anterior registro
    FOR v_records_items IN (SELECT j ->> 'cantidad' AS cantidad, j ->> 'id_item' AS id_item
                            FROM json_array_elements(v_record_afiliado.data_json_items) j)
        LOOP


            INSERT INTO mer.tmovimiento (
                estado_reg,
                id_item,
                cantidad_movida,
                comentario,
                tipo,
                id_almacen,
                fecha_reg,
                usuario_ai,
                id_usuario_reg,
                id_usuario_ai,
                id_usuario_mod,
                fecha_mod,
                id_movimiento_fk

            ) VALUES (
                         'activo',
                         cast(v_records_items.id_item as integer),
                         cast(v_records_items.cantidad as integer),
                         'entrada de producto por que se cambio de paquete algun del afiliado: '|| v_record_afiliado.codigo,
                         'entrada',
                         v_id_almacen,
                         now(),
                         'NULL',
                         p_id_usuario,
                         null,
                         NULL,
                         NULL,
                         null


                     );

        END LOOP;

    -- necesitamos agregar los nuevos items selecciona al cambiar el paquete

    FOR v_records_items IN (SELECT j ->> 'cantidad' AS cantidad, j ->> 'id_item' AS id_item
                            FROM json_array_elements(p_items) j)
        LOOP

            v_return_movimiento_item_set = vent.movimiento_almacen_producto(
                    'NULL',
                    p_id_usuario,
                    NULL,
                    'SIN SERIAL',
                    cast(v_records_items.id_item AS integer),
                    v_record_afiliado.id_punto_venta,
                    cast(v_records_items.cantidad AS integer),
                    NULL,
                    v_id_almacen
                );


        END LOOP;


    delete from afi.tpuntos_afiliado
    where id_afiliado = p_id_afiliado
      and id_periodo = v_rec_periodo.po_id_periodo
      and id_venta is null; --id venta es importante que sea null por que asi sabemos que es de afiliacion

    SELECT pi.*,
           bc.bonus,
           bc.monto,
           bc.bono_patrocinio_por_nivel,
           ts.id_set_item,
           ts.precio AS precio_del_set,
           bc.calcular_por,
           pi.cv,
           pi.monto_bono_presentador
    INTO v_puntos_item
    FROM afi.tpunto_item pi
             INNER JOIN afi.tbonus_config bc ON bc.id_bonus_config = pi.id_bonus_config
             INNER JOIN vent.tset_item ts ON ts.id_set_item = pi.id_set
    WHERE pi.id_set = p_id_set_item_nuevo;



    INSERT INTO afi.tpuntos_afiliado (estado_reg,
                                      id_set_item,
                                      id_periodo,
                                      cv,
                                      id_item,
                                      pv,
                                      fecha_reg,
                                      usuario_ai,
                                      id_usuario_reg,
                                      id_usuario_ai,
                                      id_usuario_mod,
                                      fecha_mod,
                                      id_afiliado)
    VALUES ('activo',
            p_id_set_item_nuevo,
            v_rec_periodo.po_id_periodo,
            v_puntos_item.cv,
            NULL,
            v_puntos_item.pv,
            now(),
            'null',
            p_id_usuario,
            null,
            NULL,
            NULL,
            p_id_afiliado);


    SELECT json_strip_nulls(json_build_object(
            'id_afiliado', v_record_afiliado.id_afiliado,
            'id_set_item_nuevo', p_id_set_item_nuevo,
            'userId', p_id_usuario,
            'id_periodo', v_rec_periodo.po_id_periodo::INTEGER
        ))
    INTO v_json_to_send;



    v_res_regenerar_bono_patrocinio := afi.f_regenerar_bono_patrocinio_por_paquete(v_json_to_send);


    SELECT json_strip_nulls(json_build_object(
            'res_regenerar', v_res_regenerar_bono_patrocinio
        ))
    INTO v_resp;
    RETURN v_resp;


EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
            ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;
        RETURN v_resp;


END ;
$BODY$
    LANGUAGE plpgsql VOLATILE;