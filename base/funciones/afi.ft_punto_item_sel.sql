CREATE OR REPLACE FUNCTION "afi"."ft_punto_item_sel"(	
				p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$
/**************************************************************************
 SISTEMA:		afiliacion
 FUNCION: 		afi.ft_punto_item_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'afi.tpunto_item'
 AUTOR: 		 (admin)
 FECHA:	        18-12-2015 23:29:48
 COMENTARIOS:	
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:	
 AUTOR:			
 FECHA:		
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;
			    
BEGIN

	v_nombre_funcion = 'afi.ft_punto_item_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************    
 	#TRANSACCION:  'AFI_PUNTI_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin	
 	#FECHA:		18-12-2015 23:29:48
	***********************************/

	if(p_transaccion='AFI_PUNTI_SEL')then
     				
    	begin
    		--Sentencia de la consulta
			v_consulta:='select
								punti.id_punto_item,
								punti.pv,
								punti.estado_reg,
								punti.cv,
								punti.id_set,
								punti.id_item,
								punti.usuario_ai,
								punti.fecha_reg,
								punti.id_usuario_reg,
								punti.id_usuario_ai,
								punti.fecha_mod,
								punti.id_usuario_mod,
								usu1.cuenta as usr_reg,
								usu2.cuenta as usr_mod	,
								punti.tipo,
								ite.nombre as desc_item,
								set.nombre as desc_set,
								punti.bonus_patrocinador,
								bc.bonus as desc_bonus_config
							from afi.tpunto_item punti
								inner join segu.tusuario usu1 on usu1.id_usuario = punti.id_usuario_reg
								left join segu.tusuario usu2 on usu2.id_usuario = punti.id_usuario_mod
							left JOIN mer.titem ite on ite.id_item = punti.id_item
							left JOIN vent.tset_item set on set.id_set_item = punti.id_set
							left join afi.tbonus_config bc on bc.id_bonus_config = punti.id_bonus_config
				        where  ';
			
			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;
			v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

			--Devuelve la respuesta
			return v_consulta;
						
		end;

	/*********************************    
 	#TRANSACCION:  'AFI_PUNTI_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin	
 	#FECHA:		18-12-2015 23:29:48
	***********************************/

	elsif(p_transaccion='AFI_PUNTI_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(id_punto_item)
					    from afi.tpunto_item punti
					    inner join segu.tusuario usu1 on usu1.id_usuario = punti.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = punti.id_usuario_mod
						left JOIN mer.titem ite on ite.id_item = punti.id_item
							left JOIN vent.tset_item set on set.id_set_item = punti.id_set
						left join afi.tbonus_config bc on bc.id_bonus_config = punti.id_bonus_config
					    where ';
			
			--Definicion de la respuesta		    
			v_consulta:=v_consulta||v_parametros.filtro;

			--Devuelve la respuesta
			return v_consulta;

		end;
					
	else
					     
		raise exception 'Transaccion inexistente';
					         
	end if;
					
EXCEPTION
					
	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "afi"."ft_punto_item_sel"(integer, integer, character varying, character varying) OWNER TO postgres;
