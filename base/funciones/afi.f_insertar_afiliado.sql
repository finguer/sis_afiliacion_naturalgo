CREATE OR REPLACE FUNCTION afi.f_insertar_afiliado(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE


    p_pre_registro               varchar DEFAULT 'no';
    p_nombre                     varchar DEFAULT p_params ->> 'nombre';
    p_ap_paterno                 varchar DEFAULT p_params ->> 'ap_paterno';
    p_ap_materno                 varchar DEFAULT p_params ->> 'ap_materno';
    p_ci                         varchar DEFAULT p_params ->> 'ci';
    p_correo                     varchar DEFAULT p_params ->> 'correo';
    p_celular1                   varchar DEFAULT p_params ->> 'celular1';
    p_telefono1                  varchar DEFAULT p_params ->> 'telefono1';
    p_direccion_principal        varchar DEFAULT p_params ->> 'direccion_principal';
    p_lugar_nacimiento           varchar DEFAULT p_params ->> 'lugar_nacimiento';
    p_id_patrocinador            INTEGER DEFAULT p_params ->> 'id_patrocinador';
    p_id_presentador             INTEGER DEFAULT p_params ->> 'id_presentador';
    p_id_set_item                INTEGER DEFAULT p_params ->> 'id_set_item';
    p_id_punto_venta             INTEGER DEFAULT p_params ->> 'id_punto_venta';
    p_pais                       varchar DEFAULT p_params ->> 'pais';
    p_id_moneda                  INTEGER DEFAULT p_params ->> 'id_moneda';
    p_monto_registro             numeric DEFAULT p_params ->> 'monto_registro';
    v_id_afiliado                integer DEFAULT p_params ->> 'id_afiliado';
    p_id_usuario                 integer DEFAULT p_params ->> 'userId';
    p_is_admin                   integer DEFAULT p_params ->> 'isAdmin';
    p_username                   varchar DEFAULT p_params ->> 'username';
    p_items                      json DEFAULT p_params ->> 'items';
    p_doc_dni_name               varchar DEFAULT p_params ->> 'doc_dni_name';
    v_resp                       json;
    v_nombre_funcion             TEXT;
    v_puntos_item                RECORD;
    v_padre                      INTEGER;
    v_abuelo                     INTEGER;
    v_return_movimiento_item_set BOOL;
    v_tipo                       VARCHAR;
    v_record                     RECORD;
    v_rec                        RECORD;
    v_fecha_ini                  date;
    v_fecha_fin                  date;
    v_id_almacen                 INTEGER;
    v_id_rango                   INTEGER;
    v_id_cliente                 INTEGER;
    v_tipo_venta                 varchar;
    v_nro_venta                  VARCHAR;
    v_id_sucursal                INTEGER;
    v_suc                        VARCHAR;
    v_punto                      VARCHAR;
    v_numero_correlativo         INTEGER;
    v_id_venta                   INTEGER;
    v_id_usuario                 INTEGER;
    v_id_persona                 INTEGER;
    v_ci                         segu.tpersona.ci%TYPE;
    v_codigo_siguiente           NUMERIC(16);
    v_alias                      VARCHAR;
    v_pais_origen                VARCHAR;
    v_text_json                  JSON;
    v_json_params_to_send        json;
    v_res_insertar_bonus_red     json;
    v_id_usuario_ai              integer DEFAULT NULL;
    v_usuario_ai                 varchar DEFAULT 'NULL';
    v_records_items              record;
    v_length integer;
    v_cantidad_para_validar integer DEFAULT 0;
    v_cv_para_puntos numeric(10,2);
    v_item_ids integer[];

    v_cuenta varchar;
    v_json_res_sale_point json;
    v_empresa varchar;

BEGIN


    v_nombre_funcion = 'afi.f_insertar_afiliado';
    v_empresa := pxp.f_get_variable_global('empresa_afiliacion');



    select json_array_length(p_items) as length_items
    into v_length;

    if v_length = 0 then
        RAISE EXCEPTION '%', 'NO PUEDES INGRESAR UN AFILIADO SIN SELECCIONAR PRODUCTOS';
    end if;


    SELECT id_almacen
    INTO v_id_almacen
    FROM su.tpunto_venta_almacen
    WHERE id_punto_venta = p_id_punto_venta;


    SELECT array_agg(j ->> 'id_item') as ids
    into v_item_ids
    FROM json_array_elements(p_items) j;


    IF exists(select 1 from vent.titem_no_permitido_set
              where id_set_item = p_id_set_item and id_item = ANY(v_item_ids)) THEN
        RAISE EXCEPTION 'ERROR EXISTEN ALGUNOS PRODUCTOS NO PERMITIDOS PARA ESTE SET';
    END IF;




    FOR v_records_items IN (SELECT j ->> 'cantidad' AS cantidad, j ->> 'id_item' AS id_item
                            FROM json_array_elements(p_items) j)
        LOOP
            v_cantidad_para_validar := v_cantidad_para_validar + v_records_items.cantidad::integer;

            v_return_movimiento_item_set = vent.movimiento_almacen_producto(
                    v_usuario_ai,
                    p_id_usuario,
                    v_id_usuario_ai,
                    'SIN SERIAL',
                    cast(v_records_items.id_item AS integer),
                    p_id_punto_venta,
                    cast(v_records_items.cantidad AS integer),
                    NULL,
                    v_id_almacen
                );


        END LOOP;


    --obtener codificacion para el numero
    SELECT cod.numero_siguiente, cod.alias, cod.pais
    INTO v_codigo_siguiente, v_alias, v_pais_origen
    FROM afi.tcodificacion cod
    WHERE cod.pais = p_pais
    LIMIT 1 FOR UPDATE;


    IF p_pre_registro = 'si' THEN

        v_id_persona := p_params ->> 'id_persona_pre_registro';
    ELSE

        IF exists(SELECT 1
                  FROM segu.tpersona
                  WHERE ci = p_ci) THEN
            RAISE EXCEPTION 'Este número de Carnet de Identidad ya fue registrado';
        END IF;


        --nunca deberia entrar aca
        IF exists(SELECT 1
                  FROM afi.tafiliado
                  WHERE codigo = coalesce(v_alias, '') || v_codigo_siguiente::varchar
            ) THEN
            RAISE EXCEPTION 'Este codigo ya fue registrado';
        END IF;


        INSERT INTO segu.tpersona (nombre,
                                   apellido_paterno,
                                   apellido_materno,
                                   ci,
                                   correo,
                                   celular1,
                                   telefono1,
                                   telefono2,
                                   celular2,
                                   tipo_documento,
                                   expedicion)
        VALUES (p_nombre,
                p_ap_paterno,
                p_ap_materno,
                p_ci,
                p_correo,
                p_celular1,
                p_telefono1,
                NULL,
                NULL,
                NULL,
                NULL)

        RETURNING id_persona INTO v_id_persona;

    END IF;


    INSERT INTO afi.tafiliado (direccion_principal,
                               id_persona,
                               lugar_nacimiento,
                               estado_reg,
                               fecha_nacimiento,
                               id_usuario_ai,
                               id_usuario_reg,
                               fecha_reg,
                               usuario_ai,
                               id_usuario_mod,
                               fecha_mod,
                               id_patrocinador,
                               codigo,
                               modalidad_de_registro,
                               estado,
                               id_set_item,
                               id_punto_venta,
                               pais,
                               data_json_items,
                               doc_dni_name
                               )
    VALUES (p_direccion_principal,
            v_id_persona,
            p_lugar_nacimiento,
            'activo',
            CAST(p_params ->> 'fecha_nacimiento' AS date),
            v_id_usuario_ai,
            p_id_usuario,
            now(),
            v_usuario_ai,
            NULL,
            NULL,
            p_id_patrocinador,
            coalesce(v_alias, '') || v_codigo_siguiente::varchar,
            'activo',
            'activo',
            p_id_set_item,
            p_id_punto_venta,
            p_pais,
            p_items,
            p_doc_dni_name
            )
    RETURNING id_afiliado INTO v_id_afiliado;


    v_fecha_fin = (date_trunc('MONTH', now()) + INTERVAL '1 MONTH - 1 day')::date;

    INSERT INTO afi.tafiliado_etapa(id_afiliado,
                                    fecha_fin,
                                    etapa,
                                    fecha_ini,
                                    estado_reg,
                                    id_usuario_ai,
                                    usuario_ai,
                                    fecha_reg,
                                    id_usuario_reg,
                                    fecha_mod,
                                    id_usuario_mod)
    VALUES (v_id_afiliado,
            v_fecha_fin,
            'inicio',
            now(),
            'activo',
            v_id_usuario_ai,
            v_usuario_ai,
            now(),
            p_id_usuario,
            NULL,
            NULL);


    FOR v_record
        IN (SELECT afil.*,
                   person.nombre_completo2
            FROM afi.tafiliado afil
                     INNER JOIN segu.vpersona2 person ON person.id_persona = afil.id_persona
            WHERE afil.id_afiliado = v_id_afiliado)
        LOOP

            INSERT INTO vent.tcliente (id_persona,
                                       estado_reg,
                                       nit,
                                       codigo,
                                       usuario_ai,
                                       fecha_reg,
                                       id_usuario_reg,
                                       id_usuario_ai,
                                       fecha_mod,
                                       id_usuario_mod,
                                       razon)
            VALUES (v_record.id_persona,
                    'activo',
                    v_record.codigo,
                    v_record.codigo,
                    v_record.usuario_ai,
                    now(),
                    v_record.id_usuario_reg,
                    v_record.id_usuario_ai,
                    NULL,
                    NULL,
                    v_record.nombre_completo2)
            RETURNING id_cliente INTO v_id_cliente;
        END LOOP;

    v_tipo := 'SET';


    SELECT pu.id_sucursal,
           pu.codigo,
           su.codigo,
           pu.numero_correlativo
    INTO v_id_sucursal, v_punto, v_suc, v_numero_correlativo
    FROM su.tpunto_venta pu
             INNER JOIN su.tsucursal su ON su.id_sucursal = pu.id_sucursal
    WHERE pu.id_punto_venta = p_id_punto_venta
    FOR UPDATE;


    --datos para la venta al afiliar debe ser como una venta de un producto
    --id_tipo_venta 1 v_tipo_venta tipo de venta recibo

    v_tipo_venta = 'recibo';

    v_nro_venta := 'VENAFI-' || v_suc || '-' || v_punto || '-' || v_numero_correlativo;


    v_rec = param.f_get_periodo_gestion(to_char(now(), 'YYYY-mm-dd')::DATE);


    --RAISE EXCEPTION '%',v_nro_venta;

    --Sentencia de la insercion
    INSERT INTO vent.tventa (id_cliente,
                             id_tipo_venta,
                             nro_venta,
                             estado_reg,
                             estado,
                             id_punto_venta,
                             tipo_pago,
                             fecha,
                             usuario_ai,
                             fecha_reg,
                             id_usuario_reg,
                             id_usuario_ai,
                             fecha_mod,
                             id_usuario_mod,
                             id_periodo,
                             id_moneda)
    VALUES (v_id_cliente,
            1,
            v_nro_venta,
            'activo',
            'pagado',
            p_id_punto_venta,
            'Efectivo',
            now()::DATE,
            v_usuario_ai,
            now(),
            p_id_usuario,
            v_id_usuario_ai,
            NULL,
            NULL,
            v_rec.po_id_periodo::INTEGER,
            p_id_moneda)
    RETURNING id_venta INTO v_id_venta;

    --aumentamos el numero correlativo
    UPDATE su.tpunto_venta
    SET numero_correlativo = numero_correlativo + 1
    WHERE id_punto_venta = p_id_punto_venta;


    --insercion de pago
    INSERT INTO vent.tpago(fecha,
                           monto_total,
                           tipo_pago,
                           estado_reg,
                           saldo,
                           cambio,
                           moneda,
                           id_venta,
                           pago,
                           id_usuario_ai,
                           id_usuario_reg,
                           usuario_ai,
                           fecha_reg,
                           id_usuario_mod,
                           fecha_mod,
                           id_moneda)
    VALUES (now(),
            p_monto_registro,
            'Efectivo',
            'activo',
            0,
            0,
            '',
            v_id_venta,
            p_monto_registro,
            v_id_usuario_ai,
            p_id_usuario,
            v_usuario_ai,
            now(),
            NULL,
            NULL,
            p_id_moneda);


    INSERT INTO vent.tventa_detalle (estado_reg,
                                     cantidad_producto,
                                     total,
                                     id_item,
                                     tipo,
                                     id_set,
                                     id_venta,
                                     precio_unitario,
                                     usuario_ai,
                                     fecha_reg,
                                     id_usuario_reg,
                                     id_usuario_ai,
                                     id_usuario_mod,
                                     fecha_mod,
                                     id_movimiento,
                                     id_almacen,
                                     data_json_items)
    VALUES ('activo',
            1,
            p_monto_registro,
            NULL,
            v_tipo,
            p_id_set_item,
            v_id_venta,
            p_monto_registro,
            v_usuario_ai,
            now(),
            p_id_usuario,
            v_id_usuario_ai,
            NULL,
            NULL,
            NULL,
            v_id_almacen,
            p_items);


    /*    v_return_movimiento_item_set = vent.movimiento_almacen_producto(
                v_usuario_ai,
                p_id_usuario,
                v_id_usuario_ai,
                v_tipo,
                p_id_set_item,
                p_id_punto_venta,
                1 :: INTEGER,
                NULL,
                v_id_almacen
            );*/


    --agregar puntos por el paquete agregado al inscribirse
    SELECT pi.*,
           bc.bonus,
           bc.monto,
           bc.bono_patrocinio_por_nivel,
           ts.id_set_item,
           ts.precio AS precio_del_set,
           bc.calcular_por,
           pi.cv,
           pi.monto_bono_presentador,
           bc.asignar_id_rango_directo,
           ts.cantidad_de_productos,
           bc.cv_para_puntos_red
    INTO v_puntos_item
    FROM afi.tpunto_item pi
             INNER JOIN afi.tbonus_config bc ON bc.id_bonus_config = pi.id_bonus_config
             INNER JOIN vent.tset_item ts ON ts.id_set_item = pi.id_set
    WHERE pi.id_set = p_id_set_item;

    --algunas validaciones extras para naturalgo
    /*if v_puntos_item.id_set_item = 56 then
        raise exception '%', 'NO PUEDES AFILIAR CON ESTE KIT O PAQUETE';
    end if;*/


    if(v_puntos_item.cantidad_de_productos != v_cantidad_para_validar) then
        RAISE EXCEPTION '%','ERROR LOS PRODUCTOS CONFIGURADO PARA EL PAQUETE NO ES IGUAL AL QUE SE QUIERE VENDER';
    END IF;

    if v_puntos_item.cv_para_puntos_red is not NULL THEN
        v_cv_para_puntos:=v_puntos_item.cv_para_puntos_red;
        else
        v_cv_para_puntos:= v_puntos_item.cv;
    END IF;


    SELECT json_strip_nulls(json_build_object(
            'id_set_item', v_puntos_item.id_set_item,
            'id_afiliado', v_id_afiliado,
            'id_usuario', p_id_usuario,
            '_nombre_usuario_ai', 'NULL',
            '_id_usuario_ai', '',
            'po_id_periodo', v_rec.po_id_periodo,
            'id_venta', v_id_venta
        ))
    INTO v_json_params_to_send;

    v_res_insertar_bonus_red := afi.f_insertar_bonus_afiliado(v_json_params_to_send);


    --agregamos el rango al afiliado

    IF v_puntos_item.asignar_id_rango_directo is not null then
        v_id_rango := v_puntos_item.asignar_id_rango_directo;
    else
        SELECT id_rango
        INTO v_id_rango
        FROM afi.trango
        WHERE id_rango_fk IS NULL;
    END IF;




    INSERT INTO afi.trango_afiliado (id_usuario_reg,
                                     id_usuario_mod,
                                     fecha_reg,
                                     fecha_mod,
                                     estado_reg,
                                     id_usuario_ai,
                                     usuario_ai,
                                     id_afiliado,
                                     id_rango,
                                     id_periodo)
    VALUES (p_id_usuario,
            NULL,
            now(),
            NULL,
            'activo',
            v_id_usuario_ai,
            v_usuario_ai,
            v_id_afiliado,
            v_id_rango,
            v_rec.po_id_periodo);


    --agregamos los puntos al afiliado
    INSERT INTO afi.tpuntos_afiliado (estado_reg,
                                      id_set_item,
                                      id_periodo,
                                      cv,
                                      id_item,
                                      pv,
                                      fecha_reg,
                                      usuario_ai,
                                      id_usuario_reg,
                                      id_usuario_ai,
                                      id_usuario_mod,
                                      fecha_mod,
                                      id_afiliado)
    VALUES ('activo',
            p_id_set_item,
            v_rec.po_id_periodo,
            v_cv_para_puntos,
            NULL,
            v_puntos_item.pv,
            now(),
            v_usuario_ai,
            p_id_usuario,
            v_id_usuario_ai,
            NULL,
            NULL,
            v_id_afiliado);


    v_padre = afi.f_ver_patrocinador(v_id_afiliado);
    v_abuelo = afi.f_ver_patrocinador(v_padre);


    --registramos usuario relacionado al afiliado personas

    SELECT ci
    INTO v_ci
    FROM segu.tpersona
    WHERE id_persona = v_id_persona;


    -- verificamos si mando username entonces la cuenta para su inicio de sesion debe ser con loque mandan desde el registro
    v_cuenta:= coalesce(p_username, coalesce(v_alias, '') || v_codigo_siguiente::VARCHAR);

    INSERT INTO segu.tusuario (id_clasificador,
                               cuenta,
                               contrasena,
                               fecha_caducidad,
                               fecha_reg,
                               estilo,
                               contrasena_anterior,
                               id_persona,
                               estado_reg,
                               autentificacion)
    VALUES (1,
            v_cuenta::VARCHAR,
            md5(v_ci::VARCHAR),
            '2050-04-21',
            now(),
            'xtheme-gray.css',
            md5(v_ci::VARCHAR),
            v_id_persona,
            'activo',
            'local'
            )
    RETURNING id_usuario INTO v_id_usuario;


    INSERT INTO segu.tusuario_rol (id_rol, id_usuario, fecha_reg, estado_reg)
    VALUES (5, v_id_usuario, now(), 'activo');


    UPDATE afi.tcodificacion
    SET numero_siguiente = v_codigo_siguiente + 1
    WHERE numero_siguiente = v_codigo_siguiente
      AND pais = p_pais;


    --2022 registro de presentador
    IF p_id_presentador IS NOT NULL THEN
        INSERT INTO afi.tafiliado_presentador (id_usuario_reg, id_usuario_mod, fecha_reg, fecha_mod, estado_reg,
                                               id_usuario_ai, usuario_ai, obs_dba, id_presentador,
                                               id_afiliado, id_periodo, monto)
        VALUES (p_id_usuario,
                NULL,
                now(),
                NULL,
                'activo',
                v_id_usuario_ai,
                v_usuario_ai,
                NULL,
                p_id_presentador,
                v_id_afiliado,
                v_rec.po_id_periodo::INTEGER,
                v_puntos_item.monto_bono_presentador);
    END IF;

    --2022 end registro de presentador

    if v_empresa = 'naturalgo' then
           v_json_res_sale_point:= afi.f_insert_sale_point_corp(json_strip_nulls(json_build_object(
            'id_afiliado', v_id_afiliado,
            'id_usuario', p_id_usuario
                                                           )));
    end if;


    --retornamos datos del afiliado registrado para que se imprima
   /* WITH afiliado_registrado AS
             (
                 SELECT afiliado.codigo,
                        afiliado.fecha_reg,
                        persona.nombre_completo2,
                        persona.ci,
                        persona.celular1,
                        persona.telefono1,
                        persona.correo,
                        patrocinador.codigo                   AS codigo_patrocinador,
                        persona_patrocinador.nombre_completo2 AS nombre_patrocinador,
                        venta.nro_venta,
                        setitem.nombre                        AS nombre_set,
                        pago.monto_total
                 FROM afi.tafiliado afiliado
                          INNER JOIN afi.tafiliado patrocinador ON patrocinador.id_afiliado = afiliado.id_patrocinador
                          INNER JOIN segu.vpersona2 persona ON persona.id_persona = afiliado.id_persona
                          INNER JOIN segu.vpersona2 persona_patrocinador
                                     ON persona_patrocinador.id_persona = patrocinador.id_persona
                          INNER JOIN vent.tcliente cliente ON cliente.id_persona = afiliado.id_persona
                          INNER JOIN vent.tventa venta
                                     ON venta.id_cliente = cliente.id_cliente AND venta.nro_venta LIKE '%VENAFI%'
                          INNER JOIN vent.tventa_detalle detalle ON detalle.id_venta = venta.id_venta
                          INNER JOIN vent.tset_item setitem ON setitem.id_set_item = detalle.id_set
                          INNER JOIN vent.tpago pago ON pago.id_venta = venta.id_venta
                 WHERE afiliado.id_afiliado = v_id_afiliado
             )

    SELECT TO_JSON(ar) AS afiliado_registrado
    INTO v_text_json
    FROM (
             SELECT *
             FROM afiliado_registrado
         ) ar;*/

    WITH t_venta AS (
        SELECT tv.id_venta,
               tv.nro_venta,
               tv.fecha_reg,
               tv.id_usuario_reg,
               tu.cuenta  AS usuario_reg,
               tv.id_punto_venta,
               tpv.nombre AS desc_punto_venta,
               ta.nombre  AS desc_almacen,
               tc.codigo as codigo_cliente,
               tp.monto_total
        FROM vent.tventa tv
                 INNER JOIN segu.tusuario tu ON tu.id_usuario = id_usuario_reg
                 INNER JOIN su.tpunto_venta tpv ON tpv.id_punto_venta = tv.id_punto_venta
                 INNER JOIN su.tpunto_venta_almacen tpva ON tpva.id_punto_venta = tpv.id_punto_venta
                 INNER JOIN mer.talmacen ta ON ta.id_almacen = tpva.id_almacen
                 inner join vent.tcliente tc on tc.id_cliente = tv.id_cliente
                 inner join vent.tpago tp on tp.id_venta = tv.id_venta

        WHERE tv.id_venta = v_id_venta
        LIMIT 1
    ),
         t_afiliado as (
             SELECT ta.codigo, vp.nombre_completo2, pa.codigo as codigo_patrocinador, vp2.nombre_completo2 as nombre_completo_patrocinador
             from afi.tafiliado ta
                      inner join segu.vpersona2 vp on vp.id_persona = ta.id_persona
             inner join afi.tafiliado pa on pa.id_afiliado = ta.id_patrocinador
                      inner join segu.vpersona2 vp2 on vp2.id_persona = pa.id_persona
             where ta.id_afiliado = v_id_afiliado
         ),
         t_venta_detalle AS (
             SELECT tsi.nombre AS desc_paquete, tvd.data_json_items
             FROM vent.tventa_detalle tvd
                      INNER JOIN t_venta tv ON tv.id_venta = tvd.id_venta
                      INNER JOIN vent.tset_item tsi ON tsi.id_set_item = tvd.id_set
         ),
         t_items_seleccionados AS (
             SELECT j.json ->> 'id_item' AS id_item, j.json ->> 'cantidad' AS cantidad, ti.nombre AS desc_item
             FROM (
                      SELECT json_array_elements(data_json_items) AS json
                      FROM t_venta_detalle
                  ) AS j
                      INNER JOIN mer.titem ti ON ti.id_item = cast(j.json ->> 'id_item' AS integer)
         )
    SELECT to_json(jsonData) #>> '{}' AS json
    into v_text_json
    FROM (
             SELECT *,
                    (SELECT to_json(tvd) FROM t_venta_detalle tvd)                                     AS venta_detalle,
                    (SELECT to_json(ta) FROM t_afiliado ta)                                     AS afiliado,
                    (SELECT array_to_json(array_agg(row_to_json(tis)))
                     FROM t_items_seleccionados tis)                                                   AS items_seleccionados
             FROM t_venta
         ) jsonData;


    RETURN v_text_json;


    SELECT json_strip_nulls(json_build_object(
            'success', TRUE
        ))
    INTO v_resp;
    RETURN v_resp;


EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
            ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;
        RETURN v_resp;


END ;
$BODY$
    LANGUAGE plpgsql VOLATILE;