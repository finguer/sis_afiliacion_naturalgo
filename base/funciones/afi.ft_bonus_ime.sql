CREATE OR REPLACE FUNCTION "afi"."ft_bonus_ime" (    
                p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$

/**************************************************************************
 SISTEMA:        AFILIACION
 FUNCION:         afi.ft_bonus_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tbonus'
 AUTOR:          (admin)
 FECHA:            23-11-2021 20:03:57
 COMENTARIOS:    
***************************************************************************
 HISTORIAL DE MODIFICACIONES:
#ISSUE                FECHA                AUTOR                DESCRIPCION
 #0                23-11-2021 20:03:57    admin             Creacion    
 #
 ***************************************************************************/

DECLARE

    v_nro_requerimiento        INTEGER;
    v_parametros               RECORD;
    v_id_requerimiento         INTEGER;
    v_resp                     VARCHAR;
    v_nombre_funcion           TEXT;
    v_mensaje_error            TEXT;
    v_id_bonus    INTEGER;
                
BEGIN

    v_nombre_funcion = 'afi.ft_bonus_ime';
    v_parametros = pxp.f_get_record(p_tabla);

    /*********************************    
     #TRANSACCION:  'AFI_BON_INS'
     #DESCRIPCION:    Insercion de registros
     #AUTOR:        admin    
     #FECHA:        23-11-2021 20:03:57
    ***********************************/

    IF (p_transaccion='AFI_BON_INS') THEN
                    
        BEGIN
            --Sentencia de la insercion
            INSERT INTO afi.tbonus(
            estado_reg,
            tipo,
            monto,
            id_patrocinador,
            id_afiliado,
            estado_pago,
            id_periodo,
            id_usuario_reg,
            fecha_reg,
            id_usuario_ai,
            usuario_ai,
            id_usuario_mod,
            fecha_mod
              ) VALUES (
            'activo',
            v_parametros.tipo,
            v_parametros.monto,
            v_parametros.id_patrocinador,
            v_parametros.id_afiliado,
            v_parametros.estado_pago,
            v_parametros.id_periodo,
            p_id_usuario,
            now(),
            v_parametros._id_usuario_ai,
            v_parametros._nombre_usuario_ai,
            null,
            null            
            ) RETURNING id_bonus into v_id_bonus;
            
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Bonus almacenado(a) con exito (id_bonus'||v_id_bonus||')'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_bonus',v_id_bonus::varchar);

            --Devuelve la respuesta
            RETURN v_resp;

        END;

    /*********************************    
     #TRANSACCION:  'AFI_BON_MOD'
     #DESCRIPCION:    Modificacion de registros
     #AUTOR:        admin    
     #FECHA:        23-11-2021 20:03:57
    ***********************************/

    ELSIF (p_transaccion='AFI_BON_MOD') THEN

        BEGIN
            --Sentencia de la modificacion
            UPDATE afi.tbonus SET
            tipo = v_parametros.tipo,
            monto = v_parametros.monto,
            id_patrocinador = v_parametros.id_patrocinador,
            id_afiliado = v_parametros.id_afiliado,
            estado_pago = v_parametros.estado_pago,
            id_periodo = v_parametros.id_periodo,
            id_usuario_mod = p_id_usuario,
            fecha_mod = now(),
            id_usuario_ai = v_parametros._id_usuario_ai,
            usuario_ai = v_parametros._nombre_usuario_ai
            WHERE id_bonus=v_parametros.id_bonus;
               
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Bonus modificado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_bonus',v_parametros.id_bonus::varchar);
               
            --Devuelve la respuesta
            RETURN v_resp;
            
        END;

    /*********************************    
     #TRANSACCION:  'AFI_BON_ELI'
     #DESCRIPCION:    Eliminacion de registros
     #AUTOR:        admin    
     #FECHA:        23-11-2021 20:03:57
    ***********************************/

    ELSIF (p_transaccion='AFI_BON_ELI') THEN

        BEGIN
            --Sentencia de la eliminacion
            DELETE FROM afi.tbonus
            WHERE id_bonus=v_parametros.id_bonus;
               
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Bonus eliminado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_bonus',v_parametros.id_bonus::varchar);
              
            --Devuelve la respuesta
            RETURN v_resp;

        END;

    /*********************************
     #TRANSACCION:  'AFI_BON_PAG'
     #DESCRIPCION:    Pagar Bonus
     #AUTOR:        admin
     #FECHA:        23-11-2021 20:03:57
    ***********************************/

    ELSIF (p_transaccion='AFI_BON_PAG') THEN

        BEGIN
            --Sentencia de la eliminacion
            update afi.tbonus set estado_pago = 'pagado'
            WHERE id_bonus=v_parametros.id_bonus;

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Bonus pagado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_bonus',v_parametros.id_bonus::varchar);

            --Devuelve la respuesta
            RETURN v_resp;

        END;
         
    ELSE
     
        RAISE EXCEPTION 'Transaccion inexistente: %',p_transaccion;

    END IF;

EXCEPTION
                
    WHEN OTHERS THEN
        v_resp='';
        v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
        v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
        v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
        raise exception '%',v_resp;
                        
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "afi"."ft_bonus_ime"(integer, integer, character varying, character varying) OWNER TO postgres;
