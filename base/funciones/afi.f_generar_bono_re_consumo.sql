CREATE OR REPLACE FUNCTION afi.f_generar_bono_re_consumo(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE

    p_id_usuario                       integer DEFAULT p_params ->> 'id_usuario';
    p_id_usuario_ai                    varchar DEFAULT p_params ->> '_id_usuario_ai';
    p_id_periodo                       integer DEFAULT p_params ->> 'id_periodo';
    p_nombre_usuario_ai                varchar DEFAULT p_params ->> '_nombre_usuario_ai';
    v_resp                             json;
    v_monto_test                             numeric;
    v_nombre_funcion                   TEXT;
    v_puntos_item                      RECORD;
    v_record_reconsumo                 RECORD;
    v_pagar_por_paquete                numeric(10, 2);
    v_monto_multiplicar                NUMERIC;
    v_monto_dividir                NUMERIC;
    v_validado_para_recibir_patrocinio varchar;
    v_afiliado                         record;
    v_pv_propio                     numeric;
    v_id_bonus_config integer;
    v_count integer;
    v_bonus varchar;
    v_count_veces_por_afiliado integer;
    v_id_afi_patro_recibe_bonus integer;


BEGIN
    v_nombre_funcion = 'afi.f_generar_bono_re_consumo';
    RAISE NOTICE '%', p_id_periodo;

    SELECT id_bonus_config
    into v_id_bonus_config
    from afi.tbonus_config
    where bonus = 'BONUS_RECONSUMO';


    SELECT count(*)
    into v_count
    FROM afi.tbonus
    where id_periodo = p_id_periodo and id_bonus_config = v_id_bonus_config;

    IF v_count = 0 then
        FOR v_record_reconsumo IN (
            WITH t_afiliados_productos as (
                select ta.id_afiliado, sum(tvd.cantidad_producto) cantidad_productos
                from vent.tventa_detalle tvd
                         inner join vent.tventa tv on tv.id_venta = tvd.id_venta
                         inner join vent.tcliente tc on tc.id_cliente = tv.id_cliente
                         inner join afi.tafiliado ta on ta.id_persona = tc.id_persona
                where tv.id_periodo = p_id_periodo and tvd.id_item is not null
                GROUP BY ta.id_afiliado
            ),t_afiliado_bono_multiplicador as (
                select id_afiliado, case WHEN cantidad_productos >= 7 then 5 else 4 end as bono_multiplicador
                from t_afiliados_productos
                where cantidad_productos >= 5
            )select ta.id_afiliado,ta.codigo,tap.cantidad_productos, tabm.bono_multiplicador, (tap.cantidad_productos * tabm.bono_multiplicador) as bono_reconsumo
            from t_afiliados_productos tap
                     inner join t_afiliado_bono_multiplicador tabm on tabm.id_afiliado = tap.id_afiliado
                     inner join afi.tafiliado ta on ta.id_afiliado = tap.id_afiliado
        ) LOOP
                RAISE NOTICE '%', v_record_reconsumo;


                INSERT INTO afi.tbonus (estado_pago,
                                        tipo,
                                        estado_reg,
                                        id_afiliado,
                                        monto,
                                        id_patrocinador,
                                        id_usuario_reg,
                                        usuario_ai,
                                        fecha_reg,
                                        id_usuario_ai,
                                        fecha_mod,
                                        id_usuario_mod,
                                        id_periodo,
                                        id_bonus_config,
                                        id_venta
                )
                VALUES ('pagado',
                        'BONUS_RECONSUMO',
                        'activo',
                        v_record_reconsumo.id_afiliado,
                        v_record_reconsumo.bono_reconsumo,
                        v_record_reconsumo.id_afiliado,--recibe el mismo afiliado
                        1,
                        'NULL',
                        now(),
                        NULL,
                        NULL,
                        NULL,
                        p_id_periodo,
                        v_id_bonus_config,
                        NULL
                       );


            END LOOP;

        else
        RAISE notice '%', 'ESTE AFILIADO YA TIENE CALCULADO SU BONUS RE CONSUMO EN ESTE PERIODO';
    END IF;





    SELECT json_strip_nulls(json_build_object(
            'success', TRUE
        ))
    INTO v_resp;
    RETURN v_resp;


EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
            ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;
        RETURN v_resp;


END;
$BODY$
    LANGUAGE plpgsql VOLATILE;