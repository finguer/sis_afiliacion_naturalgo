CREATE OR REPLACE FUNCTION afi.ft_oficina_ime(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
  $BODY$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_afi_patro_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tafi_patro'
 AUTOR: 		 (admin)
 FECHA:	        04-04-2015 15:40:52
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_afi_patro	integer;

  v_record_afi RECORD;

BEGIN

    v_nombre_funcion = 'afi.ft_oficina_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'OFI_IN_SEL'
 	#DESCRIPCION:	verificacion de datos
 	#AUTOR:		admin
 	#FECHA:		14-11-2015 15:40:52
	***********************************/

	if(p_transaccion='OFI_IN_SEL')then

        begin





          IF EXISTS (select 0
                     from afi.tafiliado afi
                       inner join segu.tpersona per on per.id_persona = afi.id_persona
                     where per.ci = v_parametros.ci and afi.codigo = v_parametros.codigo)
          THEN

             select per.nombre,per.apellido_paterno,per.apellido_materno,per.ci,afi.codigo
             into v_record_afi
            from afi.tafiliado afi
              inner join segu.tpersona per on per.id_persona = afi.id_persona
            where per.ci = v_parametros.ci and afi.codigo = v_parametros.codigo;


            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','mensaje de ingreso'||v_record_afi||')');
            v_resp = pxp.f_agrega_clave(v_resp,'datos','nombre='||v_record_afi.nombre||'&ci='||v_record_afi.ci||'&codigo='||v_record_afi.codigo::varchar);


            return v_resp;

            ELSE

              v_resp = pxp.f_agrega_clave(v_resp,'mensaje','mensaje de ingreso incorrecto');
              v_resp = pxp.f_agrega_clave(v_resp,'datos','mal');


              return v_resp;
            --stuff here
          END IF;




		end;


  /*********************************
 #TRANSACCION:  'OFI_DATO_AF'
 #DESCRIPCION:	VER DATOS DEL AFILIADO EN OFI
 #AUTOR:		admin
 #FECHA:		14-11-2015 15:40:52
***********************************/

  elsif(p_transaccion='OFI_DATO_AF')then

    begin
      --Sentencia de la modificacion

      if v_parametros.codigo = 5910001
        THEN

          select
            afil.direccion_principal,
            afil.lugar_nacimiento,
            afil.estado_reg,
            afil.fecha_nacimiento,
            PERSON.nombre_completo2 as desc_person,
            PERSON.ci,
            afil.codigo as codigo,
            afil.monto_registro,
            afil.modalidad_de_registro,
            afil.colocacion,
            PERSON.celular1,
            PERSON.celular2,
            PERSON.telefono1,
            PERSON.telefono2
          into v_record_afi
          from afi.tafiliado afil
            inner join segu.tusuario usu1 on usu1.id_usuario = afil.id_usuario_reg
            INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
            left join segu.tusuario usu2 on usu2.id_usuario = afil.id_usuario_mod
          WHERE afil.codigo = v_parametros.codigo and PERSON.ci = v_parametros.ci;


        ELSE

          select
            afil.direccion_principal,
            afil.lugar_nacimiento,
            afil.estado_reg,
            afil.fecha_nacimiento,
            PERSON.nombre_completo2 as desc_person,
            PERSONPA.nombre_completo2 as desc_person_patro,
            PERSONCO.nombre_completo2 as desc_person_colo,
            PERSON.ci,

            af.direccion_principal as desc_patrocinador,
            af.codigo as codigo_patrocinador,
            afco.codigo as codigo_colocacion,
            afil.codigo as codigo,
            afil.monto_registro,
            afil.modalidad_de_registro,
            afil.colocacion,
            PERSON.celular1,
            PERSON.celular2,
            PERSON.telefono1,
            PERSON.telefono2
          INTO v_record_afi
          from afi.tafiliado afil
            inner join segu.tusuario usu1 on usu1.id_usuario = afil.id_usuario_reg
            INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona

            LEFT join  afi.tafiliado af on af.id_afiliado = afil.id_patrocinador

            LEFT join  afi.tafi_patro afipatro on afipatro.id_afiliado = afil.id_afiliado
            LEFT join  afi.tafiliado afco on afco.id_afiliado = afipatro.id_colocacion

            INNER JOIN segu.vpersona PERSONPA on PERSONPA.id_persona = af.id_persona
            INNER JOIN segu.vpersona PERSONCO on PERSONCO.id_persona = afco.id_persona

            left join segu.tusuario usu2 on usu2.id_usuario = afil.id_usuario_mod
          WHERE afil.codigo = v_parametros.codigo and PERSON.ci = v_parametros.ci;


        END IF ;

      RAISE EXCEPTION '%',JSON(v_record_afi);


      --Definicion de la respuesta
      v_resp = pxp.f_agrega_clave(v_resp,'mensaje','mensaje de ingreso'||v_record_afi||')');
      v_resp = pxp.f_agrega_clave(v_resp,'datos','nombre='||v_record_afi.nombre||'&ci='||v_record_afi.ci||'&codigo='||v_record_afi.codigo::varchar);


      return v_resp;



    end;




	else

    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION

	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;
