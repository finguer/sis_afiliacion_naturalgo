CREATE OR REPLACE FUNCTION "afi"."ft_afiliado_pago_periodo_sel"(	
				p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$
/**************************************************************************
 SISTEMA:		afiliacion
 FUNCION: 		afi.ft_afiliado_pago_periodo_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'afi.tafiliado_pago_periodo'
 AUTOR: 		 (admin)
 FECHA:	        02-04-2016 16:22:34
 COMENTARIOS:	
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:	
 AUTOR:			
 FECHA:		
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;
	v_primera_gen VARCHAR;
	v_segunda_gen VARCHAR;
	v_tercera_gen VARCHAR;

  v_primera_gen_record RECORD;
  v_segunda_gen_record RECORD;
  v_tercera_gen_record RECORD;
	v_id_afiliado INTEGER;
	v_id_periodo INTEGER;
	v_id_periodo_siguiente INTEGER;
	v_where_date varchar;

    v_rec                        RECORD;
    v_length integer;
    v_footer_query varchar;
BEGIN

	v_nombre_funcion = 'afi.ft_afiliado_pago_periodo_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************    
 	#TRANSACCION:  'AFI_afpape_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin	
 	#FECHA:		02-04-2016 16:22:34
	***********************************/

	if(p_transaccion='AFI_afpape_SEL')then
     				
    	begin

				--get periodo anterior para poder obtener el rango anterior y el siguiente

				v_id_periodo_siguiente := afi.f_get_id_periodo_siguiente(v_parametros.id_periodo::INTEGER);
                v_footer_query:=' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

    		--Sentencia de la consulta
			v_consulta:='WITH t_paquetes_vendidos as (
    select tpa.id_afiliado,count(*) as total, tsi.nombre as paquete,tsi.id_set_item
    from afi.tpuntos_afiliado tpa
             inner join vent.tset_item tsi on tsi.id_set_item = tpa.id_set_item
    where tpa.id_periodo = '||v_parametros.id_periodo::INTEGER||'
    GROUP BY tpa.id_afiliado,tsi.id_set_item
)
,t_sel as (select
									afpape.id_afiliado_pago_periodo,
									afpape.id_afiliado,
									afpape.pago_bonus_registro,
									afpape.afiliados_registrados,
									afpape.habilitado,
									afpape.pago_de_red,
									afpape.estado,
									afpape.cv_propio,
									afpape.id_periodo,
									afpape.pago_total,
									afpape.descripcion_pago_red,
									afpape.pv_propio,
									afpape.estado_reg,
									afpape.id_usuario_ai,
									afpape.fecha_reg,
									afpape.usuario_ai,
									afpape.id_usuario_reg,
									afpape.fecha_mod,
									afpape.id_usuario_mod,
									usu1.cuenta as usr_reg,
									usu2.cuenta as usr_mod,
									afil.codigo,
									person.nombre_completo2,
									per.periodo,
									ges.gestion,

									afpape.primera_gen_cv,
                  afpape.primera_gen_pv,

                  afpape.segunda_gen_cv,
                  afpape.segunda_gen_pv,

                  afpape.tercera_gen_cv,
                  afpape.tercera_gen_pv,

                  afpape.cuarta_gen_cv,
                  afpape.cuarta_gen_pv,

                  afpape.quinta_gen_cv,
                  afpape.quinta_gen_pv,

                  afpape.monto_multiplicar,
                  afpape.tipo_moneda,
                  afpape.afiliados_activos,
                  afpape.niveles_cobrados,
                  afpape.sube_nivel,
                  afpape.pv_acumulado,
                  array_to_string(afpape.porcentajes_pagados, '','', ''*'') as porcentajes_pagados,
                  rango_actual.nombre as rango_actual,
                  rango_siguiente.nombre as rango_siguiente,
                  tpa.descripcion as premio,
tpv.total::varchar as kit_negocios
								from afi.tafiliado_pago_periodo afpape
									inner join segu.tusuario usu1 on usu1.id_usuario = afpape.id_usuario_reg
									left join segu.tusuario usu2 on usu2.id_usuario = afpape.id_usuario_mod
								inner join afi.tafiliado afil on afil.id_afiliado = afpape.id_afiliado
								inner join segu.vpersona person on person.id_persona = afil.id_persona
								inner join param.tperiodo per on per.id_periodo = afpape.id_periodo
								inner join param.tgestion ges on ges.id_gestion = per.id_gestion

								inner join afi.trango_afiliado ran_afi_actual on ran_afi_actual.id_afiliado = afil.id_afiliado and ran_afi_actual.id_periodo = per.id_periodo
				        inner join afi.trango rango_actual on rango_actual.id_rango = ran_afi_actual.id_rango

								inner join afi.trango_afiliado ran_afi_siguiente on ran_afi_siguiente.id_afiliado = afil.id_afiliado and ran_afi_siguiente.id_periodo = '||v_id_periodo_siguiente||'
				        inner join afi.trango rango_siguiente on rango_siguiente.id_rango = ran_afi_siguiente.id_rango
               left join t_paquetes_vendidos tpv on tpv.id_afiliado = afpape.id_afiliado and tpv.id_set_item= 56

				        left join afi.tpremio_afiliado tpa on tpa.id_afiliado = afpape.id_afiliado and tpa.id_periodo = afpape.id_periodo

				        where  '||v_parametros.filtro||' ' ||v_footer_query||' ) select * from t_sel ';



			--Definicion de la respuesta
			--v_consulta:=v_consulta||v_parametros.filtro;
			--v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

			--Devuelve la respuesta
			return v_consulta;
						
		end;

	/*********************************    
 	#TRANSACCION:  'AFI_afpape_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin	
 	#FECHA:		02-04-2016 16:22:34
	***********************************/

	elsif(p_transaccion='AFI_afpape_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(id_afiliado_pago_periodo)
					    from afi.tafiliado_pago_periodo afpape
					    inner join segu.tusuario usu1 on usu1.id_usuario = afpape.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = afpape.id_usuario_mod
						inner join afi.tafiliado afil on afil.id_afiliado = afpape.id_afiliado
								inner join segu.vpersona person on person.id_persona = afil.id_persona
								inner join param.tperiodo per on per.id_periodo = afpape.id_periodo
								inner join param.tgestion ges on ges.id_gestion = per.id_gestion
					    where ';
			
			--Definicion de la respuesta		    
			v_consulta:=v_consulta||v_parametros.filtro;

			--Devuelve la respuesta
			return v_consulta;

		end;


    /*********************************
   #TRANSACCION:  'AFI_AFPAPE_GEN'
   #DESCRIPCION:	Consulta de datos
   #AUTOR:		admin
   #FECHA:		02-04-2016 16:22:34
  ***********************************/

  elsif(p_transaccion='AFI_AFPAPE_GEN')then

      begin
        --Sentencia de la consulta




				IF pxp.f_existe_parametro(p_tabla,'id_afiliado') THEN

					v_id_afiliado := v_parametros.id_afiliado;

					ELSE
						SELECT afi.id_afiliado
						INTO v_id_afiliado
						FROM afi.tafiliado afi
							INNER JOIN segu.tusuario usu on usu.id_persona = afi.id_persona
						where usu.id_usuario = p_id_usuario;

				END IF;
        IF v_parametros.id_periodo is null then
            v_rec = param.f_get_periodo_gestion(to_char(now(), 'YYYY-mm-dd')::DATE);
            v_id_periodo:= v_rec.po_id_periodo::INTEGER;
        else
            v_id_periodo:= v_parametros.id_periodo;
        END IF;


        select tr.niveles_a_sumar_para_pv
        INTO v_length
        from afi.trango_afiliado tra
        inner join afi.trango tr on tr.id_rango = tra.id_rango
        where tra.id_afiliado = v_id_afiliado and tra.id_periodo = v_id_periodo;

       /* select coalesce(array_length(porcentajes_pagados, 1), 0) as length
        into v_length
        from  afi.tafiliado_pago_periodo tapp
        where tapp.id_afiliado = v_id_afiliado and tapp.id_periodo = v_id_periodo;*/


        v_consulta = 'WITH RECURSIVE t(nivel, id_afiliado) AS (
				SELECT
					1 AS nivel,
					afil.id_afiliado
				FROM afi.tafiliado afil
				WHERE id_patrocinador = '|| v_id_afiliado ||'

				UNION ALL
				SELECT
					nivel + 1,
					afil.id_afiliado
				FROM afi.tafiliado afil
					INNER JOIN t t ON t.id_afiliado = afil.id_patrocinador
			)
			SELECT
				t.nivel::integer,
				--t.id_afiliado::integer,
				afil.codigo::varchar,
				patro.codigo::varchar as codigo_patrocinador,
				per.nombre_completo2,
				per_patro.nombre_completo2 as nombre_completo2_patrocinador,
				sum(puntos.cv) AS sum_cv,
				sum(puntos.pv) AS sum_pv

			FROM t t
				INNER JOIN afi.tafiliado afil ON afil.id_afiliado = t.id_afiliado
				INNER JOIN afi.tafiliado patro ON patro.id_afiliado = afil.id_patrocinador
				INNER JOIN segu.vpersona2 per ON per.id_persona = afil.id_persona
				INNER JOIN segu.vpersona2 per_patro ON per_patro.id_persona = patro.id_persona

				INNER JOIN afi.tpuntos_afiliado puntos ON puntos.id_afiliado = t.id_afiliado
			WHERE t.nivel <= '||v_length||' and puntos.id_periodo = '||v_id_periodo||'

			GROUP BY t.nivel,
				--t.id_afiliado,
				afil.codigo,
				patro.codigo,
				per.nombre_completo2,
				per_patro.nombre_completo2
			ORDER BY nivel ASC';


       --Devuelve la respuesta
        return v_consulta;


      end;


    /*********************************
   #TRANSACCION:  'AFI_AFPAPE_PAGO'
   #DESCRIPCION:	Consulta de datos PARA OBTENER EL REPORTE DE PAGO
   #AUTOR:		admin
   #FECHA:		02-04-2016 16:22:34
  ***********************************/

  elsif(p_transaccion='AFI_AFPAPE_PAGO')then

      begin
        --Sentencia de la consulta

        IF v_parametros.fecha is not null and v_parametros.fecha_fin is not null then
            v_where_date:= ' and app.fecha_pago::date BETWEEN '''||v_parametros.fecha||'''::date and '''||v_parametros.fecha_fin||'''::date ';
            ELSE
            v_where_date:= '';
        END IF;

        v_consulta = '
									
WITH pago_periodo AS
(
    SELECT app.id_afiliado,app.id_periodo,app.habilitado,app.pago_bonus_registro,app.pago_total,app.pago_de_red,app.estado
    FROM afi.tafiliado_pago_periodo app
    INNER JOIN afi.tafiliado afil on afil.id_afiliado = app.id_afiliado
		INNER JOIN su.tpunto_venta pv on pv.id_punto_venta = afil.id_punto_venta
		INNER JOIN su.tsucursal suc on suc.id_sucursal = pv.id_sucursal
    where suc.id_sucursal = '||v_parametros.id_sucursal||' and app.habilitado = ''si''
    and app.id_periodo = '||v_parametros.id_periodo||' '||v_where_date||'

), totales_pagados AS
(
    SELECT sum(pp.pago_bonus_registro) sum_pago_bonus_registro,
           sum(pp.pago_de_red) sum_pago_de_red,
          sum(pp.pago_total) sum_pago_total
    FROM pago_periodo pp
  WHERE pp.estado = ''pagado''

), totales_no_pagados AS
(
    SELECT sum(pp.pago_bonus_registro) sum_pago_bonus_registro,
           sum(pp.pago_de_red) sum_pago_de_red,
          sum(pp.pago_total) sum_pago_total
    FROM pago_periodo pp
  WHERE pp.estado = ''registrado no pagado''
), totales AS
(
    SELECT sum(pp.pago_bonus_registro) sum_pago_bonus_registro,
           sum(pp.pago_de_red) sum_pago_de_red,
          sum(pp.pago_total) sum_pago_total
    FROM pago_periodo pp
)
SELECT TO_JSON(ARRAY_TO_JSON(ARRAY_agg(jsonData)) :: TEXT) #>> ''{}'' as json
FROM
  (
    SELECT
      (
        SELECT ROW_TO_JSON(f) as fechas
        FROM (
              select fecha_ini,fecha_fin
              from param.tperiodo where id_periodo = '||v_parametros.id_periodo||'
             ) f
      ),
      (
        SELECT ROW_TO_JSON(suc) as sucursal
        FROM (
               SELECT s.nombre as sucursal,param.f_literal_periodo('||v_parametros.id_periodo||')
               FROM su.tsucursal s
               WHERE s.id_sucursal = '||v_parametros.id_sucursal||'
             ) suc
      ),

      (
        SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(detalle))) as pago_periodo
        FROM (
                SELECT afil.codigo,suc.id_sucursal,suc.nombre as sucursal,pv.nombre punto_de_venta,pp.pago_bonus_registro,pp.pago_de_red,pp.pago_total
                FROM pago_periodo pp
                INNER JOIN afi.tafiliado afil on afil.id_afiliado = pp.id_afiliado
                INNER JOIN su.tpunto_venta pv on pv.id_punto_venta = afil.id_punto_venta
                INNER JOIN su.tsucursal suc on suc.id_sucursal = pv.id_sucursal
                WHERE suc.id_sucursal = '||v_parametros.id_sucursal||'
             ) detalle
      ),
      (
        SELECT ROW_TO_JSON(pp) as totales_pagados
        FROM (
               SELECT * FROM totales_pagados
             ) pp
      ),
      (
        SELECT ROW_TO_JSON(pp) as totales_no_pagados
        FROM (
               SELECT * FROM totales_no_pagados
             ) pp
      ),
      (
        SELECT ROW_TO_JSON(pp) as totales
        FROM (
               SELECT * FROM totales
             ) pp
      )
  ) jsonData
        ';


       --Devuelve la respuesta
        return v_consulta;


      end;


	else
					     
		raise exception 'Transaccion inexistente';
					         
	end if;
					
EXCEPTION
					
	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "afi"."ft_afiliado_pago_periodo_sel"(integer, integer, character varying, character varying) OWNER TO postgres;
