CREATE OR REPLACE FUNCTION afi.ft_codificacion_sel(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_codificacion_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'afi.tcodificacion'
 AUTOR: 		 (admin)
 FECHA:	        04-04-2015 17:10:04
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;

BEGIN

	v_nombre_funcion = 'afi.ft_codificacion_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'AFI_CODI_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin
 	#FECHA:		04-04-2015 17:10:04
	***********************************/

	if(p_transaccion='AFI_CODI_SEL')then

    	begin
    		--Sentencia de la consulta
			v_consulta:='select
						codi.id_codificacion,
						codi.numero_inicio,
						codi.estado_reg,
						codi.numero_siguiente,
						codi.numero_fin,
						codi.id_usuario_reg,
						codi.fecha_reg,
						codi.usuario_ai,
						codi.id_usuario_ai,
						codi.fecha_mod,
						codi.id_usuario_mod,
						usu1.cuenta as usr_reg,
						usu2.cuenta as usr_mod
						from afi.tcodificacion codi
						inner join segu.tusuario usu1 on usu1.id_usuario = codi.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = codi.id_usuario_mod
				        where  ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;
			v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

			--Devuelve la respuesta
			return v_consulta;

		end;

	/*********************************
 	#TRANSACCION:  'AFI_CODI_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin
 	#FECHA:		04-04-2015 17:10:04
	***********************************/

	elsif(p_transaccion='AFI_CODI_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(id_codificacion)
					    from afi.tcodificacion codi
					    inner join segu.tusuario usu1 on usu1.id_usuario = codi.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = codi.id_usuario_mod
					    where ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;

			--Devuelve la respuesta
			return v_consulta;

		end;

	else

		raise exception 'Transaccion inexistente';

	end if;

EXCEPTION

	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
