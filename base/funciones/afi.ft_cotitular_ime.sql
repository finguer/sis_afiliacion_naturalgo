CREATE OR REPLACE FUNCTION afi.ft_cotitular_ime(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_cotitular_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tcotitular'
 AUTOR: 		 (admin)
 FECHA:	        02-04-2015 01:19:24
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_cotitular	integer;

BEGIN

    v_nombre_funcion = 'afi.ft_cotitular_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'AFI_TITU_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin
 	#FECHA:		02-04-2015 01:19:24
	***********************************/

	if(p_transaccion='AFI_TITU_INS')then

        begin


          IF EXISTS (SELECT 0 FROM afi.tafiliado where id_persona = v_parametros.id_persona )
				THEN
          RAISE EXCEPTION '%error','esta persona es un afiliado no puede ser cotitular';


          ELSE


--Sentencia de la insercion
      insert into afi.tcotitular(
        nombre,
        id_afiliado,
        domicilio,
        apellido1,
        apellido2,
        relacion,
        estado_reg,
        telefono,
        id_usuario_ai,
        id_usuario_reg,
        usuario_ai,
        fecha_reg,
        fecha_mod,
        id_usuario_mod,
        id_persona
      ) values(
        v_parametros.nombre,
        v_parametros.id_afiliado,
        v_parametros.domicilio,
        v_parametros.apellido1,
        v_parametros.apellido2,
        v_parametros.relacion,
        'activo',
        v_parametros.telefono,
        v_parametros._id_usuario_ai,
        p_id_usuario,
        v_parametros._nombre_usuario_ai,
        now(),
        null,
        null,
        v_parametros.id_persona



      )RETURNING id_cotitular into v_id_cotitular;




        END IF;



			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Co-Titular almacenado(a) con exito (id_cotitular'||v_id_cotitular||')');
            v_resp = pxp.f_agrega_clave(v_resp,'id_cotitular',v_id_cotitular::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'AFI_TITU_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin
 	#FECHA:		02-04-2015 01:19:24
	***********************************/

	elsif(p_transaccion='AFI_TITU_MOD')then

		begin

      IF EXISTS (SELECT 0 FROM afi.tafiliado where id_persona = v_parametros.id_persona )
      THEN
        RAISE EXCEPTION '%error','esta persona es un afiliado no puede ser cotitular';


      ELSE

			--Sentencia de la modificacion
			update afi.tcotitular set
			nombre = v_parametros.nombre,
			id_afiliado = v_parametros.id_afiliado,
			domicilio = v_parametros.domicilio,
			apellido1 = v_parametros.apellido1,
			apellido2 = v_parametros.apellido2,
			relacion = v_parametros.relacion,
			telefono = v_parametros.telefono,
			fecha_mod = now(),
			id_usuario_mod = p_id_usuario,
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai,
				id_persona = v_parametros.id_persona
			where id_cotitular=v_parametros.id_cotitular;

      END IF;

			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Co-Titular modificado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_cotitular',v_parametros.id_cotitular::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'AFI_TITU_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin
 	#FECHA:		02-04-2015 01:19:24
	***********************************/

	elsif(p_transaccion='AFI_TITU_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from afi.tcotitular
            where id_cotitular=v_parametros.id_cotitular;

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Co-Titular eliminado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_cotitular',v_parametros.id_cotitular::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	else

    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION

	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;
