CREATE OR REPLACE FUNCTION "afi"."ft_bonusproductopatrocinador_ime" (	
				p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$

/**************************************************************************
 SISTEMA:		afiliacion
 FUNCION: 		afi.ft_bonusproductopatrocinador_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tbonusproductopatrocinador'
 AUTOR: 		 (admin)
 FECHA:	        06-01-2016 16:58:27
 COMENTARIOS:	
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:	
 AUTOR:			
 FECHA:		
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_bonus_producto_patrocinador	integer;
			    
BEGIN

    v_nombre_funcion = 'afi.ft_bonusproductopatrocinador_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************    
 	#TRANSACCION:  'AFI_BOPROPA_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin	
 	#FECHA:		06-01-2016 16:58:27
	***********************************/

	if(p_transaccion='AFI_BOPROPA_INS')then
					
        begin
        	--Sentencia de la insercion
        	insert into afi.tbonusproductopatrocinador(
			id_afiliado,
			total_cv,
			id_patrocinador,
			cv,
			observaciones,
			cantidad_pro,
			estado_reg,
			id_usuario_ai,
			usuario_ai,
			fecha_reg,
			id_usuario_reg,
			fecha_mod,
			id_usuario_mod
          	) values(
			v_parametros.id_afiliado,
			v_parametros.total_cv,
			v_parametros.id_patrocinador,
			v_parametros.cv,
			v_parametros.observaciones,
			v_parametros.cantidad_pro,
			'activo',
			v_parametros._id_usuario_ai,
			v_parametros._nombre_usuario_ai,
			now(),
			p_id_usuario,
			null,
			null
							
			
			
			)RETURNING id_bonus_producto_patrocinador into v_id_bonus_producto_patrocinador;
			
			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','bonusProductoPatrocinador almacenado(a) con exito (id_bonus_producto_patrocinador'||v_id_bonus_producto_patrocinador||')'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_bonus_producto_patrocinador',v_id_bonus_producto_patrocinador::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************    
 	#TRANSACCION:  'AFI_BOPROPA_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin	
 	#FECHA:		06-01-2016 16:58:27
	***********************************/

	elsif(p_transaccion='AFI_BOPROPA_MOD')then

		begin
			--Sentencia de la modificacion
			update afi.tbonusproductopatrocinador set
			id_afiliado = v_parametros.id_afiliado,
			total_cv = v_parametros.total_cv,
			id_patrocinador = v_parametros.id_patrocinador,
			cv = v_parametros.cv,
			observaciones = v_parametros.observaciones,
			cantidad_pro = v_parametros.cantidad_pro,
			fecha_mod = now(),
			id_usuario_mod = p_id_usuario,
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai
			where id_bonus_producto_patrocinador=v_parametros.id_bonus_producto_patrocinador;
               
			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','bonusProductoPatrocinador modificado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_bonus_producto_patrocinador',v_parametros.id_bonus_producto_patrocinador::varchar);
               
            --Devuelve la respuesta
            return v_resp;
            
		end;

	/*********************************    
 	#TRANSACCION:  'AFI_BOPROPA_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin	
 	#FECHA:		06-01-2016 16:58:27
	***********************************/

	elsif(p_transaccion='AFI_BOPROPA_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from afi.tbonusproductopatrocinador
            where id_bonus_producto_patrocinador=v_parametros.id_bonus_producto_patrocinador;
               
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','bonusProductoPatrocinador eliminado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_bonus_producto_patrocinador',v_parametros.id_bonus_producto_patrocinador::varchar);
              
            --Devuelve la respuesta
            return v_resp;

		end;
         
	else
     
    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION
				
	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;
				        
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "afi"."ft_bonusproductopatrocinador_ime"(integer, integer, character varying, character varying) OWNER TO postgres;
