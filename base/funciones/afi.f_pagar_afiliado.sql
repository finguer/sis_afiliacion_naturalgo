CREATE OR REPLACE FUNCTION afi.f_pagar_afiliado(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE


    v_nombre_funcion      varchar;
    p_id_afiliado_pago_periodo         integer DEFAULT p_params ->> 'id_afiliado_pago_periodo';
    p_id_usuario          integer DEFAULT p_params ->> 'userId';
    v_json_params_to_send json;
    v_res                 json;
    v_resp                json;
    v_record_afiliado     record;
    v_record_patrocinador record;
    v_rec_periodo         record;
    v_count_hijos         integer;
    v_record record;
    v_afiliado_pago_periodo json;
    v_activo_pago varchar;


BEGIN


    v_nombre_funcion = 'afi.f_pagar_afiliado';


    SELECT *
    INTO v_record
    FROM afi.tafiliado_pago_periodo
    WHERE id_afiliado_pago_periodo = p_id_afiliado_pago_periodo;

    SELECT *
    INTO v_record_afiliado
    FROM afi.tafiliado
    WHERE id_afiliado = v_record.id_afiliado;

    --vemos si esta activo para pagar mediante el periodo
    IF (SELECT estado FROM afi.tpago_conf WHERE id_periodo = v_record.id_periodo) = 'activo' THEN

        v_activo_pago = 'si';
    ELSE
        v_activo_pago = 'no';

    END IF;

    IF v_activo_pago = 'no' THEN
        RAISE EXCEPTION '%','NO TIENES ACTIVO PARA PAGAR ESTE PERIODO O NO CONFIGURASTE';
    END IF;


    IF v_record.estado = 'registrado no pagado' THEN



        UPDATE afi.tafiliado_pago_periodo
        SET estado         = 'pagado',
            fecha_pago     = now(),
            id_usuario_mod = p_id_usuario,
            fecha_mod      = now()
        WHERE id_afiliado_pago_periodo = p_id_afiliado_pago_periodo;


        UPDATE afi.tbonus SET estado_pago = 'pagado' , id_usuario_mod = p_id_usuario, fecha_mod  = now()
        WHERE id_patrocinador = v_record.id_afiliado
        and id_periodo = v_record.id_periodo;

        update afi.tafiliado_pago_periodo
        set estado_pago_bonus = 'pagado'
        WHERE id_afiliado_pago_periodo = v_record.id_periodo;


    ELSE
        RAISE EXCEPTION '%','ya se encuentra pagado';
    END IF;



    SELECT to_json(tapp)
    INTO v_afiliado_pago_periodo
    FROM afi.tafiliado_pago_periodo tapp
    WHERE tapp.id_afiliado_pago_periodo = p_id_afiliado_pago_periodo;




    RETURN v_afiliado_pago_periodo;


EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
            ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;
        RETURN v_resp;


END ;
$BODY$
    LANGUAGE plpgsql VOLATILE;