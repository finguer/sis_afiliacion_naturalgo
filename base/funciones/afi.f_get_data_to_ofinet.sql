CREATE OR REPLACE FUNCTION afi.f_get_data_to_ofinet(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE


    v_nombre_funcion                         varchar;
    p_id_afiliado                            integer DEFAULT p_params ->> 'id_afiliado';
    p_id_periodo                             integer DEFAULT p_params ->> 'id_periodo';
    v_resp                                   json;
    v_data_json                              json;
    v_estado_pago varchar;
    v_estado varchar;

BEGIN


    v_nombre_funcion = 'afi.f_get_data_to_ofinet';


    select estado
    into v_estado_pago
    from afi.tpago_conf
    where id_periodo = p_id_periodo;


    IF v_estado_pago is null or NOT EXISTS (select 1 from afi.tafiliado_pago_periodo
                                          where id_afiliado = p_id_afiliado and id_periodo = p_id_periodo) then -- si aun no se ha configurado un periodo listo para pagar entonces se volvera a generar el afiliado pago periodo

        select app.estado INTO v_estado from afi.tafiliado_pago_periodo app
        where app.id_periodo = p_id_periodo and app.id_afiliado = p_id_afiliado;

        if v_estado != 'pagado' or v_estado is NULL then

            v_data_json := afi.f_generar_afiliado_pago_periodo(
                    json_build_object(
                            'id_afiliado', p_id_afiliado,
                            'id_periodo', p_id_periodo,
                            'from_gen', 'N'
                        ));


        END IF ;


    END IF;


    RETURN v_data_json;


EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
            ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;


END ;
$BODY$
    LANGUAGE plpgsql VOLATILE;