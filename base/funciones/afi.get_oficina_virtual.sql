CREATE OR REPLACE FUNCTION afi.get_oficina_virtual (
    p_params json
)
    RETURNS json AS
$body$
DECLARE

    v_record record;
    v_nombre_funcion    varchar;
    v_params json;
    v_resp VARCHAR;
    v_resp_data json;
    v_tabla VARCHAR;
   /* v_liqui_json        json;
    v_id_liquidacion_array int[];
    v_json json;
    v_count integer;
    v_ids_factucom varchar;

    v_id_liquidacion integer DEFAULT p_params->>'id_liquidacion';
    v_tipo_tab_liqui varchar DEFAULT p_params->>'tipo_tab_liqui';
    v_filtro_value varchar DEFAULT UPPER(p_params->>'filtro_value');
    v_query_value varchar DEFAULT UPPER(p_params->>'query_value');
*/
BEGIN

    v_nombre_funcion:='afi.get_oficina_virtual';

    v_tabla =  pxp.f_crear_parametro(ARRAY[
                                         '_nombre_usuario_ai',
                                         '_id_usuario_ai',
                                         'tabla_origen',
                                         'estado_reg',
                                         'filtro',
                                         'ordenacion',
                                         'dir_ordenacion',
                                         'cantidad',
                                         'puntero',
                                         'id_periodo'
                                         ],
                                     ARRAY[
                                         '15555',
                                         '15555',
                                         'afi.tafiliado',--'tabla_origen',
                                         'activo'--'estado_reg',
                                         ,' 1=1 '
                                         ,'id_afiliado'
                                         ,'ASC'
                                         ,'1'
                                         ,'0'
                                         ,'107'
                                         ],
                                     ARRAY['varchar',
                                         'int4',
                                         'varchar',
                                         'varchar',
                                         'varchar',
                                         'varchar',
                                         'varchar',
                                         'int4',
                                         'int4',
                                         'int4'
                                         ]
        );


    v_resp = afi.ft_afiliado_sel('1','15555',v_tabla,'AFI_OFINET_SEL');
    /*execute 'select to_json(array_to_json(array_agg(jsonData)) :: text) #>> ''{}'' as json
    FROM ('||v_resp||') jsonData' into v_resp_data;*/
    execute 'select to_json(array_to_json(array_agg(jsonData)) :: text) #>> ''{}'' as json
    FROM (select * from segu.tusuario) jsonData' into v_resp_data;


  return v_resp_data;

    return v_record;

    EXCEPTION

    WHEN OTHERS THEN

       select json_strip_nulls(json_build_object(
                    'mensaje', SQLERRM,
                    'codigo_error', SQLSTATE,
                    'procedimientos', v_nombre_funcion
            ))
        into v_params;
        --raise exception '%',v_resp;
    return v_params;

END;
$body$
    LANGUAGE 'plpgsql'
    VOLATILE
    CALLED ON NULL INPUT
    SECURITY INVOKER
    COST 100;