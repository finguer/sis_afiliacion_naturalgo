CREATE OR REPLACE FUNCTION afi.f_generar_pagos_por_periodo(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE

    v_resp                                   json;
    p_id_afiliado                            integer DEFAULT p_params ->> 'id_afiliado';
    p_id_periodo                             integer DEFAULT p_params ->> 'id_periodo';
    v_nombre_funcion                         varchar;
    r                                        record;

    v_json_params_to_send                    json;

    v_activos_primer_nivel                   integer DEFAULT 0;
    v_niveles_a_cobrar_string                varchar;
    v_niveles_a_cobrar_array                 NUMERIC[];
    v_generaciones_cv                        numeric[];
    v_generaciones_pv                        numeric[];
    v_generaciones_cv_monto                  numeric[];
    v_generaciones_count_afiliados_100pts_pv integer[];
    v_generaciones_count_afiliados_activos   integer[];
    v_bonus_registro numeric;

    v_id_rango integer;
    v_resp_json json;
    v_count numeric DEFAULT  0;


BEGIN


    v_nombre_funcion = 'afi.f_generar_pagos_por_periodo';

    if p_id_periodo is null then

        raise exception '%', 'NECESITAS ENVIAR EL id_periodo';

    end if;


    -- esto es el bono retorno
    update afi.tbonus set id_patrocinador = id_afiliado
    where id_bonus_config = 4 and id_periodo = p_id_periodo;


    FOR r IN (
        SELECT ta.id_afiliado
        FROM afi.tafiliado ta
        INNER JOIN afi.trango_afiliado ran ON ran.id_afiliado = ta.id_afiliado
        WHERE ran.id_periodo = p_id_periodo
          and (case when p_id_afiliado is not null then ta.id_afiliado = p_id_afiliado else 1=1 end)
        -- and ta.id_afiliado not in(61781)
        order by ta.id_afiliado desc
        --limit 10000 offset 30000
        --LIMIT 100
    )
        LOOP
            v_count := v_count + 1;
            v_id_rango:= null;
            v_bonus_registro:= null;
            v_activos_primer_nivel:= null;
            v_generaciones_count_afiliados_activos:= null;
            v_generaciones_count_afiliados_activos:= null;
            v_generaciones_count_afiliados_100pts_pv:= null;
            v_niveles_a_cobrar_string:= null;
            v_niveles_a_cobrar_array:= null;
            v_generaciones_pv:= null;
            v_generaciones_cv:= null;
            v_generaciones_cv_monto:= null;
            v_id_rango:= null;



            SELECT json_strip_nulls(json_build_object(
                    'id_afiliado', r.id_afiliado,
                    'id_periodo', p_id_periodo,
                    'from_gen', 'Y'
                ))
            INTO v_json_params_to_send;

            v_resp_json:= afi.f_generar_afiliado_pago_periodo(v_json_params_to_send);

            RAISE NOTICE '%',v_count;



        END LOOP;



    SELECT json_strip_nulls(json_build_object(
            'success', true
        ))
    INTO v_resp;

    RETURN v_resp;


EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
            ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;


END ;
$BODY$
    LANGUAGE plpgsql VOLATILE;