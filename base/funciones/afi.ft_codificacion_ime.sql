CREATE OR REPLACE FUNCTION afi.ft_codificacion_ime(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_codificacion_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tcodificacion'
 AUTOR: 		 (admin)
 FECHA:	        04-04-2015 17:10:04
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_codificacion	integer;

BEGIN

    v_nombre_funcion = 'afi.ft_codificacion_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'AFI_CODI_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin
 	#FECHA:		04-04-2015 17:10:04
	***********************************/

	if(p_transaccion='AFI_CODI_INS')then

        begin
        	--Sentencia de la insercion
        	insert into afi.tcodificacion(
			numero_inicio,
			estado_reg,
			numero_siguiente,
			numero_fin,
			id_usuario_reg,
			fecha_reg,
			usuario_ai,
			id_usuario_ai,
			fecha_mod,
			id_usuario_mod
          	) values(
			v_parametros.numero_inicio,
			'activo',
			v_parametros.numero_siguiente,
			v_parametros.numero_fin,
			p_id_usuario,
			now(),
			v_parametros._nombre_usuario_ai,
			v_parametros._id_usuario_ai,
			null,
			null



			)RETURNING id_codificacion into v_id_codificacion;

			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Codificacion almacenado(a) con exito (id_codificacion'||v_id_codificacion||')');
            v_resp = pxp.f_agrega_clave(v_resp,'id_codificacion',v_id_codificacion::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'AFI_CODI_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin
 	#FECHA:		04-04-2015 17:10:04
	***********************************/

	elsif(p_transaccion='AFI_CODI_MOD')then

		begin
			--Sentencia de la modificacion
			update afi.tcodificacion set
			numero_inicio = v_parametros.numero_inicio,
			numero_siguiente = v_parametros.numero_siguiente,
			numero_fin = v_parametros.numero_fin,
			fecha_mod = now(),
			id_usuario_mod = p_id_usuario,
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai
			where id_codificacion=v_parametros.id_codificacion;

			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Codificacion modificado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_codificacion',v_parametros.id_codificacion::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'AFI_CODI_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin
 	#FECHA:		04-04-2015 17:10:04
	***********************************/

	elsif(p_transaccion='AFI_CODI_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from afi.tcodificacion
            where id_codificacion=v_parametros.id_codificacion;

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Codificacion eliminado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_codificacion',v_parametros.id_codificacion::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	else

    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION

	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;
