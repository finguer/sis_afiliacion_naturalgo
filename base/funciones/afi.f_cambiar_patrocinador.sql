CREATE OR REPLACE FUNCTION afi.f_cambiar_patrocinador(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE


    v_nombre_funcion      varchar;
    p_id_afiliado         integer DEFAULT p_params ->> 'id_afiliado';
    p_id_patrocinador     integer DEFAULT p_params ->> 'id_patrocinador';
    p_id_usuario          integer DEFAULT p_params ->> 'userId';
    v_json_params_to_send json;
    v_res                 json;
    v_resp                json;
    v_record_afiliado     record;
    v_record_patrocinador record;
    v_rec_periodo         record;
    v_count_hijos         integer;


BEGIN


    v_nombre_funcion = 'afi.f_cambiar_patrocinador';


    SELECT *
    INTO v_record_afiliado
    FROM afi.tafiliado
    WHERE id_afiliado = p_id_afiliado;

    SELECT *
    INTO v_record_patrocinador
    FROM afi.tafiliado
    WHERE id_afiliado = p_id_patrocinador;

    IF v_record_afiliado IS NULL THEN
        RAISE EXCEPTION '%', 'ocurrio extrano error no existe el afiliado seleccionado';
    END IF;
    IF v_record_patrocinador IS NULL THEN
        RAISE EXCEPTION '%', 'ocurrio extrano error no existe el patrocinador seleccionado';
    END IF;


    --verificamos que el afiliado no tenga aun hijos
    SELECT count(*)
    INTO v_count_hijos
    FROM afi.tafiliado
    WHERE id_patrocinador = v_record_afiliado.id_afiliado;
    IF v_count_hijos > 0 THEN
        RAISE EXCEPTION '%','NO PUEDES CAMBIAR DE PATROCINADOR POR QUE ESTE AFILIADO YA TIENE HIJOS Y LOS PUNTOS PATROCINADOR SE PODRIA ARRUINAR';
    END IF;

    IF v_record_afiliado.id_patrocinador = v_record_patrocinador.id_patrocinador THEN
        RAISE EXCEPTION '%', 'el patrocinador que quieres cambiar ya es su patrocinador';
    END IF;

    v_rec_periodo = param.f_get_periodo_gestion(to_char(v_record_afiliado.fecha_reg::date, 'YYYY-mm-dd')::DATE);


    UPDATE afi.tafiliado
    SET id_patrocinador = p_id_patrocinador
    WHERE id_afiliado = p_id_afiliado;


    --necesitamos eliminar los registro de patrocinio de este afiliado que genero hacia arriba
    DELETE
    FROM afi.tbonus
    WHERE id_afiliado_from = v_record_afiliado.id_afiliado
      AND id_periodo = v_rec_periodo.po_id_periodo::INTEGER;


    SELECT json_strip_nulls(json_build_object(
            'id_set_item', v_record_afiliado.id_set_item::integer,
            'id_afiliado', v_record_afiliado.id_afiliado,
            'id_usuario', p_id_usuario,
            '_nombre_usuario_ai', 'NULL',
            '_id_usuario_ai', '',
            'po_id_periodo', v_rec_periodo.po_id_periodo::integer
        ))
    INTO v_json_params_to_send;


    v_res := afi.f_insertar_bonus_afiliado(v_json_params_to_send::json);


    SELECT json_strip_nulls(json_build_object(
            'res', v_res
        ))
    INTO v_resp;
    RETURN v_resp;


EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
            ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;
        RETURN v_resp;


END ;
$BODY$
    LANGUAGE plpgsql VOLATILE;