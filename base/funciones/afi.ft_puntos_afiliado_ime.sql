CREATE OR REPLACE FUNCTION "afi"."ft_puntos_afiliado_ime" (	
				p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$

/**************************************************************************
 SISTEMA:		afiliacion
 FUNCION: 		afi.ft_puntos_afiliado_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tpuntos_afiliado'
 AUTOR: 		 (admin)
 FECHA:	        19-12-2015 00:21:31
 COMENTARIOS:	
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:	
 AUTOR:			
 FECHA:		
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_puntos_afiliado	integer;
			    
BEGIN

    v_nombre_funcion = 'afi.ft_puntos_afiliado_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************    
 	#TRANSACCION:  'AFI_PUAFIL_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin	
 	#FECHA:		19-12-2015 00:21:31
	***********************************/

	if(p_transaccion='AFI_PUAFIL_INS')then
					
        begin
        	--Sentencia de la insercion
        	insert into afi.tpuntos_afiliado(
			estado_reg,
			id_set_item,
			id_periodo,
			cv,
			id_item,
			pv,
			fecha_reg,
			usuario_ai,
			id_usuario_reg,
			id_usuario_ai,
			id_usuario_mod,
			fecha_mod
          	) values(
			'activo',
			v_parametros.id_set_item,
			v_parametros.id_periodo,
			v_parametros.cv,
			v_parametros.id_item,
			v_parametros.pv,
			now(),
			v_parametros._nombre_usuario_ai,
			p_id_usuario,
			v_parametros._id_usuario_ai,
			null,
			null
							
			
			
			)RETURNING id_puntos_afiliado into v_id_puntos_afiliado;
			
			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','puntos afiliado almacenado(a) con exito (id_puntos_afiliado'||v_id_puntos_afiliado||')'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_puntos_afiliado',v_id_puntos_afiliado::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************    
 	#TRANSACCION:  'AFI_PUAFIL_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin	
 	#FECHA:		19-12-2015 00:21:31
	***********************************/

	elsif(p_transaccion='AFI_PUAFIL_MOD')then

		begin
			--Sentencia de la modificacion
			update afi.tpuntos_afiliado set
			id_set_item = v_parametros.id_set_item,
			id_periodo = v_parametros.id_periodo,
			cv = v_parametros.cv,
			id_item = v_parametros.id_item,
			pv = v_parametros.pv,
			id_usuario_mod = p_id_usuario,
			fecha_mod = now(),
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai
			where id_puntos_afiliado=v_parametros.id_puntos_afiliado;
               
			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','puntos afiliado modificado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_puntos_afiliado',v_parametros.id_puntos_afiliado::varchar);
               
            --Devuelve la respuesta
            return v_resp;
            
		end;

	/*********************************    
 	#TRANSACCION:  'AFI_PUAFIL_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin	
 	#FECHA:		19-12-2015 00:21:31
	***********************************/

	elsif(p_transaccion='AFI_PUAFIL_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from afi.tpuntos_afiliado
            where id_puntos_afiliado=v_parametros.id_puntos_afiliado;
               
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','puntos afiliado eliminado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_puntos_afiliado',v_parametros.id_puntos_afiliado::varchar);
              
            --Devuelve la respuesta
            return v_resp;

		end;
         
	else
     
    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION
				
	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;
				        
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "afi"."ft_puntos_afiliado_ime"(integer, integer, character varying, character varying) OWNER TO postgres;
