CREATE OR REPLACE FUNCTION afi.ft_patro_colo_ime(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_patro_colo_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tpatro_colo'
 AUTOR: 		 (admin)
 FECHA:	        04-04-2015 15:41:13
 COMENTARIOS:	
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:	
 AUTOR:			
 FECHA:		
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_patro_colo	integer;
			    
BEGIN

    v_nombre_funcion = 'afi.ft_patro_colo_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************    
 	#TRANSACCION:  'AFI_COLO_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin	
 	#FECHA:		04-04-2015 15:41:13
	***********************************/

	if(p_transaccion='AFI_COLO_INS')then
					
        begin
        	--Sentencia de la insercion
        	insert into afi.tpatro_colo(
			derecha,
			izquierda,
			estado_reg,
			id_colocacion,
			id_usuario_ai,
			id_usuario_reg,
			fecha_reg,
			usuario_ai,
			id_usuario_mod,
			fecha_mod
          	) values(
			v_parametros.derecha,
			v_parametros.izquierda,
			'activo',
			v_parametros.id_colocacion,
			v_parametros._id_usuario_ai,
			p_id_usuario,
			now(),
			v_parametros._nombre_usuario_ai,
			null,
			null
							
			
			
			)RETURNING id_patro_colo into v_id_patro_colo;
			
			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Colocacion almacenado(a) con exito (id_patro_colo'||v_id_patro_colo||')'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_patro_colo',v_id_patro_colo::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************    
 	#TRANSACCION:  'AFI_COLO_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin	
 	#FECHA:		04-04-2015 15:41:13
	***********************************/

	elsif(p_transaccion='AFI_COLO_MOD')then

		begin
			--Sentencia de la modificacion
			update afi.tpatro_colo set
			derecha = v_parametros.derecha,
			izquierda = v_parametros.izquierda,
			id_colocacion = v_parametros.id_colocacion,
			id_usuario_mod = p_id_usuario,
			fecha_mod = now(),
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai
			where id_patro_colo=v_parametros.id_patro_colo;
               
			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Colocacion modificado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_patro_colo',v_parametros.id_patro_colo::varchar);
               
            --Devuelve la respuesta
            return v_resp;
            
		end;

	/*********************************    
 	#TRANSACCION:  'AFI_COLO_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin	
 	#FECHA:		04-04-2015 15:41:13
	***********************************/

	elsif(p_transaccion='AFI_COLO_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from afi.tpatro_colo
            where id_patro_colo=v_parametros.id_patro_colo;
               
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Colocacion eliminado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_patro_colo',v_parametros.id_patro_colo::varchar);
              
            --Devuelve la respuesta
            return v_resp;

		end;
         
	else
     
    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION
				
	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;
				        
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
