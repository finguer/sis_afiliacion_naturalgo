CREATE OR REPLACE FUNCTION "afi"."ft_pago_conf_sel"(	
				p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$
/**************************************************************************
 SISTEMA:		AFILIACION
 FUNCION: 		afi.ft_pago_conf_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'afi.tpago_conf'
 AUTOR: 		 (admin)
 FECHA:	        16-10-2016 16:46:11
 COMENTARIOS:	
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:	
 AUTOR:			
 FECHA:		
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;
			    
BEGIN

	v_nombre_funcion = 'afi.ft_pago_conf_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************    
 	#TRANSACCION:  'AFI_PAGCONF_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin	
 	#FECHA:		16-10-2016 16:46:11
	***********************************/

	if(p_transaccion='AFI_PAGCONF_SEL')then
     				
    	begin
    		--Sentencia de la consulta
			v_consulta:='select
						pagconf.id_pago_conf,
						pagconf.id_periodo,
						pagconf.estado,
						pagconf.estado_reg,
						pagconf.id_usuario_ai,
						pagconf.usuario_ai,
						pagconf.fecha_reg,
						pagconf.id_usuario_reg,
						pagconf.fecha_mod,
						pagconf.id_usuario_mod,
						usu1.cuenta as usr_reg,
						usu2.cuenta as usr_mod	,
						per.periodo as desc_periodo,
						ges.gestion as desc_gestion,
						ges.id_gestion
						from afi.tpago_conf pagconf
						inner join segu.tusuario usu1 on usu1.id_usuario = pagconf.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = pagconf.id_usuario_mod
						inner join param.tperiodo per on per.id_periodo = pagconf.id_periodo
						inner join param.tgestion ges on ges.id_gestion = per.id_gestion
				        where  ';
			
			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;
			v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

			--Devuelve la respuesta
			return v_consulta;
						
		end;

	/*********************************    
 	#TRANSACCION:  'AFI_PAGCONF_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin	
 	#FECHA:		16-10-2016 16:46:11
	***********************************/

	elsif(p_transaccion='AFI_PAGCONF_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(id_pago_conf)
					    from afi.tpago_conf pagconf
					    inner join segu.tusuario usu1 on usu1.id_usuario = pagconf.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = pagconf.id_usuario_mod
						inner join param.tperiodo per on per.id_periodo = pagconf.id_periodo
						inner join param.tgestion ges on ges.id_gestion = per.id_gestion
					    where ';
			
			--Definicion de la respuesta		    
			v_consulta:=v_consulta||v_parametros.filtro;

			--Devuelve la respuesta
			return v_consulta;

		end;
					
	else
					     
		raise exception 'Transaccion inexistente';
					         
	end if;
					
EXCEPTION
					
	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "afi"."ft_pago_conf_sel"(integer, integer, character varying, character varying) OWNER TO postgres;
