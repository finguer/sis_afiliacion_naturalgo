CREATE OR REPLACE FUNCTION "afi"."ft_rango_ime" (	
				p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$

/**************************************************************************
 SISTEMA:		AFILIACION
 FUNCION: 		afi.ft_rango_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.trango'
 AUTOR: 		 (admin)
 FECHA:	        14-10-2016 13:55:26
 COMENTARIOS:	
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:	
 AUTOR:			
 FECHA:		
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_rango	integer;
			    
BEGIN

    v_nombre_funcion = 'afi.ft_rango_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************    
 	#TRANSACCION:  'AFI_RAN_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin	
 	#FECHA:		14-10-2016 13:55:26
	***********************************/

	if(p_transaccion='AFI_RAN_INS')then
					
        begin
        	--Sentencia de la insercion
        	insert into afi.trango(
			porcentaje_de_pago,
			afiliado_activos,
			estado_reg,
			volumen_organizacional,
			volumen_cobro_nivel,
			id_rango_fk,
			volumen_personal,
			nombre,
			periodos_acumulados,
			nivel,
			usuario_ai,
			fecha_reg,
			id_usuario_reg,
			id_usuario_ai,
			fecha_mod,
			id_usuario_mod
          	) values(
			v_parametros.porcentaje_de_pago,
			v_parametros.afiliado_activos,
			'activo',
			v_parametros.volumen_organizacional,
			v_parametros.volumen_cobro_nivel,
			v_parametros.id_rango_fk,
			v_parametros.volumen_personal,
			v_parametros.nombre,
			v_parametros.periodos_acumulados,
			v_parametros.nivel,
			v_parametros._nombre_usuario_ai,
			now(),
			p_id_usuario,
			v_parametros._id_usuario_ai,
			null,
			null
							
			
			
			)RETURNING id_rango into v_id_rango;
			
			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','rango almacenado(a) con exito (id_rango'||v_id_rango||')'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_rango',v_id_rango::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************    
 	#TRANSACCION:  'AFI_RAN_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin	
 	#FECHA:		14-10-2016 13:55:26
	***********************************/

	elsif(p_transaccion='AFI_RAN_MOD')then

		begin
			--Sentencia de la modificacion
			update afi.trango set
			porcentaje_de_pago = v_parametros.porcentaje_de_pago,
			afiliado_activos = v_parametros.afiliado_activos,
			volumen_organizacional = v_parametros.volumen_organizacional,
			volumen_cobro_nivel = v_parametros.volumen_cobro_nivel,
			id_rango_fk = v_parametros.id_rango_fk,
			volumen_personal = v_parametros.volumen_personal,
			nombre = v_parametros.nombre,
			periodos_acumulados = v_parametros.periodos_acumulados,
			nivel = v_parametros.nivel,
			fecha_mod = now(),
			id_usuario_mod = p_id_usuario,
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai
			where id_rango=v_parametros.id_rango;
               
			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','rango modificado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_rango',v_parametros.id_rango::varchar);
               
            --Devuelve la respuesta
            return v_resp;
            
		end;

	/*********************************    
 	#TRANSACCION:  'AFI_RAN_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin	
 	#FECHA:		14-10-2016 13:55:26
	***********************************/

	elsif(p_transaccion='AFI_RAN_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from afi.trango
            where id_rango=v_parametros.id_rango;
               
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','rango eliminado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_rango',v_parametros.id_rango::varchar);
              
            --Devuelve la respuesta
            return v_resp;

		end;
         
	else
     
    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION
				
	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;
				        
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "afi"."ft_rango_ime"(integer, integer, character varying, character varying) OWNER TO postgres;
