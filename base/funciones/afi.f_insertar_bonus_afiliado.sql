CREATE OR REPLACE FUNCTION afi.f_insertar_bonus_afiliado(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE

    v_id_set_item                      integer DEFAULT p_params ->> 'id_set_item';
    v_id_afiliado                      integer DEFAULT p_params ->> 'id_afiliado';
    v_id_venta                         integer DEFAULT p_params ->> 'id_venta';
    p_id_usuario                       integer DEFAULT p_params ->> 'id_usuario';
    p_id_usuario_ai                    varchar DEFAULT p_params ->> '_id_usuario_ai';
    p_id_periodo                       integer DEFAULT p_params ->> 'po_id_periodo';
    v_id_bonus_config                       integer DEFAULT p_params ->> 'id_bonus_config';
    p_nombre_usuario_ai                varchar DEFAULT p_params ->> '_nombre_usuario_ai';
    v_resp                             json;
    v_monto_test                             numeric;
    v_nombre_funcion                   TEXT;
    v_puntos_item                      RECORD;
    v_record_afi_patro                 RECORD;
    v_pagar_por_paquete                numeric(10, 2);
    v_monto_multiplicar                NUMERIC;
    v_monto_dividir                NUMERIC;
    v_validado_para_recibir_patrocinio varchar;
    v_afiliado                         record;
v_pv_propio                     numeric;
    v_pais_patrocinador varchar;
    v_count_veces_por_afiliado integer;
    v_id_afi_patro_recibe_bonus integer;


BEGIN

--     todo debemos deprecar el monto_paquete que todo sea por cv_paquete
--      todo deprecar v_monto_dividir

    v_nombre_funcion = 'afi.f_insertar_bonus_afiliado';

    SELECT *
    INTO v_afiliado
    FROM afi.tafiliado
    WHERE id_afiliado = v_id_afiliado;

    select cv_vale
    into v_monto_multiplicar
    from afi.tconfig_pais
    where pais = v_afiliado.pais;


    IF v_afiliado.pais is null or v_afiliado.pais = ''
    THEN --cuando es de bolivia
        RAISE EXCEPTION '%', 'no hay codigo de area para esta afiliacion';
    END IF;


    --agregar puntos por el paquete agregado al inscribirse
    SELECT pi.*,
           bc.bonus,
           bc.monto,
           bc.bono_patrocinio_por_nivel,
           ts.id_set_item,
           ts.precio AS precio_del_set,
           bc.calcular_por,
           pi.cv,
           bc.validar_tipo_bonus,
           bc.bonos_registro_patrocinador,
           bc.afiliado_activo,
           bc.veces_por_afiliado,
           bc.quien_recibe_bono,
           bc.cada_que_tiempo
    INTO v_puntos_item
    FROM afi.tpunto_item pi
             INNER JOIN afi.tbonus_config bc ON bc.id_bonus_config = pi.id_bonus_config
             INNER JOIN vent.tset_item ts ON ts.id_set_item = pi.id_set
    WHERE pi.id_set = v_id_set_item;


    if v_puntos_item.quien_recibe_bono = 'PATROCINADOR' THEN
        v_id_afi_patro_recibe_bonus:= v_afiliado.id_patrocinador;
    ELSE
        v_id_afi_patro_recibe_bonus:= v_id_afiliado;
    end if;



    IF v_puntos_item.precio_del_set IS NULL THEN
        RAISE EXCEPTION '%', 'el precio del set es null';
    END IF;


    if v_puntos_item.veces_por_afiliado is not null then
        select count(*) as count
        into v_count_veces_por_afiliado
        from afi.tbonus
        where id_afiliado = v_id_afiliado and id_afiliado_from is null and id_bonus_config = v_puntos_item.id_bonus_config
        and (case when v_puntos_item.cada_que_tiempo is not null then id_periodo = p_id_periodo else 1=1 end);

        if v_count_veces_por_afiliado > v_puntos_item.veces_por_afiliado and v_id_afiliado != 6810 then
            raise exception '%', 'ERROR ESTE AFILIADO NO PUEDE COMPRAR ESTE PAQUETE POR QUE EXISTE UNA RESTRICCION QUE SOLO PUEDE COMPRAR '''||v_puntos_item.veces_por_afiliado::VARCHAR||''' VECES';
        end if;

    end if;



    IF v_puntos_item.cv IS NULL OR v_puntos_item.pv IS NULL THEN
        RAISE EXCEPTION '%','este set seleccionado no tiene puntos agregados porfavor mira tus configuraciones de puntos de este set';
    END IF;
    IF v_puntos_item.id_bonus_config IS NULL OR v_puntos_item.monto IS NULL THEN
        RAISE EXCEPTION '%','error en la configuracion de bonus debes registrara correctamente el bonus config el monto';
    END IF;


    IF v_puntos_item.afiliado_activo = 'Y' then
        -- en este caso tiene que tener activo con 8 productos
        select coalesce(sum(pv),0)
        INTO v_pv_propio
        from afi.tpuntos_afiliado
        where id_afiliado = v_id_afiliado and id_periodo = p_id_periodo;

        IF v_pv_propio < 240 THEN
            RAISE EXCEPTION '%', 'NO PUEDE ADQUIRIR POR QUE NO TIENE LOS PV SUFICIENTES SOLO TIENE ' || v_pv_propio;
        END IF;


    END IF;


    IF v_puntos_item.bonos_registro_patrocinador = 'si' then
        --necesitamos verificar de donde es el afiliado
        select pais
        into v_pais_patrocinador
        from afi.tafiliado
        --where id_afiliado = v_afiliado.id_patrocinador;
        where id_afiliado = v_id_afi_patro_recibe_bonus; --aca se cambio por que en el bono retorno el bono es al mismo y no al patrocinador



        IF v_pais_patrocinador is null or v_pais_patrocinador = ''
        THEN
            RAISE EXCEPTION '%', 'no hay codigo de area para esta afiliacion';
        END IF;

        select cv_vale
        into v_monto_multiplicar
        from afi.tconfig_pais
        where pais = v_pais_patrocinador;

        if v_monto_multiplicar is null then
            raise exception '%', 'NO TIENES CONFIGURADO EL PAIS PARA EL MONTO';
        end if;


        INSERT INTO afi.tbonus (estado_pago,
                                tipo,
                                estado_reg,
                                id_afiliado,
                                monto,
                                id_patrocinador,
                                id_usuario_reg,
                                usuario_ai,
                                fecha_reg,
                                id_usuario_ai,
                                fecha_mod,
                                id_usuario_mod,
                                id_periodo,
                                id_bonus_config,
                                id_venta
                                )
        VALUES ('pagado',
                v_puntos_item.bonus,
                'activo',
                v_id_afiliado,
                --(v_puntos_item.monto / v_monto_dividir),
                (v_puntos_item.monto * v_monto_multiplicar),
                v_id_afi_patro_recibe_bonus,--v_afiliado.id_patrocinador,
                p_id_usuario,
                p_nombre_usuario_ai,
                now(),
                NULL,--v_parametros._id_usuario_ai,
                NULL,
                NULL,
                p_id_periodo,
                v_puntos_item.id_bonus_config,
                v_id_venta
                );
    END IF;


    --NECESITAMOS REGISTRAR EL BONU PATROCINIO DEPENDIENDO LA CONFIGURACION POR NIVEL QUE SE TENGA EN bono_patrocinio_por_nivel

    IF v_puntos_item.bono_patrocinio_por_nivel IS NOT NULL THEN

        FOR v_record_afi_patro IN (WITH RECURSIVE t(nivel, id_afiliado, id_patrocinador) AS (
            SELECT 1 AS nivel,
                   afil.id_afiliado,
                   afil.id_patrocinador
            FROM afi.tafiliado afil
            WHERE id_afiliado = v_id_afiliado

            UNION ALL
            SELECT nivel + 1,
                   afil.id_afiliado,
                   afil.id_patrocinador
            FROM afi.tafiliado afil
                     INNER JOIN t t ON t.id_patrocinador = afil.id_afiliado
            WHERE t.nivel <= (array_length(v_puntos_item.bono_patrocinio_por_nivel, 1) - 1)
        )
                                   SELECT *
                                   FROM t)
            LOOP

                if  v_record_afi_patro.id_patrocinador is not null then

                    select pais
                    into v_pais_patrocinador
                    from afi.tafiliado
                    where id_afiliado = v_record_afi_patro.id_patrocinador;



                    IF v_pais_patrocinador is null or v_pais_patrocinador = ''
                    THEN --cuando es de bolivia
                        RAISE EXCEPTION '%', 'no hay codigo de area para esta afiliacion';
                    END IF;

                    select cv_vale
                    into v_monto_multiplicar
                    from afi.tconfig_pais
                    where pais = v_pais_patrocinador;

                    if v_monto_multiplicar is null then
                        raise exception '%', 'NO TIENES CONFIGURADO EL PAIS PARA EL MONTO';
                    end if;


                    v_validado_para_recibir_patrocinio = 'si';
                    -- debemos verificar si el patrocinador que recibira un porcentaje del bonus patrocinio esta validado para recibir
                    IF v_puntos_item.validar_tipo_bonus = 'si' THEN
                        IF exists(SELECT 1
                                  FROM afi.tpuntos_afiliado
                                  WHERE id_afiliado = v_record_afi_patro.id_patrocinador
                                    AND id_set_item = v_id_set_item) THEN
                            v_validado_para_recibir_patrocinio = 'si';
                        ELSE
                            v_validado_para_recibir_patrocinio = 'no';
                        END IF;
                    END IF;


                    IF v_puntos_item.calcular_por = 'cv_paquete' THEN


                        v_pagar_por_paquete = (v_puntos_item.cv *
                                               (v_puntos_item.bono_patrocinio_por_nivel[v_record_afi_patro.nivel] / 100)) *
                                              v_monto_multiplicar;

                    ELSIF v_puntos_item.calcular_por = 'monto_paquete' THEN
                        v_pagar_por_paquete := (v_puntos_item.precio_del_set *
                                                (v_puntos_item.bono_patrocinio_por_nivel[v_record_afi_patro.nivel] / 100));
                        v_pagar_por_paquete := round(v_pagar_por_paquete);
                    ELSE
                        v_pagar_por_paquete := 0;
                        RAISE EXCEPTION '%', 'NO HAY DEFINIDO UNA CONFIGURACION PARA CALCULAR POR cv_paquete o monto_paquete';

                    END IF;
                    --agregamos los puntos al afiliado
                    INSERT INTO afi.tbonus (estado_pago,
                                            tipo,
                                            estado_reg,
                                            id_afiliado,
                                            monto,
                                            id_patrocinador,
                                            id_usuario_reg,
                                            usuario_ai,
                                            fecha_reg,
                                            id_usuario_ai,
                                            fecha_mod,
                                            id_usuario_mod,
                                            id_periodo,
                                            id_afiliado_from,
                                            nivel_from,
                                            validado_para_pagar_a_patrocinador,
                                            id_bonus_config,
                                            id_venta
                                            )
                    VALUES ('pendiente de pago',
                            v_puntos_item.bonus,
                            'activo',
                            v_record_afi_patro.id_afiliado,
                               --(v_pagar_por_paquete / v_monto_dividir),
                            (v_pagar_por_paquete),
                            v_record_afi_patro.id_patrocinador,
                            p_id_usuario,
                            p_nombre_usuario_ai,
                            now(),
                            NULL,--p_id_usuario_ai,
                            NULL,
                            NULL,
                            p_id_periodo,
                            v_id_afiliado,
                            v_record_afi_patro.nivel,
                            v_validado_para_recibir_patrocinio,
                            v_puntos_item.id_bonus_config,
                            v_id_venta
                            );

                end if;


            END LOOP;

    END IF;


    SELECT json_strip_nulls(json_build_object(
            'success', TRUE
        ))
    INTO v_resp;
    RETURN v_resp;


EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
            ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;
        RETURN v_resp;


END;
$BODY$
    LANGUAGE plpgsql VOLATILE;