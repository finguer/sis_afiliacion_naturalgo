CREATE OR REPLACE FUNCTION afi.insertar_afi_cliente()
  RETURNS varchar
AS
  $BODY$
  /**************************************************************************
 SISTEMA:		vent
 FUNCION: 		afi.insertar_afi_cliente
 DESCRIPCION:   descuenta de almacen al momento de vender
 AUTOR: 		 (favio figueroa)
 FECHA:	       25/11/2015

****************************************************************************/
  DECLARE
    v_registros        RECORD;
    v_codigo_control   TEXT;
    v_fecha            VARCHAR(255);
    v_record           RECORD;
    v_suma_item        INTEGER;
    v_suma_item_salida INTEGER;
    v_total            INTEGER;
    v_id_almacen       INTEGER;
  BEGIN


    FOR v_record
    IN (SELECT afil.*,person.nombre_completo2
        FROM afi.tafiliado afil
    INNER JOIN segu.vpersona2 person on person.id_persona = afil.id_persona )
    LOOP

      insert into vent.tcliente(
        id_persona,
        estado_reg,
        nit,
        codigo,
        usuario_ai,
        fecha_reg,
        id_usuario_reg,
        id_usuario_ai,
        fecha_mod,
        id_usuario_mod,
        razon
      ) values(
        v_record.id_persona,
        'activo',
        v_record.codigo,
        v_record.codigo,
        v_record.usuario_ai,
        now(),
        v_record.id_usuario_reg,
        v_record.id_usuario_ai,
        null,
        null,
        v_record.nombre_completo2



      );

    END LOOP;


    RETURN TRUE;
  END
$BODY$
LANGUAGE plpgsql VOLATILE;
