CREATE OR REPLACE FUNCTION afi.f_insert_puntos_por_venta(
  pa_tipo        VARCHAR,
  pa_id_producto INTEGER,
  pa_cantidad    INTEGER,
  pa_id_cliente  INTEGER,
    pa_id_venta   INTEGER
)
  RETURNS VARCHAR
AS
  $BODY$
  DECLARE
    v_puntos               RECORD;
    v_consulta             VARCHAR;
    v_condicion            VARCHAR;
    v_id_item              INTEGER;
    v_id_set_item          INTEGER;
    v_rec                  RECORD;
    v_resp                 VARCHAR;

    v_id_afiliado          INTEGER;

    v_consulta_dos         VARCHAR;
    v_puntos_comisionables NUMERIC;
    v_id_patrocinador      INTEGER;

    v_inner                VARCHAR;
    v_json_params_to_send    json;
    v_res_insertar_bonus_red json;
    v_bono_patrocinio_por_nivel numeric[];
  BEGIN


    v_consulta_dos='select afil.id_afiliado
    from afi.tafiliado afil
      inner join segu.tpersona per on per.id_persona = afil.id_persona
      inner join vent.tcliente cli on cli.id_persona = per.id_persona
      where cli.id_cliente = ' || pa_id_cliente;

    EXECUTE v_consulta_dos
    INTO v_id_afiliado;


    IF (pa_tipo = 'SET')
    THEN
      v_inner = 'inner join vent.tset_item pro on pro.id_set_item = puti.id_set';
      v_condicion = 'puti.id_set =' || pa_id_producto;
      v_id_set_item = pa_id_producto;
      v_id_item = NULL;
    ELSE
      v_inner = 'inner join mer.titem pro on pro.id_item = puti.id_item';
      v_condicion = 'puti.id_item = ' || pa_id_producto;
      v_id_item = pa_id_producto;
      v_id_set_item = NULL;
    END IF;


    v_consulta=
    'SELECT puti.cv,puti.pv,puti.bonus_patrocinador,pro.nombre from afi.tpunto_item puti '
    || v_inner || ' where ' ||
    v_condicion;

    --RAISE EXCEPTION '%',v_consulta;


    EXECUTE v_consulta
    INTO v_puntos;


    v_rec = param.f_get_periodo_gestion(to_char(now(), 'YYYY-mm-dd') :: DATE);



    INSERT INTO afi.tpuntos_afiliado (
      estado_reg,
      id_set_item,
      id_periodo,
      cv,
      id_item,
      pv,
      fecha_reg,
      usuario_ai,
      id_usuario_reg,
      id_usuario_ai,
      id_usuario_mod,
      fecha_mod,
      id_afiliado,
        id_venta
    ) VALUES (
      'activo',
      v_id_set_item,
      v_rec.po_id_periodo,
      (v_puntos.cv * pa_cantidad),
      v_id_item,
      (v_puntos.pv * pa_cantidad),
      now(),
      'null',
      1,
      1,
      NULL,
      NULL,
      v_id_afiliado,
        pa_id_venta
    );


    IF (pa_tipo = 'SET')
    THEN
        select tbc.bono_patrocinio_por_nivel
        into v_bono_patrocinio_por_nivel
        from afi.tpunto_item tpi
        inner JOIN afi.tbonus_config tbc on tbc.id_bonus_config = tpi.id_bonus_config
        where tpi.id_set = v_id_set_item;

        IF v_bono_patrocinio_por_nivel is not NULL then

            SELECT json_strip_nulls(json_build_object(
                    'id_set_item', v_id_set_item,
                    'id_afiliado', v_id_afiliado,
                    'id_usuario', 1,
                    '_nombre_usuario_ai', 'NULL',
                    '_id_usuario_ai', '',
                    'po_id_periodo', v_rec.po_id_periodo
                ))
            INTO v_json_params_to_send;

            v_res_insertar_bonus_red := afi.f_insertar_bonus_afiliado(v_json_params_to_send);

        END IF;

    END IF;



    IF (v_puntos.bonus_patrocinador = 'SI')
    THEN

      v_puntos_comisionables = v_puntos.cv * pa_cantidad;

      SELECT afil.id_patrocinador
      INTO v_id_patrocinador
      FROM afi.tafiliado afil
      WHERE afil.id_afiliado = v_id_afiliado;

      INSERT INTO afi.tbonusproductopatrocinador (
        id_afiliado,
        total_cv,
        id_patrocinador,
        cv,
        observaciones,
        cantidad_pro,
        estado_reg,
        id_usuario_ai,
        usuario_ai,
        fecha_reg,
        id_usuario_reg,
        fecha_mod,
        id_usuario_mod,
          id_venta
      ) VALUES (
        v_id_afiliado,
        v_puntos_comisionables,
        v_id_patrocinador,
        v_puntos.cv,
        v_puntos.nombre,
        pa_cantidad,
        'activo',
        1,
        1,
        now(),
        NULL,
        NULL,
        NULL,
          pa_id_venta


      );
    END IF;


    RETURN 'exito';


    EXCEPTION
    WHEN OTHERS THEN
      v_resp='';
      v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', SQLERRM);
      v_resp = pxp.f_agrega_clave(v_resp, 'codigo_error', SQLSTATE);
      v_resp = pxp.f_agrega_clave(v_resp, 'procedimientos', 'f_insert_puntos_por_venta');
      RAISE EXCEPTION '%', v_resp;


  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;