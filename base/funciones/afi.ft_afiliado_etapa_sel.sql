CREATE OR REPLACE FUNCTION "afi"."ft_afiliado_etapa_sel"(	
				p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$
/**************************************************************************
 SISTEMA:		afiliacion
 FUNCION: 		afi.ft_afiliado_etapa_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'afi.tafiliado_etapa'
 AUTOR: 		 (admin)
 FECHA:	        04-01-2016 16:42:12
 COMENTARIOS:	
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:	
 AUTOR:			
 FECHA:		
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;
			    
BEGIN

	v_nombre_funcion = 'afi.ft_afiliado_etapa_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************    
 	#TRANSACCION:  'AFI_AFET_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin	
 	#FECHA:		04-01-2016 16:42:12
	***********************************/

	if(p_transaccion='AFI_AFET_SEL')then
     				
    	begin
    		--Sentencia de la consulta
			v_consulta:='select
						afet.id_afiliado_etapa,
						afet.id_afiliado,
						afet.fecha_fin,
						afet.etapa,
						afet.fecha_ini,
						afet.estado_reg,
						afet.id_usuario_ai,
						afet.usuario_ai,
						afet.fecha_reg,
						afet.id_usuario_reg,
						afet.fecha_mod,
						afet.id_usuario_mod,
						usu1.cuenta as usr_reg,
						usu2.cuenta as usr_mod	
						from afi.tafiliado_etapa afet
						inner join segu.tusuario usu1 on usu1.id_usuario = afet.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = afet.id_usuario_mod
				        where  ';
			
			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;
			v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

			--Devuelve la respuesta
			return v_consulta;
						
		end;

	/*********************************    
 	#TRANSACCION:  'AFI_AFET_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin	
 	#FECHA:		04-01-2016 16:42:12
	***********************************/

	elsif(p_transaccion='AFI_AFET_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(id_afiliado_etapa)
					    from afi.tafiliado_etapa afet
					    inner join segu.tusuario usu1 on usu1.id_usuario = afet.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = afet.id_usuario_mod
					    where ';
			
			--Definicion de la respuesta		    
			v_consulta:=v_consulta||v_parametros.filtro;

			--Devuelve la respuesta
			return v_consulta;

		end;
					
	else
					     
		raise exception 'Transaccion inexistente';
					         
	end if;
					
EXCEPTION
					
	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "afi"."ft_afiliado_etapa_sel"(integer, integer, character varying, character varying) OWNER TO postgres;
