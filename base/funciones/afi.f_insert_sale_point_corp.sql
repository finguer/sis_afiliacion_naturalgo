CREATE OR REPLACE FUNCTION afi.f_insert_sale_point_corp(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE


    v_nombre_funcion      varchar;
    p_id_usuario          integer DEFAULT p_params ->> 'id_usuario';
    p_id_afiliado         integer DEFAULT p_params ->> 'id_afiliado';
    v_res                 json;
    v_resp                json;
    v_record_afiliado     record;
    v_id_cliente          INTEGER;
    v_id_usuario          INTEGER;
    v_record_hijos        record;
    v_count               INTEGER;
    v_mensaje             varchar;
    v_json_array          json[];
    v_json_hijos          json;
    v_pais                varchar;
    v_id_pais             integer;
    v_id_sucursal         integer;
    v_id_almacen         integer;
    v_id_punto_venta         integer;
    v_codigo_afiliado     varchar;
    v_id_usuario_afiliado integer;
    v_id_persona integer;
    v_codigo_corp varchar;


BEGIN


    v_nombre_funcion = 'afi.f_insert_sale_point_corp';



    select ta.pais, ta.codigo, tu.id_usuario, ta.id_persona
    into v_pais, v_codigo_afiliado, v_id_usuario_afiliado, v_id_persona
    from afi.tafiliado ta
    inner join segu.tusuario tu on tu.id_persona = ta.id_persona
    where id_afiliado = p_id_afiliado;

    v_codigo_corp:= concat('CORP_', v_codigo_afiliado);


    --verificar que este afiliado no tenga nada configurado para almacenes y punto de venta
   /* if exists (select 1 from su.tusuario_punto_venta where id_usuario =v_id_usuario_afiliado) then
        raise exception '%', 'YA SE TIENE CONFIGURADO ESTE AFILIADO PARA PUNTO DE VENTA';
    end if;*/

    select id_pais
    into v_id_pais
    from su.tpais
    where upper(nombre) = upper(v_pais);

    if v_id_pais is not null then



    select id_sucursal
    into v_id_sucursal
    from su.tsucursal
    where id_pais = v_id_pais
      and codigo = 'CORP';

    if v_id_sucursal is null then
        insert into su.tsucursal(estado_reg,
                                 ciudad,
                                 id_pais,
                                 codigo,
                                 estado,
                                 nombre,
                                 direccion,
                                 id_usuario_reg,
                                 usuario_ai,
                                 fecha_reg,
                                 id_usuario_ai,
                                 id_usuario_mod,
                                 fecha_mod)
        values ('activo',
                'CORP',
                v_id_pais,
                'CORP',
                'activo',
                'CORP',
                'CORP',
                p_id_usuario,
                'NULL',
                now(),
                NULL,
                null,
                null)
        RETURNING id_sucursal into v_id_sucursal;

    end if;

    --crear almacen
    insert into mer.talmacen (id_usuario_reg, id_usuario_mod, fecha_reg,
                              fecha_mod, estado_reg, id_usuario_ai,
                              usuario_ai, nombre,
                              id_sucursal, obs_dba)
    values (p_id_usuario, null, now(),
            null, 'activo', null,
            'null', concat('CORP_', v_codigo_afiliado),
            v_id_sucursal, 'almacen creado para afiliado corporativo') RETURNING id_almacen into v_id_almacen;
    --crear punto de venta
    insert into su.tpunto_venta (id_usuario_reg, id_usuario_mod, fecha_reg,
                                 fecha_mod, estado_reg, id_usuario_ai,
                                 usuario_ai, nombre,
                                 descripcion, codigo, estado, id_sucursal,
                                 numero_correlativo, obs_dba)
    values (p_id_usuario, null, now(),
            null, 'activo', null,
            'null', concat('PTV_', v_codigo_afiliado), 'punto de venta para un afiliado corporativo',
            concat('PTV_', v_codigo_afiliado), 'activo', v_id_sucursal, 1, null) returning id_punto_venta into v_id_punto_venta;
    --asignar afiliado a almacen y punto de venta
    insert into su.tusuario_almacen (id_usuario_reg, id_usuario_mod, fecha_reg,
                                     fecha_mod, estado_reg, id_usuario_ai,
                                     usuario_ai, id_usuario,
                                     id_almacen, rol, obs_dba)
    values (p_id_usuario, null, now(), null, 'activo', null, 'null', v_id_usuario_afiliado, v_id_almacen, 'afiliado_corp', null);

    insert into su.tusuario_punto_venta (id_usuario_reg, id_usuario_mod, fecha_reg,
                                         fecha_mod, estado_reg, id_usuario_ai,
                                         usuario_ai, id_usuario,
                                         id_punto_venta, rol, obs_dba)
    values (p_id_usuario, null, now(), null, 'activo', null, 'null', v_id_usuario_afiliado, v_id_punto_venta, 'afiliado_corp', null);

    insert into su.tpunto_venta_almacen (id_usuario_reg, id_usuario_mod, fecha_reg, fecha_mod, estado_reg, id_usuario_ai, usuario_ai, id_punto_venta, id_almacen, obs_dba)
    values (p_id_usuario, null, now(), null, 'activo', null, 'null', v_id_punto_venta, v_id_almacen, null);

    --insert the customer
    insert into vent.tcliente(
        id_persona,
        estado_reg,
        nit,
        codigo,
        usuario_ai,
        fecha_reg,
        id_usuario_reg,
        id_usuario_ai,
        fecha_mod,
        id_usuario_mod,
        razon,
        telefono,
        tipo,
        id_almacen
    ) values(
                    v_id_persona,
                'activo',
                    v_codigo_corp,
                    v_codigo_corp,
                'NULL',
                now(),
                    p_id_usuario,
                null,
                null,
                null,
                    v_codigo_corp,
                    v_codigo_corp,
                'CORPORATIVO',
                v_id_almacen



            )RETURNING id_cliente into v_id_cliente;


    SELECT json_strip_nulls(json_build_object(
            'success', TRUE,
            'id_afiliado', p_id_afiliado,
            'id_punto_venta', v_id_punto_venta,
            'id_almacen', v_id_almacen
                            ))
    INTO v_resp;


    else
        SELECT json_strip_nulls(json_build_object(
                'success', FALSE,
                'id_afiliado', p_id_afiliado
                                ))
        INTO v_resp;

    end if;

    RETURN v_resp;


EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
                                ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;
        RETURN v_resp;


END ;
$BODY$
    LANGUAGE plpgsql VOLATILE;