CREATE OR REPLACE FUNCTION afi.f_ver_patrocinador(p_id_afiliado int4)
  RETURNS int4
AS
$BODY$
  DECLARE
  v_id_aux INTEGER;
BEGIN

select id_patrocinador INTO v_id_aux from afi.tafi_patro
  WHERE id_afiliado = p_id_afiliado;

  RETURN v_id_aux;


END;
$BODY$
LANGUAGE plpgsql VOLATILE;
