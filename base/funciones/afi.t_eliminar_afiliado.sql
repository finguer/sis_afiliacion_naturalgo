CREATE OR REPLACE FUNCTION afi.f_eliminar_afiliado(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE


    v_nombre_funcion  varchar;
    p_id_afiliado     integer DEFAULT p_params ->> 'id_afiliado';
    v_res             json;
    v_resp            json;
    v_record_afiliado record;
    v_id_cliente      INTEGER;
    v_id_usuario      INTEGER;
    v_record_hijos    record;
    v_count           INTEGER;
    v_mensaje         varchar;
    v_json_array      json[];
    v_json_hijos      json;


BEGIN


    v_nombre_funcion = 'afi.f_eliminar_afiliado';


    SELECT *
    INTO v_record_afiliado
    FROM afi.tafiliado
    WHERE id_afiliado = p_id_afiliado;


    IF v_record_afiliado IS NULL THEN
        RAISE EXCEPTION '%', 'ocurrio extrano error no existe el afiliado seleccionado';
    END IF;


    SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(afi)))
    INTO v_json_hijos
    FROM (
             SELECT *
             FROM afi.tafiliado
             WHERE id_patrocinador = v_record_afiliado.id_afiliado
         ) afi;

    FOR v_record_hijos IN (
        select cast(j ->> 'id_afiliado' as integer) as id_afiliado,
               cast(j ->> 'codigo' as varchar) as codigo
        from json_array_elements(v_json_hijos) j
    )
        LOOP

            v_json_array := array_append(v_json_array,
                                         json_strip_nulls(json_build_object(
                                                 'id_afiliado', v_record_hijos.id_afiliado,
                                                 'codigo', v_record_hijos.codigo
                                             ))
                );


            --modificamos sus hijos a su patrocinador del que eliminaremos
            UPDATE afi.tafiliado
            SET id_patrocinador = v_record_afiliado.id_patrocinador
            WHERE id_afiliado = v_record_hijos.id_afiliado;


        END LOOP;


    --eliminamos su cliente para compras
    SELECT id_cliente
    INTO v_id_cliente
    FROM vent.tcliente
    WHERE id_persona = v_record_afiliado.id_persona;


  /*  UPDATE vent.tventa SET id_cliente = NULL WHERE id_cliente = v_id_cliente;
    DELETE FROM vent.tcliente WHERE id_persona = v_record_afiliado.id_persona;*/


    --eliminamos sus puntos
    UPDATE afi.tpuntos_afiliado SET estado_reg = 'inactivo' WHERE id_afiliado = v_record_afiliado.id_afiliado;

    --eliminamos sus rangos por periodo
    UPDATE afi.trango_afiliado SET estado_reg = 'inactivo' WHERE id_afiliado = v_record_afiliado.id_afiliado;

    --eliminamos su cuenta bancaria
    UPDATE afi.tcuenta_bancaria SET estado_reg = 'inactivo' WHERE id_afiliado = v_record_afiliado.id_afiliado;

    --eliminamos pago periodo
    update afi.tafiliado_pago_periodo SET estado_reg = 'inactivo' WHERE id_afiliado = v_record_afiliado.id_afiliado;

    -- eliminamos los bonus que generan multinivel
    update afi.tbonus SET estado_reg = 'inactivo' WHERE id_afiliado_from = v_record_afiliado.id_afiliado;

    --eliminamos su afiliado

    update afi.tafiliado SET estado_reg = 'inactivo'  WHERE id_afiliado = v_record_afiliado.id_afiliado;

    INSERT INTO afi.tlog_afiliado_eliminado (afiliado, hijos)
    VALUES (to_json(v_record_afiliado),v_json_hijos);


    --eliminamos su usuario y persona
    SELECT count(*)
    INTO v_count
    FROM segu.tusuario
    WHERE id_persona = v_record_afiliado.id_persona;

    IF v_count = 1 THEN
        SELECT id_usuario
        INTO v_id_usuario
        FROM segu.tusuario
        WHERE id_persona = v_record_afiliado.id_persona;

        IF v_id_usuario IS NOT NULL THEN
            --DELETE FROM segu.tusuario_rol WHERE id_usuario = v_id_usuario;
            update segu.tusuario set contrasena = md5('afiliadoanulado') WHERE id_usuario = v_id_usuario;
        END IF;

        --DELETE FROM segu.tpersona WHERE id_persona = v_record_afiliado.id_persona;

    END IF;


    SELECT json_strip_nulls(json_build_object(
            'success', TRUE
        ))
    INTO v_resp;
    RETURN v_resp;


EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
            ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;
        RETURN v_resp;


END ;
$BODY$
    LANGUAGE plpgsql VOLATILE;