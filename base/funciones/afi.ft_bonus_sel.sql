CREATE OR REPLACE FUNCTION afi.ft_bonus_sel(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_bonus_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'afi.tbonus'
 AUTOR: 		 (admin)
 FECHA:	        11-04-2015 16:33:17
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;

BEGIN

	v_nombre_funcion = 'afi.ft_bonus_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'AFI_BON_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin
 	#FECHA:		11-04-2015 16:33:17
	***********************************/

	if(p_transaccion='AFI_BON_SEL')then

    	begin
    		--Sentencia de la consulta
			v_consulta:='select
						bon.id_bonus,
						bon.tipo,
						bon.estado_reg,
						bon.estado_pago,
						bon.id_afiliado,
						bon.monto,
						bon.id_patrocinador,
						bon.id_usuario_reg,
						bon.usuario_ai,
						bon.fecha_reg,
						bon.id_usuario_ai,
						bon.fecha_mod,
						bon.id_usuario_mod,
						usu1.cuenta as usr_reg,
						usu2.cuenta as usr_mod,
						patro.codigo as codigo_patrocinador,
						afil.codigo as codigo_afiliado,
						vp.nombre_completo2 as desc_persona,
						bon.id_periodo,
						bon.nivel_from,
						afil_from.codigo as codigo_afiliado_from
						from afi.tbonus bon
						inner join segu.tusuario usu1 on usu1.id_usuario = bon.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = bon.id_usuario_mod

            inner join afi.tafiliado patro on patro.id_afiliado = bon.id_patrocinador
            inner join afi.tafiliado afil on afil.id_afiliado = bon.id_afiliado
            left join afi.tafiliado afil_from on afil_from.id_afiliado = bon.id_afiliado_from
            inner join segu.vpersona vp on vp.id_persona = afil.id_persona
				        where  ';


			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;
			v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

			--Devuelve la respuesta
			return v_consulta;

		end;

	/*********************************
 	#TRANSACCION:  'AFI_BON_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin
 	#FECHA:		11-04-2015 16:33:17
	***********************************/

	elsif(p_transaccion='AFI_BON_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(id_bonus)
					    from afi.tbonus bon
					    inner join segu.tusuario usu1 on usu1.id_usuario = bon.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = bon.id_usuario_mod
					    where ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;

			--Devuelve la respuesta
			return v_consulta;

		end;

	else

		raise exception 'Transaccion inexistente';

	end if;

EXCEPTION

	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
