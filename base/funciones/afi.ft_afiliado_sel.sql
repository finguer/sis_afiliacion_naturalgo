CREATE OR REPLACE FUNCTION afi.ft_afiliado_sel(p_administrador INT4, p_id_usuario INT4, p_tabla VARCHAR,
                                               p_transaccion   VARCHAR)
  RETURNS VARCHAR
AS
  $BODY$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_afiliado_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'afi.tafiliado'
 AUTOR: 		 (admin)
 FECHA:	        02-04-2015 01:06:18
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

  DECLARE

    v_consulta       VARCHAR;
    v_parametros     RECORD;
    v_nombre_funcion TEXT;
    v_resp           VARCHAR;
    v_id_afiliado    INTEGER;

    v_id_afiliado_pago_periodo	integer;

    v_tabla_aux VARCHAR;
    v_estado VARCHAR;
    v_rec_gestion_periodo                        RECORD;
    v_id_rango_afiliado                        INTEGER;
    v_id_rango                        INTEGER;
    v_pago_conf                        boolean;
    v_resp_json                        json;
    v_json_params_to_send                        json;


  BEGIN

    v_nombre_funcion = 'afi.ft_afiliado_sel';
    v_parametros = pxp.f_get_record(p_tabla);

    /*********************************
     #TRANSACCION:  'AFI_AFIL_SEL'
     #DESCRIPCION:	Consulta de datos
     #AUTOR:		admin
     #FECHA:		02-04-2015 01:06:18
    ***********************************/

    IF (p_transaccion = 'AFI_AFIL_SEL')
    THEN

      BEGIN
        --Sentencia de la consulta
        v_consulta:='select
									afil.id_afiliado,
									afil.direccion_principal,
									afil.id_persona,
									afil.lugar_nacimiento,
									afil.estado_reg,
									afil.fecha_nacimiento,
									afil.id_usuario_ai,
									afil.id_usuario_reg,
									afil.fecha_reg,
									afil.usuario_ai,
									afil.id_usuario_mod,
									afil.fecha_mod,
									usu1.cuenta as usr_reg,
									usu2.cuenta as usr_mod	,
									PERSON.nombre_completo2 as desc_person,
									PERSON.ci,
									af.codigo as codigo_patrocinador,
									afil.codigo as codigo,
									af.id_afiliado as id_patrocinador,
									afil.monto_registro,
									afil.modalidad_de_registro,
									afil.id_set_item,
									afil.id_punto_venta,
									tpva.id_almacen
								from afi.tafiliado afil
									inner join segu.tusuario usu1 on usu1.id_usuario = afil.id_usuario_reg
									INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
									LEFT JOIN su.tpunto_venta_almacen tpva on afil.id_punto_venta = tpva.id_punto_venta
									LEFT join  afi.tafiliado af on af.id_afiliado = afil.id_patrocinador
									left join segu.tusuario usu2 on usu2.id_usuario = afil.id_usuario_mod
				        where  ';

        /*IF pxp.f_existe_parametro(p_tabla,'solo_registrados_por_usuario') THEN

            IF p_id_usuario in (
                              13103,
                              11329,
                              12937,
                              13604,
                              15847,
                              14001,
                              14007,
                              14002,
                              14005,
                              14000,
                              12614,
                              14912,
                              16423,
                              16814,
                              14884,
                              28,
                              11444,
                              6
              ) AND  v_parametros.solo_registrados_por_usuario = 'si' THEN
            v_consulta := v_consulta || ' afil.id_usuario_reg = '||p_id_usuario||' and ';
          END IF;
        END IF;*/
        IF pxp.f_existe_parametro(p_tabla,'solo_registrados_por_usuario') THEN
            IF p_administrador = 0 THEN
                v_consulta := v_consulta || ' afil.id_usuario_reg = '||p_id_usuario||' and ';
            END IF;
        END IF;




        --Definicion de la respuesta
        v_consulta:=v_consulta || v_parametros.filtro;
        v_consulta:=
        v_consulta || ' order by ' || v_parametros.ordenacion || ' ' || v_parametros.dir_ordenacion || ' limit ' ||
        v_parametros.cantidad || ' offset ' || v_parametros.puntero;

        --Devuelve la respuesta
        RETURN v_consulta;

      END;

    /*********************************
     #TRANSACCION:  'AFI_AFIL_CONT'
     #DESCRIPCION:	Conteo de registros
     #AUTOR:		admin
     #FECHA:		02-04-2015 01:06:18
    ***********************************/

    ELSIF (p_transaccion = 'AFI_AFIL_CONT')
      THEN

        BEGIN
          --Sentencia de la consulta de conteo de registros
          v_consulta:='select count(afil.id_afiliado)
					    from afi.tafiliado afil
					    inner join segu.tusuario usu1 on usu1.id_usuario = afil.id_usuario_reg
									INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
									LEFT join  afi.tafiliado af on af.id_afiliado = afil.id_patrocinador
									left join segu.tusuario usu2 on usu2.id_usuario = afil.id_usuario_mod
					    where ';

          IF pxp.f_existe_parametro(p_tabla,'solo_registrados_por_usuario') THEN
              IF p_administrador = 0 THEN
                  v_consulta := v_consulta || ' afil.id_usuario_reg = '||p_id_usuario||' and ';
              END IF;
          END IF;

          --Definicion de la respuesta
          v_consulta:=v_consulta || v_parametros.filtro;

          --Devuelve la respuesta
          RETURN v_consulta;

        END;


    /*********************************
   #TRANSACCION:  'AFI_AFIARB_SEL'
   #DESCRIPCION:	listado tipo arbol
   #AUTOR:		admin
   #FECHA:		02-04-2015 01:06:18
  ***********************************/

    ELSIF (p_transaccion = 'AFI_AFIARB_SEL')
      THEN

        BEGIN


          IF v_parametros.inicio = 'si'
          THEN
            v_consulta:='select

									PERSON.nombre_completo2 as desc_person,

									null::numeric as codigo_patrocinador,
									afil.codigo as codigo,
									  PERSON.celular1,
                  PERSON.telefono1

								from afi.tafiliado afil
									inner join segu.tusuario usu1 on usu1.id_usuario = afil.id_usuario_reg
									INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
									LEFT join  afi.tafiliado af on af.id_afiliado = afil.id_patrocinador
									left join segu.tusuario usu2 on usu2.id_usuario = afil.id_usuario_mod

				        where  ';

          ELSE
            v_consulta:='select

									PERSON.nombre_completo2 as desc_person,

								af.codigo as codigo_patrocinador,
									afil.codigo as codigo,
									  PERSON.celular1,
                  PERSON.telefono1

								from afi.tafiliado afil
									inner join segu.tusuario usu1 on usu1.id_usuario = afil.id_usuario_reg
									INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
									LEFT join  afi.tafiliado af on af.id_afiliado = afil.id_patrocinador
									left join segu.tusuario usu2 on usu2.id_usuario = afil.id_usuario_mod

				        where  ';
          END IF;




          --Definicion de la respuesta
          v_consulta:=v_consulta || v_parametros.filtro;
          v_consulta:= v_consulta || ' ' || ' limit ' ||
          v_parametros.cantidad;
          --Devuelve la respuesta
          RETURN v_consulta;
        END;

    /*********************************
   #TRANSACCION:  'AFI_AFIARB_CONT'
   #DESCRIPCION:	Conteo de registros
   #AUTOR:		admin
   #FECHA:		02-04-2015 01:06:18
  ***********************************/

    ELSIF (p_transaccion = 'AFI_AFIARB_CONT')
      THEN

        BEGIN
          --Sentencia de la consulta de conteo de registros
          v_consulta:='select count(afil.id_afiliado)
					    from afi.tafiliado afil
					    inner join segu.tusuario usu1 on usu1.id_usuario = afil.id_usuario_reg
									INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
									LEFT join  afi.tafiliado af on af.id_afiliado = afil.id_patrocinador
									left join segu.tusuario usu2 on usu2.id_usuario = afil.id_usuario_mod

					    where ';

          --Definicion de la respuesta
          v_consulta:=v_consulta || v_parametros.filtro;

          --Devuelve la respuesta
          RETURN v_consulta;

        END;

    /*********************************
   #TRANSACCION:  'AFI_AFIRED_SEL'
   #DESCRIPCION:	red todos
   #AUTOR:		admin
   #FECHA:		02-04-2015 01:06:18
  ***********************************/

    ELSIF (p_transaccion = 'AFI_AFIRED_SEL')
      THEN

        BEGIN


          select afil.id_afiliado
          into v_id_afiliado
          FROM afi.tafiliado afil
          INNER JOIN segu.tusuario usu on usu.id_persona = afil.id_persona
          where usu.id_usuario = p_id_usuario;

          --Sentencia de la consulta de conteo de registros
          v_consulta:='

                    WITH RECURSIVE t(nivel, id_afiliado,id_patrocinador) AS (
                      SELECT
                        1 AS nivel,
                        afil.id_afiliado,
                        afil.id_patrocinador
                      from afi.tafiliado afil
                      WHERE afil.id_afiliado = '||v_id_afiliado||'

                      UNION ALL

                      SELECT
                        nivel + 1,
                        afil.id_afiliado,
                        afil.id_patrocinador
                      FROM afi.tafiliado afil
                        INNER JOIN t t on t.id_afiliado = afil.id_patrocinador

                    )
                    SELECT
                      t.nivel::integer,
                      --t.id_afiliado,
                      --t.id_patrocinador,
                      perafi.nombre_completo1 as nombre_afiliado,
                      afil.codigo,
                      afilpatro.codigo as codigo_patrocinador

                    FROM t
                      inner join afi.tafiliado afil on afil.id_afiliado = t.id_afiliado
                      inner join segu.vpersona perafi on perafi.id_persona = afil.id_persona
                      left join afi.tafiliado afilpatro on afilpatro.id_afiliado = afil.id_patrocinador
                    ORDER BY nivel,afil.id_patrocinador

          ';


          --Devuelve la respuesta
          RETURN v_consulta;

        END;


    /*********************************
     #TRANSACCION:  'AFI_OFINET_SEL'
     #DESCRIPCION:	DATOS PARA LA OFICINA VIRTUAL POR USUARIO
     #AUTOR:		admin
     #FECHA:		23-07-2019 18:04:19
    ***********************************/

    elsif(p_transaccion='AFI_OFINET_SEL')then

      begin

        if v_parametros.id_periodo is null THEN
          v_rec_gestion_periodo = param.f_get_periodo_gestion(to_char(now(), 'YYYY-mm-dd')::DATE);

          ELSE
            select v_parametros.id_periodo as po_id_periodo INTO v_rec_gestion_periodo;
        END IF;



        select afil.id_afiliado
        into v_id_afiliado
        FROM afi.tafiliado afil
        INNER JOIN segu.tusuario usu on usu.id_persona = afil.id_persona
        where usu.id_usuario = p_id_usuario;



        select true as pago_conf into v_pago_conf from afi.tpago_conf
        where id_periodo = v_rec_gestion_periodo.po_id_periodo;

        IF v_pago_conf is null or NOT EXISTS (select 1 from afi.tafiliado_pago_periodo
            where id_afiliado = v_id_afiliado and id_periodo = v_rec_gestion_periodo.po_id_periodo) then -- si aun no se ha configurado un periodo listo para pagar entonces se volvera a generar el afiliado pago periodo

            select app.estado INTO v_estado from afi.tafiliado_pago_periodo app
            where app.id_periodo = v_rec_gestion_periodo.po_id_periodo and app.id_afiliado = v_id_afiliado;

            if v_estado != 'pagado' or v_estado is NULL then

                SELECT json_strip_nulls(json_build_object(
                        'id_afiliado', v_id_afiliado,
                        'id_periodo', v_rec_gestion_periodo.po_id_periodo,
                        'from_gen', 'N'
                    ))
                INTO v_json_params_to_send;

                v_resp_json:= afi.f_generar_afiliado_pago_periodo(v_json_params_to_send);



            END IF ;


        END IF;








        --Sentencia de la consulta de conteo de registros
        v_consulta:='

          WITH datos_afiliado AS
          (
              select
                afiliado.id_afiliado,
                afiliado.direccion_principal,
                afiliado.lugar_nacimiento,
                afiliado.fecha_nacimiento,
                usuario.cuenta,
                persona.nombre_completo2 as desc_person,
                persona.ci,
                patrocinador.codigo as codigo_patrocinador,
                afiliado.codigo as codigo
              from afi.tafiliado afiliado
                inner join segu.tusuario usuario on usuario.id_persona = afiliado.id_persona
                INNER JOIN segu.vpersona persona on persona.id_persona = afiliado.id_persona
                INNER join  afi.tafiliado patrocinador on patrocinador.id_afiliado = afiliado.id_patrocinador
              where usuario.id_usuario = '||p_id_usuario||'
          ), pago_periodo AS
          (
            select
              afpape.*,
              afil.codigo,
              person.nombre_completo2,
              per.periodo,
              ges.gestion,
              array_to_string(afpape.porcentajes_pagados, '','', ''*'') as porcentajes_pagados
            from afi.tafiliado_pago_periodo afpape
              inner join afi.tafiliado afil on afil.id_afiliado = afpape.id_afiliado
              inner join segu.vpersona person on person.id_persona = afil.id_persona
              inner join segu.tusuario usuario on usuario.id_persona = afil.id_persona
              inner join param.tperiodo per on per.id_periodo = afpape.id_periodo
              inner join param.tgestion ges on ges.id_gestion = per.id_gestion
            where usuario.id_usuario = '||p_id_usuario||' and per.id_periodo = '||v_rec_gestion_periodo.po_id_periodo||'
          ),rango as
          (
          select ran.nombre as rango,ranafi.id_afiliado FROM afi.trango_afiliado ranafi
          INNER JOIN afi.trango ran on ran.id_rango = ranafi.id_rango
          where ranafi.id_afiliado = '||v_id_afiliado||' and ranafi.id_periodo = '||v_rec_gestion_periodo.po_id_periodo||'
          ),
          periodos_disponibles as
          (
            select
              per.id_periodo,
              ges.gestion,
              pxp.f_obtener_literal_periodo(per.periodo,0) as periodo
            from param.tperiodo per
            INNER JOIN param.tgestion ges on ges.id_gestion = per.id_gestion
            WHERE per.fecha_ini >= (now() - INTERVAL ''4 MONTH'')::date
            and per.fecha_fin <= (date_trunc(''month'', now()::date) + interval ''1 month'' - interval ''1 day'')::date
            order by per.id_periodo desc

          ),
          habilitado_a_cobrar_paquete as (
                select tbc.bonus
                from afi.tpuntos_afiliado tpa
                inner join afi.tpunto_item tpi on tpi.id_set = tpa.id_set_item
                inner join afi.tbonus_config tbc on tbc.id_bonus_config = tpi.id_bonus_config
                where tpa.id_afiliado = '||v_id_afiliado||'
                and tbc.bono_patrocinio_por_nivel is not null
                GROUP BY tbc.bonus

          ),
          bonus_patrocinio_detalle as (
                 select tb.tipo,
                        tb.monto,
                        tb.nivel_from,
                        tb.validado_para_pagar_a_patrocinador,
                        afiliado_hijo_directo.codigo as codigo_afiliado_hijo_directo,
                        afiliado_from.codigo as codigo_from,
                        persona_hijo_directo.nombre_completo2 as nombre_hijo_directo,
                        persona_from.nombre_completo2 as nombre_hijo_from,
                        tb.fecha_reg
                 from afi.tbonus tb
                 inner join afi.tbonus_config tbc on tbc.bonus = tb.tipo
                                                    and tbc.bonus != ''BONUS_REGISTRO''
                 inner join afi.tafiliado afiliado_hijo_directo on afiliado_hijo_directo.id_afiliado = tb.id_afiliado
                 inner join afi.tafiliado afiliado_from on afiliado_from.id_afiliado = tb.id_afiliado_from --aca no sale el bono registro por que el id_afiliado_from tiene que ser not null
                 inner join segu.vpersona persona_hijo_directo on persona_hijo_directo.id_persona = afiliado_hijo_directo.id_persona
                 inner join segu.vpersona persona_from on persona_from.id_persona = afiliado_from.id_persona
                 where tb.id_patrocinador = '||v_id_afiliado||'
                 and id_periodo = '||v_rec_gestion_periodo.po_id_periodo||'
                 ORDER BY tb.id_afiliado, tb.nivel_from ASC

             ),
             bonus_patrocinio_totales as (
                 select bpd.tipo,bpd.validado_para_pagar_a_patrocinador, sum(bpd.monto)
                 from bonus_patrocinio_detalle bpd
                 GROUP BY bpd.tipo, bpd.validado_para_pagar_a_patrocinador

             ),
             bono_presentador as (
                 select COALESCE(sum(monto), 0) as total
                 from afi.tafiliado_presentador tap
                 where tap.id_presentador = '||v_id_afiliado||' and tap.id_periodo = '||v_rec_gestion_periodo.po_id_periodo||'

             )
          select to_json(array_to_json(array_agg(jsonData)) :: text) #>> ''{}'' as json
          from
            (
              select
                da.direccion_principal,
                da.lugar_nacimiento,
                da.fecha_nacimiento,
                da.cuenta,
                da.desc_person,
                da.ci,
                da.codigo_patrocinador,
                da.codigo,
                ran.rango,
                (
                  select array_to_json(array_agg(row_to_json(pp))) as pago_periodo
                  from (
                         select * from pago_periodo
                       ) pp
                ),
                 (
                  select array_to_json(array_agg(row_to_json(pp))) as periodos_disponibles
                  from (
                         select * from periodos_disponibles
                       ) pp
                ),
                 (
                  select array_to_json(array_agg(row_to_json(hacp))) as habilitado_a_cobrar_paquete
                  from (
                         select * from habilitado_a_cobrar_paquete
                       ) hacp
                ),
                 (
                  select array_to_json(array_agg(row_to_json(bpd))) as bonus_patrocinio_detalle
                  from (
                         select * from bonus_patrocinio_detalle
                       ) bpd
                ),
                 (
                  select array_to_json(array_agg(row_to_json(bpt))) as bonus_patrocinio_totales
                  from (
                         select * from bonus_patrocinio_totales
                       ) bpt
                ),
                 (
                  select TO_JSON(bp) as bono_presentador
                  from (
                         select * from bono_presentador
                       ) bp
                )
              FROM datos_afiliado da
              inner join rango ran on ran.id_afiliado = da.id_afiliado
            ) jsonData


			';

        --raise exception '%',v_consulta;

        --Devuelve la respuesta
        return v_consulta;

      end;

    /*********************************
   #TRANSACCION:  'AFI_AFILPRE_SEL'
   #DESCRIPCION:	Consulta de datos
   #AUTOR:		admin
   #FECHA:		02-04-2020 01:06:18
  ***********************************/

    ELSIF (p_transaccion = 'AFI_AFILPRE_SEL')
    THEN

      BEGIN
        --Sentencia de la consulta
        v_consulta:='select per.id_persona, afil.id_afiliado as id_patrocinador, vper.nombre_completo2, vper.ci, afil.codigo
                    from segu.tpersona per
                      inner join segu.vpersona2 vper on vper.id_persona = per.id_persona
                    inner JOIN segu.tusuario usu on usu.id_usuario = per.id_usuario_reg
                    INNER JOIN afi.tafiliado afil on afil.id_persona = usu.id_persona
                    where  ';




        --Definicion de la respuesta
        v_consulta:=v_consulta || v_parametros.filtro;
        v_consulta:= v_consulta || ' order by ' || v_parametros.ordenacion || ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

        --Devuelve la respuesta
        RETURN v_consulta;

      END;

      /*********************************
       #TRANSACCION:  'AFI_AFILPRE_CONT'
       #DESCRIPCION:	Conteo de registros
       #AUTOR:		admin
       #FECHA:		02-04-2020 01:06:18
      ***********************************/

    ELSIF (p_transaccion = 'AFI_AFILPRE_CONT')
      THEN

        BEGIN
          --Sentencia de la consulta de conteo de registros
          v_consulta:='select count(per.id_persona)
                    from segu.tpersona per
                      inner join segu.vpersona2 vper on vper.id_persona = per.id_persona
                    inner JOIN segu.tusuario usu on usu.id_usuario = per.id_usuario_reg
                    INNER JOIN afi.tafiliado afil on afil.id_persona = usu.id_persona
            where ';

          --Definicion de la respuesta
          v_consulta:=v_consulta || v_parametros.filtro;

          --Devuelve la respuesta
          RETURN v_consulta;

        END;


      ELSE

      RAISE EXCEPTION 'Transaccion inexistente';

    END IF;

    EXCEPTION

    WHEN OTHERS THEN
      v_resp='';
      v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', SQLERRM);
      v_resp = pxp.f_agrega_clave(v_resp, 'codigo_error', SQLSTATE);
      v_resp = pxp.f_agrega_clave(v_resp, 'procedimientos', v_nombre_funcion);
      RAISE EXCEPTION '%', v_resp;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;
