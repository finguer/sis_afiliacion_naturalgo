CREATE OR REPLACE FUNCTION "afi"."ft_bonus_config_ime" (    
                p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$

/**************************************************************************
 SISTEMA:        AFILIACION
 FUNCION:         afi.ft_bonus_config_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tbonus_config'
 AUTOR:          (admin)
 FECHA:            22-11-2021 18:49:07
 COMENTARIOS:    
***************************************************************************
 HISTORIAL DE MODIFICACIONES:
#ISSUE                FECHA                AUTOR                DESCRIPCION
 #0                22-11-2021 18:49:07    admin             Creacion    
 #
 ***************************************************************************/

DECLARE

    v_nro_requerimiento        INTEGER;
    v_parametros               RECORD;
    v_id_requerimiento         INTEGER;
    v_resp                     VARCHAR;
    v_nombre_funcion           TEXT;
    v_mensaje_error            TEXT;
    v_id_bonus_config    INTEGER;
                
BEGIN

    v_nombre_funcion = 'afi.ft_bonus_config_ime';
    v_parametros = pxp.f_get_record(p_tabla);

    /*********************************    
     #TRANSACCION:  'AFI_BONCON_INS'
     #DESCRIPCION:    Insercion de registros
     #AUTOR:        admin    
     #FECHA:        22-11-2021 18:49:07
    ***********************************/

    IF (p_transaccion='AFI_BONCON_INS') THEN
                    
        BEGIN
            --Sentencia de la insercion
            INSERT INTO afi.tbonus_config(
            estado_reg,
            bonus,
            monto,
            id_usuario_reg,
            fecha_reg,
            id_usuario_ai,
            usuario_ai,
            id_usuario_mod,
            fecha_mod
              ) VALUES (
            'activo',
            v_parametros.bonus,
            v_parametros.monto,
            p_id_usuario,
            now(),
            v_parametros._id_usuario_ai,
            v_parametros._nombre_usuario_ai,
            null,
            null            
            ) RETURNING id_bonus_config into v_id_bonus_config;
            
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Bonus Config almacenado(a) con exito (id_bonus_config'||v_id_bonus_config||')'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_bonus_config',v_id_bonus_config::varchar);

            --Devuelve la respuesta
            RETURN v_resp;

        END;

    /*********************************    
     #TRANSACCION:  'AFI_BONCON_MOD'
     #DESCRIPCION:    Modificacion de registros
     #AUTOR:        admin    
     #FECHA:        22-11-2021 18:49:07
    ***********************************/

    ELSIF (p_transaccion='AFI_BONCON_MOD') THEN

        BEGIN
            --Sentencia de la modificacion
            UPDATE afi.tbonus_config SET
            bonus = v_parametros.bonus,
            monto = v_parametros.monto,
            id_usuario_mod = p_id_usuario,
            fecha_mod = now(),
            id_usuario_ai = v_parametros._id_usuario_ai,
            usuario_ai = v_parametros._nombre_usuario_ai
            WHERE id_bonus_config=v_parametros.id_bonus_config;
               
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Bonus Config modificado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_bonus_config',v_parametros.id_bonus_config::varchar);
               
            --Devuelve la respuesta
            RETURN v_resp;
            
        END;

    /*********************************    
     #TRANSACCION:  'AFI_BONCON_ELI'
     #DESCRIPCION:    Eliminacion de registros
     #AUTOR:        admin    
     #FECHA:        22-11-2021 18:49:07
    ***********************************/

    ELSIF (p_transaccion='AFI_BONCON_ELI') THEN

        BEGIN
            --Sentencia de la eliminacion
            DELETE FROM afi.tbonus_config
            WHERE id_bonus_config=v_parametros.id_bonus_config;
               
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Bonus Config eliminado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_bonus_config',v_parametros.id_bonus_config::varchar);
              
            --Devuelve la respuesta
            RETURN v_resp;

        END;
         
    ELSE
     
        RAISE EXCEPTION 'Transaccion inexistente: %',p_transaccion;

    END IF;

EXCEPTION
                
    WHEN OTHERS THEN
        v_resp='';
        v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
        v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
        v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
        raise exception '%',v_resp;
                        
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "afi"."ft_bonus_config_ime"(integer, integer, character varying, character varying) OWNER TO postgres;
