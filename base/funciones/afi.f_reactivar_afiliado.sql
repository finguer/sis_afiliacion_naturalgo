CREATE OR REPLACE FUNCTION afi.f_reactivar_afiliado(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE


    p_id_afiliado                INTEGER DEFAULT p_params ->> 'id_afiliado';
    p_id_set_item                INTEGER DEFAULT p_params ->> 'id_set_item';
    p_id_punto_venta             INTEGER DEFAULT p_params ->> 'id_punto_venta';
    p_id_moneda                  INTEGER DEFAULT p_params ->> 'id_moneda';
    p_precio                     numeric DEFAULT p_params ->> 'precio';
    v_id_afiliado                integer DEFAULT p_params ->> 'id_afiliado';
    p_id_usuario                 integer DEFAULT p_params ->> 'userId';
    p_is_admin                   integer DEFAULT p_params ->> 'isAdmin';
    p_items                      json DEFAULT p_params ->> 'items';
    v_resp                       json;
    v_nombre_funcion             TEXT;
    v_puntos_item                RECORD;
    v_return_movimiento_item_set BOOL;
    v_tipo                       VARCHAR;
    v_rec                        RECORD;
    v_id_almacen                 INTEGER;
    v_id_rango                   INTEGER;
    v_nivel_rango_paquete        INTEGER;
    v_id_cliente                 INTEGER;
    v_tipo_venta                 varchar;
    v_nro_venta                  VARCHAR;
    v_id_sucursal                INTEGER;
    v_suc                        VARCHAR;
    v_punto                      VARCHAR;
    v_numero_correlativo         INTEGER;
    v_id_venta                   INTEGER;

    v_json_params_to_send        json;
    v_res_insertar_bonus_red     json;
    v_id_usuario_ai              integer DEFAULT NULL;
    v_usuario_ai                 varchar DEFAULT 'NULL';
    v_records_items              record;
    v_record_afiliado record;
    v_cantidad_para_validar integer DEFAULT 0;

    v_cv_para_puntos numeric(10,2);
    v_item_ids INTEGER[];

BEGIN


    v_nombre_funcion = 'afi.f_reactivar_afiliado';

    v_rec = param.f_get_periodo_gestion(to_char(now(), 'YYYY-mm-dd')::DATE);


    select ta.id_afiliado, ta.codigo, tra.id_rango, tr.nivel
    into v_record_afiliado
    from afi.tafiliado ta
    inner join afi.trango_afiliado tra on tra.id_afiliado = ta.id_afiliado and id_periodo = v_rec.po_id_periodo::INTEGER
    inner join afi.trango tr on tr.id_rango = tra.id_rango
    where ta.id_afiliado = p_id_afiliado;


    if v_record_afiliado is null then
        raise exception '%', 'NO PUEDES VENDERLE POR QUE NO TIENE UN RANGO ASIGNADO EN EL PERIODO ACTUAL';
    end if;


    SELECT id_almacen
    INTO v_id_almacen
    FROM su.tpunto_venta_almacen
    WHERE id_punto_venta = p_id_punto_venta;

    SELECT array_agg(j ->> 'id_item') as ids
    into v_item_ids
    FROM json_array_elements(p_items) j;


    IF exists(select 1 from vent.titem_no_permitido_set
              where id_set_item = p_id_set_item and id_item = ANY(v_item_ids)) THEN
        RAISE EXCEPTION 'ERROR EXISTEN ALGUNOS PRODUCTOS NO PERMITIDOS PARA ESTE SET';
    END IF;

    FOR v_records_items IN (SELECT j ->> 'cantidad' AS cantidad, j ->> 'id_item' AS id_item
                            FROM json_array_elements(p_items) j)
        LOOP

        v_cantidad_para_validar := v_cantidad_para_validar + v_records_items.cantidad::integer;
            v_return_movimiento_item_set = vent.movimiento_almacen_producto(
                    v_usuario_ai,
                    p_id_usuario,
                    v_id_usuario_ai,
                    'SIN SERIAL',
                    cast(v_records_items.id_item AS integer),
                    p_id_punto_venta,
                    cast(v_records_items.cantidad AS integer),
                    NULL,
                    v_id_almacen
                );


        END LOOP;


    v_tipo := 'SET';


    SELECT pu.id_sucursal,
           pu.codigo,
           su.codigo,
           pu.numero_correlativo
    INTO v_id_sucursal, v_punto, v_suc, v_numero_correlativo
    FROM su.tpunto_venta pu
             INNER JOIN su.tsucursal su ON su.id_sucursal = pu.id_sucursal
    WHERE pu.id_punto_venta = p_id_punto_venta
    FOR UPDATE;


    --datos para la venta al afiliar debe ser como una venta de un producto
    --id_tipo_venta 1 v_tipo_venta tipo de venta recibo

    v_tipo_venta = 'recibo';

    v_nro_venta := 'VEN-' || v_suc || '-' || v_punto || '-' || v_numero_correlativo;




    -- obtener el id cliente del afiliado
    select id_cliente
    into v_id_cliente
    from vent.tcliente
    where codigo = v_record_afiliado.codigo;

              --Sentencia de la insercion
    INSERT INTO vent.tventa (id_cliente,
                             id_tipo_venta,
                             nro_venta,
                             estado_reg,
                             estado,
                             id_punto_venta,
                             tipo_pago,
                             fecha,
                             usuario_ai,
                             fecha_reg,
                             id_usuario_reg,
                             id_usuario_ai,
                             fecha_mod,
                             id_usuario_mod,
                             id_periodo,
                             id_moneda)
    VALUES (v_id_cliente,
            1,
            v_nro_venta,
            'activo',
            'pagado',
            p_id_punto_venta,
            'Efectivo',
            now()::DATE,
            v_usuario_ai,
            now(),
            p_id_usuario,
            v_id_usuario_ai,
            NULL,
            NULL,
            v_rec.po_id_periodo::INTEGER,
            p_id_moneda)
    RETURNING id_venta INTO v_id_venta;

    --aumentamos el numero correlativo
    UPDATE su.tpunto_venta
    SET numero_correlativo = numero_correlativo + 1
    WHERE id_punto_venta = p_id_punto_venta;




    --insercion de pago
    INSERT INTO vent.tpago(fecha,
                           monto_total,
                           tipo_pago,
                           estado_reg,
                           saldo,
                           cambio,
                           moneda,
                           id_venta,
                           pago,
                           id_usuario_ai,
                           id_usuario_reg,
                           usuario_ai,
                           fecha_reg,
                           id_usuario_mod,
                           fecha_mod,
                           id_moneda)
    VALUES (now(),
            p_precio,
            'Efectivo',
            'activo',
            0,
            0,
            '',
            v_id_venta,
            p_precio,
            v_id_usuario_ai,
            p_id_usuario,
            v_usuario_ai,
            now(),
            NULL,
            NULL,
            p_id_moneda);


    INSERT INTO vent.tventa_detalle (estado_reg,
                                     cantidad_producto,
                                     total,
                                     id_item,
                                     tipo,
                                     id_set,
                                     id_venta,
                                     precio_unitario,
                                     usuario_ai,
                                     fecha_reg,
                                     id_usuario_reg,
                                     id_usuario_ai,
                                     id_usuario_mod,
                                     fecha_mod,
                                     id_movimiento,
                                     id_almacen,
                                     data_json_items)
    VALUES ('activo',
            1,
            p_precio,
            NULL,
            v_tipo,
            p_id_set_item,
            v_id_venta,
            p_precio,
            v_usuario_ai,
            now(),
            p_id_usuario,
            v_id_usuario_ai,
            NULL,
            NULL,
            NULL,
            v_id_almacen,
            p_items);


    --agregar puntos por el paquete agregado al inscribirse
    SELECT pi.*,
           bc.bonus,
           bc.monto,
           bc.bono_patrocinio_por_nivel,
           ts.id_set_item,
           ts.precio AS precio_del_set,
           bc.calcular_por,
           pi.cv,
           pi.monto_bono_presentador,
           bc.asignar_id_rango_directo,
           ts.cantidad_de_productos,
           bc.cv_para_puntos_red
    INTO v_puntos_item
    FROM afi.tpunto_item pi
             LEFT JOIN afi.tbonus_config bc ON bc.id_bonus_config = pi.id_bonus_config
             INNER JOIN vent.tset_item ts ON ts.id_set_item = pi.id_set
    WHERE pi.id_set = p_id_set_item;


    if(v_puntos_item.cantidad_de_productos != v_cantidad_para_validar) then
        RAISE EXCEPTION '%','ERROR LOS PRODUCTOS CONFIGURADO PARA EL PAQUETE NO ES IGUAL AL QUE SE QUIERE VENDER';
    END IF;

    if v_puntos_item.cv_para_puntos_red is not NULL THEN
        v_cv_para_puntos:=v_puntos_item.cv_para_puntos_red;
    else
        v_cv_para_puntos:= v_puntos_item.cv;
    END IF;

    -- solo si tiene bonus relacionado dispara esto si no no
    IF v_puntos_item.id_bonus_config is not null then
        SELECT json_strip_nulls(json_build_object(
                'id_set_item', v_puntos_item.id_set_item,
                'id_afiliado', v_id_afiliado,
                'id_usuario', p_id_usuario,
                '_nombre_usuario_ai', 'NULL',
                '_id_usuario_ai', '',
                'po_id_periodo', v_rec.po_id_periodo,
                'id_venta', v_id_venta
            ))
        INTO v_json_params_to_send;

        v_res_insertar_bonus_red := afi.f_insertar_bonus_afiliado(v_json_params_to_send);

    END IF;



    --agregamos el rango al afiliado

    IF v_puntos_item.asignar_id_rango_directo IS NOT NULL THEN
        -- necesitamos verificar si el rango es menor al actual del afiliado
        -- en caso que sea inferior al actual debe mantener su actual

        select nivel
        INTO v_nivel_rango_paquete
        from afi.trango
        where id_rango = v_puntos_item.asignar_id_rango_directo;

        IF v_nivel_rango_paquete > v_record_afiliado.nivel then
            v_id_rango := v_puntos_item.asignar_id_rango_directo;

            delete from afi.trango_afiliado
            where id_afiliado = v_id_afiliado and id_periodo = v_rec.po_id_periodo;

            INSERT INTO afi.trango_afiliado (id_usuario_reg,
                                             id_usuario_mod,
                                             fecha_reg,
                                             fecha_mod,
                                             estado_reg,
                                             id_usuario_ai,
                                             usuario_ai,
                                             id_afiliado,
                                             id_rango,
                                             id_periodo)
            VALUES (p_id_usuario,
                    NULL,
                    now(),
                    NULL,
                    'activo',
                    v_id_usuario_ai,
                    v_usuario_ai,
                    v_id_afiliado,
                    v_id_rango,
                    v_rec.po_id_periodo);

        END IF;

    END IF;









    --agregamos los puntos al afiliado
    INSERT INTO afi.tpuntos_afiliado (estado_reg,
                                      id_set_item,
                                      id_periodo,
                                      cv,
                                      id_item,
                                      pv,
                                      fecha_reg,
                                      usuario_ai,
                                      id_usuario_reg,
                                      id_usuario_ai,
                                      id_usuario_mod,
                                      fecha_mod,
                                      id_afiliado, id_venta)
    VALUES ('activo',
            p_id_set_item,
            v_rec.po_id_periodo,
            v_cv_para_puntos,
            NULL,
            v_puntos_item.pv,
            now(),
            v_usuario_ai,
            p_id_usuario,
            v_id_usuario_ai,
            NULL,
            NULL,
            v_id_afiliado,
            v_id_venta --esto estaba null antes ahora se llena desde el 24 nov 2023 a las 12:34
            );




    WITH t_venta AS (
        SELECT tv.id_venta,
               tv.nro_venta,
               tv.fecha_reg,
               tv.id_usuario_reg,
               tu.cuenta  AS usuario_reg,
               tv.id_punto_venta,
               tpv.nombre AS desc_punto_venta,
               ta.nombre  AS desc_almacen,
               tc.codigo as codigo_cliente,
               tp.monto_total
        FROM vent.tventa tv
                 INNER JOIN segu.tusuario tu ON tu.id_usuario = id_usuario_reg
                 INNER JOIN su.tpunto_venta tpv ON tpv.id_punto_venta = tv.id_punto_venta
                 INNER JOIN su.tpunto_venta_almacen tpva ON tpva.id_punto_venta = tpv.id_punto_venta
                 INNER JOIN mer.talmacen ta ON ta.id_almacen = tpva.id_almacen
        inner join vent.tcliente tc on tc.id_cliente = tv.id_cliente
                 inner join vent.tpago tp on tp.id_venta = tv.id_venta

        WHERE tv.id_venta = v_id_venta
        LIMIT 1
    ),
         t_afiliado as (
             SELECT ta.codigo, vp.nombre_completo2
             from afi.tafiliado ta
             inner join segu.vpersona2 vp on vp.id_persona = ta.id_persona
             where id_afiliado = p_id_afiliado
         ),
         t_venta_detalle AS (
             SELECT tsi.nombre AS desc_paquete, tvd.data_json_items
             FROM vent.tventa_detalle tvd
                      INNER JOIN t_venta tv ON tv.id_venta = tvd.id_venta
                      INNER JOIN vent.tset_item tsi ON tsi.id_set_item = tvd.id_set
         ),
         t_items_seleccionados AS (
             SELECT j.json ->> 'id_item' AS id_item, j.json ->> 'cantidad' AS cantidad, ti.nombre AS desc_item
             FROM (
                      SELECT json_array_elements(data_json_items) AS json
                      FROM t_venta_detalle
                  ) AS j
                      INNER JOIN mer.titem ti ON ti.id_item = cast(j.json ->> 'id_item' AS integer)
         )
    SELECT to_json(jsonData) #>> '{}' AS json
    into v_resp
    FROM (
             SELECT *,
                    (SELECT to_json(tvd) FROM t_venta_detalle tvd)                                     AS venta_detalle,
                    (SELECT to_json(ta) FROM t_afiliado ta)                                     AS afiliado,
                    (SELECT array_to_json(array_agg(row_to_json(tis)))
                     FROM t_items_seleccionados tis)                                                   AS items_seleccionados
             FROM t_venta
         ) jsonData;


  /*  SELECT json_strip_nulls(json_build_object(
            'success', TRUE,
            'id_venta',v_id_venta
        ))
    INTO v_resp;*/
    RETURN v_resp;




EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
            ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;
        RETURN v_resp;


END ;
$BODY$
    LANGUAGE plpgsql VOLATILE;