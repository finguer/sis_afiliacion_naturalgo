CREATE OR REPLACE FUNCTION afi.ft_afiliado_ime(p_administrador INT4, p_id_usuario INT4, p_tabla VARCHAR,
                                               p_transaccion   VARCHAR)
  RETURNS VARCHAR
AS
  $BODY$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_afiliado_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tafiliado'
 AUTOR: 		 (admin)
 FECHA:	        02-04-2015 01:06:18
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

  DECLARE

    v_nro_requerimiento          INTEGER;
    v_parametros                 RECORD;
    v_id_requerimiento           INTEGER;
    v_resp                       VARCHAR;
    v_nombre_funcion             TEXT;
    v_mensaje_error              TEXT;
    v_id_afiliado                INTEGER;
    v_id_colocacion              INTEGER;
    v_count_colocacion           INTEGER;
    --v_codigo_siguiente           NUMERIC(16);
    v_id_codificacion            NUMERIC(16);

    v_padre                      INTEGER;
    v_abuelo                     INTEGER;

    v_return_movimiento_item_set BOOL;
    v_tipo                       VARCHAR;
    v_record                     RECORD;

    v_puntos_item                RECORD;
    v_rec                        RECORD;
    v_record_afi_patro                        RECORD;

    v_fecha_ini date;
    v_fecha_fin date;
    v_id_almacen INTEGER;
    v_id_rango INTEGER;
    v_id_cliente INTEGER;
    v_tipo_venta varchar;
    v_nro_venta VARCHAR;

      v_id_sucursal       INTEGER;
    v_suc               VARCHAR;
    v_punto             VARCHAR;
    v_numero_correlativo INTEGER;
    v_id_venta INTEGER;
    v_id_usuario INTEGER;
    v_id_persona INTEGER;
    v_ci segu.tpersona.ci%TYPE;
    v_codigo_siguiente NUMERIC(16);
    v_alias VARCHAR;
    v_pais_origen VARCHAR;
    v_text_json text;




    v_record_hijos record;
    v_count INTEGER;
    v_mensaje varchar;
    v_bonus_monto numeric(10,2);
    v_pagar_por_paquete numeric(10,2);
    v_monto_multiplicar NUMERIC;
    v_json_params_to_send json;
      v_res_insertar_bonus_red json;


  BEGIN


    v_nombre_funcion = 'afi.ft_afiliado_ime';
    v_parametros = pxp.f_get_record(p_tabla);

    /*********************************
     #TRANSACCION:  'AFI_AFIL_INS'
     #DESCRIPCION:	Insercion de registros
     #AUTOR:		admin
     #FECHA:		02-04-2015 01:06:18
    ***********************************/

    IF (p_transaccion = 'AFI_AFIL_INS')
    THEN

      BEGIN
        --Sentencia de la insercion



        --obtener codificacion para el numero
        SELECT cod.numero_siguiente, cod.alias, cod.pais
        INTO v_codigo_siguiente, v_alias, v_pais_origen
        FROM afi.tcodificacion cod
        WHERE cod.pais = v_parametros.pais
        LIMIT 1 FOR UPDATE ;

        IF v_parametros.pre_registro = 'si' THEN

          v_id_persona := v_parametros.id_persona_pre_registro;
        ELSE

          if exists(select 1 from segu.tpersona
          where ci = v_parametros.ci) then
            raise exception 'Este número de Carnet de Identidad ya fue registrado';
          end if;


          --nunca deberia entrar aca
          if exists(select 1 from afi.tafiliado
          where codigo = coalesce(v_alias, '') || v_codigo_siguiente::varchar
           ) then
            raise exception 'Este codigo ya fue registrado';
          end if;


          insert into segu.tpersona (
            nombre,
            apellido_paterno,
            apellido_materno,
            ci,
            correo,
            celular1,
            telefono1,
            telefono2,
            celular2,
            tipo_documento,
            expedicion
          )
          values(
            v_parametros.nombre,
            v_parametros.ap_paterno,
            v_parametros.ap_materno,
            v_parametros.ci,
            v_parametros.correo,
            v_parametros.celular1,
            v_parametros.telefono1,
            NULL ,
            NULL ,
            NULL,
            NULL
          )

          RETURNING id_persona INTO v_id_persona;

        END IF;






        INSERT INTO afi.tafiliado (
          direccion_principal,
          id_persona,
          lugar_nacimiento,
          estado_reg,
          fecha_nacimiento,
          id_usuario_ai,
          id_usuario_reg,
          fecha_reg,
          usuario_ai,
          id_usuario_mod,
          fecha_mod,
          id_patrocinador,
          codigo,

          modalidad_de_registro,
          estado,
          id_set_item,
          id_punto_venta,
                                   pais

        ) VALUES (
          v_parametros.direccion_principal,
          v_id_persona,
          v_parametros.lugar_nacimiento,
          'activo',
          v_parametros.fecha_nacimiento,
          v_parametros._id_usuario_ai,
          p_id_usuario,
          now(),
          v_parametros._nombre_usuario_ai,
          NULL,
          NULL,
          v_parametros.id_patrocinador,
          coalesce(v_alias, '') || v_codigo_siguiente::varchar,

          'activo',
          'activo',
          v_parametros.id_set_item,
          v_parametros.id_punto_venta,
                  v_parametros.pais


        )
        RETURNING id_afiliado
          INTO v_id_afiliado;




        v_fecha_fin=(date_trunc('MONTH', now()) + INTERVAL '1 MONTH - 1 day')::date;

        insert into afi.tafiliado_etapa(
          id_afiliado,
          fecha_fin,
          etapa,
          fecha_ini,
          estado_reg,
          id_usuario_ai,
          usuario_ai,
          fecha_reg,
          id_usuario_reg,
          fecha_mod,
          id_usuario_mod
        ) values(
          v_id_afiliado,
          v_fecha_fin,
          'inicio',
          now(),
          'activo',
          v_parametros._id_usuario_ai,
          v_parametros._nombre_usuario_ai,
          now(),
          p_id_usuario,
          null,
          null



        );


        FOR v_record
        IN (SELECT
              afil.*,
              person.nombre_completo2
            FROM afi.tafiliado afil
              INNER JOIN segu.vpersona2 person ON person.id_persona = afil.id_persona
            WHERE afil.id_afiliado = v_id_afiliado)
        LOOP

          INSERT INTO vent.tcliente (
            id_persona,
            estado_reg,
            nit,
            codigo,
            usuario_ai,
            fecha_reg,
            id_usuario_reg,
            id_usuario_ai,
            fecha_mod,
            id_usuario_mod,
            razon
          ) VALUES (
            v_record.id_persona,
            'activo',
            v_record.codigo,
            v_record.codigo,
            v_record.usuario_ai,
            now(),
            v_record.id_usuario_reg,
            v_record.id_usuario_ai,
            NULL,
            NULL,
            v_record.nombre_completo2

          ) RETURNING id_cliente
            INTO v_id_cliente;
        END LOOP;

        v_tipo := 'SET';



          SELECT
            pu.id_sucursal,
            pu.codigo,
            su.codigo,
            pu.numero_correlativo
          INTO v_id_sucursal, v_punto, v_suc, v_numero_correlativo
          FROM su.tpunto_venta pu
            INNER JOIN su.tsucursal su ON su.id_sucursal = pu.id_sucursal
          WHERE pu.id_punto_venta = v_parametros.id_punto_venta;

        SELECT id_almacen INTO v_id_almacen
        FROM su.tpunto_venta_almacen
        where id_punto_venta = v_parametros.id_punto_venta;

          --datos para la venta al afiliar debe ser como una venta de un producto
        --id_tipo_venta 1 v_tipo_venta tipo de venta recibo

          v_tipo_venta = 'recibo';

          v_nro_venta:='VENAFI-' || v_suc || '-' || v_punto || '-' || v_numero_correlativo;


        v_rec = param.f_get_periodo_gestion(to_char(now(), 'YYYY-mm-dd')::DATE);

        --RAISE EXCEPTION '%',v_nro_venta;

        --Sentencia de la insercion
        INSERT INTO vent.tventa (
          id_cliente,
          id_tipo_venta,
          nro_venta,
          estado_reg,
          estado,
          id_punto_venta,
          tipo_pago,
          fecha,
          usuario_ai,
          fecha_reg,
          id_usuario_reg,
          id_usuario_ai,
          fecha_mod,
          id_usuario_mod,
          id_periodo,
          id_moneda
        ) VALUES (
          v_id_cliente,
          1,
          v_nro_venta,
          'activo',
          'pagado',
          v_parametros.id_punto_venta,
          'Efectivo',
          now()::DATE,
          v_parametros._nombre_usuario_ai,
          now(),
          p_id_usuario,
          v_parametros._id_usuario_ai,
          NULL,
          NULL,
          v_rec.po_id_periodo::INTEGER,
          v_parametros.id_moneda



        ) RETURNING id_venta
          INTO v_id_venta;

        --aumentamos el numero correlativo
        UPDATE su.tpunto_venta set numero_correlativo = numero_correlativo + 1
        where id_punto_venta = v_parametros.id_punto_venta;



        --insercion de pago
        insert into vent.tpago(
          fecha,
          monto_total,
          tipo_pago,
          estado_reg,
          saldo,
          cambio,
          moneda,
          id_venta,
          pago,
          id_usuario_ai,
          id_usuario_reg,
          usuario_ai,
          fecha_reg,
          id_usuario_mod,
          fecha_mod,
          id_moneda
        ) values(
          now(),
          v_parametros.monto_registro,
          'Efectivo',
          'activo',
          0,
          0,
          '',
          v_id_venta,
          v_parametros.monto_registro,
          v_parametros._id_usuario_ai,
          p_id_usuario,
          v_parametros._nombre_usuario_ai,
          now(),
          null,
          null,
          v_parametros.id_moneda
        );


        INSERT INTO vent.tventa_detalle (
          estado_reg,
          cantidad_producto,
          total,
          id_item,
          tipo,
          id_set,
          id_venta,
          precio_unitario,
          usuario_ai,
          fecha_reg,
          id_usuario_reg,
          id_usuario_ai,
          id_usuario_mod,
          fecha_mod,
          id_movimiento,
          id_almacen

        ) VALUES (
          'activo',
          1,
          v_parametros.monto_registro,
          NULL ,
          v_tipo,
          v_parametros.id_set_item,
          v_id_venta,
          v_parametros.monto_registro,
          v_parametros._nombre_usuario_ai,
          now(),
          p_id_usuario,
          v_parametros._id_usuario_ai,
          NULL,
          NULL,
          NULL ,
          v_id_almacen


        );



          v_return_movimiento_item_set = vent.movimiento_almacen_producto(
            v_parametros._nombre_usuario_ai,
            p_id_usuario,
            v_parametros._id_usuario_ai,
            v_tipo,
            v_parametros.id_set_item,
            v_parametros.id_punto_venta,
            1 :: INTEGER,
            NULL ,
            v_id_almacen

        );



        --agregar puntos por el paquete agregado al inscribirse
        SELECT pi.*,
               bc.bonus,
               bc.monto,
               bc.bono_patrocinio_por_nivel,
               ts.id_set_item,
               ts.precio AS precio_del_set,
               bc.calcular_por,
               pi.cv,
               bc.asignar_id_rango_directo
        INTO v_puntos_item
        FROM afi.tpunto_item pi
                 INNER JOIN afi.tbonus_config bc ON bc.id_bonus_config = pi.id_bonus_config
                 INNER JOIN vent.tset_item ts ON ts.id_set_item = pi.id_set
        WHERE pi.id_set = v_parametros.id_set_item;

        IF v_puntos_item.asignar_id_rango_directo is not null then
            v_id_rango := v_puntos_item.asignar_id_rango_directo;
        else
            SELECT id_rango
            INTO v_id_rango
            FROM afi.trango
            WHERE id_rango_fk IS NULL;
        END IF;




        SELECT json_strip_nulls(json_build_object(
                'id_set_item', v_puntos_item.id_set_item,
                'id_afiliado', v_id_afiliado,
                'id_usuario', p_id_usuario,
                '_nombre_usuario_ai', 'NULL',
                '_id_usuario_ai', '',
                'po_id_periodo', v_rec.po_id_periodo
            ))
        INTO v_json_params_to_send;

        v_res_insertar_bonus_red := afi.f_insertar_bonus_afiliado(v_json_params_to_send);






     


        INSERT INTO afi.trango_afiliado (id_usuario_reg,
                                         id_usuario_mod,
                                         fecha_reg,
                                         fecha_mod,
                                         estado_reg,
                                         id_usuario_ai,
                                         usuario_ai,
                                         id_afiliado,
                                         id_rango,
                                         id_periodo) VALUES (p_id_usuario,
                                                             NULL,
                                                             now(),
                                                             NULL,
                                                             'activo',
                                                             v_parametros._id_usuario_ai,
                                                             v_parametros._nombre_usuario_ai,
                                                             v_id_afiliado,
                                                             v_id_rango,
                                                             v_rec.po_id_periodo);





        --agregamos los puntos al afiliado
        INSERT INTO afi.tpuntos_afiliado (
          estado_reg,
          id_set_item,
          id_periodo,
          cv,
          id_item,
          pv,
          fecha_reg,
          usuario_ai,
          id_usuario_reg,
          id_usuario_ai,
          id_usuario_mod,
          fecha_mod,
            id_afiliado
        ) VALUES (
          'activo',
          v_parametros.id_set_item,
          v_rec.po_id_periodo,
          v_puntos_item.cv,
          null,
          v_puntos_item.pv,
          now(),
          v_parametros._nombre_usuario_ai,
          p_id_usuario,
          v_parametros._id_usuario_ai,
          NULL,
          NULL,
            v_id_afiliado
        );


        v_padre = afi.f_ver_patrocinador(v_id_afiliado);
        v_abuelo = afi.f_ver_patrocinador(v_padre);


        --registramos usuario relacionado al afiliado personas

        select ci
        into v_ci
        from segu.tpersona
        WHERE id_persona = v_id_persona;

        INSERT INTO segu.tusuario (
          id_clasificador,
          cuenta,
          contrasena,
          fecha_caducidad,
          fecha_reg,
          estilo,
          contrasena_anterior,
          id_persona,
          estado_reg,
          autentificacion
        )
        VALUES (
          1,
          coalesce(v_alias, '') || v_codigo_siguiente::VARCHAR,
          md5(v_ci::VARCHAR),
          '2050-04-21',
          now(),
          'xtheme-gray.css',
          md5(v_ci::VARCHAR),
          v_id_persona,
          'activo',
          'local'
        ) RETURNING id_usuario into v_id_usuario;


        INSERT INTO segu.tusuario_rol ( id_rol, id_usuario, fecha_reg, estado_reg)
        VALUES (5, v_id_usuario, now(), 'activo');



        update afi.tcodificacion set numero_siguiente = v_codigo_siguiente + 1
        where numero_siguiente = v_codigo_siguiente AND pais = v_parametros.pais;



        --retornamos datos del afiliado registrado para que se imprima
        WITH afiliado_registrado AS
        (
            SELECT afiliado.codigo,
              afiliado.fecha_reg,
              persona.nombre_completo2,
              persona.ci,
              persona.celular1,
              persona.telefono1,
              persona.correo,
              patrocinador.codigo as codigo_patrocinador,
              persona_patrocinador.nombre_completo2 as nombre_patrocinador,
              venta.nro_venta,
              setitem.nombre as nombre_set,
              pago.monto_total
            FROM afi.tafiliado afiliado
              INNER JOIN afi.tafiliado patrocinador ON patrocinador.id_afiliado = afiliado.id_patrocinador
              INNER JOIN segu.vpersona2 persona ON persona.id_persona = afiliado.id_persona
              INNER JOIN segu.vpersona2 persona_patrocinador ON persona_patrocinador.id_persona = patrocinador.id_persona
              INNER JOIN vent.tcliente cliente ON cliente.id_persona = afiliado.id_persona
              INNER JOIN vent.tventa venta ON venta.id_cliente = cliente.id_cliente AND venta.nro_venta like '%VENAFI%'
              INNER JOIN vent.tventa_detalle detalle ON detalle.id_venta = venta.id_venta
              INNER JOIN vent.tset_item setitem ON setitem.id_set_item = detalle.id_set
              INNER JOIN vent.tpago pago ON pago.id_venta = venta.id_venta
            WHERE afiliado.id_afiliado = v_id_afiliado
        ) SELECT TO_JSON(ARRAY_TO_JSON(ARRAY_agg(jsonData)) :: TEXT) #>> '{}' AS JSON
        INTO v_text_json
          FROM
            (
              SELECT
                (
                  SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(ar))) as afiliado_registrado
                  FROM (
                         SELECT * from afiliado_registrado
                       ) ar
                )
            ) AS jsonData;





        --Definicion de la respuesta
        v_resp = pxp.f_agrega_clave(v_resp, 'mensaje',
                                    'Afiliado almacenado(a) con exito (id_afiliado' || v_id_afiliado || ')');
        v_resp = pxp.f_agrega_clave(v_resp, 'id_afiliado', v_id_afiliado :: VARCHAR);
        v_resp = pxp.f_agrega_clave(v_resp, 'json_res', v_text_json :: VARCHAR);

        --Devuelve la respuesta
        RETURN v_resp;

      END;

    /*********************************
     #TRANSACCION:  'AFI_AFIL_MOD'
     #DESCRIPCION:	Modificacion de registros
     #AUTOR:		admin
     #FECHA:		02-04-2015 01:06:18
    ***********************************/

    ELSIF (p_transaccion = 'AFI_AFIL_MOD')
      THEN

        BEGIN
          --Sentencia de la modificacion
          UPDATE afi.tafiliado
          SET
            direccion_principal = v_parametros.direccion_principal,
            id_persona          = v_parametros.id_persona,
            lugar_nacimiento    = v_parametros.lugar_nacimiento,
            fecha_nacimiento    = v_parametros.fecha_nacimiento,
            id_usuario_mod      = p_id_usuario,
            fecha_mod           = now(),
            id_usuario_ai       = v_parametros._id_usuario_ai,
            usuario_ai          = v_parametros._nombre_usuario_ai,
            id_patrocinador     = v_parametros.id_patrocinador
          WHERE id_afiliado = v_parametros.id_afiliado;

          --Definicion de la respuesta
          v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', 'Afiliado modificado(a)');
          v_resp = pxp.f_agrega_clave(v_resp, 'id_afiliado', v_parametros.id_afiliado :: VARCHAR);

          --Devuelve la respuesta
          RETURN v_resp;

        END;

    /*********************************
     #TRANSACCION:  'AFI_AFIL_ELI'
     #DESCRIPCION:	Eliminacion de registros
     #AUTOR:		admin
     #FECHA:		02-04-2015 01:06:18
    ***********************************/

    ELSIF (p_transaccion = 'AFI_AFIL_ELI')
      THEN

        BEGIN
            raise EXCEPTION '%', 'no puedes eliminar necesitas eliminar con compresion dinamica ya que una eliminacion podria conllevar a muchos errores';
          --Sentencia de la eliminacion
          DELETE FROM afi.tafiliado
          WHERE id_afiliado = v_parametros.id_afiliado;

          --Definicion de la respuesta
          v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', 'Afiliado eliminado(a)');
          v_resp = pxp.f_agrega_clave(v_resp, 'id_afiliado', v_parametros.id_afiliado :: VARCHAR);

          --Devuelve la respuesta
          RETURN v_resp;

        END;

    /*********************************
     #TRANSACCION:  'AFI_AFIL_COMD'
     #DESCRIPCION:	COMPRESION DINAMICA Y ELIMINAR AFILIADOS INACTIVOS
     #AUTOR:		admin
     #FECHA:		02-04-2015 01:06:18
    ***********************************/

    ELSIF (p_transaccion = 'AFI_AFIL_COMD')
      THEN

        BEGIN
          v_mensaje := '';

          --eliminar afiliados inactivos
          --recorremos todos los afiliados que ya no esten activos que yano compraron nada hace 6 meses
          FOR v_record IN (
            SELECT afil.id_afiliado,afil.id_patrocinador,afil.codigo,afil.id_persona,afil.codigo
            FROM afi.tafiliado afil
            where afil.id_afiliado not in (
              SELECT afil.id_afiliado
              FROM afi.tafiliado afil
                INNER JOIN afi.tpuntos_afiliado pa ON pa.id_afiliado = afil.id_afiliado
              WHERE pa.fecha_reg::date >= now()::date - INTERVAL '12 month'
              GROUP BY afil.id_afiliado
            )
            ORDER BY afil.id_afiliado DESC
           /* SELECT afil.id_afiliado,afil.id_patrocinador,afil.codigo,afil.id_persona,afil.codigo
            FROM afi.tafiliado afil where afil.codigo in ('591013997', '591014014')*/

          )
          LOOP

            v_mensaje := v_mensaje || 'hijos de :' || v_record.codigo ;

            FOR v_record_hijos IN (
              select * from afi.tafiliado where id_patrocinador = v_record.id_afiliado
            )LOOP
              v_mensaje := v_mensaje || v_record_hijos.id_afiliado;


              --modificamos sus hijos a su patrocinador del que eliminaremos
              UPDATE afi.tafiliado SET id_patrocinador = v_record.id_patrocinador
              WHERE id_afiliado = v_record_hijos.id_afiliado;



            END LOOP;
            v_mensaje := v_mensaje || '-------fin hijos ------';



            --eliminamos su cliente para compras
            SELECT id_cliente
            INTO v_id_cliente
            FROM vent.tcliente WHERE id_persona = v_record.id_persona;


            UPDATE vent.tventa SET id_cliente = NULL where id_cliente = v_id_cliente;
            DELETE FROM vent.tcliente where id_persona = v_record.id_persona;


            --eliminamos sus puntos
            DELETE FROM afi.tpuntos_afiliado where id_afiliado = v_record.id_afiliado;

            --eliminamos sus rangos por periodo
            DELETE FROM afi.trango_afiliado where id_afiliado = v_record.id_afiliado;

            --eliminamos su cuenta bancaria
            DELETE FROM afi.tcuenta_bancaria where id_afiliado = v_record.id_afiliado;

            --eliminamos pago periodo
            DELETE FROM afi.tafiliado_pago_periodo where id_afiliado = v_record.id_afiliado;

            -- eliminamos los bonus que generan multinivel
            delete from afi.tbonus where id_afiliado_from = v_record.id_afiliado;

            --eliminamos su afiliado
            DELETE FROM afi.tafiliado WHERE id_afiliado = v_record.id_afiliado;



            --eliminamos su usuario y persona
            select count(*)
            INTO v_count
            FROM segu.tusuario WHERE id_persona = v_record.id_persona;

            IF v_count = 1 THEN
              select id_usuario
              INTO v_id_usuario
              FROM segu.tusuario WHERE id_persona = v_record.id_persona;

              IF v_id_usuario is not NULL THEN
                DELETE FROM segu.tusuario_rol where id_usuario = v_id_usuario;
                DELETE FROM segu.tusuario where id_usuario = v_id_usuario;
              END IF;

              DELETE FROM segu.tpersona where id_persona = v_record.id_persona;

            END IF;




          END LOOP;



          --Definicion de la respuesta
          v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', 'compresion dinamica ejecutado');
          v_resp = pxp.f_agrega_clave(v_resp, 'mensaje_res', v_mensaje :: VARCHAR);

          --Devuelve la respuesta
          RETURN v_resp;

        END;

    /*********************************
    #TRANSACCION:  'AFI_AFIL_PREREG'
    #DESCRIPCION:	PRE REGISTRO DE AFILIADOS - INSERT PERSONA
    #AUTOR:		admin
    #FECHA:		08-04-2020 01:06:18
   ***********************************/

    ELSIF (p_transaccion = 'AFI_AFIL_PREREG')
      THEN

        BEGIN


          --Verificación de persona ya registrada
          --CI
          if exists(select 1 from segu.tpersona
          where ci = v_parametros.personaJson::JSON->>'ci') then
            raise exception 'Este número de Carnet de Identidad ya fue registrado';
          end if;

          --Nombre completo
          if exists(select 1 from segu.tpersona
          where upper(nombre) = upper(v_parametros.personaJson::JSON->>'nombre')
                and upper(apellido_paterno) = upper(v_parametros.personaJson::JSON->>'ap_paterno')
                and upper(apellido_materno) = upper(v_parametros.personaJson::JSON->>'ap_materno')) then
            raise exception 'Persona ya registrada';
          end if;


          insert into segu.tpersona (
            nombre,
            apellido_paterno,
            apellido_materno,
            ci,
            correo,
            celular1,
            telefono1,
            telefono2,
            celular2,
            tipo_documento,
            expedicion,
            fecha_reg,
            id_usuario_reg
          )
          values(
            upper(v_parametros.personaJson::JSON->>'nombre'),
            upper(v_parametros.personaJson::JSON->>'ap_paterno'),
            upper(v_parametros.personaJson::JSON->>'ap_materno'),
            v_parametros.personaJson::JSON->>'ci',
            null,
            v_parametros.personaJson::JSON->>'telefono',
            v_parametros.personaJson::JSON->>'telefono',
            null,
            null,
            'documento_identidad',
            NULL,
            now(),
            p_id_usuario
          )

          RETURNING id_persona INTO v_id_persona;


          --Definicion de la respuesta
          v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', 'PRE REGISTRO correcto');

          --Devuelve la respuesta
          RETURN v_resp;

        END;



    ELSE

      RAISE EXCEPTION 'Transaccion inexistente: %', p_transaccion;

    END IF;

    EXCEPTION

    WHEN OTHERS THEN
      v_resp='';
      v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', SQLERRM);
      v_resp = pxp.f_agrega_clave(v_resp, 'codigo_error', SQLSTATE);
      v_resp = pxp.f_agrega_clave(v_resp, 'procedimientos', v_nombre_funcion);
      RAISE EXCEPTION '%', v_resp;

  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;
