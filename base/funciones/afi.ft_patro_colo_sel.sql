CREATE OR REPLACE FUNCTION afi.ft_patro_colo_sel (
  p_administrador integer,
  p_id_usuario integer,
  p_tabla varchar,
  p_transaccion varchar
)
RETURNS varchar AS
$body$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_patro_colo_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'afi.tpatro_colo'
 AUTOR: 		 (admin)
 FECHA:	        04-04-2015 15:41:13
 COMENTARIOS:	
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:	
 AUTOR:			
 FECHA:		
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;
			    
BEGIN

	v_nombre_funcion = 'afi.ft_patro_colo_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************    
 	#TRANSACCION:  'AFI_COLO_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin	
 	#FECHA:		04-04-2015 15:41:13
	***********************************/

	if(p_transaccion='AFI_COLO_SEL')then
     				
    	begin
    		--Sentencia de la consulta
			v_consulta:='select
						colo.id_patro_colo,
						colo.derecha,
						colo.izquierda,
						colo.estado_reg,
						colo.id_colocacion,
						colo.id_usuario_ai,
						colo.id_usuario_reg,
						colo.fecha_reg,
						colo.usuario_ai,
						colo.id_usuario_mod,
						colo.fecha_mod,
						usu1.cuenta as usr_reg,
						usu2.cuenta as usr_mod,
							PERSON.nombre_completo2 as desc_person,
						PERSON.ci,
						afil.codigo,
						af.codigo as codigo_izquierda,
						af2.codigo as codigo_derecha,
                       
             PERSON.correo,
                  PERSON.telefono1,
                  PERSON.celular1,
                   afil.colocacion,
                   afil.id_patrocinador
						from afi.tpatro_colo colo
						inner join segu.tusuario usu1 on usu1.id_usuario = colo.id_usuario_reg
						INNER JOIN afi.tafiliado afil on afil.id_afiliado = colo.id_colocacion
						INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
						left join afi.tafiliado af on af.id_afiliado = colo.izquierda
						left join afi.tafiliado af2 on af2.id_afiliado = colo.derecha
						left join segu.tusuario usu2 on usu2.id_usuario = colo.id_usuario_mod
				        where  ';
			
			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;
			v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

			--Devuelve la respuesta
			return v_consulta;
						
		end;

	/*********************************    
 	#TRANSACCION:  'AFI_COLO_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin	
 	#FECHA:		04-04-2015 15:41:13
	***********************************/

	elsif(p_transaccion='AFI_COLO_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(id_patro_colo)
					    from afi.tpatro_colo colo
					    inner join segu.tusuario usu1 on usu1.id_usuario = colo.id_usuario_reg
					    INNER JOIN afi.tafiliado afil on afil.id_afiliado = colo.id_colocacion
						INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
						left join afi.tafiliado af on af.id_afiliado = colo.izquierda
						left join afi.tafiliado af2 on af2.id_afiliado = colo.derecha
						left join segu.tusuario usu2 on usu2.id_usuario = colo.id_usuario_mod
					    where ';
			
			--Definicion de la respuesta		    
			v_consulta:=v_consulta||v_parametros.filtro;

			--Devuelve la respuesta
			return v_consulta;

		end;
					
	else
					     
		raise exception 'Transaccion inexistente';
					         
	end if;
					
EXCEPTION
					
	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;