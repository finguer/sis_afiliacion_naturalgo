CREATE OR REPLACE FUNCTION "afi"."ft_afiliado_etapa_ime" (	
				p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$

/**************************************************************************
 SISTEMA:		afiliacion
 FUNCION: 		afi.ft_afiliado_etapa_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tafiliado_etapa'
 AUTOR: 		 (admin)
 FECHA:	        04-01-2016 16:42:12
 COMENTARIOS:	
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:	
 AUTOR:			
 FECHA:		
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_afiliado_etapa	integer;
			    
BEGIN

    v_nombre_funcion = 'afi.ft_afiliado_etapa_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************    
 	#TRANSACCION:  'AFI_AFET_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin	
 	#FECHA:		04-01-2016 16:42:12
	***********************************/

	if(p_transaccion='AFI_AFET_INS')then
					
        begin
        	--Sentencia de la insercion
        	insert into afi.tafiliado_etapa(
			id_afiliado,
			fecha_fin,
			etapa,
			fecha_ini,
			estado_reg,
			id_usuario_ai,
			usuario_ai,
			fecha_reg,
			id_usuario_reg,
			fecha_mod,
			id_usuario_mod
          	) values(
			v_parametros.id_afiliado,
			v_parametros.fecha_fin,
			v_parametros.etapa,
			v_parametros.fecha_ini,
			'activo',
			v_parametros._id_usuario_ai,
			v_parametros._nombre_usuario_ai,
			now(),
			p_id_usuario,
			null,
			null
							
			
			
			)RETURNING id_afiliado_etapa into v_id_afiliado_etapa;
			
			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Etapa Afiliado almacenado(a) con exito (id_afiliado_etapa'||v_id_afiliado_etapa||')'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_afiliado_etapa',v_id_afiliado_etapa::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************    
 	#TRANSACCION:  'AFI_AFET_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin	
 	#FECHA:		04-01-2016 16:42:12
	***********************************/

	elsif(p_transaccion='AFI_AFET_MOD')then

		begin
			--Sentencia de la modificacion
			update afi.tafiliado_etapa set
			id_afiliado = v_parametros.id_afiliado,
			fecha_fin = v_parametros.fecha_fin,
			etapa = v_parametros.etapa,
			fecha_ini = v_parametros.fecha_ini,
			fecha_mod = now(),
			id_usuario_mod = p_id_usuario,
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai
			where id_afiliado_etapa=v_parametros.id_afiliado_etapa;
               
			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Etapa Afiliado modificado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_afiliado_etapa',v_parametros.id_afiliado_etapa::varchar);
               
            --Devuelve la respuesta
            return v_resp;
            
		end;

	/*********************************    
 	#TRANSACCION:  'AFI_AFET_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin	
 	#FECHA:		04-01-2016 16:42:12
	***********************************/

	elsif(p_transaccion='AFI_AFET_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from afi.tafiliado_etapa
            where id_afiliado_etapa=v_parametros.id_afiliado_etapa;
               
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Etapa Afiliado eliminado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_afiliado_etapa',v_parametros.id_afiliado_etapa::varchar);
              
            --Devuelve la respuesta
            return v_resp;

		end;
         
	else
     
    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION
				
	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;
				        
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "afi"."ft_afiliado_etapa_ime"(integer, integer, character varying, character varying) OWNER TO postgres;
