CREATE OR REPLACE FUNCTION afi.f_get_data_afiliado_periodo(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE


    v_nombre_funcion                varchar;
    p_id_afiliado                   integer DEFAULT p_params ->> 'id_afiliado';
    p_id_periodo                    integer DEFAULT p_params ->> 'id_periodo';
    v_records_items                 record;
    v_record_afiliado               record;
    v_res_regenerar_bono_patrocinio json;
    v_rec_periodo                   record;
    v_json_to_send                  json;
    v_resp                          json;
    v_return_movimiento_item_set    BOOL;
    v_id_almacen                    INTEGER;
    v_puntos_item                   record;
    v_empresa                       varchar;


BEGIN


    v_nombre_funcion = 'afi.f_get_data_afiliado_periodo';

    v_empresa := pxp.f_get_variable_global('empresa_afiliacion');

    WITH
        RECURSIVE
        t(nivel, id_afiliado) AS (
            SELECT 1 AS nivel,
                   afil.id_afiliado
            FROM afi.tafiliado afil
            WHERE id_patrocinador = p_id_afiliado

            UNION ALL
            SELECT nivel + 1,
                   afil.id_afiliado
            FROM afi.tafiliado afil
                     INNER JOIN t t ON t.id_afiliado = afil.id_patrocinador
        ),
        t_current_afiliado AS (
            SELECT ta.id_afiliado,
                   ta.id_patrocinador,
                   ta.estado_reg as estado_afiliado, --si esta inactivo el afiliado no podra cobrar
                   ta.photo,
                   ta.fecha_reg,
                   ran.id_rango,
                   ran.nombre        AS nombre_rango,
                   ran.nivel         AS nivel_rango,
                   ran.porcentajes,
                   ran.niveles_a_sumar_para_pv,
                   tra.id_periodo,
                   ran.volumen_personal,
                   ran.volumen_organizacional,
                   pais.codigo_area,
                   ta.pais,
                   tcp.cv_vale,
                   tcp.moneda,
                   tcp.cv_vale as monto_multiplicar,
                   CASE
                       WHEN ta.pais = 'BOLIVIA' THEN json_build_object(
                               'monto_multiplicar', 2,
                               'monto_dividir', 1,
                               'tipo_moneda', 'bs'
                           )
                       WHEN ta.pais = 'PERU' THEN
                           json_build_object(
                                   'monto_multiplicar', 1,
                                   'monto_dividir', 2,
                                   'tipo_moneda', 'soles'
                               )
                       WHEN ta.pais = 'BRASIL' THEN
                           json_build_object(
                                   'monto_multiplicar', 2,
                                   'monto_dividir', 1,
                                   'tipo_moneda', 'reales'
                               ) END AS config_pais

            FROM afi.tafiliado ta
                     INNER JOIN afi.trango_afiliado tra ON tra.id_afiliado = ta.id_afiliado
                     INNER JOIN afi.trango ran ON ran.id_rango = tra.id_rango
                     INNER JOIN su.tpunto_venta pu ON pu.id_punto_venta = ta.id_punto_venta
                     INNER JOIN su.tsucursal su ON su.id_sucursal = pu.id_sucursal
                     INNER JOIN su.tpais pais ON pais.id_pais = su.id_pais
                 INNER JOIN afi.tconfig_pais tcp on tcp.pais = ta.pais

            WHERE ta.id_afiliado = p_id_afiliado
              AND tra.id_periodo = p_id_periodo
            LIMIT 1
        ),
        t_puntos_propio_detalle AS (
            SELECT pv, cv, fecha_reg, id_periodo
            FROM afi.tpuntos_afiliado
            WHERE id_afiliado = (SELECT t_current_afiliado.id_afiliado FROM t_current_afiliado)
              AND id_periodo = (SELECT t_current_afiliado.id_periodo FROM t_current_afiliado)
        ),
        t_puntos_acumulado AS (
            SELECT coalesce(pv,0) as pv_acumulado
            from afi.tpunto_acumulado
            where id_afiliado = (SELECT t_current_afiliado.id_afiliado FROM t_current_afiliado)
              AND id_periodo = (SELECT t_current_afiliado.id_periodo FROM t_current_afiliado)
            limit 1
        ),
        t_puntos_propio AS (
            SELECT COALESCE(sum(COALESCE(cv, 0)), 0) cv, COALESCE(sum(COALESCE(pv, 0)), 0) pv
            FROM t_puntos_propio_detalle
        ),
        t_puntos_propio_del_1_a_15 AS (
            SELECT COALESCE(sum(COALESCE(cv, 0)), 0) cv, COALESCE(sum(COALESCE(pv, 0)), 0) pv
            FROM t_puntos_propio_detalle
            where fecha_reg::date between '2023-05-01'::date and '2023-05-15'::date and id_periodo = p_id_periodo
        ),
        t_red AS (
            SELECT t.nivel,
                   t.id_afiliado,
                   afil.id_patrocinador,
                   afil.codigo,
                   patro.codigo                AS codigo_patrocinador,
               afil.pais,
                /*per.nombre_completo2,
                per_patro.nombre_completo2  AS nombre_patrocinador,*/
                   COALESCE(sum(puntos.cv), 0) AS sum_cv,
                   COALESCE(sum(puntos.pv), 0) AS sum_pv

            FROM t t
                     INNER JOIN afi.tafiliado afil ON afil.id_afiliado = t.id_afiliado
                     INNER JOIN afi.tafiliado patro ON patro.id_afiliado = afil.id_patrocinador
                     INNER JOIN segu.vpersona2 per ON per.id_persona = afil.id_persona
                     INNER JOIN segu.vpersona2 per_patro ON per_patro.id_persona = patro.id_persona

                     INNER JOIN afi.tpuntos_afiliado puntos ON puntos.id_afiliado = t.id_afiliado

            WHERE puntos.id_periodo = p_id_periodo
              AND nivel <= 7 -- this should be a  variable
            GROUP BY t.nivel,
                     t.id_afiliado,
                     afil.id_patrocinador,
                 afil.pais,
                     afil.codigo,
                     patro.codigo
                /*per.nombre_completo2,
                per_patro.nombre_completo2*/
            ORDER BY nivel ASC
        ),
        t_afiliados_con_240pv_en_primer_nivel AS (
            SELECT tr.nivel, count(*) AS count
            FROM t_red tr
            WHERE sum_pv >= 240
            GROUP BY tr.nivel
            ORDER BY tr.nivel ASC
        ),
    t_afiliados_toda_mi_red as (
        select tr.id_afiliado,tr.id_patrocinador, tr.sum_pv, tr.sum_cv, tr.nivel as nivel_red, ran.id_rango, ran.nombre as rango, ran.volumen_personal
        from t_red tr
        INNER JOIN afi.trango_afiliado tra ON tra.id_afiliado = tr.id_afiliado
        INNER JOIN afi.trango ran ON ran.id_rango = tra.id_rango
        WHERE tra.id_periodo = p_id_periodo
    ),
    t_afiliados_con_sus_puntos_a_calificar_para_este_periodo as (
        SELECT tr.nivel as nivel_red,
               tapp.id_afiliado,
               tapp.data_json_aux->'sube_nivel'->'rango'->>'nombre' as rango,
                tapp.data_json_aux->'sube_nivel'->'rango'->>'id_rango' as id_rango
        FROM t_red tr
        inner join afi.tafiliado_pago_periodo tapp on tapp.id_afiliado = tr.id_afiliado and tapp.id_periodo = p_id_periodo
        where tapp.sube_nivel = 'si'
    ),
    t_afiliados_rangos as (
      select tatmr.nivel_red, tatmr.id_afiliado,
             case when tacspacpep.rango is not null then tacspacpep.rango else tatmr.rango end as rango,
             case when tacspacpep.id_rango is not null then tacspacpep.id_rango::integer else tatmr.id_rango end as id_rango
      from t_afiliados_toda_mi_red tatmr
      left join t_afiliados_con_sus_puntos_a_calificar_para_este_periodo tacspacpep on tacspacpep.id_afiliado = tatmr.id_afiliado
    ),
    t_cantidad_afiliados_por_rango as (
      select nivel_red, id_rango, rango, count(*)
      from t_afiliados_rangos
      group by id_rango,rango, nivel_red
    ),
        t_afiliados_activos AS (
        SELECT tr.id_afiliado,tr.id_patrocinador, tr.nivel_red as nivel
        FROM t_afiliados_toda_mi_red tr
          where tr.sum_pv >= tr.volumen_personal
        ),
        t_afiliados_inactivos AS (
        SELECT tr.id_afiliado, tr.id_patrocinador, tr.nivel_red as nivel
        FROM t_afiliados_toda_mi_red tr
        WHERE tr.sum_pv < tr.volumen_personal
        ),
        t_red_con_compresion_dinamica
            AS ( -- aca debemos hacer compresion dinamica de los inactivos sus hijos pasarles un nivel menos
            SELECT tr.nivel - 1 AS nivel, tr.nivel AS nivel_anterior, tr.id_afiliado, tr.id_patrocinador
            FROM t_afiliados_inactivos tai
                     INNER JOIN t_red tr ON tr.id_patrocinador = tai.id_afiliado
            WHERE tr.nivel <= (SELECT nivel_rango FROM t_current_afiliado)
            ORDER BY tr.nivel
        ),

        t_merge_nivel AS (
            /*SELECT CASE WHEN trccd.id_afiliado IS NOT NULL THEN trccd.nivel ELSE tr.nivel END AS nivel,
                   CASE WHEN trccd.id_afiliado IS NOT NULL THEN 'si' ELSE 'no' END            AS es_compresion,
                   tr.id_afiliado,
                   tr.id_patrocinador,
                   tr.codigo,
                   tr.codigo_patrocinador,
                   tr.nombre_completo2,
                   tr.nombre_patrocinador,
                   tr.sum_cv,
                   tr.sum_pv

            FROM t_red tr
                     LEFT JOIN t_red_con_compresion_dinamica trccd ON trccd.id_afiliado = tr.id_afiliado*/
            SELECT *
            FROM t_red
        ),
    t_sum_por_niveles_completo_por_pais AS (
        SELECT nivel,pais, coalesce(sum(sum_pv), 0) AS total_pv, coalesce(sum(sum_cv), 0) AS total_cv
        FROM t_merge_nivel
        GROUP BY nivel, pais
        ORDER BY nivel ASC
    ),
        t_sum_por_niveles_completo AS (
            SELECT nivel, coalesce(sum(sum_pv), 0) AS total_pv, coalesce(sum(sum_cv), 0) AS total_cv
            FROM t_merge_nivel
            GROUP BY nivel
            ORDER BY nivel ASC
        ),
    t_sum_puntos_por_paquete_no_tomar_encuenta_por_pais AS (
        SELECT nivel,pais, COALESCE(sum(tpa.pv), 0) AS total_pv, COALESCE(sum(tpa.cv), 0) AS total_cv
        FROM afi.tpuntos_afiliado tpa
                 INNER JOIN t_merge_nivel tr ON tr.id_afiliado = tpa.id_afiliado
                 INNER JOIN afi.tpunto_item tpi ON tpi.id_set = tpa.id_set_item
                 INNER JOIN afi.tbonus_config tb ON tb.id_bonus_config = tpi.id_bonus_config
        WHERE id_periodo = p_id_periodo
          AND (('naturalgo' = 'naturalgo' AND tr.nivel > 1) OR ('naturalgo' = 'canaan' AND tr.nivel > 3))
          AND (
            (tb.generar_regalias_a_niveles IS NOT NULL AND tb.generar_regalias_a_niveles < tr.nivel)
                OR
            tpa.pv = 0 AND tpa.cv = 0
            )
        GROUP BY tr.nivel, tr.pais
        ORDER BY tr.nivel
    ),
        t_sum_puntos_por_paquete_no_tomar_encuenta AS (
            SELECT nivel, COALESCE(sum(tpa.pv), 0) AS total_pv, COALESCE(sum(tpa.cv), 0) AS total_cv
            FROM afi.tpuntos_afiliado tpa
                     INNER JOIN t_merge_nivel tr ON tr.id_afiliado = tpa.id_afiliado
                     INNER JOIN afi.tpunto_item tpi ON tpi.id_set = tpa.id_set_item
                     INNER JOIN afi.tbonus_config tb ON tb.id_bonus_config = tpi.id_bonus_config
            WHERE id_periodo = p_id_periodo
              AND ((v_empresa = 'naturalgo' AND tr.nivel > 1) OR (v_empresa = 'vid' AND tr.nivel > 1) OR ('naturalgo' = 'canaan' AND tr.nivel > 3))
              AND (
                    (tb.generar_regalias_a_niveles IS NOT NULL AND tb.generar_regalias_a_niveles < tr.nivel)
                    OR
                    tpa.pv = 0 AND tpa.cv = 0
                )
            GROUP BY tr.nivel
            ORDER BY tr.nivel
        ),
        t_sum_por_niveles AS (
            SELECT tspnc.nivel,
                   (COALESCE(tspnc.total_pv, 0) - COALESCE(tspppnte.total_pv, 0)) AS total_pv,
                   (COALESCE(tspnc.total_cv, 0) - COALESCE(tspppnte.total_cv, 0)) AS total_cv
            FROM t_sum_por_niveles_completo tspnc
                     LEFT JOIN t_sum_puntos_por_paquete_no_tomar_encuenta tspppnte ON tspppnte.nivel = tspnc.nivel
            ORDER BY tspnc.nivel
        ),
    t_sum_por_niveles_por_pais AS (
        SELECT tspncpp.nivel,
               tspncpp.pais,
               (COALESCE(tspncpp.total_pv, 0) - COALESCE(tspppnte.total_pv, 0)) AS total_pv,
               (COALESCE(tspncpp.total_cv, 0) - COALESCE(tspppnte.total_cv, 0)) AS total_cv
        FROM t_sum_por_niveles_completo_por_pais tspncpp
                 LEFT JOIN t_sum_puntos_por_paquete_no_tomar_encuenta_por_pais tspppnte ON tspppnte.nivel = tspncpp.nivel and tspppnte.pais = tspncpp.pais
        ORDER BY tspncpp.nivel, tspncpp.pais
    ),
        t_count_activos_por_nivel AS (
            SELECT nivel, count(*) AS count
            FROM t_afiliados_activos
            GROUP BY nivel
            ORDER BY nivel ASC
        ),

        t_count_inactivos_por_nivel AS (
            SELECT nivel, count(*) AS count
            FROM t_afiliados_inactivos
            GROUP BY nivel
            ORDER BY nivel ASC
        ),
        t_habilitado AS (
            SELECT CASE
                       WHEN ((SELECT pv FROM t_puntos_propio) >= (SELECT volumen_personal FROM t_current_afiliado))
                           THEN 'si'
                       ELSE 'no' END AS habilitado
        ),
        t_porcentajes_segun_el_rango AS (
            SELECT row_number() OVER () AS rnum, p.porcentaje
            FROM (
                     SELECT unnest(porcentajes) AS porcentaje
                     FROM t_current_afiliado
                 ) p
        ),
        t_config_niveles_a_cobrar AS (
            ---- agregar nivel 4 y porcentaje de 10 si tiene 6 o mas personas directas que esten activos ----
            /*SELECT unnest(ARRAY [4]) AS rnum, unnest(ARRAY [10]) AS porcentaje
            WHERE (SELECT count FROM t_count_activos_por_nivel WHERE nivel = 1) >= 6
              AND (SELECT array_length(porcentajes, 1) AS count_porcentajes
                   FROM t_current_afiliado) <= 3
              AND 'naturalgo' = 'naturalgo'
              AND 'si' = (SELECT habilitado FROM t_habilitado)
              ---- end ----
            UNION*/
            ---- subir el porcentaje al nivel 1 a 50% y al 3 nivel con 60% si tiene 5 o mas activos directos que tengan 240 pv----
            SELECT unnest(ARRAY [1,2,3]) AS rnum, unnest(ARRAY [50,15,60]) AS porcentaje
            WHERE (SELECT count FROM t_afiliados_con_240pv_en_primer_nivel WHERE nivel = 1) >= 5
              AND 'naturalgo' = 'naturalgo'
              AND 'si' = (SELECT habilitado FROM t_habilitado)
              ---- end ----
            UNION
            ---- obtenemos sus porcentajes de su rango ----
            SELECT row_number() OVER () AS rnum, p.porcentaje
            FROM (SELECT unnest(porcentajes) AS porcentaje
                  FROM t_current_afiliado) p
            WHERE 'si' = (SELECT habilitado FROM t_habilitado)
            ---- end ----
            UNION
             ---- si tiene 50 pv y no esta habilitado entonces podra cobrar el primer nivel solamente
            SELECT unnest(ARRAY [1]) AS rnum, unnest(ARRAY [40]) AS porcentaje
            WHERE (SELECT pv FROM t_puntos_propio) >= 120
              AND 'naturalgo' = 'naturalgo'
              AND 'no' = (SELECT habilitado FROM t_habilitado)
            ---- end ----
            /*UNION
             ----PROMOCION NATURALGO DEL 1 AL 15 si tiene 300 pv y esta habiliado se habilita 4 niveles para cobrar
            select unnest(ARRAY [1,2,3,4]) AS rnum, unnest(ARRAY [40,15,40,10]) AS porcentaje
            WHERE (SELECT pv FROM t_puntos_propio_del_1_a_15) >= 300
              AND 'naturalgo' = 'naturalgo'
              AND 'si' = (SELECT habilitado FROM t_habilitado)
            ---- end ----*/
            UNION
             ---- si tiene 240 pv y no esta habilitado entonces podra cobrar el primer nivel solamente
            SELECT unnest(ARRAY [1,2,3]) AS rnum, unnest(ARRAY [40,15,40]) AS porcentaje
            WHERE (SELECT pv FROM t_puntos_propio) >= 240
              AND 'naturalgo' = 'naturalgo'
              AND 'no' = (SELECT habilitado FROM t_habilitado)
            ---- end ----
            /*UNION
            -- esto todo se debe hacer que dure 3 meses preguntar a marlon esto manana a primera hora
            select unnest(ARRAY [1,2,3,4]) AS rnum, unnest(ARRAY [40,15,40,10]) AS porcentaje
            where 'si' = (SELECT habilitado FROM t_habilitado)
              and (select count(*)
                    from afi.tbonus
                     where id_bonus_config = 4 and id_periodo =p_id_periodo
                       and id_afiliado  = p_id_afiliado
                      and id_afiliado_from is null
                      and nivel_from is null) >= 2*/

        ),
        t_niveles_a_cobrar AS (
            SELECT rnum, max(porcentaje) AS porcentaje
            FROM t_config_niveles_a_cobrar
            GROUP BY 1
            ORDER BY rnum
        ),
        t_sum_por_niveles_por_rango as (
            select tnac.rnum as nivel, coalesce(tspn.total_cv, 0) as total_cv, coalesce(tspn.total_pv, 0) as total_pv
            from t_niveles_a_cobrar tnac
                     left join t_sum_por_niveles tspn on tspn.nivel = tnac.rnum
            ORDER BY tnac.rnum
    ),
    t_sum_por_niveles_por_rango_por_pais as (
        select tnac.rnum as nivel, tspn.pais, coalesce(tspn.total_cv, 0) as total_cv, coalesce(tspn.total_pv, 0) as total_pv
        from t_niveles_a_cobrar tnac
                 left join t_sum_por_niveles_por_pais tspn on tspn.nivel = tnac.rnum
        ORDER BY tnac.rnum
        ),
        t_generaciones_cv_monto AS (
            SELECT tspn.nivel,
                   ((tspn.total_cv * (tnac.porcentaje / 100)) *
                   (cast(tca.monto_multiplicar AS numeric))) AS generaciones_cv_monto
            FROM t_sum_por_niveles_por_rango tspn
                     INNER JOIN t_niveles_a_cobrar tnac ON tnac.rnum = tspn.nivel
                     CROSS JOIN t_current_afiliado tca
        ),
    t_generaciones_cv_monto_por_pais AS (
        SELECT tspn.nivel,
               tspn.pais,
               ((tspn.total_cv * (tnac.porcentaje / 100)) *
                (cast(tca.monto_multiplicar AS numeric))) AS generaciones_cv_monto
        FROM t_sum_por_niveles_por_rango_por_pais tspn
                 INNER JOIN t_niveles_a_cobrar tnac ON tnac.rnum = tspn.nivel
                 CROSS JOIN t_current_afiliado tca
    ),
        t_generaciones_cv_monto_a_cobrar AS (
            SELECT tgcm.nivel, tgcm.generaciones_cv_monto
            FROM t_generaciones_cv_monto tgcm
                     INNER JOIN t_niveles_a_cobrar tnac ON tnac.rnum = tgcm.nivel
        ),
    t_generaciones_cv_monto_a_cobrar_por_pais AS (
        SELECT tgcm.nivel,tgcm.pais, tgcm.generaciones_cv_monto
        FROM t_generaciones_cv_monto_por_pais tgcm
                 INNER JOIN t_niveles_a_cobrar tnac ON tnac.rnum = tgcm.nivel
    ),
    t_generaciones_cv_monto_a_cobrar_agrupado_por_nivel_pais AS (
        SELECT tgcmacpp.pais, sum(tgcmacpp.generaciones_cv_monto) as total_red
        FROM t_generaciones_cv_monto_a_cobrar_por_pais tgcmacpp
        group by tgcmacpp.pais
    ),
        t_sum_pv_niveles_correspondientes AS (
            SELECT coalesce(sum(tspn.total_pv), 0) AS sum_generaciones_pv
            FROM t_sum_por_niveles tspn
            WHERE tspn.nivel <= (SELECT niveles_a_sumar_para_pv FROM t_current_afiliado)
        ),
        t_pv_total_para_calificar AS (
            SELECT coalesce((SELECT pv_acumulado FROM t_puntos_acumulado), 0) +
                   coalesce((SELECT pv FROM t_puntos_propio),0) +
                   coalesce((SELECT sum_generaciones_pv FROM t_sum_pv_niveles_correspondientes),0) AS pv_para_calificar
        ),
    t_rangos_por_puntos_y_llaves as (
         select tr.*
         FROM afi.trango tr
         WHERE tr.volumen_organizacional <= (SELECT pv_para_calificar FROM t_pv_total_para_calificar)
         and exists(
             select 1
             from t_cantidad_afiliados_por_rango tcapr
             left join jsonb_array_elements(COALESCE(tr.requisitos->'children'->'ranks', '[]'::jsonb)) as js on true
             where tcapr.nivel_red = 1
             and (
                 (((js->>'rankId') IS NOT NULL and tcapr.id_rango = (js->>'rankId')::INT)
                    and ((js->>'amount') IS NOT NULL AND tcapr.count >= (js->>'amount')::INT))
             or ((js->>'rankId') IS NULL)
                 )
         )
         ORDER BY tr.nivel DESC
         --LIMIT 1
    ),
        t_rango_sube_nivel AS (
            SELECT *
            FROM (
                     SELECT CASE
                                WHEN (tr.nivel > (SELECT nivel_rango FROM t_current_afiliado))
                                    THEN
                                    json_build_object(
                                            'sube_nivel', 'si',
                                            'rango', to_json(tr)
                                        )

                                ELSE
                                    json_build_object(
                                            'sube_nivel', 'no'
                                        )
                                END AS combo
                 FROM t_rangos_por_puntos_y_llaves tr
                 --WHERE tr.volumen_organizacional <= (SELECT pv_para_calificar FROM t_pv_total_para_calificar)
                     ORDER BY tr.nivel DESC
                     LIMIT 1
                 ) x
        ),
        t_bonus AS (
            SELECT tb.id_bonus,
                   tb.monto,
                   tb.fecha_reg,
                   tb.nivel_from,
                   tb.id_afiliado_from,
                   tb.id_bonus_config,
                   tbc.tipo_pago_bonus_patrocinador_directo,
                   tb.id_periodo,
               tbc.bonus,
               ta.pais as pais_afil_hijo
            FROM afi.tbonus tb
                     INNER JOIN afi.tbonus_config tbc ON tbc.id_bonus_config = tb.id_bonus_config
            inner join afi.tafiliado ta on ta.id_afiliado = tb.id_afiliado
            WHERE tb.id_patrocinador = p_id_afiliado
              AND tb.id_periodo = p_id_periodo
              --and (tb.validado_para_pagar_a_patrocinador = 'si' or tb.validado_para_pagar_a_patrocinador is null)
        ),
        t_bonus_sum AS (
            SELECT tbc.bonus,
                   (coalesce(sum(coalesce(tb.monto, 0)), 0)) AS sum
            FROM afi.tbonus_config tbc
                     LEFT JOIN t_bonus tb ON tb.id_bonus_config = tbc.id_bonus_config AND tb.nivel_from IS NULL
            GROUP BY tbc.bonus
        ),
    t_bonus_sum_por_pais AS (
        SELECT tbc.bonus,tb.pais_afil_hijo,
               (coalesce(sum(coalesce(tb.monto, 0)), 0)) AS sum
        FROM afi.tbonus_config tbc
                 LEFT JOIN t_bonus tb ON tb.id_bonus_config = tbc.id_bonus_config AND tb.nivel_from IS NULL
        GROUP BY tbc.bonus, tb.pais_afil_hijo
    ),
        t_bonus_multinivel_sum AS (
            SELECT tbc.bonus,
                   (coalesce(sum(coalesce(tb.monto, 0)), 0)) AS sum
            FROM afi.tbonus_config tbc
                     LEFT JOIN t_bonus tb ON tb.id_bonus_config = tbc.id_bonus_config AND tb.nivel_from IS NOT NULL
            GROUP BY tbc.bonus
        ),
    t_bonus_multinivel_sum_por_pais AS (
        SELECT tbc.bonus, tb.pais_afil_hijo,
               (coalesce(sum(coalesce(tb.monto, 0)), 0)) AS sum
        FROM afi.tbonus_config tbc
                 LEFT JOIN t_bonus tb ON tb.id_bonus_config = tbc.id_bonus_config AND tb.nivel_from IS NOT NULL
        GROUP BY tbc.bonus, tb.pais_afil_hijo
    ),
        t_registros_directos AS (
            SELECT *
            FROM t_bonus
            WHERE (nivel_from IS NULL OR nivel_from = 1)
        ),
        t_registros_directos_count AS (
            SELECT count(*) AS count
            FROM t_registros_directos
        ),
        t_bonus_tipo_instante_detalle AS (
            SELECT tb.id_bonus, tb.monto
            FROM t_bonus tb
            WHERE tb.tipo_pago_bonus_patrocinador_directo = 'instante'
              AND tb.nivel_from IS NULL
              AND tb.id_afiliado_from IS NULL
        ),
        t_bonus_tipo_instante AS (
            SELECT coalesce(sum(monto), 0) AS sum
            FROM t_bonus_tipo_instante_detalle
        ),
        t_bonus_tipo_mensual_detalle AS (
            SELECT tb.id_bonus, tb.monto
            FROM t_bonus tb
            WHERE tb.tipo_pago_bonus_patrocinador_directo = 'mensual'
              AND tb.nivel_from IS NULL
              AND tb.id_afiliado_from IS NULL
        ),
        t_bonus_tipo_mensual AS (
            SELECT coalesce(sum(monto), 0) AS sum
            FROM t_bonus_tipo_mensual_detalle
        ),
        t_bono_patrocinio_para_los_afiliados_antes_de_2022 AS (
            WITH t_bonus_patrocinio_que_esta_validado_para_cobrar AS (
                SELECT tbc.id_bonus_config
                FROM afi.tbonus_config tbc
                WHERE (tbc.validar_tipo_bonus = 'no' OR tbc.validar_tipo_bonus = 'si')
              --AND tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
                GROUP BY tbc.id_bonus_config
            ),
                 t_sum_patrocinio_con_validacion AS (
                     SELECT json_build_object('detalle', json_agg(tb.*), 'sum', coalesce(sum(tb.monto), 0)) AS data_json
                     FROM t_bonus tb
                              INNER JOIN t_bonus_patrocinio_que_esta_validado_para_cobrar tbc
                                         ON tbc.id_bonus_config = tb.id_bonus_config
                     WHERE tb.nivel_from IS NOT NULL
                       AND tb.id_afiliado_from IS NOT NULL
                 )
            SELECT *
            FROM t_sum_patrocinio_con_validacion
        ),
        t_bono_patrocinio_para_los_afiliados_despues_de_2022 AS (
            WITH t_bonus_patrocinio_que_esta_validado_para_cobrar AS (
                SELECT tbc.id_bonus_config
                FROM afi.tpuntos_afiliado tpa
                         INNER JOIN afi.tpunto_item tpi ON tpi.id_set = tpa.id_set_item
                         INNER JOIN afi.tbonus_config tbc ON tbc.id_bonus_config = tpi.id_bonus_config
                WHERE tpa.id_afiliado = p_id_afiliado
                  AND tbc.bono_patrocinio_por_nivel IS NOT NULL
                  AND tbc.validar_tipo_bonus = 'si'
              --AND tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
                GROUP BY tbc.id_bonus_config
                UNION ALL
                SELECT tbc.id_bonus_config
                FROM afi.tbonus_config tbc
                WHERE tbc.validar_tipo_bonus = 'no'
              --AND tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
                GROUP BY tbc.id_bonus_config
            ),
                 t_sum_patrocinio_con_validacion AS (
                     SELECT json_build_object('detalle', json_agg(tb.*), 'sum', coalesce(sum(tb.monto), 0)) AS data_json
                     FROM t_bonus tb
                              INNER JOIN t_bonus_patrocinio_que_esta_validado_para_cobrar tbc
                                         ON tbc.id_bonus_config = tb.id_bonus_config
                     WHERE tb.nivel_from IS NOT NULL
                       AND tb.id_afiliado_from IS NOT NULL
             )
        SELECT *
        FROM t_sum_patrocinio_con_validacion
    ),
    t_bono_patrocinio_para_los_afiliados_despues_de_2022_por_pais AS (
        WITH t_bonus_patrocinio_que_esta_validado_para_cobrar AS (
            SELECT tbc.id_bonus_config
            FROM afi.tpuntos_afiliado tpa
                     INNER JOIN afi.tpunto_item tpi ON tpi.id_set = tpa.id_set_item
                     INNER JOIN afi.tbonus_config tbc ON tbc.id_bonus_config = tpi.id_bonus_config
            WHERE tpa.id_afiliado = p_id_afiliado
              AND tbc.bono_patrocinio_por_nivel IS NOT NULL
              AND tbc.validar_tipo_bonus = 'si'
            --AND tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
            GROUP BY tbc.id_bonus_config
            UNION ALL
            SELECT tbc.id_bonus_config
            FROM afi.tbonus_config tbc
            WHERE tbc.validar_tipo_bonus = 'no'
            --AND tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
            GROUP BY tbc.id_bonus_config
        ),
             t_sum_patrocinio_con_validacion AS (
                 SELECT pais_afil_hijo as pais,coalesce(sum(tb.monto), 0) as total_bono
                 FROM t_bonus tb
                          INNER JOIN t_bonus_patrocinio_que_esta_validado_para_cobrar tbc
                                     ON tbc.id_bonus_config = tb.id_bonus_config
                 WHERE tb.nivel_from IS NOT NULL
                   AND tb.id_afiliado_from IS NOT NULL
                 group by tb.pais_afil_hijo
                 )
            SELECT *
            FROM t_sum_patrocinio_con_validacion
        ),
        t_bono_patrocinio AS (
            SELECT CASE
                       WHEN ((SELECT fecha_reg FROM t_current_afiliado)::date < '01-01-2022')
                           THEN
                           antes_2022.data_json

                       ELSE
                           despues_2022.data_json
                       END AS data_json
            FROM t_bono_patrocinio_para_los_afiliados_antes_de_2022 antes_2022
                     CROSS JOIN t_bono_patrocinio_para_los_afiliados_despues_de_2022 despues_2022
        ),
    t_bono_patrocinio_por_pais AS (
        SELECT  *
        FROM t_bono_patrocinio_para_los_afiliados_despues_de_2022_por_pais
    ),
        t_total AS (
            SELECT (SELECT SUM(m.generaciones_cv_monto) FROM t_generaciones_cv_monto_a_cobrar m) AS total_red,
                   (SELECT cast(tpb.data_json ->> 'sum' AS numeric)
                    FROM t_bono_patrocinio tpb)                                                  AS total_bono_patrocinio,
                   (SELECT sum FROM t_bonus_tipo_instante tbti)                                  AS total_bono_instante,
                   (SELECT sum FROM t_bonus_tipo_mensual tbtm)                                   AS total_bono_mensual,
                   (SELECT SUM(m.generaciones_cv_monto) FROM t_generaciones_cv_monto_a_cobrar m)
                       + (SELECT cast(tpb.data_json ->> 'sum' AS numeric) FROM t_bono_patrocinio tpb)
                       + (SELECT sum FROM t_bonus_tipo_instante tbti)
                       + (SELECT sum FROM t_bonus_tipo_mensual tbtm)                             AS total_ganado,
                   (SELECT SUM(m.generaciones_cv_monto) FROM t_generaciones_cv_monto_a_cobrar m)
                       + (SELECT cast(tpb.data_json ->> 'sum' AS numeric) FROM t_bono_patrocinio tpb)
                       + (SELECT sum FROM t_bonus_tipo_mensual tbtm)                             AS total_a_pagar
        )
    SELECT TO_JSON(ROW_TO_JSON(jsonData) :: TEXT) #>> '{}' AS json
    INTO v_resp
    FROM (
             SELECT (
                        SELECT CASE
                                   WHEN (SELECT t_current_afiliado.estado_afiliado FROM t_current_afiliado)  != 'inactivo' and habilitado = 'si' THEN 'si'
                                   WHEN (SELECT t_current_afiliado.estado_afiliado FROM t_current_afiliado)  != 'inactivo' and habilitado = 'no' AND (SELECT pv FROM t_puntos_propio) >= 50 THEN 'si'
                                   ELSE 'no'
                                   END AS habilitado

                        FROM t_habilitado
                    )                                                                                 AS habilitado,
                    (
                        SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(niveles_a_cobrar))), '[]')
                        FROM (
                                 SELECT *
                                 FROM t_niveles_a_cobrar
                             ) niveles_a_cobrar
                    )                                                                                 AS niveles_a_cobrar,
                    (
                        SELECT to_json(current_afiliado)
                        FROM (
                                 SELECT *
                                 FROM t_current_afiliado
                             ) current_afiliado
                    )                                                                                 AS current_afiliado,
                    (
                        SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tpser))), '[]')
                        FROM (
                                 SELECT *
                                 FROM t_porcentajes_segun_el_rango
                             ) tpser
                    )                                                                                 AS porcentajes_segun_el_rango,
                    (
                        SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tppd))), '[]')
                        FROM (
                                 SELECT *
                                 FROM t_puntos_propio_detalle
                             ) tppd
                    )                                                                                 AS puntos_propio_detalle,
                    (
                        SELECT to_json(tpp)
                        FROM (
                                 SELECT *
                                 FROM t_puntos_propio
                             ) tpp
                    )                                                                                 AS puntos_propio_sum,

                    (
                        SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(trd))), '[]')
                        FROM (
                                 SELECT *
                                 FROM t_registros_directos
                             ) trd
                    )                                                                                 AS registros_directos,
                    (
                        SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tspn))), '[]')
                        FROM (
                                 SELECT *
                                 FROM t_sum_por_niveles
                             ) tspn
                    )                                                                                    sum_por_niveles_sin_condicion,
                    (
                        SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tspn))), '[]')
                        FROM (
                                 SELECT *
                                 FROM t_sum_por_niveles_por_rango
                             ) tspn
                    )                                                                                    sum_por_niveles,
                    (
                        SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tgcm))), '[]')
                        FROM (
                                 SELECT *
                                 FROM t_generaciones_cv_monto
                             ) tgcm
                    )                                                                                    generaciones_cv_monto,
                    (
                    SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tgcm))), '[]')
                    FROM (
                             SELECT *
                             FROM t_generaciones_cv_monto_por_pais
                         ) tgcm
                )                                                                                    generaciones_cv_monto_por_pais,
                (
                    SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tgcmacpp))), '[]')
                    FROM (
                             SELECT *
                             FROM t_generaciones_cv_monto_a_cobrar_por_pais
                         ) tgcmacpp
                )                                                                                    generaciones_cv_monto_a_cobrar_por_pais,
                (
                    SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tgcmacapnp))), '[]')
                    FROM (
                             SELECT *
                             FROM t_generaciones_cv_monto_a_cobrar_agrupado_por_nivel_pais
                         ) tgcmacapnp
                )                                                                                    generaciones_cv_monto_a_cobrar_agrupado_por_nivel_pais,
                (
                        SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tac240pepn))), '[]')
                        FROM (
                                 SELECT *
                                 FROM t_afiliados_con_240pv_en_primer_nivel
                             ) tac240pepn
                    )                                                                                    afiliados_con_240pv_en_primer_nivel,
                    (
                        SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tcapn))), '[]')
                        FROM (
                                 SELECT *
                                 FROM t_count_activos_por_nivel
                             ) tcapn
                    )                                                                                    activos,
                    (
                        SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tcipn))), '[]')
                        FROM (
                                 SELECT *
                                 FROM t_count_inactivos_por_nivel
                             ) tcipn
                    )                                                                                    inactivos,
                    (
                        SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tr))), '[]')
                        FROM (
                                 SELECT *
                                 FROM t_red
                             ) tr
                    )                                                                                 AS red,
                    (
                        SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tbs))), '[]')
                        FROM (
                                 SELECT *
                                 FROM t_bonus_sum
                             ) tbs
                    )                                                                                 AS bonus_sum,
                    (
                    SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tbs))), '[]')
                    FROM (
                             SELECT *
                             FROM t_bonus_sum_por_pais
                         ) tbs
                )                                                                                 AS bonus_sum_por_pais,
                (
                    SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tbppp))), '[]')
                    FROM (
                             SELECT *
                             FROM t_bono_patrocinio_por_pais
                         ) tbppp
                )                                                                                 AS bono_patrocinio_por_pais,
                (
                        SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tbms))), '[]')
                        FROM (
                                 SELECT *
                                 FROM t_bonus_multinivel_sum
                             ) tbms
                    )                                                                                 AS bonus_multinivel_sum,
                    (
                    SELECT coalesce(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(tbms))), '[]')
                    FROM (
                             SELECT *
                             FROM t_bonus_multinivel_sum_por_pais
                         ) tbms
                )                                                                                 AS bonus_multinivel_sum_por_pais,
                (
                        SELECT to_json(tt)
                        FROM (
                                 SELECT *
                                 FROM t_total
                             ) tt
                    )                                                                                    totales,
                    (SELECT COALESCE(SUM(m.generaciones_cv_monto), 0) FROM t_generaciones_cv_monto m) AS total_cv_monto,
                    (SELECT sum_generaciones_pv FROM t_sum_pv_niveles_correspondientes)               AS sum_pv_niveles_correspondientes,
                    (SELECT pv_para_calificar FROM t_pv_total_para_calificar)                         AS pv_para_calificar,
                    (SELECT trsn.combo FROM t_rango_sube_nivel trsn)                                  AS sube_nivel,
                    (SELECT tpb.data_json FROM t_bono_patrocinio tpb)                                 AS bono_patrocinio,
                    (SELECT trdc.count FROM t_registros_directos_count trdc)                          AS registros_directos_count,
                    coalesce((SELECT tpacu.pv_acumulado FROM t_puntos_acumulado tpacu),0)                          AS pv_acumulado
         ) jsonData;


    RETURN v_resp;


EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
            ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;


END ;
$BODY$
    LANGUAGE plpgsql VOLATILE;