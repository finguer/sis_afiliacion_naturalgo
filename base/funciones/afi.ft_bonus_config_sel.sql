CREATE OR REPLACE FUNCTION "afi"."ft_bonus_config_sel"(    
                p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$
/**************************************************************************
 SISTEMA:        AFILIACION
 FUNCION:         afi.ft_bonus_config_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'afi.tbonus_config'
 AUTOR:          (admin)
 FECHA:            22-11-2021 18:49:07
 COMENTARIOS:    
***************************************************************************
 HISTORIAL DE MODIFICACIONES:
#ISSUE                FECHA                AUTOR                DESCRIPCION
 #0                22-11-2021 18:49:07    admin             Creacion    
 #
 ***************************************************************************/

DECLARE

    v_consulta            VARCHAR;
    v_parametros          RECORD;
    v_nombre_funcion      TEXT;
    v_resp                VARCHAR;
                
BEGIN

    v_nombre_funcion = 'afi.ft_bonus_config_sel';
    v_parametros = pxp.f_get_record(p_tabla);

    /*********************************    
     #TRANSACCION:  'AFI_BONCON_SEL'
     #DESCRIPCION:    Consulta de datos
     #AUTOR:        admin    
     #FECHA:        22-11-2021 18:49:07
    ***********************************/

    IF (p_transaccion='AFI_BONCON_SEL') THEN
                     
        BEGIN
            --Sentencia de la consulta
            v_consulta:='SELECT
                        boncon.id_bonus_config,
                        boncon.estado_reg,
                        boncon.bonus,
                        boncon.monto,
                        boncon.id_usuario_reg,
                        boncon.fecha_reg,
                        boncon.id_usuario_ai,
                        boncon.usuario_ai,
                        boncon.id_usuario_mod,
                        boncon.fecha_mod,
                        usu1.cuenta as usr_reg,
                        usu2.cuenta as usr_mod    
                        FROM afi.tbonus_config boncon
                        JOIN segu.tusuario usu1 ON usu1.id_usuario = boncon.id_usuario_reg
                        LEFT JOIN segu.tusuario usu2 ON usu2.id_usuario = boncon.id_usuario_mod
                        WHERE  ';
            
            --Definicion de la respuesta
            v_consulta:=v_consulta||v_parametros.filtro;
            v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

            --Devuelve la respuesta
            RETURN v_consulta;
                        
        END;

    /*********************************    
     #TRANSACCION:  'AFI_BONCON_CONT'
     #DESCRIPCION:    Conteo de registros
     #AUTOR:        admin    
     #FECHA:        22-11-2021 18:49:07
    ***********************************/

    ELSIF (p_transaccion='AFI_BONCON_CONT') THEN

        BEGIN
            --Sentencia de la consulta de conteo de registros
            v_consulta:='SELECT COUNT(id_bonus_config)
                         FROM afi.tbonus_config boncon
                         JOIN segu.tusuario usu1 ON usu1.id_usuario = boncon.id_usuario_reg
                         LEFT JOIN segu.tusuario usu2 ON usu2.id_usuario = boncon.id_usuario_mod
                         WHERE ';
            
            --Definicion de la respuesta            
            v_consulta:=v_consulta||v_parametros.filtro;

            --Devuelve la respuesta
            RETURN v_consulta;

        END;
                    
    ELSE
                         
        RAISE EXCEPTION 'Transaccion inexistente';
                             
    END IF;
                    
EXCEPTION
                    
    WHEN OTHERS THEN
            v_resp='';
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
            v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
            v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
            RAISE EXCEPTION '%',v_resp;
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "afi"."ft_bonus_config_sel"(integer, integer, character varying, character varying) OWNER TO postgres;
