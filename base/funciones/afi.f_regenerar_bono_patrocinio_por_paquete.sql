CREATE OR REPLACE FUNCTION afi.f_regenerar_bono_patrocinio_por_paquete(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE


    v_nombre_funcion      varchar;
    p_id_afiliado         integer DEFAULT p_params ->> 'id_afiliado';
    p_id_set_item_nuevo   integer DEFAULT p_params ->> 'id_set_item_nuevo';
    p_id_usuario          integer DEFAULT p_params ->> 'userId';
    p_id_periodo          integer DEFAULT p_params ->> 'id_periodo';
    v_json_params_to_send json;
    v_res                 json;
    v_resp                json;


BEGIN



    v_nombre_funcion = 'afi.f_regenerar_bono_patrocinio_por_paquete';

    --necesitamos eliminar los registro de patrocinio de este afiliado que genero hacia arriba
    DELETE
    FROM afi.tbonus
    WHERE id_afiliado_from = p_id_afiliado
      AND id_periodo = p_id_periodo;






    SELECT json_strip_nulls(json_build_object(
            'id_set_item', p_id_set_item_nuevo,
            'id_afiliado', p_id_afiliado,
            'id_usuario', p_id_usuario,
            '_nombre_usuario_ai', 'NULL',
            '_id_usuario_ai', '',
            'po_id_periodo', p_id_periodo
        ))
    INTO v_json_params_to_send;


    v_res := afi.f_insertar_bonus_afiliado(v_json_params_to_send::json);


    SELECT json_strip_nulls(json_build_object(
            'res', v_res
        ))
    INTO v_resp;
    RETURN v_resp;


EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
            ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;
        RETURN v_resp;


END ;
$BODY$
    LANGUAGE plpgsql VOLATILE;