CREATE OR REPLACE FUNCTION afi.f_get_pago_periodo_afiliado(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_id_afiliado int4, p_id_periodo INT4)
    RETURNS int4
AS
$BODY$
DECLARE
    v_parametros RECORD;
    v_resp		            varchar;
    v_nombre_funcion        text;
    v_id_afiliado_pago_periodo	integer;

    v_bonus_registro_ya_pagado NUMERIC;
    v_bonus_registro_no_pagado NUMERIC;
    v_bonus_patrocinio_multinivel NUMERIC;
    v_bonus NUMERIC;
    v_cv_propio NUMERIC;
    v_pv_propio numeric;
    v_pv_propio_promo_1_12_feb2022 numeric;
    v_pv_propio_promo_1_15_mar2022 numeric;
    v_bono_retorno_por_compra_activacion numeric;
    v_habilitado VARCHAR;

    v_record RECORD;
    v_record_rango_porcentaje RECORD;
    v_porcentaje_a_pagar numeric;


    v_pago_total_ganado NUMERIC(10,2);
    v_pago_total NUMERIC(10,2);
    v_pago_red numeric(10,2);

    v_estado VARCHAR;



    v_personas_comprando_10 NUMERIC; -- 17 de marzo de 2019
    v_count_hijos_directos_con_premium integer;

    v_total_nivel_1_cv NUMERIC;
    v_total_nivel_1_pv NUMERIC;

    v_total_nivel_2_cv NUMERIC;
    v_total_nivel_2_pv NUMERIC;

    v_total_nivel_3_cv NUMERIC;
    v_total_nivel_3_pv NUMERIC;

    v_total_nivel_4_cv NUMERIC;
    v_total_nivel_4_pv NUMERIC;

    v_total_nivel_5_cv NUMERIC;
    v_total_nivel_5_pv NUMERIC;

    v_total_nivel_6_cv NUMERIC;
    v_total_nivel_6_pv NUMERIC;




    v_codigo_area VARCHAR;
    v_pais VARCHAR;
    v_monto_multiplicar NUMERIC;
    v_monto_dividir NUMERIC;
    v_tipo_moneda VARCHAR;

    v_rango_para_su_siguiente_periodo RECORD;
    v_rango_obtenido_con_sus_puntos RECORD;
    v_record_rango_actual RECORD;
    v_record_rango_siguiente RECORD;

    v_volumen_puntos_hijos NUMERIC;
    v_afiliados_activos_en_red INTEGER;



    v_niveles_para_cobrar INTEGER;


    v_count_aux INTEGER;
    pv_niveles_correspondientes NUMERIC;
    cv_total_pago_niveles_correspondientes NUMERIC;

    v_porcentaje_nivel NUMERIC;
    v_niveles_cobrados VARCHAR;

    v_sube_nivel VARCHAR;


    v_id_periodo_siguiente INTEGER;
    v_id_rango_nuevo INTEGER;


    v_id_afiliado_in INTEGER;
    v_id_periodo_in INTEGER;
    v_id_afiliado_pago_periodo_aux INTEGER;

    v_porcentajes_pagados NUMERIC[]; --20 de julio 2019

    v_continuar VARCHAR;


    v_rec_gestion_periodo                        RECORD;
    v_bonus_count integer;
    v_generaciones_cv       numeric[];
    v_generaciones_pv       numeric[];
    v_generaciones_cv_monto numeric[];
    v_generaciones_count_afiliados_100pts_pv integer[];
    v_generaciones_count_afiliados_activos integer[];
    v_data_json_aux json;

    v_record_afiliado record;
    v_promo_cuarto_nivel_2022 varchar DEFAULT 'no';
    v_sum_pv_for_afiliacion numeric(10,2);
    v_sum_cv_for_afiliacion numeric(10,2);


    v_total_presentador numeric(10,2);
    v_id_afiliados_inactivo_primer_nivel integer[];
    v_compresion_dinamica_nivel_1_sum record;



BEGIN




    v_nombre_funcion = 'afi.f_get_pago_periodo_afiliado';
    v_parametros = pxp.f_get_record(p_tabla);


    IF p_id_afiliado is NOT NULL THEN
        v_id_afiliado_in = p_id_afiliado;
    ELSE
        v_id_afiliado_in = v_parametros.id_afiliado;
    END IF;


    IF p_id_periodo is NOT NULL THEN
        v_id_periodo_in = p_id_periodo;
    ELSE
        v_id_periodo_in = v_parametros.id_periodo;
    END IF;

    --vemos el rango del afiliado


    select * INTO v_record_rango_actual FROM afi.trango_afiliado ranafi
                                                 INNER JOIN afi.trango ran on ran.id_rango = ranafi.id_rango
    where ranafi.id_afiliado = v_id_afiliado_in and ranafi.id_periodo = v_id_periodo_in;

    --verificamos que si exista un rango en un periodo seleccionado si no no deberia seguir
    IF v_record_rango_actual.id_rango IS NULL THEN

        RAISE EXCEPTION '%','NO PUEDES GENERAR PARA ESTE PERIODO '||v_id_afiliado_in;
    END IF;
    --vemos el rango siguiente por si sube nivel o puede cobrar
    select * INTO v_record_rango_siguiente FROM afi.trango ran
    where ran.id_rango_fk = v_record_rango_actual.id_rango;




    --Sentencia de la eliminacion
    SELECT afil.*, pais.codigo_area
    into v_record_afiliado
    FROM afi.tafiliado afil
             INNER JOIN su.tpunto_venta pu on pu.id_punto_venta = afil.id_punto_venta
             INNER JOIN su.tsucursal su on su.id_sucursal = pu.id_sucursal
             inner JOIN su.tpais pais on pais.id_pais = su.id_pais
    where afil.id_afiliado = v_id_afiliado_in;


    /*IF v_record_afiliado.codigo_area = '591'
    THEN --cuando es de bolivia

      v_monto_multiplicar = 2; --es de dos bolivianos
      v_tipo_moneda = 'bs';
    ELSIF v_record_afiliado.codigo_area = '051'
      THEN --cuando es de peru

        v_monto_multiplicar = 1; -- es un sol
        v_tipo_moneda = 'soles';

        --si es peru entonces el bonus se divide entre dos
        --v_bonus = v_bonus / 2;

    ELSE
      RAISE EXCEPTION '%', 'no hay codigo de area para esta afiliacion';
    END IF;*/


    IF v_record_afiliado.pais = 'BOLIVIA'
    THEN --cuando es de bolivia
        v_monto_multiplicar = 2; --es de dos bolivianos
        v_monto_dividir = 1; -- el monto total se dividira con este numero yese sera lo que se le pagara
        v_tipo_moneda = 'bs';
    ELSIF v_record_afiliado.pais = 'PERU'
    THEN --cuando es de peru
        v_monto_multiplicar = 1; -- es un sol
        v_monto_dividir = 2; -- el monto total se dividira con este numero yese sera lo que se le pagara
        v_tipo_moneda = 'soles';
    ELSE
        RAISE EXCEPTION '%', 'no hay codigo de area para esta afiliacion';
    END IF;





    --verificamos de donde corresponde el afiliado si es peruano o boliviano


    v_continuar := 'Y';
    IF EXISTS (SELECT 0 FROM afi.tafiliado_pago_periodo
               where id_afiliado = v_id_afiliado_in and id_periodo = v_id_periodo_in)
    THEN
        --sii existe el afiliado con el mismo periodo se borra para registrar uno actualizado o lo mismo



        select estado,id_afiliado_pago_periodo into v_estado,v_id_afiliado_pago_periodo_aux from afi.tafiliado_pago_periodo
        where id_afiliado = v_id_afiliado_in and id_periodo = v_id_periodo_in;



        if v_estado = 'pagado' then

            --RAISE EXCEPTION '%','ya se encuentra pagado';
            v_continuar := 'N';

        ELSE

            DELETE FROM afi.tafiliado_pago_periodo
            where id_afiliado_pago_periodo = v_id_afiliado_pago_periodo_aux
              and id_periodo = v_id_periodo_in
              and id_afiliado = v_id_afiliado_in;

        END IF ;




    END IF;

    --verificamos que ya no se actualize los datos de la tabla pago periodo si ya estamos en otro periodo
    IF v_continuar = 'Y' THEN
        v_rec_gestion_periodo = param.f_get_periodo_gestion(to_char(now(), 'YYYY-mm-dd')::DATE);
        --si el periodo es menor al periodo actual de la fecha now entonces no se deberia actualizar los datos
        IF v_rec_gestion_periodo.po_id_periodo < v_id_periodo_in AND p_administrador =0 THEN
            v_continuar := 'N';
        END IF;
    END IF;

    --controlamos que si ya esta habilitado el perido para pagar ya se volvera a generar el pago periodo del afiliado
    IF EXISTS (select 0
               FROM afi.tpago_conf
               where id_periodo = v_id_periodo_in)
    THEN
        v_continuar := 'N';
    END IF;



    IF v_continuar = 'Y' THEN

        -- debemos obtener el total de bonus que sea de registro y que no tenga nivel from
        SELECT sum(tb.monto)
        into v_bonus_registro_ya_pagado -- este bonus ya se paga apenas registra al afiliado a su patrocinador asi que por eso no se debe tomar encuenta
        from afi.tbonus tb
                 inner join afi.tbonus_config tbc on tbc.id_bonus_config = tb.id_bonus_config
        where tb.id_periodo = v_id_periodo_in
          and tb.id_patrocinador = v_id_afiliado_in
          and tbc.tipo_pago_bonus_patrocinador_directo = 'instante';

        if(v_bonus_registro_ya_pagado is NULL )then
            v_bonus_registro_ya_pagado = 0;
        END IF ;


        -- necesitamos validar si el afiliado tiene la condicion si tiene el mismo paquete dentro su compra o registro para cobrar este bonus para NATURALGO

        SELECT sum(tb.monto)
        into v_bonus_registro_no_pagado
        from afi.tbonus tb
                 inner join afi.tbonus_config tbc on tbc.id_bonus_config = tb.id_bonus_config
                 left JOIN afi.tpunto_item tpi on tpi.id_bonus_config = tbc.id_bonus_config
        where tb.id_periodo = v_id_periodo_in
          and tb.id_patrocinador = v_id_afiliado_in
          and tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'

          and tb.nivel_from is null and tb.id_afiliado_from is null;

        if(v_bonus_registro_no_pagado is NULL )then
            v_bonus_registro_no_pagado = 0;
        END IF ;


        --validamos si el afiliado fue registrado antes del 2022 todos pueden cobrar el bonus paquete
        if v_record_afiliado.fecha_reg::date < '01-01-2022' then

            WITH t_bonus_patrocinio_que_esta_validado_para_cobrar AS (
                SELECT tbc.id_bonus_config
                FROM afi.tbonus_config tbc
                WHERE (tbc.validar_tipo_bonus = 'no' or tbc.validar_tipo_bonus = 'si')
                  and tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
                GROUP BY tbc.id_bonus_config
            ), t_sum_patrocinio_con_validacion as (
                SELECT sum(tb.monto) as sum
                from afi.tbonus tb
                         inner join t_bonus_patrocinio_que_esta_validado_para_cobrar tbc on tbc.id_bonus_config = tb.id_bonus_config
                where tb.id_periodo = v_id_periodo_in
                  and tb.id_patrocinador = v_id_afiliado_in
                  and tb.nivel_from is not null and tb.id_afiliado_from is not null
            )select sum
            into v_bonus_patrocinio_multinivel
            from t_sum_patrocinio_con_validacion;

        else

            WITH t_bonus_patrocinio_que_esta_validado_para_cobrar AS (
                SELECT tbc.id_bonus_config
                FROM afi.tpuntos_afiliado tpa
                         INNER JOIN afi.tpunto_item tpi ON tpi.id_set = tpa.id_set_item
                         INNER JOIN afi.tbonus_config tbc ON tbc.id_bonus_config = tpi.id_bonus_config
                WHERE tpa.id_afiliado = v_id_afiliado_in
                  AND tbc.bono_patrocinio_por_nivel IS NOT NULL
                  AND tbc.validar_tipo_bonus = 'si'
                  and tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
                GROUP BY tbc.id_bonus_config
                UNION ALL
                SELECT tbc.id_bonus_config
                FROM afi.tbonus_config tbc
                WHERE tbc.validar_tipo_bonus = 'no'
                  and tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
                GROUP BY tbc.id_bonus_config
            ), t_sum_patrocinio_con_validacion as (
                SELECT sum(tb.monto) as sum
                from afi.tbonus tb
                         inner join t_bonus_patrocinio_que_esta_validado_para_cobrar tbc on tbc.id_bonus_config = tb.id_bonus_config
                where tb.id_periodo = v_id_periodo_in
                  and tb.id_patrocinador = v_id_afiliado_in
                  and tb.nivel_from is not null and tb.id_afiliado_from is not null
            )select sum
            into v_bonus_patrocinio_multinivel
            from t_sum_patrocinio_con_validacion;

        END IF;




        if(v_bonus_patrocinio_multinivel is NULL )then
            v_bonus_patrocinio_multinivel = 0;
        END IF ;



        --obtenemos el monto total del bonus adquerido por suscribir registros directos
        select sum(monto)
        into v_bonus
        from afi.tbonus
        where id_periodo = v_id_periodo_in
          and id_patrocinador = v_id_afiliado_in;

        --contamos cuantos registros tuvo
        select count(*)
        into v_bonus_count
        from afi.tbonus
        where id_periodo = v_id_periodo_in
          and id_patrocinador = v_id_afiliado_in
          and (nivel_from is null or nivel_from = 1);


        if(v_bonus is NULL )then
            v_bonus = 0;
        END IF ;

        if v_tipo_moneda = 'soles' THEN
            v_bonus = v_bonus / 2;

        END IF;


        --obtenemos el total de puntos comisionables y de volumen propios del seleccionado
        select sum(cv),sum(pv)
        into v_cv_propio,v_pv_propio
        from afi.tpuntos_afiliado
        where id_afiliado = v_id_afiliado_in and id_periodo = v_id_periodo_in;


        if(v_cv_propio is NULL )then
            v_cv_propio = 0;
        END IF ;

        if(v_pv_propio is NULL )then
            v_pv_propio = 0;
        END IF ;


        --vemos si esta habilitado para poder cobrar
        if(v_pv_propio >= v_record_rango_actual.volumen_personal)THEN
            v_habilitado = 'si';
        ELSE
            v_habilitado = 'no';
        END IF ;


        --obtenemos el bono presentador
        SELECT coalesce(sum(monto), 0 )
        into v_total_presentador
        FROM afi.tafiliado_presentador
        where id_presentador = v_id_afiliado_in and id_periodo = v_id_periodo_in;




        -- obtenemos el total de retorno por sus compras bono retorno activacion
        WITH t_sum_cv_por_compra_retorno as
                 (
                     SELECT sum(tpa.cv) as sum
                     FROM afi.tpuntos_afiliado tpa
                              INNER JOIN mer.titem ti ON ti.id_item = tpa.id_item
                              INNER JOIN afi.tpunto_item tpi ON tpi.id_item = ti.id_item
                     WHERE tpa.id_afiliado = v_id_afiliado_in
                       AND tpa.id_periodo = v_id_periodo_in
                       AND tpi.bono_retorno = 'Y'
                 ) select case when sum >= v_record_rango_actual.volumen_personal then ((sum / 35) * 4) / v_monto_dividir else 0 end as total_retorno
        into v_bono_retorno_por_compra_activacion
        from t_sum_cv_por_compra_retorno;








        --vemos cuantos niveles puede cobrar  segun su rango
        v_niveles_para_cobrar = v_record_rango_actual.niveles_a_cobrar;


        IF pxp.f_get_variable_global('empresa_afiliacion') = 'naturalgo' THEN

            --esto es por si no se habilito con lo normal entonces  si tienen 50 como volumen personal podra cobrar soloamente un nivel
            IF (v_habilitado = 'no' AND v_pv_propio >= 50) THEN

                v_habilitado = 'si';
                --le habilitamos solo el primer nivel para cobrar
                v_niveles_para_cobrar = 1;

            END IF;

        END IF;


        --promocion para naturalgo si del 1feb2022 al 12feb2022 compro 8 productos osea 104 pv entonces cobrara 4 niveles
        IF pxp.f_get_variable_global('empresa_afiliacion') = 'naturalgo' THEN
            select sum(pv)
            into v_pv_propio_promo_1_12_feb2022
            from afi.tpuntos_afiliado
            where id_afiliado = v_id_afiliado_in and id_periodo = v_id_periodo_in
              and fecha_reg::date BETWEEN '01-02-2022'::date and '12-02-2022'::date;

            IF v_pv_propio_promo_1_12_feb2022 >= 104 and v_niveles_para_cobrar < 4 THEN
                v_promo_cuarto_nivel_2022:= 'si';
                v_niveles_para_cobrar = 4;
            END IF;
        END IF;



        -- ver si puede habilitarse con la opcion de activacion cero
        if(v_habilitado = 'no') then

            IF EXISTS (
                    WITH t_activacion_cero as (
                        select count(b.*) as count, bc.id_bonus_config, bc.activacion_cero
                        from afi.tbonus b
                                 inner join afi.tafiliado ta on ta.id_afiliado = b.id_afiliado
                                 inner join afi.tbonus_config bc on bc.id_bonus_config = b.id_bonus_config
                        where ta.id_patrocinador = v_id_afiliado_in
                          and b.id_periodo = v_id_periodo_in
                          and bc.activacion_cero is not null
                        group BY bc.id_bonus_config
                    ) select 1 from t_activacion_cero
                    where count = activacion_cero
                ) THEN

                -- do something
                v_habilitado = 'si';
            END IF;



        END IF;






        v_afiliados_activos_en_red = 0;



        v_personas_comprando_10 = 0; -- 17 de marzo 2019

        /* IF pxp.f_get_variable_global('empresa_afiliacion') = 'naturalgo' THEN

             --requermiento marlo naturalgo 8 de marzo 2022
             -- si tiene hijos directos que compraron paquete entonces entran al conteo de plan de 90 dias
             select  COALESCE(count(*), 0 )
             into v_count_hijos_directos_con_premium
             from afi.tbonus tb
                      inner join afi.tafiliado ta on ta.id_afiliado = tb.id_afiliado
             where ta.id_patrocinador = v_id_afiliado_in and tb.id_periodo = v_id_periodo_in and tb.id_bonus_config in(3) and tb.id_afiliado_from is null;

             v_personas_comprando_10:= v_count_hijos_directos_con_premium;
       END IF;*/



        v_generaciones_cv:= '{0,0,0,0,0,0}' ;
        v_generaciones_pv:= '{0,0,0,0,0,0}' ;
        v_generaciones_cv_monto:= '{0,0,0,0,0,0}' ;
        v_generaciones_count_afiliados_100pts_pv:= '{0,0,0,0,0,0}' ;
        v_generaciones_count_afiliados_activos:= '{0,0,0,0,0,0}' ;


        -- 16 dec 2021 marlo queria que una compra de una paquete genere sus propias regalias y nivel
        -- se debe obtener toda la configuracion desde bonus config una columna de json
        -- verificamos si este usuario tiene alguna compra o registro de un paquete con esta configuracion especial
        --todo

        -- vemos todos los niveles pero solo tomamos en cuenta hasta el 5 nivel
        FOR v_record IN (WITH RECURSIVE t(nivel, id_afiliado) AS (
            SELECT
                1 AS nivel,
                afil.id_afiliado
            FROM afi.tafiliado afil
            WHERE id_patrocinador = v_id_afiliado_in

            UNION ALL
            SELECT
                    nivel + 1,
                    afil.id_afiliado
            FROM afi.tafiliado afil
                     INNER JOIN t t ON t.id_afiliado = afil.id_patrocinador
        )
                         SELECT
                             t.nivel,
                             t.id_afiliado,
                             afil.codigo,
                             patro.codigo,
                             per.nombre_completo2,
                             per_patro.nombre_completo2,
                             sum(puntos.cv) AS sum_cv,
                             sum(puntos.pv) AS sum_pv

                         FROM t t
                                  INNER JOIN afi.tafiliado afil ON afil.id_afiliado = t.id_afiliado
                                  INNER JOIN afi.tafiliado patro ON patro.id_afiliado = afil.id_patrocinador
                                  INNER JOIN segu.vpersona2 per ON per.id_persona = afil.id_persona
                                  INNER JOIN segu.vpersona2 per_patro ON per_patro.id_persona = patro.id_persona

                                  INNER JOIN afi.tpuntos_afiliado puntos ON puntos.id_afiliado = t.id_afiliado

                         WHERE puntos.id_periodo = v_id_periodo_in
                           AND nivel <= 6 -- this should be a  variable
                         GROUP BY t.nivel,
                                  t.id_afiliado,
                                  afil.codigo,
                                  patro.codigo,
                                  per.nombre_completo2,
                                  per_patro.nombre_completo2
                         ORDER BY nivel ASC) LOOP --comienza el ciclo

        if v_generaciones_pv[v_record.nivel] is null then
            v_generaciones_pv[v_record.nivel]:=0;
        END IF;
        if v_generaciones_cv[v_record.nivel] is null then
            v_generaciones_cv[v_record.nivel]:=0;
        END IF;
        if v_generaciones_count_afiliados_100pts_pv[v_record.nivel] is null then
            v_generaciones_count_afiliados_100pts_pv[v_record.nivel]:=0;
        END IF;
        if v_generaciones_count_afiliados_activos[v_record.nivel] is null then
            v_generaciones_count_afiliados_activos[v_record.nivel]:=0;
        END IF;

        IF v_record.sum_cv is NULL THEN
            v_record.sum_cv = 0;
        END IF;
        IF v_record.sum_pv is NULL THEN
            v_record.sum_pv = 0;
        END IF;



        -- solo si es afiliado del primer nivel se deben tomar en cuenta los puntos pv de las afiliaciones de paquete
        IF (pxp.f_get_variable_global('empresa_afiliacion') = 'canaan' and v_record_afiliado.pais = 'BOLIVIA' AND v_record.nivel > 3)
            OR
           (pxp.f_get_variable_global('empresa_afiliacion') = 'naturalgo' AND v_record.nivel > 1)
        THEN

            select  coalesce(sum(tpa.pv), 0) as sum_pv_for_afiliacion, coalesce(sum(tpa.cv), 0) as sum_cv_for_afiliacion
            into v_sum_pv_for_afiliacion,v_sum_cv_for_afiliacion
            from afi.tpuntos_afiliado tpa
            inner join afi.tpunto_item tpi on tpi.id_set = tpa.id_set_item
            inner join afi.tbonus_config tb on tb.id_bonus_config = tpi.id_bonus_config
            where id_periodo = v_id_periodo_in
            and  id_afiliado = v_record.id_afiliado
            and (
                (tb.generar_regalias_a_niveles is not NULL and tb.generar_regalias_a_niveles < v_record.nivel)
                    or
                tpa.pv = 0 and tpa.cv = 0
            );


            v_record.sum_pv:= v_record.sum_pv - v_sum_pv_for_afiliacion;
            v_record.sum_cv:= v_record.sum_cv - v_sum_cv_for_afiliacion;


        END IF;


        --vemos cuantos estan activos en la red seleccionada
        SELECT ran.volumen_personal INTO v_volumen_puntos_hijos
        FROM afi.trango_afiliado ranafi
                 inner JOIN afi.trango ran on ran.id_rango = ranafi.id_rango
        where ranafi.id_afiliado = v_record.id_afiliado and ranafi.id_periodo = v_id_periodo_in;

        --RAISE EXCEPTION '%',v_volumen_puntos_hijos;

        IF v_record.sum_pv >= v_volumen_puntos_hijos THEN
            v_generaciones_count_afiliados_activos[v_record.nivel]:= v_generaciones_count_afiliados_activos[v_record.nivel] + 1;
            v_afiliados_activos_en_red = v_afiliados_activos_en_red + 1;
        ELSE
            IF v_record.nivel = 1 then
                --agregamos los id afiliado inactivos que esten menos de 104
                v_id_afiliados_inactivo_primer_nivel:= array_append(v_id_afiliados_inactivo_primer_nivel, v_record.id_afiliado);
            END IF;




        END IF;




        v_generaciones_pv[v_record.nivel] := COALESCE(v_generaciones_pv[v_record.nivel], 0) + COALESCE(v_record.sum_pv, 0);
        v_generaciones_cv[v_record.nivel] := COALESCE(v_generaciones_cv[v_record.nivel],0) + COALESCE(v_record.sum_cv, 0);

        IF v_record.sum_pv >= 100 THEN
            v_generaciones_count_afiliados_100pts_pv[v_record.nivel] := v_generaciones_count_afiliados_100pts_pv[v_record.nivel] + 1;
        END IF;


        --sumamos por niveles
        IF v_record.nivel = 1
        THEN
            -- 17 de marzo de 2019 nuevo requerimiento para ver si se tiene 5 hijos comprando 100 pts
            IF v_record.sum_pv >= 240 THEN
                v_personas_comprando_10 := v_personas_comprando_10 + 1;


            END IF;


        END IF;


    END LOOP;--termina el ciclo


    -- COMPRESION DINAMICA
    -- comprimimos el 1 nivel que no estan habilitados para que los segundos nivles del 1 nivel pasen al 1 nivel del afiliado que esta haciendo la peticion
        /*if(v_id_afiliados_inactivo_primer_nivel is not null) THEN
            select sum(puntos.cv) AS sum_cv,
                   sum(puntos.pv) AS sum_pv
            into v_compresion_dinamica_nivel_1_sum
            from afi.tafiliado ta
                     INNER JOIN afi.tpuntos_afiliado puntos ON puntos.id_afiliado = ta.id_afiliado
            where ta.id_afiliado = ANY (v_id_afiliados_inactivo_primer_nivel) and id_periodo = v_id_periodo_in;
            IF v_compresion_dinamica_nivel_1_sum is not null THEN

                --aumentamos los puntos al primer nivel
                v_generaciones_pv[1] := COALESCE(v_generaciones_pv[1], 0) + COALESCE(v_compresion_dinamica_nivel_1_sum.sum_pv, 0);
                v_generaciones_cv[1] := COALESCE(v_generaciones_cv[1],0) + COALESCE(v_compresion_dinamica_nivel_1_sum.sum_cv, 0);
                --descontamos los puntos al segundo nivel por que estos se fueron al 1
                v_generaciones_pv[2] := COALESCE(v_generaciones_pv[2], 0) - COALESCE(v_compresion_dinamica_nivel_1_sum.sum_pv, 0);
                v_generaciones_cv[2] := COALESCE(v_generaciones_cv[2],0) - COALESCE(v_compresion_dinamica_nivel_1_sum.sum_cv, 0);
            END IF;

        END IF;
*/




        --promocion para naturalgo si del 1mar2022 al 15mar2022 si tiene 6 hijos directos con 8 productos entonces se habilitara el 4 nivel
        IF pxp.f_get_variable_global('empresa_afiliacion') = 'naturalgo' THEN
            IF v_personas_comprando_10 >= 6 and v_niveles_para_cobrar <= 3 THEN
                v_promo_cuarto_nivel_2022:= 'si';
                v_niveles_para_cobrar = 4;
            END IF;
        END IF;




        --verificamos que este habilitado por los parametros de afiliados activos en la red
        --el rango de afiliados activos tiene que ser menor o igual a los activos en red
        IF v_record_rango_actual.afiliado_activos <= v_afiliados_activos_en_red THEN

            if v_habilitado = 'si' THEN
                v_habilitado = 'si';
            ELSE
                v_habilitado = 'no';
            END IF;

        ELSE
            v_habilitado = 'no';
        END IF;


        --veremos cuanto tienen en pv en sus niveles correspondientes segun su rango


        --vemos si sus pv son igual o mayo al volumen de cobro del siguiente nivel con esto podran
        --cobrar el siguiente nivel mas pero ser ese rango correspondiente
        /*IF v_pv_propio >= v_record_rango_siguiente.volumen_cobro_nivel THEN

          --si es mas cobra el siguiente nivel mas
          v_niveles_para_cobrar = v_record_rango_siguiente.nivel;

          --vemos si en los demas niveles puede cobrar
          --ver que nivel maximo puede cobrar con sus puntos

          select nivel
          INTO v_niveles_para_cobrar
          from afi.trango
          where volumen_cobro_nivel <= v_pv_propio ORDER BY nivel DESC limit 1;

        ELSE
          --si no es solo cobra el nivel que tiene
          v_niveles_para_cobrar = v_record_rango_actual.nivel;
        END IF;*/


        v_count_aux = 1;
        pv_niveles_correspondientes = 0;
        cv_total_pago_niveles_correspondientes = 0;

        v_niveles_cobrados = '';



        WHILE v_count_aux <= v_niveles_para_cobrar LOOP

                if v_generaciones_cv[v_count_aux] is not null and v_generaciones_pv[v_count_aux] is not null THEN

                    v_niveles_cobrados = v_niveles_cobrados || v_count_aux::VARCHAR || ',';
                    --suma los pv para ver si subiera nivel o no

                    pv_niveles_correspondientes = pv_niveles_correspondientes + COALESCE(v_generaciones_pv[v_count_aux], 0);

                    --calculamos el monto a pagar dependiendo su nivel

                    --vemos primero los porcentajes de cada nivel
                    v_porcentaje_nivel = v_record_rango_actual.porcentajes[v_count_aux];




                    --20 de julio 2019 - nuevo requerimient de marlon si se tiene 5 personas comprando 100
                    --se aumenta el porcentaje en el primer nivel 40 % y tercero 50 %
                    IF pxp.f_get_variable_global('empresa_afiliacion') = 'naturalgo' THEN
                        IF v_personas_comprando_10 >= 5 AND v_count_aux = 1 THEN
                            v_porcentaje_nivel = 50;
                        ELSIF v_personas_comprando_10 >= 5 AND v_count_aux = 3 THEN
                            v_porcentaje_nivel = 60;
                        END IF;

                        IF v_porcentaje_nivel is null and v_promo_cuarto_nivel_2022 = 'si' and v_count_aux = 4 THEN
                            v_porcentaje_nivel:= 10;
                        END IF;

                    END IF;


                    v_porcentajes_pagados[v_count_aux] := v_porcentaje_nivel;


                    cv_total_pago_niveles_correspondientes = cv_total_pago_niveles_correspondientes + ((v_generaciones_cv[v_count_aux] * (v_porcentaje_nivel/100)) * v_monto_multiplicar) ;

                END IF;

                v_count_aux = v_count_aux + 1;
            END LOOP;



        --raise EXCEPTION '%',v_record_rango_actual;
        -- 2020-11-20 agregar validacion para sumar puntos para subir rangos
        IF v_record_rango_actual.nivel >= 5 THEN -- si es mas de diamante entonces sumara mas niveles
        -- agregar validacion si es arriba de diamante suma su 6 nivel para subir de rango
            pv_niveles_correspondientes =  COALESCE(v_generaciones_pv[1], 0) + COALESCE(v_generaciones_pv[2], 0) + COALESCE(v_generaciones_pv[3] , 0)+ COALESCE(v_generaciones_pv[4] , 0)+ COALESCE(v_generaciones_pv[5], 0) + COALESCE(v_generaciones_pv[6], 0);
        ELSE
            --por el momento sumar siempre 5 niveles para calificar soloc aligicar PV
            pv_niveles_correspondientes =  COALESCE(v_generaciones_pv[1], 0) + COALESCE(v_generaciones_pv[2], 0) + COALESCE(v_generaciones_pv[3] , 0)+ COALESCE(v_generaciones_pv[4] , 0)+ COALESCE(v_generaciones_pv[5], 0);
        END IF;



        --  desde 31 de octubre de 2020 el punto personal pv personal se sumara a los pv de los niveles para subir rango
        pv_niveles_correspondientes = pv_niveles_correspondientes + v_pv_propio;




        v_pago_total_ganado = (cv_total_pago_niveles_correspondientes + v_bonus);
        v_pago_total = cv_total_pago_niveles_correspondientes + v_bonus_registro_no_pagado + v_bonus_patrocinio_multinivel + v_bono_retorno_por_compra_activacion + v_total_presentador;



        FOR v_record_rango_porcentaje IN (


            WITH t_rango_porcentaje as (
                select unnest(v_porcentajes_pagados) as porcentaje
                /* SELECT unnest(porcentajes) as porcentaje
                 FROM afi.trango
                 WHERE id_rango = v_record_rango_actual.id_rango*/
            )
            SELECT row_number() OVER () as rnum, trp.porcentaje
            from t_rango_porcentaje trp

        )
            LOOP

                IF v_generaciones_cv[v_record_rango_porcentaje.rnum] is not null then

                    v_porcentaje_a_pagar:= v_record_rango_porcentaje.porcentaje;

                    IF pxp.f_get_variable_global('empresa_afiliacion') = 'naturalgo' THEN

                        IF v_personas_comprando_10 >= 5 THEN
                            IF v_record_rango_porcentaje.rnum = 1 THEN
                                v_porcentaje_a_pagar := 50;
                            END IF;
                            IF v_record_rango_porcentaje.rnum = 3 THEN
                                v_porcentaje_a_pagar := 60;
                            END IF;

                        END IF;
                    END IF;

                    --el punto comionable vale 2bs y en peru 1 sol
                    -- este es el monto total por cada generacion segun su porcentaje de rango configurado
                    v_generaciones_cv_monto[v_record_rango_porcentaje.rnum]:= (v_generaciones_cv[v_record_rango_porcentaje.rnum] * (v_porcentaje_a_pagar / 100)) * v_monto_multiplicar;


                END IF;

            END LOOP;


        SELECT
            sum(a) AS total
        into v_pago_red
        FROM
            (
                SELECT
                    unnest(v_generaciones_cv_monto) AS a
            ) AS b;






        --sacamos el periodo siguiente para agregar su rango nuevo
        v_id_periodo_siguiente = afi.f_get_id_periodo_siguiente(v_id_periodo_in);

        --eliminamos el rango afiliado en dicho periodo para agregar uno nnuevo
        DELETE FROM afi.trango_afiliado where id_periodo = v_id_periodo_siguiente and id_afiliado = v_id_afiliado_in;

        --vemos si los puntos de volumen aquellos que hacen subir nivel
        --verificamos si sube nivel o mantiene su nivel (rangos)
        IF pv_niveles_correspondientes >= v_record_rango_siguiente.volumen_organizacional THEN
            --si subio

            v_id_rango_nuevo = v_record_rango_siguiente.id_rango;
            v_sube_nivel = 'si';
        ELSE
            --no subio nivel
            v_id_rango_nuevo = v_record_rango_actual.id_rango;
            v_sube_nivel = 'no';
        END IF;

        --logica para verificar a que rango le corresponde en caso de que suba nivel
        SELECT *
        INTO v_rango_obtenido_con_sus_puntos
        FROM afi.trango
        WHERE volumen_organizacional <= pv_niveles_correspondientes
        ORDER BY nivel desc
        LIMIT 1;

        IF v_rango_obtenido_con_sus_puntos.nivel > v_record_rango_actual.nivel THEN
            v_sube_nivel = 'si';
            v_id_rango_nuevo = v_rango_obtenido_con_sus_puntos.id_rango;
            v_rango_para_su_siguiente_periodo = v_rango_obtenido_con_sus_puntos;

        else
            v_id_rango_nuevo = v_record_rango_actual.id_rango;
            v_sube_nivel = 'no';
            v_rango_para_su_siguiente_periodo = v_record_rango_actual;


        END IF;

        --insertamos el rango nuevo del afiliado
       /* INSERT INTO afi.trango_afiliado (id_usuario_reg,
                                         id_usuario_mod,
                                         fecha_reg,
                                         fecha_mod,
                                         estado_reg,
                                         id_usuario_ai,
                                         usuario_ai,
                                         id_afiliado,
                                         id_rango,
                                         id_periodo) VALUES (p_id_usuario,
                                                             NULL,
                                                             now(),
                                                             NULL,
                                                             'activo',
                                                             v_parametros._id_usuario_ai,
                                                             v_parametros._nombre_usuario_ai,
                                                             v_id_afiliado_in,
                                                             v_id_rango_nuevo,
                                                             v_id_periodo_siguiente);*/







        --crear un json que tenga datos auxiliares con los que se trabajo para obtener estos totales y puntos etc
        SELECT json_strip_nulls(json_build_object(
                'rango_siguiente_volumen_organizacional', v_record_rango_siguiente.volumen_organizacional,
                'rango_siguiente_nombre', v_record_rango_siguiente.nombre,
                'rango_actual_nombre', v_record_rango_actual.nombre,
                'rango_en_su_siguiente_periodo', v_rango_para_su_siguiente_periodo.nombre,
                'pv_acumulado', pv_niveles_correspondientes,
                'personas_comprando_10', v_personas_comprando_10,
                'id_afiliados_inactivo_primer_nivel', v_id_afiliados_inactivo_primer_nivel
            ))
        INTO v_data_json_aux;




        --Sentencia de la insercion
        insert into afi.tafiliado_pago_periodo(
            id_afiliado,
            pago_bonus_registro,
            afiliados_registrados,
            habilitado,
            pago_de_red,
            estado,
            cv_propio,
            id_periodo,
            pago_total,
            descripcion_pago_red,
            pv_propio,
            estado_reg,
            id_usuario_ai,
            fecha_reg,
            usuario_ai,
            id_usuario_reg,
            fecha_mod,
            id_usuario_mod,


            primera_gen_cv,
            primera_gen_pv,

            segunda_gen_cv,
            segunda_gen_pv,

            tercera_gen_cv,
            tercera_gen_pv,

            cuarta_gen_cv,
            cuarta_gen_pv,

            quinta_gen_cv,
            quinta_gen_pv,
            sexta_gen_pv,

            monto_multiplicar,
            tipo_moneda,
            afiliados_activos,
            niveles_cobrados,
            sube_nivel,
            pv_acumulado,
            porcentajes_pagados,
            generaciones_cv,
            generaciones_pv,
            generaciones_cv_monto,
            generaciones_count_afiliados_100pts_pv,
            generaciones_count_afiliados_activos,
            monto_bonus_ya_pagado,
            total_ganado,
            monto_bonus_no_pagado,
            monto_bonus_multinivel,
            total_bono_retorno_activacion,
            data_json_aux

        ) values(
                    v_id_afiliado_in,
                    v_bonus,
                    v_bonus_count,
                    v_habilitado,
                    v_pago_red,
                    'registrado no pagado',
                    v_cv_propio,
                    v_id_periodo_in,
                    v_pago_total,
                    '',
                    v_pv_propio,
                    'activo',
                    v_parametros._id_usuario_ai,
                    now(),
                    v_parametros._nombre_usuario_ai,
                    p_id_usuario,
                    null,
                    null,


                    0,
                    0,

                    0,
                    0,

                    0,
                    0,

                    0,
                    0,

                    0,
                    0,
                    0,

                    v_monto_multiplicar,
                    v_tipo_moneda,
                    v_afiliados_activos_en_red,
                    v_niveles_cobrados,
                    v_sube_nivel,
                    pv_niveles_correspondientes,
                    v_porcentajes_pagados,
                    v_generaciones_cv,
                    v_generaciones_pv,
                    v_generaciones_cv_monto,
                    v_generaciones_count_afiliados_100pts_pv,
                    v_generaciones_count_afiliados_activos,
                    v_bonus_registro_ya_pagado,
                    v_pago_total_ganado,
                    v_bonus_registro_no_pagado,
                    v_bonus_patrocinio_multinivel,
                    v_bono_retorno_por_compra_activacion,
                    v_data_json_aux



                )RETURNING id_afiliado_pago_periodo into v_id_afiliado_pago_periodo;

    ELSE
        v_id_afiliado_pago_periodo := 0;
    END IF;





    RETURN v_id_afiliado_pago_periodo;


EXCEPTION
    WHEN OTHERS THEN
        v_resp='';
        v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', SQLERRM);
        v_resp = pxp.f_agrega_clave(v_resp, 'codigo_error', SQLSTATE);
        v_resp = pxp.f_agrega_clave(v_resp, 'procedimientos', 'f_get_pago_periodo_afiliado');
        RAISE EXCEPTION '%', v_resp;


END;
$BODY$
    LANGUAGE plpgsql VOLATILE;