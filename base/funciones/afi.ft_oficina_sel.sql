CREATE OR REPLACE FUNCTION afi.ft_oficina_sel(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
  $BODY$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_oficina_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'afi.tafiliado'
 AUTOR: 		 (admin)
 FECHA:	        02-04-2015 01:06:18
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;

BEGIN

	v_nombre_funcion = 'afi.ft_oficina_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'AFI_AFIL_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin
 	#FECHA:		02-04-2015 01:06:18
	***********************************/

	if(p_transaccion='AFI_OFI_SEL')then

    	begin



            v_consulta := 'select
										  afil.direccion_principal,
										  afil.lugar_nacimiento,
										  afil.estado_reg,
										  afil.fecha_nacimiento,
										  PERSON.nombre_completo2 as desc_person,
										  PERSON.ci,
										  afil.codigo as codigo,

										  PERSON.celular1,
										  PERSON.celular2,
										  PERSON.telefono1,
										  PERSON.telefono2,
										  afil.id_afiliado as li,
										  patro.codigo as codigo_patrocinador,
										  persona2.nombre_completo2 as desc_person_patrocinador
										from afi.tafiliado afil
										  inner join segu.tusuario usu1 on usu1.id_usuario = afil.id_usuario_reg
										  INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
										  left join segu.tusuario usu2 on usu2.id_usuario = afil.id_usuario_mod
										  left join afi.tafiliado patro on patro.id_afiliado = afil.id_patrocinador
										  left join segu.vpersona persona2 on persona2.id_persona = patro.id_persona
										  WHERE ';



			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;
			--v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;



			--Devuelve la respuesta
			return v_consulta;

		end;

	/*********************************
 	#TRANSACCION:  'AFI_OFI_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin
 	#FECHA:		02-04-2015 01:06:18
	***********************************/

	elsif(p_transaccion='AFI_OFI_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(afil.id_afiliado)
					    from afi.tafiliado afil
					    inner join segu.tusuario usu1 on usu1.id_usuario = afil.id_usuario_reg
					    INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
						left join segu.tusuario usu2 on usu2.id_usuario = afil.id_usuario_mod
						inner join afi.tafiliado patro on patro.id_afiliado = afil.id_afiliado
						inner join segu.vpersona persona2 on persona2.id_persona = patro.id_persona
					    where ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;





			--Devuelve la respuesta
			return v_consulta;

		end;



  /*********************************
 #TRANSACCION:  'OFI_ARB_SEL'
 #DESCRIPCION:	LISTAR arbol de los afil
 #AUTOR:		admin
 #FECHA:		02-04-2015 01:06:18
***********************************/
  elsif(p_transaccion='OFI_ARB_SEL')then

    begin
      --Sentencia de la consulta de conteo de registros
      v_consulta:='select count(id_afiliado)
					    from afi.tafiliado afil
					    inner join segu.tusuario usu1 on usu1.id_usuario = afil.id_usuario_reg
					    INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
						left join segu.tusuario usu2 on usu2.id_usuario = afil.id_usuario_mod
					    where ';

      --Definicion de la respuesta
      v_consulta:=v_consulta||v_parametros.filtro;





      --Devuelve la respuesta
      return v_consulta;

    end;



	else

		raise exception 'Transaccion inexistente';

	end if;

EXCEPTION

	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
