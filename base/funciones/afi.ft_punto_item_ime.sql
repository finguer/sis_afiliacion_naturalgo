CREATE OR REPLACE FUNCTION "afi"."ft_punto_item_ime" (	
				p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$

/**************************************************************************
 SISTEMA:		afiliacion
 FUNCION: 		afi.ft_punto_item_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tpunto_item'
 AUTOR: 		 (admin)
 FECHA:	        18-12-2015 23:29:48
 COMENTARIOS:	
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:	
 AUTOR:			
 FECHA:		
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_punto_item	integer;
			    
BEGIN

    v_nombre_funcion = 'afi.ft_punto_item_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************    
 	#TRANSACCION:  'AFI_PUNTI_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin	
 	#FECHA:		18-12-2015 23:29:48
	***********************************/

	if(p_transaccion='AFI_PUNTI_INS')then
					
        begin

          IF EXISTS (SELECT 0 FROM afi.tpunto_item where id_item = v_parametros.id_item )
          THEN
          RAISE EXCEPTION '%','ya existe este item con puntos';
          END IF;


          IF EXISTS (SELECT 0 FROM afi.tpunto_item where id_set = v_parametros.id_set )
          THEN
            RAISE EXCEPTION '%','ya existe este set con puntos';
          END IF;


        	--Sentencia de la insercion
        	insert into afi.tpunto_item(
			pv,
			estado_reg,
			cv,
			id_set,
			id_item,
			usuario_ai,
			fecha_reg,
			id_usuario_reg,
			id_usuario_ai,
			fecha_mod,
			id_usuario_mod,
							tipo,
							bonus_patrocinador,
        	                            id_bonus_config
          	) values(
			v_parametros.pv,
			'activo',
			v_parametros.cv,
			v_parametros.id_set,
			v_parametros.id_item,
			v_parametros._nombre_usuario_ai,
			now(),
			p_id_usuario,
			v_parametros._id_usuario_ai,
			null,
			null,
							v_parametros.tipo,
							v_parametros.bonus_patrocinador,
							v_parametros.id_bonus_config

			
			
			)RETURNING id_punto_item into v_id_punto_item;
			
			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','punto item almacenado(a) con exito (id_punto_item'||v_id_punto_item||')'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_punto_item',v_id_punto_item::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************    
 	#TRANSACCION:  'AFI_PUNTI_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin	
 	#FECHA:		18-12-2015 23:29:48
	***********************************/

	elsif(p_transaccion='AFI_PUNTI_MOD')then

		begin
			--Sentencia de la modificacion
			update afi.tpunto_item set
			pv = v_parametros.pv,
			cv = v_parametros.cv,
			id_set = v_parametros.id_set,
			id_item = v_parametros.id_item,
			fecha_mod = now(),
			id_usuario_mod = p_id_usuario,
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai,
				tipo = v_parametros.tipo,
        bonus_patrocinador = v_parametros.bonus_patrocinador,
            id_bonus_config = v_parametros.id_bonus_config
			where id_punto_item=v_parametros.id_punto_item;
               
			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','punto item modificado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_punto_item',v_parametros.id_punto_item::varchar);
               
            --Devuelve la respuesta
            return v_resp;
            
		end;

	/*********************************    
 	#TRANSACCION:  'AFI_PUNTI_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin	
 	#FECHA:		18-12-2015 23:29:48
	***********************************/

	elsif(p_transaccion='AFI_PUNTI_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from afi.tpunto_item
            where id_punto_item=v_parametros.id_punto_item;
               
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','punto item eliminado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_punto_item',v_parametros.id_punto_item::varchar);
              
            --Devuelve la respuesta
            return v_resp;

		end;
         
	else
     
    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION
				
	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;
				        
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "afi"."ft_punto_item_ime"(integer, integer, character varying, character varying) OWNER TO postgres;
