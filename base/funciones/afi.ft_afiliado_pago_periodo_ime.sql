CREATE OR REPLACE FUNCTION afi.ft_afiliado_pago_periodo_ime(p_administrador int4, p_id_usuario int4, p_tabla varchar,
                                                            p_transaccion varchar)
    RETURNS varchar
AS
$BODY$
/**************************************************************************
SISTEMA:		afiliacion
FUNCION: 		afi.ft_afiliado_pago_periodo_ime
DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tafiliado_pago_periodo'
AUTOR: 		 (admin)
FECHA:	        02-04-2016 16:22:34
COMENTARIOS:
***************************************************************************
HISTORIAL DE MODIFICACIONES:

DESCRIPCION:
AUTOR:
FECHA:
***************************************************************************/

DECLARE

    v_nro_requerimiento                    integer;
    v_parametros                           record;
    v_id_requerimiento                     integer;
    v_resp                                 varchar;
    v_nombre_funcion                       text;
    v_mensaje_error                        text;
    v_id_afiliado_pago_periodo             integer;
    v_bonus                                NUMERIC;
    v_cv_propio                            NUMERIC;
    v_pv_propio                            numeric;
    v_cv_hijos                             NUMERIC;
    v_pv_hijos                             numeric;
    v_habilitado                           VARCHAR;
    v_record                               RECORD;
    v_record_afiliado                      RECORD;
    v_validado_para_pagar_bonus_patrocinio varchar;
    v_id_bonus_config_a_modificar          integer[];
    v_cv_nietos                            numeric;
    v_pv_nietos                            numeric;
    v_cv_nietos_aux                        numeric;
    v_pv_nietos_aux                        numeric;
    v_pago_hijos                           NUMERIC(10, 2);
    v_pago_nietos                          NUMERIC(10, 2);
    v_pago_total                           NUMERIC(10, 2);
    v_pago_red                             numeric(10, 2);
    v_estado                               VARCHAR;
    v_record_primer_nivel                  RECORD;
    v_total_nivel_1_cv                     NUMERIC;
    v_total_nivel_1_pv                     NUMERIC;
    v_total_nivel_2_cv                     NUMERIC;
    v_total_nivel_2_pv                     NUMERIC;
    v_total_nivel_3_cv                     NUMERIC;
    v_total_nivel_3_pv                     NUMERIC;
    v_total_nivel_4_cv                     NUMERIC;
    v_total_nivel_4_pv                     NUMERIC;
    v_total_nivel_5_cv                     NUMERIC;
    v_total_nivel_5_pv                     NUMERIC;
    v_pago_total_nivel_1                   NUMERIC;
    v_pago_total_nivel_2                   NUMERIC;
    v_pago_total_nivel_3                   NUMERIC;
    v_pago_total_nivel_4                   NUMERIC;
    v_pago_total_nivel_5                   NUMERIC;
    v_codigo_area                          VARCHAR;
    v_monto_multiplicar                    NUMERIC;
    v_tipo_moneda                          VARCHAR;
    v_record_rango_actual                  RECORD;
    v_record_rango_siguiente               RECORD;
    v_volumen_puntos_hijos                 NUMERIC;
    v_afiliados_activos_en_red             INTEGER;
    v_niveles_para_cobrar                  INTEGER;
    v_total_nivel_pv                       NUMERIC[];
    v_total_nivel_cv                       NUMERIC[];
    v_count_aux                            INTEGER;
    pv_niveles_correspondientes            NUMERIC;
    cv_total_pago_niveles_correspondientes NUMERIC;
    v_porcentaje_nivel                     NUMERIC;
    v_niveles_cobrados                     VARCHAR;
    v_sube_nivel                           VARCHAR;
    v_id_periodo_siguiente                 INTEGER;
    v_id_rango_nuevo                       INTEGER;
    v_activo_pago                          VARCHAR;
    v_rec_gestion_periodo                  RECORD;
    v_id_periodo                           INTEGER;
    v_res_json                             json;
    v_pago_conf                            boolean;
    v_exist                                boolean;

    v_json_params_to_send                    json;
    v_resp_json json;



BEGIN

    v_nombre_funcion = 'afi.ft_afiliado_pago_periodo_ime';
    v_parametros = pxp.f_get_record(p_tabla);

    /*********************************
     #TRANSACCION:  'AFI_afpape_INS'
     #DESCRIPCION:	Insercion de registros
     #AUTOR:		admin
     #FECHA:		02-04-2016 16:22:34
    ***********************************/

    IF (p_transaccion = 'AFI_afpape_INS') THEN

        BEGIN
            --Sentencia de la insercion
            INSERT INTO afi.tafiliado_pago_periodo(cv_hijos,
                                                   id_afiliado,
                                                   pago_bonus_registro,
                                                   habilitado,
                                                   pago_de_red,
                                                   estado,
                                                   cv_propio,
                                                   id_periodo,
                                                   pago_total,
                                                   descripcion_pago_red,
                                                   pv_hijos,
                                                   pv_propio,
                                                   estado_reg,
                                                   id_usuario_ai,
                                                   fecha_reg,
                                                   usuario_ai,
                                                   id_usuario_reg,
                                                   fecha_mod,
                                                   id_usuario_mod)
            VALUES (v_parametros.cv_hijos,
                    v_parametros.id_afiliado,
                    v_parametros.pago_bonus_registro,
                    v_parametros.habilitado,
                    v_parametros.pago_de_red,
                    v_parametros.estado,
                    v_parametros.cv_propio,
                    v_parametros.id_periodo,
                    v_parametros.pago_total,
                    v_parametros.descripcion_pago_red,
                    v_parametros.pv_hijos,
                    v_parametros.pv_propio,
                    'activo',
                    v_parametros._id_usuario_ai,
                    now(),
                    v_parametros._nombre_usuario_ai,
                    p_id_usuario,
                    NULL,
                    NULL)
            RETURNING id_afiliado_pago_periodo INTO v_id_afiliado_pago_periodo;

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp, 'mensaje',
                                        'afiliado pago periodo almacenado(a) con exito (id_afiliado_pago_periodo' ||
                                        v_id_afiliado_pago_periodo || ')');
            v_resp = pxp.f_agrega_clave(v_resp, 'id_afiliado_pago_periodo', v_id_afiliado_pago_periodo::varchar);

            --Devuelve la respuesta
            RETURN v_resp;

        END;

        /*********************************
     #TRANSACCION:  'AFI_afpape_MOD'
     #DESCRIPCION:	Modificacion de registros
     #AUTOR:		admin
     #FECHA:		02-04-2016 16:22:34
    ***********************************/

    ELSIF (p_transaccion = 'AFI_afpape_MOD') THEN

        BEGIN
            --Sentencia de la modificacion
            UPDATE afi.tafiliado_pago_periodo
            SET cv_hijos             = v_parametros.cv_hijos,
                id_afiliado          = v_parametros.id_afiliado,
                pago_bonus_registro  = v_parametros.pago_bonus_registro,
                habilitado           = v_parametros.habilitado,
                pago_de_red          = v_parametros.pago_de_red,
                estado               = v_parametros.estado,
                cv_propio            = v_parametros.cv_propio,
                id_periodo           = v_parametros.id_periodo,
                pago_total           = v_parametros.pago_total,
                descripcion_pago_red = v_parametros.descripcion_pago_red,
                pv_hijos             = v_parametros.pv_hijos,
                pv_propio            = v_parametros.pv_propio,
                fecha_mod            = now(),
                id_usuario_mod       = p_id_usuario,
                id_usuario_ai        = v_parametros._id_usuario_ai,
                usuario_ai           = v_parametros._nombre_usuario_ai
            WHERE id_afiliado_pago_periodo = v_parametros.id_afiliado_pago_periodo;

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', 'afiliado pago periodo modificado(a)');
            v_resp = pxp.f_agrega_clave(v_resp, 'id_afiliado_pago_periodo',
                                        v_parametros.id_afiliado_pago_periodo::varchar);

            --Devuelve la respuesta
            RETURN v_resp;

        END;

        /*********************************
     #TRANSACCION:  'AFI_afpape_ELI'
     #DESCRIPCION:	Eliminacion de registros
     #AUTOR:		admin
     #FECHA:		02-04-2016 16:22:34
    ***********************************/

    ELSIF (p_transaccion = 'AFI_afpape_ELI') THEN

        BEGIN
            RAISE EXCEPTION '%', 'NO PUEDES ELIMINAR UN PAGO PERIODO';
            --Sentencia de la eliminacion
            DELETE
            FROM afi.tafiliado_pago_periodo
            WHERE id_afiliado_pago_periodo = v_parametros.id_afiliado_pago_periodo;

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', 'afiliado pago periodo eliminado(a)');
            v_resp = pxp.f_agrega_clave(v_resp, 'id_afiliado_pago_periodo',
                                        v_parametros.id_afiliado_pago_periodo::varchar);

            --Devuelve la respuesta
            RETURN v_resp;

        END;

        /*********************************
     #TRANSACCION:  'AFI_AFPAPE_GENALL'
     #DESCRIPCION: generamos los pagos para todos los afiliados
     #AUTOR:		admin
     #FECHA:		15-10-2016 16:22:34
    ***********************************/

    ELSIF (p_transaccion = 'AFI_AFPAPE_GENALL') THEN

        BEGIN
            --Sentencia de la eliminacion

            IF pxp.f_existe_parametro(p_tabla, 'id_periodo') THEN

                v_id_periodo := v_parametros.id_periodo;
            ELSE
                --verificamos si el periodo es enviado o desde el crontab
                --obtenemos el periodo de un mes atras
                v_rec_gestion_periodo :=
                        param.f_get_periodo_gestion(to_char(now() - INTERVAL '1 MONTH', 'YYYY-mm-dd')::DATE);
                --si el periodo es menor al periodo actual de la fecha now entonces no se deberia actualizar los datos
                v_id_periodo := v_rec_gestion_periodo.po_id_periodo;
            END IF;


            SELECT json_strip_nulls(json_build_object(
                    'id_periodo', v_id_periodo
                ))
            INTO v_json_params_to_send;

            v_resp_json:= afi.f_generar_pagos_por_periodo(v_json_params_to_send);




            --DEBEMOS VALIDAR SI EL PERIODO YA ESTA CERRADO
            --TODO

            /*FOR v_record IN (SELECT afi.id_afiliado
                             FROM afi.tafiliado afi
                                      INNER JOIN afi.trango_afiliado ran ON ran.id_afiliado = afi.id_afiliado
                             WHERE ran.id_periodo = v_id_periodo)
                LOOP

                    SELECT sum(pv)
                    INTO v_pv_propio
                    FROM afi.tpuntos_afiliado
                    WHERE id_afiliado = v_record.id_afiliado
                      AND id_periodo = v_id_periodo;

                    IF (v_pv_propio IS NULL) THEN
                        v_pv_propio = 0;
                    END IF;


                    --vemos si esta habilitado para poder cobrar
                    --if(v_pv_propio >= v_record_rango_actual.volumen_personal)THEN
                    v_id_afiliado_pago_periodo = afi.f_get_pago_periodo_afiliado(p_administrador, p_id_usuario, p_tabla,
                                                                                 v_record.id_afiliado, v_id_periodo);

                    --END IF ;

                END LOOP;*/

            v_resp = 'todo correcto';

            --Devuelve la respuesta
            RETURN v_resp;


        END;


        /*********************************
   #TRANSACCION:  'AFI_AFPAPE_GEPA'
   #DESCRIPCION:	generacion de pago para el afiliado seleccionado
   #AUTOR:		admin
   #FECHA:		02-04-2016 16:22:34
  ***********************************/

    ELSIF (p_transaccion = 'AFI_AFPAPE_GEPA') THEN

        BEGIN

            SELECT TRUE AS pago_conf
            INTO v_pago_conf
            FROM afi.tpago_conf
            WHERE id_periodo = v_parametros.id_periodo;

            SELECT TRUE AS exist
            INTO v_exist
            FROM afi.tafiliado_pago_periodo
            WHERE id_afiliado = v_parametros.id_afiliado
              AND id_periodo = v_parametros.id_periodo;

            --IF v_pago_conf IS NULL AND v_exist IS NULL THEN -- si aun no se ha configurado un periodo listo para pagar entonces se volvera a generar el afiliado pago periodo
                /*v_id_afiliado_pago_periodo = afi.f_get_pago_periodo_afiliado(p_administrador, p_id_usuario, p_tabla,
                                                                             v_parametros.id_afiliado,
                                                                             v_parametros.id_periodo);
*/
            --END IF;


            WITH datos_afiliado AS
                     (
                         SELECT afiliado.id_afiliado,
                                afiliado.direccion_principal,
                                afiliado.lugar_nacimiento,
                                afiliado.fecha_nacimiento,
                                usuario.cuenta,
                                persona.nombre_completo2 AS desc_person,
                                persona.ci,
                                patrocinador.codigo      AS codigo_patrocinador,
                                afiliado.codigo          AS codigo
                         FROM afi.tafiliado afiliado
                                  INNER JOIN segu.tusuario usuario ON usuario.id_persona = afiliado.id_persona
                                  INNER JOIN segu.vpersona persona ON persona.id_persona = afiliado.id_persona
                                  INNER JOIN afi.tafiliado patrocinador
                                             ON patrocinador.id_afiliado = afiliado.id_patrocinador
                         WHERE afiliado.id_afiliado = v_parametros.id_afiliado
                     ),
                 pago_periodo AS
                     (
                         SELECT afpape.*,
                                afil.codigo,
                                person.nombre_completo2,
                                per.periodo,
                                ges.gestion
                         FROM afi.tafiliado_pago_periodo afpape
                                  INNER JOIN afi.tafiliado afil ON afil.id_afiliado = afpape.id_afiliado
                                  INNER JOIN segu.vpersona person ON person.id_persona = afil.id_persona
                                  INNER JOIN segu.tusuario usuario ON usuario.id_persona = afil.id_persona
                                  INNER JOIN param.tperiodo per ON per.id_periodo = afpape.id_periodo
                                  INNER JOIN param.tgestion ges ON ges.id_gestion = per.id_gestion
                         WHERE afil.id_afiliado = v_parametros.id_afiliado
                           AND per.id_periodo = v_parametros.id_periodo
                     ),
                 rango AS
                     (
                         SELECT ran.nombre AS rango, ranafi.id_afiliado
                         FROM afi.trango_afiliado ranafi
                                  INNER JOIN afi.trango ran ON ran.id_rango = ranafi.id_rango
                         WHERE ranafi.id_afiliado = v_parametros.id_afiliado
                           AND ranafi.id_periodo = v_parametros.id_periodo
                     ),
                 periodos_disponibles AS
                     (
                         SELECT per.id_periodo,
                                ges.gestion,
                                pxp.f_obtener_literal_periodo(per.periodo, 0) AS periodo
                         FROM param.tperiodo per
                                  INNER JOIN param.tgestion ges ON ges.id_gestion = per.id_gestion
                         WHERE per.fecha_ini >= (now() - INTERVAL '4 MONTH')::date
                           AND per.fecha_fin <=
                               (date_trunc('month', now()::date) + INTERVAL '1 MONTH' - INTERVAL '1 DAY')::date
                         ORDER BY per.id_periodo DESC
                     ),
                 habilitado_a_cobrar_paquete AS (
                     SELECT tbc.bonus
                     FROM afi.tpuntos_afiliado tpa
                              INNER JOIN afi.tpunto_item tpi ON tpi.id_set = tpa.id_set_item
                              INNER JOIN afi.tbonus_config tbc ON tbc.id_bonus_config = tpi.id_bonus_config
                     WHERE tpa.id_afiliado = v_parametros.id_afiliado
                       AND tbc.bono_patrocinio_por_nivel IS NOT NULL
                     GROUP BY tbc.bonus
                 ),
                 bonus_patrocinio_detalle AS (
                     SELECT tb.tipo,
                            tb.monto,
                            tb.nivel_from,
                            tb.validado_para_pagar_a_patrocinador,
                            afiliado_hijo_directo.codigo AS codigo_afiliado_hijo_directo,
                            afiliado_from.codigo         AS codigo_from
                     FROM afi.tbonus tb
                              INNER JOIN afi.tbonus_config tbc ON tbc.bonus = tb.tipo
                         --AND tbc.bonus != 'BONUS_REGISTRO'
                              INNER JOIN afi.tafiliado afiliado_hijo_directo
                                         ON afiliado_hijo_directo.id_afiliado = tb.id_afiliado
                              INNER JOIN afi.tafiliado afiliado_from
                                         ON afiliado_from.id_afiliado = tb.id_afiliado_from --aca no sale el bono registro por que el id_afiliado_from tiene que ser not null
                     WHERE tb.id_patrocinador = v_parametros.id_afiliado
                       AND id_periodo = v_parametros.id_periodo
                     ORDER BY tb.id_afiliado, tb.nivel_from ASC
                 ),
                 bonus_patrocinio_totales AS (
                     SELECT bpd.tipo, bpd.validado_para_pagar_a_patrocinador, sum(bpd.monto)
                     FROM bonus_patrocinio_detalle bpd
                     GROUP BY bpd.tipo, bpd.validado_para_pagar_a_patrocinador
                 )
            SELECT to_json(array_to_json(array_agg(jsonData)) :: text) #>> '{}' AS JSON
            INTO v_res_json
            FROM (
                     SELECT da.direccion_principal,
                            da.lugar_nacimiento,
                            da.fecha_nacimiento,
                            da.cuenta,
                            da.desc_person,
                            da.ci,
                            da.codigo_patrocinador,
                            da.codigo,
                            ran.rango,
                            (
                                SELECT array_to_json(array_agg(row_to_json(pp))) AS pago_periodo
                                FROM (
                                         SELECT *
                                         FROM pago_periodo
                                     ) pp
                            ),
                            (
                                SELECT array_to_json(array_agg(row_to_json(pp))) AS periodos_disponibles
                                FROM (
                                         SELECT *
                                         FROM periodos_disponibles
                                     ) pp
                            ),
                            (
                                SELECT array_to_json(array_agg(row_to_json(hacp))) AS habilitado_a_cobrar_paquete
                                FROM (
                                         SELECT *
                                         FROM habilitado_a_cobrar_paquete
                                     ) hacp
                            ),
                            (
                                SELECT array_to_json(array_agg(row_to_json(bpd))) AS bonus_patrocinio_detalle
                                FROM (
                                         SELECT *
                                         FROM bonus_patrocinio_detalle
                                     ) bpd
                            ),
                            (
                                SELECT array_to_json(array_agg(row_to_json(bpt))) AS bonus_patrocinio_totales
                                FROM (
                                         SELECT *
                                         FROM bonus_patrocinio_totales
                                     ) bpt
                            )
                     FROM datos_afiliado da
                              INNER JOIN rango ran ON ran.id_afiliado = da.id_afiliado
                 ) jsonData;

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', v_res_json::varchar);
            v_resp = pxp.f_agrega_clave(v_resp, 'json', v_res_json::varchar);

            --Devuelve la respuesta
            RETURN v_resp;

        END;


        /*********************************
   #TRANSACCION:  'AFI_AFPAPE_PAG'
   #DESCRIPCION:	PAGAR
   #AUTOR:		admin
   #FECHA:		02-04-2016 16:22:34
  ***********************************/

    ELSIF (p_transaccion = 'AFI_AFPAPE_PAG') THEN

        BEGIN

            SELECT *
            INTO v_record
            FROM afi.tafiliado_pago_periodo
            WHERE id_afiliado_pago_periodo = v_parametros.id_afiliado_pago_periodo;

            SELECT *
            INTO v_record_afiliado
            FROM afi.tafiliado
            WHERE id_afiliado = v_record.id_afiliado;

            --vemos si esta activo para pagar mediante el periodo
            IF (SELECT estado FROM afi.tpago_conf WHERE id_periodo = v_record.id_periodo) = 'activo' THEN

                v_activo_pago = 'si';
            ELSE
                v_activo_pago = 'no';

            END IF;

            IF v_activo_pago = 'no' THEN
                RAISE EXCEPTION '%','NO TIENES ACTIVO PARA PAGAR ESTE PERIODO O NO CONFIGURASTE';
            END IF;


            IF v_record.estado = 'registrado no pagado' THEN

                IF EXISTS(SELECT 1
                          FROM public.tafiliado_pago_periodo_pagados_febrero_2022_bk
                          WHERE id_afiliado = v_record.id_afiliado
                            AND id_periodo = v_record.id_periodo) THEN
                    RAISE EXCEPTION '%', 'NO PUEDES PAGAR POR QUE YA SE PAGO COMUNICATE CON ADMINISTRACION';
                END IF;


                UPDATE afi.tafiliado_pago_periodo
                SET estado         = 'pagado',
                    fecha_pago     = now(),
                    id_usuario_mod = p_id_usuario,
                    fecha_mod      = now()
                WHERE id_afiliado_pago_periodo = v_parametros.id_afiliado_pago_periodo;

                -- necesitamos mirar si esta habilitado para pagar los bonus de los paquetes
                v_validado_para_pagar_bonus_patrocinio := 'si';
                IF v_record_afiliado.fecha_reg::date < '01-01-2022' THEN

                    WITH t_bonus_patrocinio_que_esta_validado_para_cobrar AS (
                        SELECT tbc.id_bonus_config
                        FROM afi.tbonus_config tbc
                        WHERE (tbc.validar_tipo_bonus = 'no' OR tbc.validar_tipo_bonus = 'si')
                          AND tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
                        GROUP BY tbc.id_bonus_config
                    )
                    SELECT ARRAY_AGG(id_bonus_config::varchar)
                    INTO v_id_bonus_config_a_modificar
                    FROM t_bonus_patrocinio_que_esta_validado_para_cobrar;

                ELSE

                    WITH t_bonus_patrocinio_que_esta_validado_para_cobrar AS (
                        SELECT tbc.id_bonus_config
                        FROM afi.tpuntos_afiliado tpa
                                 INNER JOIN afi.tpunto_item tpi ON tpi.id_set = tpa.id_set_item
                                 INNER JOIN afi.tbonus_config tbc ON tbc.id_bonus_config = tpi.id_bonus_config
                        WHERE tpa.id_afiliado = v_record.id_afiliado
                          AND tbc.bono_patrocinio_por_nivel IS NOT NULL
                          AND tbc.validar_tipo_bonus = 'si'
                          AND tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
                        GROUP BY tbc.id_bonus_config
                        UNION ALL
                        SELECT tbc.id_bonus_config
                        FROM afi.tbonus_config tbc
                        WHERE tbc.validar_tipo_bonus = 'no'
                          AND tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
                        GROUP BY tbc.id_bonus_config
                    )
                    SELECT ARRAY_AGG(id_bonus_config::varchar)
                    INTO v_id_bonus_config_a_modificar
                    FROM t_bonus_patrocinio_que_esta_validado_para_cobrar;


                END IF;

                IF(v_id_bonus_config_a_modificar IS NOT NULL) THEN

                    UPDATE afi.tbonus SET estado_pago = 'pagado' , id_usuario_mod = p_id_usuario, fecha_mod  = now()
                    WHERE id_patrocinador = v_record.id_afiliado
                      AND id_bonus_config = ANY (v_id_bonus_config_a_modificar)
                      and nivel_from is not null and id_afiliado_from is not null;

                    update afi.tafiliado_pago_periodo
                    set estado_pago_bonus = 'pagado'
                    WHERE id_afiliado_pago_periodo = v_parametros.id_afiliado_pago_periodo;

                END IF;


            ELSE



                RAISE EXCEPTION '%','ya se encuentra pagado';
            END IF;


            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', 'afiliado pago periodo eliminado(a)');
            v_resp = pxp.f_agrega_clave(v_resp, 'id_afiliado_pago_periodo',
                                        v_parametros.id_afiliado_pago_periodo::varchar);

            --Devuelve la respuesta
            RETURN v_resp;

        END;

        /*********************************
           #TRANSACCION:  'AFI_AFPAPE_PBON'
           #DESCRIPCION:	PAGAR BONUS DE PAQUETES
           #AUTOR:		admin
           #FECHA:		02-04-2016 16:22:34
       ***********************************/

    ELSIF (p_transaccion = 'AFI_AFPAPE_PBON') THEN

        BEGIN

            SELECT *
            INTO v_record
            FROM afi.tafiliado_pago_periodo
            WHERE id_afiliado_pago_periodo = v_parametros.id_afiliado_pago_periodo;

            SELECT *
            INTO v_record_afiliado
            FROM afi.tafiliado
            WHERE id_afiliado = v_record.id_afiliado;

            --vemos si esta activo para pagar mediante el periodo
            IF (SELECT estado FROM afi.tpago_conf WHERE id_periodo = v_record.id_periodo) = 'activo' THEN

                v_activo_pago = 'si';
            ELSE
                v_activo_pago = 'no';

            END IF;

            IF v_activo_pago = 'no' THEN
                RAISE EXCEPTION '%','NO TIENES ACTIVO PARA PAGAR ESTE PERIODO O NO CONFIGURASTE';
            END IF;

            IF v_record.estado_pago_bonus is null THEN



                -- necesitamos mirar si esta habilitado para pagar los bonus de los paquetes
                v_validado_para_pagar_bonus_patrocinio := 'si';
                IF v_record_afiliado.fecha_reg::date < '01-01-2022' THEN

                    WITH t_bonus_patrocinio_que_esta_validado_para_cobrar AS (
                        SELECT tbc.id_bonus_config
                        FROM afi.tbonus_config tbc
                        WHERE (tbc.validar_tipo_bonus = 'no' OR tbc.validar_tipo_bonus = 'si')
                          AND tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
                        GROUP BY tbc.id_bonus_config
                    )
                    SELECT ARRAY_AGG(id_bonus_config::varchar)
                    INTO v_id_bonus_config_a_modificar
                    FROM t_bonus_patrocinio_que_esta_validado_para_cobrar;

                ELSE

                    WITH t_bonus_patrocinio_que_esta_validado_para_cobrar AS (
                        SELECT tbc.id_bonus_config
                        FROM afi.tpuntos_afiliado tpa
                                 INNER JOIN afi.tpunto_item tpi ON tpi.id_set = tpa.id_set_item
                                 INNER JOIN afi.tbonus_config tbc ON tbc.id_bonus_config = tpi.id_bonus_config
                        WHERE tpa.id_afiliado = v_record.id_afiliado
                          AND tbc.bono_patrocinio_por_nivel IS NOT NULL
                          AND tbc.validar_tipo_bonus = 'si'
                          AND tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
                        GROUP BY tbc.id_bonus_config
                        UNION ALL
                        SELECT tbc.id_bonus_config
                        FROM afi.tbonus_config tbc
                        WHERE tbc.validar_tipo_bonus = 'no'
                          AND tbc.tipo_pago_bonus_patrocinador_directo = 'mensual'
                        GROUP BY tbc.id_bonus_config
                    )
                    SELECT ARRAY_AGG(id_bonus_config::varchar)
                    INTO v_id_bonus_config_a_modificar
                    FROM t_bonus_patrocinio_que_esta_validado_para_cobrar;


                END IF;

                IF(v_id_bonus_config_a_modificar IS NOT NULL) THEN

                    UPDATE afi.tbonus SET estado_pago = 'pagado' , id_usuario_mod = p_id_usuario, fecha_mod  = now()
                    WHERE id_patrocinador = v_record.id_afiliado
                      AND id_bonus_config = ANY (v_id_bonus_config_a_modificar)
                      and nivel_from is not null and id_afiliado_from is not null;

                    update afi.tafiliado_pago_periodo
                    set estado_pago_bonus = 'pagado'
                    where id_afiliado_pago_periodo = v_parametros.id_afiliado_pago_periodo;

                END IF;

                ELSE
                RAISE EXCEPTION '%', 'BONUS YA FUE PAGADO';

            END IF;




            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', 'afiliado pago periodo eliminado(a)');
            v_resp = pxp.f_agrega_clave(v_resp, 'id_afiliado_pago_periodo',
                                        v_parametros.id_afiliado_pago_periodo::varchar);

            --Devuelve la respuesta
            RETURN v_resp;

        END;


        /*********************************
   #TRANSACCION:  'AFI_AFPAPE_PREMI'
   #DESCRIPCION:	PREMIAR
   #AUTOR:		admin
   #FECHA:		09-02-2022 16:22:34
  ***********************************/

    ELSIF (p_transaccion = 'AFI_AFPAPE_PREMI') THEN

        BEGIN
            --Sentencia de la eliminacion


            INSERT INTO afi.tpremio_afiliado(id_periodo,
                                             id_afiliado,
                                             descripcion,
                                             estado_reg,
                                             id_usuario_ai,
                                             fecha_reg,
                                             usuario_ai,
                                             id_usuario_reg,
                                             fecha_mod,
                                             id_usuario_mod)
            VALUES (v_parametros.id_periodo,
                    v_parametros.id_afiliado,
                    v_parametros.descripcion,
                    'activo',
                    v_parametros._id_usuario_ai,
                    now(),
                    v_parametros._nombre_usuario_ai,
                    p_id_usuario,
                    NULL,
                    NULL);

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', 'afiliado pago periodo eliminado(a)');
            v_resp = pxp.f_agrega_clave(v_resp, 'id_afiliado', v_parametros.id_afiliado::varchar);

            --Devuelve la respuesta
            RETURN v_resp;

        END;


    ELSE

        RAISE EXCEPTION 'Transaccion inexistente: %',p_transaccion;

    END IF;

EXCEPTION

    WHEN OTHERS THEN
        v_resp = '';
        v_resp = pxp.f_agrega_clave(v_resp, 'mensaje', SQLERRM);
        v_resp = pxp.f_agrega_clave(v_resp, 'codigo_error', SQLSTATE);
        v_resp = pxp.f_agrega_clave(v_resp, 'procedimientos', v_nombre_funcion);
        RAISE EXCEPTION '%',v_resp;

END;
$BODY$
    LANGUAGE plpgsql VOLATILE;