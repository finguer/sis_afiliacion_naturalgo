CREATE OR REPLACE FUNCTION afi.ft_cotitular_sel(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_cotitular_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'afi.tcotitular'
 AUTOR: 		 (admin)
 FECHA:	        02-04-2015 01:19:24
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;

BEGIN

	v_nombre_funcion = 'afi.ft_cotitular_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'AFI_TITU_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin
 	#FECHA:		02-04-2015 01:19:24
	***********************************/

	if(p_transaccion='AFI_TITU_SEL')then

    	begin
    		--Sentencia de la consulta
			v_consulta:='select
						titu.id_cotitular,
						titu.nombre,
						titu.id_afiliado,
						titu.domicilio,
						titu.apellido1,
						titu.apellido2,
						titu.relacion,
						titu.estado_reg,
						titu.telefono,
						titu.id_usuario_ai,
						titu.id_usuario_reg,
						titu.usuario_ai,
						titu.fecha_reg,
						titu.fecha_mod,
						titu.id_usuario_mod,
						usu1.cuenta as usr_reg,
						usu2.cuenta as usr_mod,
						PERSON.nombre_completo2 as desc_person
						from afi.tcotitular titu
						inner join segu.tusuario usu1 on usu1.id_usuario = titu.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = titu.id_usuario_mod
												INNER JOIN segu.vpersona PERSON on PERSON.id_persona = titu.id_persona

				        where  ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;
			v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

			--Devuelve la respuesta
			return v_consulta;

		end;

	/*********************************
 	#TRANSACCION:  'AFI_TITU_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin
 	#FECHA:		02-04-2015 01:19:24
	***********************************/

	elsif(p_transaccion='AFI_TITU_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(id_cotitular)
					    from afi.tcotitular titu
					    inner join segu.tusuario usu1 on usu1.id_usuario = titu.id_usuario_reg
						left join segu.tusuario usu2 on usu2.id_usuario = titu.id_usuario_mod
												INNER JOIN segu.vpersona PERSON on PERSON.id_persona = titu.id_persona

					    where ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;

			--Devuelve la respuesta
			return v_consulta;

		end;

	else

		raise exception 'Transaccion inexistente';

	end if;

EXCEPTION

	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
