CREATE OR REPLACE FUNCTION afi.ft_afi_patro_sel(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_afi_patro_sel
 DESCRIPCION:   Funcion que devuelve conjuntos de registros de las consultas relacionadas con la tabla 'afi.tafi_patro'
 AUTOR: 		 (admin)
 FECHA:	        04-04-2015 15:40:52
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_consulta    		varchar;
	v_parametros  		record;
	v_nombre_funcion   	text;
	v_resp				varchar;

BEGIN

	v_nombre_funcion = 'afi.ft_afi_patro_sel';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'AFI_PATRO_SEL'
 	#DESCRIPCION:	Consulta de datos
 	#AUTOR:		admin
 	#FECHA:		04-04-2015 15:40:52
	***********************************/

	if(p_transaccion='AFI_PATRO_SEL')then

    	begin
    		--Sentencia de la consulta
			v_consulta:='select
						patro.id_afi_patro,
						patro.id_colocacion,
						patro.id_patrocinador,
						patro.estado_reg,
						patro.id_afiliado,
						patro.id_usuario_ai,
						patro.id_usuario_reg,
						patro.fecha_reg,
						patro.usuario_ai,
						patro.id_usuario_mod,
						patro.fecha_mod,
						usu1.cuenta as usr_reg,
						usu2.cuenta as usr_mod,
								PERSON.nombre_completo2 as desc_person,
								PERSON.ci,
								afil.codigo,
								  PERSON.correo,
                  PERSON.telefono1,
                  PERSON.celular1
						from afi.tafi_patro patro
						inner join segu.tusuario usu1 on usu1.id_usuario = patro.id_usuario_reg
						inner join afi.tafiliado afil on afil.id_afiliado = patro.id_afiliado
						INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona

						left join segu.tusuario usu2 on usu2.id_usuario = patro.id_usuario_mod
				        where  ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;
			v_consulta:=v_consulta||' order by ' ||v_parametros.ordenacion|| ' ' || v_parametros.dir_ordenacion || ' limit ' || v_parametros.cantidad || ' offset ' || v_parametros.puntero;

			--Devuelve la respuesta
			return v_consulta;

		end;

	/*********************************
 	#TRANSACCION:  'AFI_PATRO_CONT'
 	#DESCRIPCION:	Conteo de registros
 	#AUTOR:		admin
 	#FECHA:		04-04-2015 15:40:52
	***********************************/

	elsif(p_transaccion='AFI_PATRO_CONT')then

		begin
			--Sentencia de la consulta de conteo de registros
			v_consulta:='select count(id_afi_patro)
					    from afi.tafi_patro patro
					    inner join segu.tusuario usu1 on usu1.id_usuario = patro.id_usuario_reg
					    inner join afi.tafiliado afil on afil.id_afiliado = patro.id_afiliado
						INNER JOIN segu.vpersona PERSON on PERSON.id_persona = afil.id_persona
						left join segu.tusuario usu2 on usu2.id_usuario = patro.id_usuario_mod
					    where ';

			--Definicion de la respuesta
			v_consulta:=v_consulta||v_parametros.filtro;

			--Devuelve la respuesta
			return v_consulta;

		end;

	else

		raise exception 'Transaccion inexistente';

	end if;

EXCEPTION

	WHEN OTHERS THEN
			v_resp='';
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
			v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
			v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
			raise exception '%',v_resp;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
