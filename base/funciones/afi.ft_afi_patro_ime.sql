CREATE OR REPLACE FUNCTION afi.ft_afi_patro_ime(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
	RETURNS varchar
AS
	$BODY$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_afi_patro_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tafi_patro'
 AUTOR: 		 (admin)
 FECHA:	        04-04-2015 15:40:52
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_afi_patro	integer;

BEGIN

    v_nombre_funcion = 'afi.ft_afi_patro_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'AFI_PATRO_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin
 	#FECHA:		04-04-2015 15:40:52
	***********************************/

	if(p_transaccion='AFI_PATRO_INS')then

        begin
        	--Sentencia de la insercion
        	insert into afi.tafi_patro(
			id_colocacion,
			id_patrocinador,
			estado_reg,
			id_afiliado,
			id_usuario_ai,
			id_usuario_reg,
			fecha_reg,
			usuario_ai,
			id_usuario_mod,
			fecha_mod
          	) values(
			v_parametros.id_colocacion,
			v_parametros.id_patrocinador,
			'activo',
			v_parametros.id_afiliado,
			v_parametros._id_usuario_ai,
			p_id_usuario,
			now(),
			v_parametros._nombre_usuario_ai,
			null,
			null



			)RETURNING id_afi_patro into v_id_afi_patro;

			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Patrocinador almacenado(a) con exito (id_afi_patro'||v_id_afi_patro||')');
            v_resp = pxp.f_agrega_clave(v_resp,'id_afi_patro',v_id_afi_patro::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'AFI_PATRO_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin
 	#FECHA:		04-04-2015 15:40:52
	***********************************/

	elsif(p_transaccion='AFI_PATRO_MOD')then

		begin
			--Sentencia de la modificacion
			update afi.tafi_patro set
			id_colocacion = v_parametros.id_colocacion,
			id_patrocinador = v_parametros.id_patrocinador,
			id_afiliado = v_parametros.id_afiliado,
			id_usuario_mod = p_id_usuario,
			fecha_mod = now(),
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai
			where id_afi_patro=v_parametros.id_afi_patro;

			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Patrocinador modificado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_afi_patro',v_parametros.id_afi_patro::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'AFI_PATRO_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin
 	#FECHA:		04-04-2015 15:40:52
	***********************************/

	elsif(p_transaccion='AFI_PATRO_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from afi.tafi_patro
            where id_afi_patro=v_parametros.id_afi_patro;

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Patrocinador eliminado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_afi_patro',v_parametros.id_afi_patro::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	else

    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION

	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;
