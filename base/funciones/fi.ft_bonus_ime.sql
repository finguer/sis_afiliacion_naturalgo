CREATE OR REPLACE FUNCTION afi.ft_bonus_ime(p_administrador int4, p_id_usuario int4, p_tabla varchar, p_transaccion varchar)
  RETURNS varchar
AS
$BODY$
  /**************************************************************************
 SISTEMA:		Afiliacion
 FUNCION: 		afi.ft_bonus_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tbonus'
 AUTOR: 		 (admin)
 FECHA:	        11-04-2015 16:33:17
 COMENTARIOS:
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:
 AUTOR:
 FECHA:
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_bonus	integer;

BEGIN

    v_nombre_funcion = 'afi.ft_bonus_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************
 	#TRANSACCION:  'AFI_BON_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin
 	#FECHA:		11-04-2015 16:33:17
	***********************************/

	if(p_transaccion='AFI_BON_INS')then

        begin
        	--Sentencia de la insercion
        	insert into afi.tbonus(
			tipo,
			estado_reg,
			id_afiliado,
			monto,
			id_patrocinador,
			id_usuario_reg,
			usuario_ai,
			fecha_reg,
			id_usuario_ai,
			fecha_mod,
			id_usuario_mod
          	) values(
			v_parametros.tipo,
			'activo',
			v_parametros.id_afiliado,
			v_parametros.monto,
			v_parametros.id_patrocinador,
			p_id_usuario,
			v_parametros._nombre_usuario_ai,
			now(),
			v_parametros._id_usuario_ai,
			null,
			null



			)RETURNING id_bonus into v_id_bonus;

			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','bonus almacenado(a) con exito (id_bonus'||v_id_bonus||')');
            v_resp = pxp.f_agrega_clave(v_resp,'id_bonus',v_id_bonus::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'AFI_BON_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin
 	#FECHA:		11-04-2015 16:33:17
	***********************************/

	elsif(p_transaccion='AFI_BON_MOD')then

		begin
			--Sentencia de la modificacion
			update afi.tbonus set
			tipo = v_parametros.tipo,
			id_afiliado = v_parametros.id_afiliado,
			monto = v_parametros.monto,
			id_patrocinador = v_parametros.id_patrocinador,
			fecha_mod = now(),
			id_usuario_mod = p_id_usuario,
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai
			where id_bonus=v_parametros.id_bonus;

			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','bonus modificado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_bonus',v_parametros.id_bonus::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************
 	#TRANSACCION:  'AFI_BON_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin
 	#FECHA:		11-04-2015 16:33:17
	***********************************/

	elsif(p_transaccion='AFI_BON_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from afi.tbonus
            where id_bonus=v_parametros.id_bonus;

            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','bonus eliminado(a)');
            v_resp = pxp.f_agrega_clave(v_resp,'id_bonus',v_parametros.id_bonus::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	else

    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION

	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;
