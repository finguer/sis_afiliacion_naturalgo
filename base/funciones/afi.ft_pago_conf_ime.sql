CREATE OR REPLACE FUNCTION "afi"."ft_pago_conf_ime" (	
				p_administrador integer, p_id_usuario integer, p_tabla character varying, p_transaccion character varying)
RETURNS character varying AS
$BODY$

/**************************************************************************
 SISTEMA:		AFILIACION
 FUNCION: 		afi.ft_pago_conf_ime
 DESCRIPCION:   Funcion que gestiona las operaciones basicas (inserciones, modificaciones, eliminaciones de la tabla 'afi.tpago_conf'
 AUTOR: 		 (admin)
 FECHA:	        16-10-2016 16:46:11
 COMENTARIOS:	
***************************************************************************
 HISTORIAL DE MODIFICACIONES:

 DESCRIPCION:	
 AUTOR:			
 FECHA:		
***************************************************************************/

DECLARE

	v_nro_requerimiento    	integer;
	v_parametros           	record;
	v_id_requerimiento     	integer;
	v_resp		            varchar;
	v_nombre_funcion        text;
	v_mensaje_error         text;
	v_id_pago_conf	integer;
			    
BEGIN

    v_nombre_funcion = 'afi.ft_pago_conf_ime';
    v_parametros = pxp.f_get_record(p_tabla);

	/*********************************    
 	#TRANSACCION:  'AFI_PAGCONF_INS'
 	#DESCRIPCION:	Insercion de registros
 	#AUTOR:		admin	
 	#FECHA:		16-10-2016 16:46:11
	***********************************/

	if(p_transaccion='AFI_PAGCONF_INS')then
					
        begin
        	--Sentencia de la insercion
        	insert into afi.tpago_conf(
			id_periodo,
			estado,
			estado_reg,
			id_usuario_ai,
			usuario_ai,
			fecha_reg,
			id_usuario_reg,
			fecha_mod,
			id_usuario_mod
          	) values(
			v_parametros.id_periodo,
			v_parametros.estado,
			'activo',
			v_parametros._id_usuario_ai,
			v_parametros._nombre_usuario_ai,
			now(),
			p_id_usuario,
			null,
			null
							
			
			
			)RETURNING id_pago_conf into v_id_pago_conf;
			
			--Definicion de la respuesta
			v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Pago conf almacenado(a) con exito (id_pago_conf'||v_id_pago_conf||')'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_pago_conf',v_id_pago_conf::varchar);

            --Devuelve la respuesta
            return v_resp;

		end;

	/*********************************    
 	#TRANSACCION:  'AFI_PAGCONF_MOD'
 	#DESCRIPCION:	Modificacion de registros
 	#AUTOR:		admin	
 	#FECHA:		16-10-2016 16:46:11
	***********************************/

	elsif(p_transaccion='AFI_PAGCONF_MOD')then

		begin
			--Sentencia de la modificacion
			update afi.tpago_conf set
			id_periodo = v_parametros.id_periodo,
			estado = v_parametros.estado,
			fecha_mod = now(),
			id_usuario_mod = p_id_usuario,
			id_usuario_ai = v_parametros._id_usuario_ai,
			usuario_ai = v_parametros._nombre_usuario_ai
			where id_pago_conf=v_parametros.id_pago_conf;
               
			--Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Pago conf modificado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_pago_conf',v_parametros.id_pago_conf::varchar);
               
            --Devuelve la respuesta
            return v_resp;
            
		end;

	/*********************************    
 	#TRANSACCION:  'AFI_PAGCONF_ELI'
 	#DESCRIPCION:	Eliminacion de registros
 	#AUTOR:		admin	
 	#FECHA:		16-10-2016 16:46:11
	***********************************/

	elsif(p_transaccion='AFI_PAGCONF_ELI')then

		begin
			--Sentencia de la eliminacion
			delete from afi.tpago_conf
            where id_pago_conf=v_parametros.id_pago_conf;
               
            --Definicion de la respuesta
            v_resp = pxp.f_agrega_clave(v_resp,'mensaje','Pago conf eliminado(a)'); 
            v_resp = pxp.f_agrega_clave(v_resp,'id_pago_conf',v_parametros.id_pago_conf::varchar);
              
            --Devuelve la respuesta
            return v_resp;

		end;
         
	else
     
    	raise exception 'Transaccion inexistente: %',p_transaccion;

	end if;

EXCEPTION
				
	WHEN OTHERS THEN
		v_resp='';
		v_resp = pxp.f_agrega_clave(v_resp,'mensaje',SQLERRM);
		v_resp = pxp.f_agrega_clave(v_resp,'codigo_error',SQLSTATE);
		v_resp = pxp.f_agrega_clave(v_resp,'procedimientos',v_nombre_funcion);
		raise exception '%',v_resp;
				        
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION "afi"."ft_pago_conf_ime"(integer, integer, character varying, character varying) OWNER TO postgres;
