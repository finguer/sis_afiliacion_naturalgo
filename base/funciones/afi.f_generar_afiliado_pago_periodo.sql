CREATE OR REPLACE FUNCTION afi.f_generar_afiliado_pago_periodo(
    p_params json
)
    RETURNS json AS
$BODY$
DECLARE


    v_nombre_funcion                         varchar;
    p_id_afiliado                            integer DEFAULT p_params ->> 'id_afiliado';
    p_id_periodo                             integer DEFAULT p_params ->> 'id_periodo';
    p_from_gen                               varchar DEFAULT p_params ->> 'from_gen'; -- para saber de donde se esta generando
    v_resp                                   json;
    v_data_json                              json;
    v_activos_primer_nivel                   integer DEFAULT 0;
    v_niveles_a_cobrar_string                varchar;
    v_niveles_a_cobrar_array                 NUMERIC[];
    v_generaciones_cv                        numeric[];
    v_generaciones_pv                        numeric[];
    v_generaciones_cv_monto                  numeric[];
    v_generaciones_count_afiliados_100pts_pv integer[];
    v_generaciones_count_afiliados_activos   integer[];
    v_bonus_registro                         numeric;
    v_bonus_retorno                          numeric;
    v_id_periodo                             integer;
    v_id_rango                               integer;
    v_count_activos                               integer;


BEGIN


    v_nombre_funcion = 'afi.f_generar_afiliado_pago_periodo';

    v_id_rango := NULL;
    v_bonus_registro := NULL;
    v_activos_primer_nivel := NULL;
    v_generaciones_count_afiliados_activos := NULL;
    v_generaciones_count_afiliados_activos := NULL;
    v_generaciones_count_afiliados_100pts_pv := NULL;
    v_niveles_a_cobrar_string := NULL;
    v_niveles_a_cobrar_array := NULL;
    v_generaciones_pv := NULL;
    v_generaciones_cv := NULL;
    v_generaciones_cv_monto := NULL;
    v_id_rango := NULL;


    v_data_json := afi.f_get_data_afiliado_periodo(
            json_build_object(
                    'id_afiliado', p_id_afiliado,
                    'id_periodo', p_id_periodo
                ));

    SELECT a -> 'sum'
    INTO v_bonus_registro
    FROM json_array_elements(v_data_json -> 'bonus_sum') a
    WHERE cast(a ->> 'bonus' AS varchar) = 'BONUS_REGISTRO';

    select count(*)
    into v_count_activos
    from json_array_elements(v_data_json -> 'activos');



    IF v_count_activos > 0 THEN
        SELECT a -> 'count'
        INTO v_activos_primer_nivel
        FROM json_array_elements(v_data_json -> 'activos') a
        WHERE cast(a ->> 'nivel' AS integer) = 1;

        SELECT array_agg(j ->> 'count')
        INTO v_generaciones_count_afiliados_activos
        FROM json_array_elements(v_data_json -> 'activos') j;

    ELSE
        v_activos_primer_nivel := 0;
        v_generaciones_count_afiliados_activos := NULL;
    END IF;

    IF v_count_activos > 0 THEN

        SELECT array_agg(j ->> 'count')
        INTO v_generaciones_count_afiliados_100pts_pv
        FROM json_array_elements(v_data_json -> 'afiliados_con_240pv_en_primer_nivel') j;

    ELSE
        v_generaciones_count_afiliados_100pts_pv := NULL;
    END IF;


    IF v_data_json ->> 'niveles_a_cobrar' IS NOT NULL THEN
        SELECT string_agg(j ->> 'rnum', ','), array_agg(j ->> 'porcentaje')
        INTO v_niveles_a_cobrar_string, v_niveles_a_cobrar_array
        FROM json_array_elements(v_data_json -> 'niveles_a_cobrar') j;
    ELSE
        v_niveles_a_cobrar_string := '0';
        v_niveles_a_cobrar_array := NULL;

    END IF;


    IF v_data_json ->> 'sum_por_niveles' IS NOT NULL THEN
        SELECT array_agg(j ->> 'total_pv'), array_agg(j ->> 'total_cv')
        INTO v_generaciones_pv, v_generaciones_cv
        FROM json_array_elements(v_data_json -> 'sum_por_niveles') j;
    ELSE
        v_generaciones_pv := NULL;
        v_generaciones_cv := NULL;

    END IF;


    IF v_data_json ->> 'generaciones_cv_monto' IS NOT NULL THEN
        SELECT array_agg(j ->> 'generaciones_cv_monto')
        INTO v_generaciones_cv_monto
        FROM json_array_elements(v_data_json -> 'generaciones_cv_monto') j;
    ELSE
        v_generaciones_cv_monto := NULL;
    END IF;




    IF v_data_json -> 'current_afiliado' IS NOT NULL THEN


        delete from afi.tafiliado_pago_periodo
        where id_afiliado = p_id_afiliado and id_periodo = p_id_periodo and estado != 'pagado';


        INSERT INTO afi.tafiliado_pago_periodo(id_afiliado,
                                               pago_bonus_registro,
                                               afiliados_registrados,
                                               habilitado,
                                               pago_de_red,
                                               estado,
                                               cv_propio,
                                               id_periodo,
                                               pago_total,
                                               descripcion_pago_red,
                                               pv_propio,
                                               estado_reg,
                                               id_usuario_ai,
                                               fecha_reg,
                                               usuario_ai,
                                               id_usuario_reg,
                                               fecha_mod,
                                               id_usuario_mod,
                                               primera_gen_cv,
                                               primera_gen_pv,
                                               segunda_gen_cv,
                                               segunda_gen_pv,
                                               tercera_gen_cv,
                                               tercera_gen_pv,
                                               cuarta_gen_cv,
                                               cuarta_gen_pv,
                                               quinta_gen_cv,
                                               quinta_gen_pv,
                                               sexta_gen_pv,
                                               monto_multiplicar,
                                               tipo_moneda,
                                               afiliados_activos,
                                               niveles_cobrados,
                                               sube_nivel,
                                               pv_acumulado,
                                               porcentajes_pagados,
                                               generaciones_cv,
                                               generaciones_pv,
                                               generaciones_cv_monto,
                                               generaciones_count_afiliados_100pts_pv,
                                               generaciones_count_afiliados_activos,
                                               monto_bonus_ya_pagado,
                                               total_ganado,
                                               monto_bonus_no_pagado,
                                               monto_bonus_multinivel,
                                               total_bono_retorno_activacion,
                                               data_json_aux)
        VALUES (p_id_afiliado,
                v_bonus_registro,
                cast(v_data_json ->> 'registros_directos_count' AS integer),
                v_data_json ->> 'habilitado',
                cast(v_data_json ->> 'total_cv_monto' AS numeric),
                'registrado no pagado',
                cast(v_data_json -> 'puntos_propio_sum' ->> 'cv' AS numeric),
                p_id_periodo,
                cast(v_data_json -> 'totales' ->> 'total_a_pagar' AS numeric),
                'new logic',
                cast(v_data_json -> 'puntos_propio_sum' ->> 'pv' AS numeric),
                'activo',
                1,
                now(),
                1,
                1,
                NULL,
                NULL,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                cast(v_data_json -> 'current_afiliado' -> 'config_pais' ->> 'monto_multiplicar' AS numeric),
                v_data_json -> 'current_afiliado' -> 'config_pais' ->> 'tipo_moneda',
                v_activos_primer_nivel,
                v_niveles_a_cobrar_string,
                v_data_json -> 'sube_nivel' ->> 'sube_nivel',
                cast(v_data_json ->> 'pv_para_calificar' AS numeric),
                v_niveles_a_cobrar_array,
                v_generaciones_cv,
                v_generaciones_pv,
                v_generaciones_cv_monto,
                v_generaciones_count_afiliados_100pts_pv,
                v_generaciones_count_afiliados_activos,
                cast(v_data_json -> 'totales' ->> 'total_bono_instante' AS numeric),--v_bonus_registro_ya_pagado,
                cast(v_data_json -> 'totales' ->> 'total_ganado' AS numeric),--v_pago_total_ganado,
                cast(v_data_json -> 'totales' ->> 'total_bono_mensual' AS numeric),--v_bonus_registro_no_pagado,
                cast(v_data_json -> 'totales' ->> 'total_bono_patrocinio' AS numeric),--v_bonus_patrocinio_multinivel,
                0,--v_bono_retorno_por_compra_activacion,
                v_data_json);


        IF v_data_json -> 'sube_nivel' ->> 'sube_nivel' = 'si' THEN
            v_id_rango := cast(v_data_json -> 'sube_nivel' -> 'rango' ->> 'id_rango' AS integer);
        ELSE
            v_id_rango := cast(v_data_json -> 'current_afiliado' ->> 'id_rango' AS integer);
        END IF;



        if p_from_gen = 'Y' THEN -- SOLO SE CREARA UN NUEVO PERIODO CUANDO ESTE SERA Y CUANDO ES ENVIADO EN FIN DE MES EL CRON

            delete from afi.trango_afiliado
            where id_afiliado = p_id_afiliado and id_periodo = p_id_periodo +1;


            INSERT INTO afi.trango_afiliado (id_usuario_reg,
                                             id_usuario_mod,
                                             fecha_reg,
                                             fecha_mod,
                                             estado_reg,
                                             id_usuario_ai,
                                             usuario_ai,
                                             id_afiliado,
                                             id_rango,
                                             id_periodo)
            VALUES (1,
                    NULL,
                    now(),
                    NULL,
                    'activo',
                    1,
                    1,
                    p_id_afiliado,
                    --cast(v_data_json -> 'current_afiliado' ->> 'id_rango' AS integer),
                    v_id_rango,
                    p_id_periodo + 1);
        END IF;



        else

            v_id_rango := null;
            select id_rango
            into v_id_rango
            from afi.trango_afiliado
                where id_afiliado = p_id_afiliado and id_periodo = p_id_periodo;
        --no tiene nada pero debemos darle su rango en el periodo nuevo
            INSERT INTO afi.trango_afiliado (id_usuario_reg,
                                             id_usuario_mod,
                                             fecha_reg,
                                             fecha_mod,
                                             estado_reg,
                                             id_usuario_ai,
                                             usuario_ai,
                                             id_afiliado,
                                             id_rango,
                                             id_periodo)
            VALUES (1,
                    NULL,
                    now(),
                    NULL,
                    'activo',
                    1,
                    1,
                    p_id_afiliado,
                    id_rango,
                    p_id_periodo + 1);


    END IF;




    RETURN v_data_json;


EXCEPTION
    WHEN OTHERS THEN
        SELECT json_strip_nulls(json_build_object(
                'SQLERRM', SQLERRM,
                'SQLSTATE', SQLSTATE,
                'PROCEDURE', v_nombre_funcion
            ))
        INTO v_resp;
        RAISE EXCEPTION '%',v_resp;


END ;
$BODY$
    LANGUAGE plpgsql VOLATILE;