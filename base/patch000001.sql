/***********************************I-SCP-FFP-AFI-1-12/04/2015****************************************/

CREATE TABLE afi.tcotitular(
  id_cotitular SERIAL NOT NULL,

  nombre VARCHAR(250),
  apellido1 VARCHAR(255),
  apellido2 VARCHAR(255),
  domicilio NUMERIC(10,2) NOT NULL,
  relacion NUMERIC(10,2) NOT NULL,
  id_afiliado INTEGER,
  telefono INTEGER,

  CONSTRAINT tcotitular_pkey PRIMARY KEY(id_cotitular)
) INHERITS (pxp.tbase) WITHOUT OIDS;


CREATE TABLE afi.tafiliado(
  id_afiliado SERIAL NOT NULL,
  fecha_nacimiento DATE,
  lugar_nacimiento VARCHAR(255),
  direccion_principal VARCHAR(255),
  id_persona INTEGER,
  CONSTRAINT tafiliado_pkey PRIMARY KEY(id_afiliado)
) INHERITS (pxp.tbase) WITHOUT OIDS;







CREATE TABLE afi.tafi_patro(
  id_afi_patro SERIAL NOT NULL,

  id_afiliado INTEGER,
  id_patrocinador INTEGER,
  id_colocacion INTEGER,
  CONSTRAINT tafi_patro_pkey PRIMARY KEY(id_afi_patro)
) INHERITS (pxp.tbase) WITHOUT OIDS;



CREATE TABLE afi.tpatro_colo(
  id_patro_colo SERIAL NOT NULL,
  id_colocacion INTEGER,
  izquierda INTEGER,
  derecha integer,
  CONSTRAINT tpatro_colo_pkey PRIMARY KEY(id_patro_colo)
) INHERITS (pxp.tbase) WITHOUT OIDS;


CREATE TABLE afi.tcodificacion(
  id_codificacion SERIAL NOT NULL,
  numero_inicio NUMERIC,
  numero_fin NUMERIC,
  numero_siguiente NUMERIC,
  CONSTRAINT tcodificacion_pkey PRIMARY KEY(id_codificacion)
) INHERITS (pxp.tbase) WITHOUT OIDS;





CREATE TABLE afi.tmodalidad_afiliacion(
  id_modalidad SERIAL NOT NULL,
  tipo VARCHAR(255),
  monto NUMERIC,
  id_afiliado INTEGER,
  CONSTRAINT tmodalidad_afiliacion_pkey PRIMARY KEY(id_modalidad)
) INHERITS (pxp.tbase) WITHOUT OIDS;



CREATE TABLE afi.tbonus(
  id_bonus SERIAL NOT NULL,
  tipo VARCHAR(255),
  monto NUMERIC,
  id_patrocinador INTEGER,
  id_afiliado INTEGER,
  CONSTRAINT tbonus_pkey PRIMARY KEY(id_bonus)
) INHERITS (pxp.tbase) WITHOUT OIDS;

/***********************************F-SCP-FFP-AFI-1-12/04/2015****************************************/



/***********************************I-SCP-FFP-AFI-2-13/04/2015****************************************/


ALTER TABLE afi.tafiliado ADD id_patrocinador INTEGER null;


/***********************************F-SCP-FFP-AFI-2-13/04/2015****************************************/

/***********************************I-SCP-FFP-AFI-1-14/04/2015****************************************/

--ALTER TABLE afi.tafiliado ADD colocacion VARCHAR(200) NULL;

/***********************************F-SCP-FFP-AFI-1-14/04/2015****************************************/


/***********************************I-SCP-FFP-AFI-1-09/05/2015****************************************/

update afi.tbonus set monto = 15 where monto = 10;

ALTER TABLE afi.tcotitular ADD id_persona INT NULL;

/***********************************F-SCP-FFP-AFI-1-09/05/2015****************************************/

/***********************************I-SCP-FFP-AFI-1-27/08/2015****************************************/

ALTER TABLE afi.tbonus ADD estado_pago VARCHAR(255) NULL;

update afi.tbonus set estado_pago = 'debe' where id_bonus > 0;

/***********************************F-SCP-FFP-AFI-1-27/08/2015****************************************/


/***********************************I-SCP-FFP-AFI-1-28/11/2015****************************************/

ALTER TABLE afi.tafiliado ADD id_set_item INTEGER NULL;
ALTER TABLE afi.tafiliado ADD id_punto_venta INTEGER NULL;

/***********************************F-SCP-FFP-AFI-1-28/11/2015****************************************/



/***********************************I-SCP-FFP-AFI-1-18/12/2015****************************************/
CREATE TABLE afi.tpunto_item(
  id_punto_item SERIAL NOT NULL,
  pv NUMERIC,
  cv NUMERIC,
  id_item INTEGER,
  id_set INTEGER,
  CONSTRAINT tpunto_item_pkey PRIMARY KEY(id_punto_item)
) INHERITS (pxp.tbase) WITHOUT OIDS;

ALTER TABLE afi.tpunto_item ADD tipo VARCHAR(255) NULL;

/***********************************F-SCP-FFP-AFI-1-18/12/2015****************************************/

/***********************************I-SCP-FFP-AFI-1-04/01/2016****************************************/

CREATE TABLE afi.tafiliado_etapa(
  id_afiliado_etapa SERIAL NOT NULL,
  id_afiliado INTEGER,
  etapa VARCHAR(255),
  fecha_ini DATE,
  fecha_fin DATE,
  CONSTRAINT tafiliado_etapa_pkey PRIMARY KEY(id_afiliado_etapa)
) INHERITS (pxp.tbase) WITHOUT OIDS;



CREATE TABLE afi.tpuntos_afiliado (
  id_puntos_afiliado SERIAL,
  pv NUMERIC,
  cv NUMERIC,
  id_periodo INTEGER,
  id_item INTEGER,
  id_set_item INTEGER,
  id_afiliado INTEGER,
  CONSTRAINT tpuntos_afiliado_pkey PRIMARY KEY(id_puntos_afiliado)
) INHERITS (pxp.tbase)

WITH (oids = false);



INSERT INTO pxp.variable_global ( variable, valor, descripcion) VALUES ( 'afiliacion', 'si', 'si existe el sistema de afiliacion activo');


ALTER TABLE afi.tpunto_item ADD bonus_patrocinador VARCHAR(255) NULL;

CREATE TABLE afi.tbonusProductoPatrocinador(
  id_bonus_producto_patrocinador SERIAL NOT NULL,
  id_afiliado INTEGER,
  id_patrocinador INTEGER,
  observaciones VARCHAR(255),
  cv NUMERIC(10,2),
  cantidad_pro INTEGER,
  total_cv NUMERIC(10,2),
  CONSTRAINT tbonusProductoPatrocinador_pkey PRIMARY KEY(id_bonus_producto_patrocinador)
) INHERITS (pxp.tbase) WITHOUT OIDS;



ALTER TABLE afi.tbonusproductopatrocinador ADD id_periodo INTEGER NULL;


/***********************************F-SCP-FFP-AFI-1-04/01/2016****************************************/



/***********************************I-SCP-FFP-AFI-1-01/04/2016****************************************/

ALTER TABLE afi.tbonus ADD id_periodo INTEGER NULL;
/***********************************F-SCP-FFP-AFI-1-01/04/2016****************************************/

/***********************************I-SCP-FFP-AFI-1-02/04/2016****************************************/

CREATE TABLE afi.tafiliado_pago_periodo(
  id_afiliado_pago_periodo  SERIAL NOT NULL,
  id_afiliado INTEGER,
  id_periodo INTEGER,
  habilitado VARCHAR(255),
  cv_propio NUMERIC(10,2),
  pv_propio NUMERIC(10,2),
  cv_hijos NUMERIC(10,2),
  pv_hijos NUMERIC(10,2),
  pago_bonus_registro NUMERIC(10,2),
  pago_de_red NUMERIC(10,2),
  estado VARCHAR(255),
  descripcion_pago_red VARCHAR(255),
  pago_total NUMERIC(10,2),

  CONSTRAINT tafiliado_pago_periodo_pkey PRIMARY KEY(id_afiliado_pago_periodo)
) INHERITS (pxp.tbase) WITHOUT OIDS;


ALTER TABLE afi.tafiliado_pago_periodo ADD cv_nietos NUMERIC(10,2) NOT NULL;
ALTER TABLE afi.tafiliado_pago_periodo ADD pv_nietos NUMERIC(10,2) NOT NULL;

ALTER TABLE afi.tafiliado_pago_periodo ADD pago_hijos NUMERIC(10,2) NOT NULL;
ALTER TABLE afi.tafiliado_pago_periodo ADD pago_nietos NUMERIC(10,2) NOT NULL;


/***********************************F-SCP-FFP-AFI-1-02/04/2016****************************************/


/***********************************I-SCP-FFP-AFI-1-04/04/2016****************************************/

ALTER TABLE afi.tafiliado_pago_periodo ADD html_red TEXT NULL;

/***********************************F-SCP-FFP-AFI-1-04/04/2016****************************************/


/***********************************I-SCP-FFP-AFI-1-08/04/2016****************************************/

ALTER TABLE afi.tpuntos_afiliado ADD id_venta INTEGER NULL;

ALTER TABLE afi.tbonusproductopatrocinador ADD id_venta INTEGER NULL;

/***********************************F-SCP-FFP-AFI-1-08/04/2016****************************************/

/***********************************I-SCP-FFP-AFI-1-09/10/2016****************************************/

ALTER TABLE afi.tafiliado_pago_periodo ADD "primera_gen_cv" NUMERIC(10,2) NULL;
ALTER TABLE afi.tafiliado_pago_periodo ADD "primera_gen_pv" NUMERIC(10,2) NULL;

ALTER TABLE afi.tafiliado_pago_periodo ADD "segunda_gen_cv" NUMERIC(10,2) NULL;
ALTER TABLE afi.tafiliado_pago_periodo ADD "segunda_gen_pv" NUMERIC(10,2) NULL;

ALTER TABLE afi.tafiliado_pago_periodo ADD "tercera_gen_cv" NUMERIC(10,2) NULL;
ALTER TABLE afi.tafiliado_pago_periodo ADD "tercera_gen_pv" NUMERIC(10,2) NULL;

ALTER TABLE afi.tafiliado_pago_periodo ADD "cuarta_gen_cv" NUMERIC(10,2) NULL;
ALTER TABLE afi.tafiliado_pago_periodo ADD "cuarta_gen_pv" NUMERIC(10,2) NULL;

ALTER TABLE afi.tafiliado_pago_periodo ADD "quinta_gen_cv" NUMERIC(10,2) NULL;
ALTER TABLE afi.tafiliado_pago_periodo ADD "quinta_gen_pv" NUMERIC(10,2) NULL;

ALTER TABLE afi.tafiliado_pago_periodo ADD tipo_moneda VARCHAR(255) NULL;
ALTER TABLE afi.tafiliado_pago_periodo ADD monto_multiplicar NUMERIC(10,2) NULL;


ALTER TABLE afi.tafiliado_pago_periodo DROP cv_hijos;
ALTER TABLE afi.tafiliado_pago_periodo DROP pv_hijos;
ALTER TABLE afi.tafiliado_pago_periodo DROP cv_nietos;
ALTER TABLE afi.tafiliado_pago_periodo DROP pv_nietos;
ALTER TABLE afi.tafiliado_pago_periodo DROP pago_hijos;
ALTER TABLE afi.tafiliado_pago_periodo DROP pago_nietos;



CREATE TABLE afi.trango(
  id_rango  SERIAL NOT NULL,
  id_afiliado INTEGER,
  nombre VARCHAR(255),
  nivel INTEGER,
  volumen_personal NUMERIC,
  volumen_organizacional NUMERIC,
  porcentaje_de_pago numeric(10,2),
  volumen_cobro_nivel NUMERIC,
  afiliado_activos INTEGER,
  periodos_acumulados INTEGER,
  id_rango_fk INTEGER,

  CONSTRAINT trango_pkey PRIMARY KEY(id_rango)
) INHERITS (pxp.tbase) WITHOUT OIDS;

COMMENT ON COLUMN afi.trango.nombre
IS 'el nombre del rango';
COMMENT ON COLUMN afi.trango.nivel
IS 'el tope de los niveles que puede cobrar si es 1 cobrara solo del 1 si es dos cobrara del 1 al 2 asi susesivamente';
COMMENT ON COLUMN afi.trango.volumen_personal
IS 'volumen propio para poder estar activo';
COMMENT ON COLUMN afi.trango.volumen_organizacional
IS 'volumen para poder calificar a dicho nivel';
COMMENT ON COLUMN afi.trango.porcentaje_de_pago
IS 'el porcentaje en cual se paga su nivel';
COMMENT ON COLUMN afi.trango.volumen_cobro_nivel
IS 'volumen pra poder cobrar el nivel de este sin ser este rango';

COMMENT ON COLUMN afi.trango.afiliado_activos
IS 'afiliados que tienen que estar activo para poder cobrar en tu red';


COMMENT ON COLUMN afi.trango.periodos_acumulados
IS 'periodos en que se puede acumular los puntos para poder subir este nivel';


CREATE UNIQUE INDEX trango_nivel_uindex ON afi.trango (nivel);
CREATE UNIQUE INDEX trango_id_rango_fk_uindex ON afi.trango (id_rango_fk);
CREATE UNIQUE INDEX trango_nombre_uindex ON afi.trango (nombre);


ALTER TABLE afi.trango DROP id_afiliado;


CREATE TABLE afi.trango_afiliado(
  id_rango_afiliado  SERIAL NOT NULL,
  id_afiliado INTEGER,
  id_rango INTEGER,
  id_periodo INTEGER,

  CONSTRAINT trango_afiliado_pkey PRIMARY KEY(id_rango_afiliado)
) INHERITS (pxp.tbase) WITHOUT OIDS;



/*


insert INTO afi.trango_afiliado(id_usuario_reg,
                                id_usuario_mod,
                                fecha_reg,
                                fecha_mod,
                                estado_reg,
                                id_usuario_ai,
                                usuario_ai,
                                id_afiliado,
                                id_rango,
                                id_periodo) SELECT 1,NULL ,now(),NULL ,'activo',NULL ,'NULL',afil.id_afiliado,1,19
                                            FROM afi.tafiliado afil

*/


ALTER TABLE afi.tafiliado_pago_periodo ADD afiliados_activos INTEGER NULL;
ALTER TABLE afi.tafiliado_pago_periodo ADD niveles_cobrados VARCHAR(255) NULL;

ALTER TABLE afi.tafiliado_pago_periodo ADD pv_acumulado NUMERIC NULL;

ALTER TABLE afi.tafiliado_pago_periodo ADD sube_nivel VARCHAR(2) NULL;




CREATE TABLE afi.tpago_conf(
  id_pago_conf  SERIAL NOT NULL,
  id_periodo INTEGER,
  estado VARCHAR(255),
  CONSTRAINT tpago_conf_pkey PRIMARY KEY(id_pago_conf)
) INHERITS (pxp.tbase) WITHOUT OIDS;


/***********************************F-SCP-FFP-AFI-1-09/10/2016****************************************/

/***********************************I-SCP-FFP-AFI-1-20/07/2019****************************************/

ALTER TABLE afi.trango ADD niveles_a_cobrar INTEGER NULL;
ALTER TABLE afi.trango ADD porcentajes NUMERIC[] NULL;
ALTER TABLE afi.tafiliado_pago_periodo ADD porcentajes_pagados NUMERIC[] NULL;

CREATE UNIQUE INDEX trango_afiliado_periodo_index ON afi.trango_afiliado (id_periodo, id_rango, id_afiliado);

CREATE TABLE afi.tcuenta_bancaria(
  id_cuenta_bancaria SERIAL NOT NULL,
  id_afiliado INTEGER,
  nro_cuenta VARCHAR(255),
  tipo_cuenta VARCHAR(255),
  banco VARCHAR(255),
  CONSTRAINT tcuenta_bancaria_pkey PRIMARY KEY(id_cuenta_bancaria)
) INHERITS (pxp.tbase) WITHOUT OIDS;

CREATE UNIQUE INDEX tcuenta_bancaria_id_afiliado_uindex ON afi.tcuenta_bancaria (id_afiliado);



CREATE UNIQUE INDEX tafiliado_pago_periodo_index ON afi.tafiliado_pago_periodo (id_afiliado,id_periodo);


ALTER TABLE ONLY afi.trango_afiliado
  ADD CONSTRAINT fk_trango__id_afiliado
FOREIGN KEY (id_afiliado) REFERENCES afi.tafiliado(id_afiliado);


ALTER TABLE ONLY afi.trango_afiliado
  ADD CONSTRAINT fk_trango__id_rango
FOREIGN KEY (id_rango) REFERENCES afi.trango(id_rango);


ALTER TABLE ONLY afi.tafiliado_pago_periodo
  ADD CONSTRAINT fk_tafpape__id_afiliado
FOREIGN KEY (id_afiliado) REFERENCES afi.tafiliado(id_afiliado);



ALTER TABLE ONLY afi.tafiliado_pago_periodo
  ADD CONSTRAINT fk_tafpape__id_periodo
FOREIGN KEY (id_periodo) REFERENCES param.tperiodo(id_periodo);

ALTER TABLE ONLY afi.tafiliado
  ADD CONSTRAINT fk_tafil__id_patrocinador
FOREIGN KEY (id_patrocinador) REFERENCES afi.tafiliado(id_afiliado);




ALTER TABLE afi.tcodificacion ADD pais VARCHAR(100) NULL;

alter table afi.tafiliado_pago_periodo
    add afiliados_registrados integer;



alter table afi.tafiliado_pago_periodo
    add sexta_gen_pv numeric(10,2);


alter table afi.tafiliado_pago_periodo
    add fecha_pago date;

CREATE TABLE afi.tbonus_config(
                                     id_bonus_config SERIAL NOT NULL,
                                     bonus VARCHAR(255),
                                     monto numeric(10,2),
                                     CONSTRAINT tbonus_config_pkey PRIMARY KEY(id_bonus_config)
) INHERITS (pxp.tbase) WITHOUT OIDS;

alter table afi.tpunto_item
    add id_bonus_config integer;


alter table afi.tbonus_config
    add config_json json;


/***********************************F-SCP-FFP-AFI-1-20/07/2019****************************************/


/***********************************I-SCP-FFP-AFI-1-03/01/2022****************************************/

--cambios en canaan
alter table afi.tcodificacion
	add alias varchar(5);


alter table afi.tafiliado alter column codigo type varchar(255) using codigo::varchar(255);


alter table afi.tbonus_config
    add bono_patrocinio_por_nivel numeric[];

alter table afi.tbonus
    add id_afiliado_from integer;

comment on column afi.tbonus.id_afiliado_from is 'este campo es por que puede que el bonus venga de un afiliado de mas abajo de un subnieto o nieto entonces es para tener ese registro';

alter table afi.tbonus
    add nivel_from integer;

comment on column afi.tbonus.nivel_from is 'este campo es para saber desde que nivel vino';



alter table afi.tbonus_config
    add validar_tipo_bonus varchar(2);



comment on column afi.tbonus_config.validar_tipo_bonus is 'si este campo esta como si entonces solo el bonus sera para aquellas personas que tengna el mismo bonus comprado o afiliado si es no entonces no ppodran cobrar el bonus que le toque en ningun nivel de bono patrocinio';


alter table afi.tbonus
    add validado_para_pagar_a_patrocinador varchar(2);


alter table afi.tbonus_config
    add calcular_por varchar default 'monto_paquete';


alter table afi.tafiliado
    add pais varchar(100);


alter table afi.tbonus_config
    add bonos_registro_patrocinador varchar(2) default 'si';

-- 29 de enero 2022
alter table afi.tafiliado_pago_periodo
    add generaciones_cv numeric[];

alter table afi.tafiliado_pago_periodo
    add generaciones_pv numeric[];

alter table afi.tafiliado_pago_periodo
    add generaciones_cv_monto numeric[];


alter table afi.tafiliado_pago_periodo
    add generaciones_count_afiliados_100pts_pv integer[];

comment on column afi.tafiliado_pago_periodo.generaciones_count_afiliados_100pts_pv is 'este campo es para ver cuantos afiliados tienen con arriba de 100 pts pv';


alter table afi.tafiliado_pago_periodo
    add generaciones_count_afiliados_activos integer[];

comment on column afi.tafiliado_pago_periodo.generaciones_count_afiliados_activos is 'este campo es para ver cuantos afiliados estan activos en distintos niveles';


-- 30 de enero 2022

alter table afi.tafiliado_pago_periodo
    add monto_bonus_ya_pagado numeric(10,2);

comment on column afi.tafiliado_pago_periodo.monto_bonus_ya_pagado is 'esta columna es para guardar el total del bonus de registro o que no tenga el nivel from y afiliado from null';

alter table afi.tafiliado_pago_periodo
    add total_ganado numeric(10,2);

comment on column afi.tafiliado_pago_periodo.total_ganado is 'aca muestra el total ganado del afiliado con todo incluido red bonus bonus patrocinio o bonus registro';



-- 31 de enero 2022

alter table afi.tbonus_config
    add tipo_pago_bonus_patrocinador_directo varchar default 'instante';

comment on column afi.tbonus_config.tipo_pago_bonus_patrocinador_directo is 'esta columna es para saber cuando se pagara este bono si sera instante o mensual pero solo el bono directo a patrocinador no tiene nada que ver con la red de bono patrocinio';



alter table afi.tbonus
    add id_bonus_config integer;


alter table afi.tafiliado_pago_periodo
    add monto_bonus_no_pagado numeric(10,2);

alter table afi.tafiliado_pago_periodo
    add monto_bonus_multinivel numeric(10,2);



----


CREATE TABLE afi.tafiliado_presentador(
  id_afiliado_presentador SERIAL NOT NULL,
  id_presentador integer,
  id_afiliado integer,
  id_periodo integer,
  monto numeric(10,2),
  CONSTRAINT tafiliado_presentador_pkey PRIMARY KEY(id_afiliado_presentador)
) INHERITS (pxp.tbase) WITHOUT OIDS;

CREATE TABLE afi.t_bonus_presentador(
  id_bonus_presentador SERIAL NOT NULL,
  id_presentador integer,
  id_afiliado integer,
  monto numeric(10,2),
  CONSTRAINT t_bonus_presentador_pkey PRIMARY KEY(id_bonus_presentador)
) INHERITS (pxp.tbase) WITHOUT OIDS;


alter table afi.tbonus_config
    add activacion_cero integer;

comment on column afi.tbonus_config.activacion_cero is 'esta columna sirve por si un afiliado no esta habilitado por que no ha comprado nada pero ha registrado a varios con una cierta cantidad donde se indica aqui podra habilitarse';



alter table afi.tpunto_item
    add bono_retorno varchar(1) default 'N';

alter table afi.tafiliado_pago_periodo
    add total_bono_retorno_activacion numeric(10,2) default 0;

alter table afi.tafiliado_pago_periodo
    add data_json_aux json;

-----
alter table afi.tafiliado
    add data_json_items json;



alter table afi.tpunto_item
    add monto_bono_presentador numeric(10,2) default 0;




ALTER TABLE afi.tafiliado_presentador
    ADD CONSTRAINT tafiliado_presentador_unique_presentador_afiliado UNIQUE (id_afiliado, id_presentador);



CREATE TABLE afi.tlog_afiliado_eliminado(
                                        id_log_afiliado_eliminado SERIAL NOT NULL,
                                        afiliado json,
                                        hijos json,
                                        fecha_eliminado timestamp DEFAULT  now(),
                                        CONSTRAINT tlog_afiliado_eliminado_pkey PRIMARY KEY(id_log_afiliado_eliminado)
) ;

------natu premiar
CREATE TABLE afi.tpremio_afiliado(
                                        id_premio_afiliado SERIAL NOT NULL,
                                        id_afiliado integer,
                                        id_periodo integer,
                                        descripcion text,
                                        CONSTRAINT tpremio_afiliado_pkey PRIMARY KEY(id_premio_afiliado)
) INHERITS (pxp.tbase) WITHOUT OIDS;



-----------------24 mar 2022
alter table afi.tafiliado_pago_periodo
    add estado_pago_bonus varchar;


alter table afi.trango
    add niveles_a_sumar_para_pv integer DEFAULT 5;



alter table afi.tbonus_config
    add generar_regalias_a_niveles integer;

alter table afi.tbonus_config
    add asignar_id_rango_directo integer;
comment on column afi.tbonus_config.asignar_id_rango_directo is 'esta columna sera un integer que contendra un id_rango que se quiera asignar directament por ejemplo algun paquete podra asignarte rango oro o de un nivel mas arriba';



alter table vent.tventa_detalle
    add data_json_items json;
comment on column vent.tventa_detalle.data_json_items is 'esta columna es para saber que productos se escogieron en el paquete';


alter table afi.tbonus_config
    add afiliado_activo varchar(1) default 'N';

comment on column afi.tbonus_config.afiliado_activo is 'este campo es para validad si el afiliado esta activo entonces se podra hacer esa venta si no le generar un error';



alter table afi.tbonus_config
    add cv_para_puntos_red numeric(10,2);

comment on column afi.tbonus_config.cv_para_puntos_red is 'esta columan servira para que tome  en cuenta al momento de dar puntos al afiliado para el cobro normal de la red normal';



alter table afi.tlog_afiliado_eliminado
    add id_usuario_reg integer;



--09nov22
alter table afi.tbonus_config
    add veces_por_afiliado integer;

comment on column afi.tbonus_config.veces_por_afiliado is 'esta columna controla cuantas veces puede comprar el afiliado';



--26ENERO23
alter table afi.tbonus_config
    add quien_recibe_bono varchar(50) not null DEFAULT 'PATROCINADOR';

comment on column afi.tbonus_config.quien_recibe_bono is 'esta columan nos servira para determinara quien recibira el bono si el patrocinador o el mismo afiliado en caso de algun paquete de bono retorno';


--09feb2023
alter table afi.tbonus_config
    add cada_que_tiempo varchar(50);

comment on column afi.tbonus_config.cada_que_tiempo is 'calor en dias esto va vinculado con veces por afiliado por cada que tiempo la restriccion';

--cambios VID 29MAR2023 EJECUTAR ESTO EN NATURALGO

CREATE TABLE afi.tconfig_pais(
                                        id_config_pais SERIAL NOT NULL,
                                        pais varchar,
                                        cv_vale numeric(10,2),
                                        moneda varchar(20),
                                        json_config json,
                                        CONSTRAINT tconfig_pais_pkey PRIMARY KEY(id_config_pais)
) INHERITS (pxp.tbase) WITHOUT OIDS;

alter table afi.tafiliado
    add photo varchar(255);

ALTER TABLE afi.tbonus ADD id_venta INTEGER NULL;




CREATE TABLE afi.tmaterial_apoyo(
                                    id_material_apoyo SERIAL NOT NULL,
                                    nombre varchar(255),
                                    descripcion text,
                                    CONSTRAINT tmaterial_apoyo_pkey PRIMARY KEY(id_material_apoyo)
) INHERITS (pxp.tbase) WITHOUT OIDS;

CREATE TABLE afi.t_pre_registro(
                                   id_pre_registro SERIAL NOT NULL,
                                   data json,
                                   id_patrocinador integer,
                                   CONSTRAINT t_pre_registro_pkey PRIMARY KEY(id_pre_registro)
) INHERITS (pxp.tbase) WITHOUT OIDS;

alter table afi.tafiliado
    add doc_dni_name varchar(200);

--cambios en vid para incluir puntos acumualdo
CREATE TABLE afi.tpunto_acumulado(
                                 id_punto_acumulado SERIAL NOT NULL,
                                 id_afiliado integer,
                                 id_periodo integer,
                                 pv numeric(10,2), -- son los puntos acumulados que tienes disponibles para usar en el periodo
                                 CONSTRAINT tpunto_acumulado_pkey PRIMARY KEY(id_punto_acumulado)
) INHERITS (pxp.tbase) WITHOUT OIDS;


-----31mar2024
alter table afi.trango
    add requisitos jsonb;


/***********************************F-SCP-FFP-AFI-1-03/01/2022****************************************/






