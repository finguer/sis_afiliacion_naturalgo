/********************************************I-DAT-FFP-AFI-0-31/12/2012********************************************/


INSERT INTO segu.tsubsistema( codigo, nombre, fecha_reg, prefijo,
  estado_reg, nombre_carpeta, id_subsis_orig)
VALUES ( 'AFI', 'AFILIACION', '2011-11-23', 'AFI', 'activo', 'AFILIACION', NULL);
----------------------------------
--COPY LINES TO data.sql FILE
---------------------------------

select pxp.f_insert_tgui ('AFILIACION', '', 'AFI', 'si', 1, '', 1, '', '', 'AFI');
select pxp.f_insert_tgui ('Afiliado', 'Afiliado', 'AFILI', 'si', 1, 'sis_afiliacion/vista/afiliado/Afiliado.php', 2, '', 'Afiliado', 'AFI');
select pxp.f_insert_tgui ('Codificacion', 'Ennumeracion', 'CODIF', 'si', 2, 'sis_afiliacion/vista/codificacion/Codificacion.php', 2, '', 'Codificacion', 'AFI');
select pxp.f_insert_tgui ('Piramide', 'Piramide', 'PIR', 'si', 3, 'sis_afiliacion/vista/vista_diagramas/piramide.php', 2, '', 'Piramide', 'AFI');
select pxp.f_insert_tgui ('Binario', 'Binario', 'BINA', 'si', 4, 'sis_afiliacion/vista/vista_diagramas/binario.php', 2, '', 'Binario', 'AFI');
----------------------------------
--COPY LINES TO dependencies.sql FILE
---------------------------------

select pxp.f_insert_testructura_gui ('AFI', 'SISTEMA');
select pxp.f_insert_testructura_gui ('AFILI', 'AFI');
select pxp.f_insert_testructura_gui ('CODIF', 'AFI');
select pxp.f_insert_testructura_gui ('PIR', 'AFI');
select pxp.f_insert_testructura_gui ('BINA', 'AFI');

/*****************************F-DAT-FFP-AFI-0-05/08/2014*************/


/********************************************I-DAT-FFP-AFI-0-29/03/2016********************************************/

select pxp.f_insert_tgui ('puntos item', 'puntos item', 'PUNTITE', 'si', 5, 'sis_afiliacion/vista/punto_item/PuntoItem.php', 2, '', 'PuntoItem', 'AFI');
/*****************************F-DAT-FFP-AFI-0-29/03/2016*************/


/********************************************I-DAT-FFP-AFI-0-04/04/2016********************************************/

select pxp.f_insert_tgui ('afiliado pago periodo', 'AFILIADO PAGO PERIODO', 'AFILPAPE', 'si', 5, 'sis_afiliacion/vista/afiliado_pago_periodo/AfiliadoPagoPeriodo.php', 2, '', 'AfiliadoPagoPeriodo', 'AFI');

/*****************************F-DAT-FFP-AFI-0-04/04/2016*************/

/********************************************I-DAT-FFP-AFI-0-19/10/2016********************************************/


select pxp.f_insert_tgui ('Parametros', 'parametros', 'PARAFI', 'si', 1, '', 2, '', '', 'AFI');
select pxp.f_insert_tgui ('Rango', 'rango', 'RANG', 'si', 1, 'sis_afiliacion/vista/rango/Rango.php', 3, '', 'Rango', 'AFI');
select pxp.f_insert_tgui ('Pago confi', 'pago confi', 'PAGOCON', 'si', 2, 'sis_afiliacion/vista/pago_conf/PagoConf.php', 3, '', 'PagoConf', 'AFI');

/*****************************F-DAT-FFP-AFI-0-19/10/2016*************/


