<?php
/****************************************************************************************
*@package pXP
*@file gen-ACTBonusConfig.php
*@author  (admin)
*@date 22-11-2021 18:49:07
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo

 HISTORIAL DE MODIFICACIONES:
 #ISSUE                FECHA                AUTOR                DESCRIPCION
  #0                22-11-2021 18:49:07    admin             Creacion    
  #
*****************************************************************************************/

class ACTBonusConfig extends ACTbase{    
            
    function listarBonusConfig(){
		$this->objParam->defecto('ordenacion','id_bonus_config');

        $this->objParam->defecto('dir_ordenacion','asc');
		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
            $this->objReporte = new Reporte($this->objParam,$this);
            $this->res = $this->objReporte->generarReporteListado('MODBonusConfig','listarBonusConfig');
        } else{
        	$this->objFunc=$this->create('MODBonusConfig');
            
        	$this->res=$this->objFunc->listarBonusConfig($this->objParam);
        }
        $this->res->imprimirRespuesta($this->res->generarJson());
    }
                
    function insertarBonusConfig(){
        $this->objFunc=$this->create('MODBonusConfig');    
        if($this->objParam->insertar('id_bonus_config')){
            $this->res=$this->objFunc->insertarBonusConfig($this->objParam);            
        } else{            
            $this->res=$this->objFunc->modificarBonusConfig($this->objParam);
        }
        $this->res->imprimirRespuesta($this->res->generarJson());
    }
                        
    function eliminarBonusConfig(){
        	$this->objFunc=$this->create('MODBonusConfig');    
        $this->res=$this->objFunc->eliminarBonusConfig($this->objParam);
        $this->res->imprimirRespuesta($this->res->generarJson());
    }
            
}

?>