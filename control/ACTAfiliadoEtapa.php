<?php
/**
*@package pXP
*@file gen-ACTAfiliadoEtapa.php
*@author  (admin)
*@date 04-01-2016 16:42:12
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTAfiliadoEtapa extends ACTbase{    
			
	function listarAfiliadoEtapa(){
		$this->objParam->defecto('ordenacion','id_afiliado_etapa');

		$this->objParam->defecto('dir_ordenacion','asc');
		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODAfiliadoEtapa','listarAfiliadoEtapa');
		} else{
			$this->objFunc=$this->create('MODAfiliadoEtapa');
			
			$this->res=$this->objFunc->listarAfiliadoEtapa($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarAfiliadoEtapa(){
		$this->objFunc=$this->create('MODAfiliadoEtapa');	
		if($this->objParam->insertar('id_afiliado_etapa')){
			$this->res=$this->objFunc->insertarAfiliadoEtapa($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarAfiliadoEtapa($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarAfiliadoEtapa(){
			$this->objFunc=$this->create('MODAfiliadoEtapa');	
		$this->res=$this->objFunc->eliminarAfiliadoEtapa($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>