<?php
/**
*@package pXP
*@file gen-ACTAfiliadoPagoPeriodo.php
*@author  (admin)
*@date 02-04-2016 16:22:34
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/
require_once(dirname(__FILE__) . '/../reportes/REPAfiliadoPagoPeriodo.php');

class ACTAfiliadoPagoPeriodo extends ACTbase{    
			
	function listarAfiliadoPagoPeriodo(){
		$this->objParam->defecto('ordenacion','id_afiliado_pago_periodo');

		$this->objParam->defecto('dir_ordenacion','asc');

        $this->objParam->addFiltro("afpape.habilitado = ''si''"); // solo mostramos los habilitados

        if($this->objParam->getParametro('id_periodo')!=''){
			$this->objParam->addFiltro("afpape.id_periodo = ''".$this->objParam->getParametro('id_periodo')."''");
		}
		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODAfiliadoPagoPeriodo','listarAfiliadoPagoPeriodo');
		} else{
			$this->objFunc=$this->create('MODAfiliadoPagoPeriodo');
			
			$this->res=$this->objFunc->listarAfiliadoPagoPeriodo($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarAfiliadoPagoPeriodo(){
		$this->objFunc=$this->create('MODAfiliadoPagoPeriodo');	
		if($this->objParam->insertar('id_afiliado_pago_periodo')){
			$this->res=$this->objFunc->insertarAfiliadoPagoPeriodo($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarAfiliadoPagoPeriodo($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarAfiliadoPagoPeriodo(){
			$this->objFunc=$this->create('MODAfiliadoPagoPeriodo');	
		$this->res=$this->objFunc->eliminarAfiliadoPagoPeriodo($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}





	function generarPagoAfiliado(){
		$this->objFunc=$this->create('MODAfiliadoPagoPeriodo');
		$this->res=$this->objFunc->generarPagoAfiliado($this->objParam);




		$pagado = 'no';
		if($this->res->getTipo()=='ERROR'){
			if($this->res->getMensaje() == 'ya se encuentra pagado'){

				$pagado = 'si';
			}else{
				$this->res->imprimirRespuesta($this->res->generarJson());
				exit;
			}


		}

        $afil_pago_periodo = $this->res->getDatos();

		echo json_encode($afil_pago_periodo);
		exit;

        var_dump($afil_pago_periodo);
        exit;






        //obtener
		$this->objParam->parametros_consulta['ordenacion'] = 'id_afiliado_pago_periodo';
		$this->objParam->parametros_consulta['dir_ordenacion'] = 'asc';
		$this->objParam->parametros_consulta['filtro'] = ' 0 = 0 ';




		if($pagado == 'no'){

			$afil_pago_periodo = $this->res->getDatos();
			$this->objParam->addFiltro("afpape.id_afiliado_pago_periodo = " .  $afil_pago_periodo['id_afiliado_pago_periodo']);



        }else{

			$this->objParam->addFiltro("afpape.id_afiliado = ".$this->objParam->getParametro('id_afiliado') );
			$this->objParam->addFiltro("afpape.id_periodo = ".$this->objParam->getParametro('id_periodo') );


		}

		$this->objFunc = $this->create('MODAfiliadoPagoPeriodo');
		$this->res = $this->objFunc->listarAfiliadoPagoPeriodo($this->objParam);

		if($this->res->getTipo()=='ERROR'){
			$this->res->imprimirRespuesta($this->res->generarJson());
			exit;
		}


      


		if($this->objParam->getParametro('impresion')=='si'){







			$afiliadoPagoPeriodoSel = $this->res->getDatos();



			



			//obtener

			$this->objParam->parametros_consulta['filtro'] = ' 0 = 0 ';
			$this->objFunc=$this->create('MODAfiliadoPagoPeriodo');
			$this->res=$this->objFunc->listarRedPuntos($this->objParam);


			if($this->res->getTipo()=='ERROR'){
				$this->res->imprimirRespuesta($this->res->generarJson());
				exit;
			}

			$redPuntos = $this->res->getDatos();


			$this->objReporteFormato = new REPAfiliadoPagoPeriodo($this->objParam);


			$html = $this->objReporteFormato->pagoAfiliadoPrint($afiliadoPagoPeriodoSel,$redPuntos);

			



			$temp['html'] = $html;

			$this->res->setDatos($html);
			$this->res->imprimirRespuesta($this->res->generarJson());

	}else{
			$this->res->imprimirRespuesta($this->res->generarJson());

		}


	}

	function PagoAfiliado(){
		$this->objFunc=$this->create('MODAfiliadoPagoPeriodo');
		$this->res=$this->objFunc->PagoAfiliado($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
	function PagarBonusPaquete(){
		$this->objFunc=$this->create('MODAfiliadoPagoPeriodo');
		$this->res=$this->objFunc->PagarBonusPaquete($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}

	function listarRedPuntos(){
		$this->objFunc=$this->create('MODAfiliadoPagoPeriodo');
		$this->res=$this->objFunc->listarRedPuntos($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
	function generarPagos(){
    
		$this->objFunc=$this->create('MODAfiliadoPagoPeriodo');
		$this->res=$this->objFunc->generarPagos($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
	function premiar(){
		$this->objFunc=$this->create('MODAfiliadoPagoPeriodo');
		$this->res=$this->objFunc->premiar($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}

	function listarSumPagosDetalle(){

        //obtener

        $this->objParam->parametros_consulta['filtro'] = ' 0 = 0 ';
        $this->objFunc=$this->create('MODAfiliadoPagoPeriodo');
        $this->res=$this->objFunc->listarSumPagosDetalle($this->objParam);


        if($this->res->getTipo()=='ERROR'){
            $this->res->imprimirRespuesta($this->res->generarJson());
            exit;
        }

        $sumPagoDetalle = $this->res->getDatos();



        $this->objReporteFormato = new REPAfiliadoPagoPeriodo($this->objParam);


        $html = $this->objReporteFormato->SumPagoDetalle($sumPagoDetalle);


        $temp['html'] = $html;

        $this->res->setDatos($html);

		$this->res->imprimirRespuesta($this->res->generarJson());
	}




}

?>