<?php
/**
*@package pXP
*@file gen-ACTAfiliado.php
*@author  (admin)
*@date 02-04-2015 01:06:18
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTAfiliadoVirtual extends ACTbase{
			
	function listarAfiliadoOficinaVirtual(){

		$this->objFunc=$this->create('MODAfiliadoVirtual');

		$this->res=$this->objFunc->listarAfiliadoOficinaVirtual($this->objParam);

		$this->res->imprimirRespuesta($this->res->generarJson());
	}

	function conteoRamas(){



		$this->objFunc=$this->create('MODAfiliadoVirtual');

		$this->res=$this->objFunc->conteoRamas($this->objParam);

		$this->res->imprimirRespuesta($this->res->generarJson());


	}

	function tablaBinario(){



		$this->objFunc=$this->create('MODAfiliadoVirtual');

		$this->res=$this->objFunc->tablaBinario($this->objParam);

		$this->res->imprimirRespuesta($this->res->generarJson());

	}



			
}

?>