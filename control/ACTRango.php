<?php
/**
*@package pXP
*@file gen-ACTRango.php
*@author  (admin)
*@date 14-10-2016 13:55:26
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTRango extends ACTbase{    
			
	function listarRango(){
		$this->objParam->defecto('ordenacion','id_rango');

		$this->objParam->defecto('dir_ordenacion','asc');
		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODRango','listarRango');
		} else{
			$this->objFunc=$this->create('MODRango');
			
			$this->res=$this->objFunc->listarRango($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarRango(){
		$this->objFunc=$this->create('MODRango');	
		if($this->objParam->insertar('id_rango')){
			$this->res=$this->objFunc->insertarRango($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarRango($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarRango(){
			$this->objFunc=$this->create('MODRango');	
		$this->res=$this->objFunc->eliminarRango($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>