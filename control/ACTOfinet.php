<?php
/**
 *@package pXP
 *@file gen-ACTCotitular.php
 *@author  (admin)
 *@date 02-04-2015 01:19:24
 *@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
 */

class ACTOfinet extends ACTbase{



    function verCodigosIngreso(){
        $this->objFunc=$this->create('MODOfinet');
        $this->res=$this->objFunc->verCodigosIngreso($this->objParam);
        $this->res->imprimirRespuesta($this->res->generarJson());
    }


    function verDatosAfiliado(){

        if($this->objParam->getParametro('codigo')!=''){
            $this->objParam->addFiltro("afil.codigo = ''".$this->objParam->getParametro('codigo')."''");
        }

        if($this->objParam->getParametro('ci')!=''){
            $this->objParam->addFiltro("PERSON.ci = ''".$this->objParam->getParametro('ci')."''");
        }

        $this->objFunc=$this->create('MODOfinet');
        $this->res=$this->objFunc->verDatosAfiliado($this->objParam);
        $this->res->imprimirRespuesta($this->res->generarJson());
    }

}

?>