<?php
/**
*@package pXP
*@file gen-ACTBonus.php
*@author  (admin)
*@date 11-04-2015 16:33:17
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTBonus extends ACTbase{    
			
	function listarBonus(){
		$this->objParam->defecto('ordenacion','id_bonus');

		$this->objParam->defecto('dir_ordenacion','asc');


		if($this->objParam->getParametro('id_patrocinador') != ''){
			$this->objParam->addFiltro("bon.id_patrocinador = ''".$this->objParam->getParametro('id_patrocinador')."''");
		}

		if($this->objParam->getParametro('id_periodo') != ''){
			$this->objParam->addFiltro("bon.id_periodo = ''".$this->objParam->getParametro('id_periodo')."''");
		}

		if($this->objParam->getParametro('estado_pago') != ''){
			$this->objParam->addFiltro("bon.estado_pago = ''".$this->objParam->getParametro('estado_pago')."''");
		}

		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODBonus','listarBonus');
		} else{
			$this->objFunc=$this->create('MODBonus');
			
			$this->res=$this->objFunc->listarBonus($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarBonus(){
		$this->objFunc=$this->create('MODBonus');	
		if($this->objParam->insertar('id_bonus')){
			$this->res=$this->objFunc->insertarBonus($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarBonus($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarBonus(){
			$this->objFunc=$this->create('MODBonus');	
		$this->res=$this->objFunc->eliminarBonus($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}

	function generarPagoBonus(){

		$this->objParam->defecto('ordenacion','id_bonus');

		$this->objParam->defecto('dir_ordenacion','asc');

		$this->objParam->addFiltro("bon.id_patrocinador = ''".$this->objParam->getParametro('id_afiliado')."''");
		$this->objParam->addFiltro("bon.tipo = ''directo''");
		$this->objParam->addFiltro("bon.estado_pago = ''debe''");
		$this->objFunc=$this->create('MODBonus');
		$this->res=$this->objFunc->listarBonus($this->objParam);

		$directos = $this->res->getDatos();

		$this->objParam->parametros_consulta['filtro'] = ' 0 = 0 ';
		$this->objParam->addFiltro("bon.id_patrocinador = ''".$this->objParam->getParametro('id_afiliado')."''");
		$this->objParam->addFiltro("bon.tipo = ''indirecto''");
		$this->objParam->addFiltro("bon.estado_pago = ''debe''");
		$this->objFunc2=$this->create('MODBonus');
		$this->res2=$this->objFunc2->listarBonus($this->objParam);

		$indirectos = $this->res2->getDatos();
		$directos_total = sizeof($directos) * 50;
		$indirectos_total = sizeof($indirectos) * 15;

		$pagar = $this->pagar($this->objParam->getParametro('id_afiliado'));
		if($pagar == "correcto") {


			$html = '<!doctype html>
				<html>
				<head>
				<meta charset="utf-8">
				<title>Documento sin título</title>
				</head>

				<body onLoad="window.print();">
				<table width="100%" border="0">
					<tr>
					<td colspan="4"><div align="center">DIRECTOS</div></td>
				  </tr>
				  <tr>
					<td>AFI</td>
					<td>GRACIAS AQUIEN</td>
					<td>TIPO</td>
					<td>MONTO</td>
				  </tr>';

			foreach ($directos as $item_directo) {
				$html .= '<tr>
					<td>' . $item_directo['codigo_patrocinador'] . '</td>
					<td>' . $item_directo['codigo_afiliado'] . '</td>
					<td>' . $item_directo['tipo'] . '</td>
					<td>' . $item_directo['monto'] . '</td>
				  </tr>';
			}

			$html .= '<tr>
					<td colspan="3"><div align="right">TOTAL</div></td>
					<td>' . $directos_total . '</td>
				  </tr>
				</table>

				<table width="100%" border="0">
				<tr>
					<td colspan="4"><div align="center">INDIRECTOS</div></td>
				  </tr>
				  <tr>
					<td>AFI</td>
					<td>GRACIAS AQUIEN</td>
					<td>TIPO</td>
					<td>MONTO</td>
				  </tr>';
			foreach ($indirectos as $item_indirecto) {
				$html .= '<tr>
					<td>' . $item_indirecto['codigo_patrocinador'] . '</td>
					<td>' . $item_indirecto['codigo_afiliado'] . '</td>
					<td>' . $item_indirecto['tipo'] . '</td>
					<td>' . $item_indirecto['monto'] . '</td>
				  </tr>';
			}
			$html .= '<tr>
					<td colspan="3"><div align="right">TOTAL</div></td>
					<td>' . $indirectos_total . '</td>
				  </tr>
				</table>

				</body>
				</html>';

			$this->res->setDatos($html);
			$this->res->imprimirRespuesta($this->res->generarJson());
		}



	}
	function pagar($id_afiliado){
		$this->objFunc=$this->create('MODBonus');
		$res =$this->objFunc->pagar($id_afiliado);
		return $res;
	}

	function verSeDebe(){
		$this->objFunc=$this->create('MODBonus');
		$this->res=$this->objFunc->verSeDebe($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
	function pagarBonus(){
		$this->objFunc=$this->create('MODBonus');
		$this->res=$this->objFunc->pagarBonus($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>