<?php
/**
*@package pXP
*@file gen-ACTCodificacion.php
*@author  (admin)
*@date 04-04-2015 17:10:04
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTCodificacion extends ACTbase{    
			
	function listarCodificacion(){
		$this->objParam->defecto('ordenacion','id_codificacion');

		$this->objParam->defecto('dir_ordenacion','asc');
		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODCodificacion','listarCodificacion');
		} else{
			$this->objFunc=$this->create('MODCodificacion');
			
			$this->res=$this->objFunc->listarCodificacion($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarCodificacion(){
		$this->objFunc=$this->create('MODCodificacion');	
		if($this->objParam->insertar('id_codificacion')){
			$this->res=$this->objFunc->insertarCodificacion($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarCodificacion($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarCodificacion(){
			$this->objFunc=$this->create('MODCodificacion');	
		$this->res=$this->objFunc->eliminarCodificacion($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>