<?php
/**
*@package pXP
*@file gen-ACTPuntosAfiliado.php
*@author  (admin)
*@date 19-12-2015 00:21:31
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTPuntosAfiliado extends ACTbase{    
			
	function listarPuntosAfiliado(){
		$this->objParam->defecto('ordenacion','id_puntos_afiliado');

		$this->objParam->defecto('dir_ordenacion','asc');
		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODPuntosAfiliado','listarPuntosAfiliado');
		} else{
			$this->objFunc=$this->create('MODPuntosAfiliado');
			
			$this->res=$this->objFunc->listarPuntosAfiliado($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarPuntosAfiliado(){
		$this->objFunc=$this->create('MODPuntosAfiliado');	
		if($this->objParam->insertar('id_puntos_afiliado')){
			$this->res=$this->objFunc->insertarPuntosAfiliado($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarPuntosAfiliado($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarPuntosAfiliado(){
			$this->objFunc=$this->create('MODPuntosAfiliado');	
		$this->res=$this->objFunc->eliminarPuntosAfiliado($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>