<?php
/**
*@package pXP
*@file gen-ACTPagoConf.php
*@author  (admin)
*@date 16-10-2016 16:46:11
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTPagoConf extends ACTbase{    
			
	function listarPagoConf(){
		$this->objParam->defecto('ordenacion','id_pago_conf');

		$this->objParam->defecto('dir_ordenacion','asc');
		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODPagoConf','listarPagoConf');
		} else{
			$this->objFunc=$this->create('MODPagoConf');
			
			$this->res=$this->objFunc->listarPagoConf($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarPagoConf(){
		$this->objFunc=$this->create('MODPagoConf');	
		if($this->objParam->insertar('id_pago_conf')){
			$this->res=$this->objFunc->insertarPagoConf($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarPagoConf($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarPagoConf(){
			$this->objFunc=$this->create('MODPagoConf');	
		$this->res=$this->objFunc->eliminarPagoConf($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>