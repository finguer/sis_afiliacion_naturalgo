<?php
/**
*@package pXP
*@file gen-ACTCotitular.php
*@author  (admin)
*@date 02-04-2015 01:19:24
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTCotitular extends ACTbase{    
			
	function listarCotitular(){
		$this->objParam->defecto('ordenacion','id_cotitular');


		if($this->objParam->getParametro('id_afiliado')!=''){
			$this->objParam->addFiltro("titu.id_afiliado = ''".$this->objParam->getParametro('id_afiliado')."''");
		}


		$this->objParam->defecto('dir_ordenacion','asc');
		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODCotitular','listarCotitular');
		} else{
			$this->objFunc=$this->create('MODCotitular');
			
			$this->res=$this->objFunc->listarCotitular($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarCotitular(){
		$this->objFunc=$this->create('MODCotitular');	
		if($this->objParam->insertar('id_cotitular')){
			$this->res=$this->objFunc->insertarCotitular($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarCotitular($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarCotitular(){
			$this->objFunc=$this->create('MODCotitular');	
		$this->res=$this->objFunc->eliminarCotitular($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>