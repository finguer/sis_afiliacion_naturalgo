<?php
/**
*@package pXP
*@file gen-ACTPuntoItem.php
*@author  (admin)
*@date 18-12-2015 23:29:48
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTPuntoItem extends ACTbase{    
			
	function listarPuntoItem(){
		$this->objParam->defecto('ordenacion','id_punto_item');

		$this->objParam->defecto('dir_ordenacion','asc');
		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODPuntoItem','listarPuntoItem');
		} else{
			$this->objFunc=$this->create('MODPuntoItem');
			
			$this->res=$this->objFunc->listarPuntoItem($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarPuntoItem(){
		$this->objFunc=$this->create('MODPuntoItem');	
		if($this->objParam->insertar('id_punto_item')){
			$this->res=$this->objFunc->insertarPuntoItem($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarPuntoItem($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarPuntoItem(){
			$this->objFunc=$this->create('MODPuntoItem');	
		$this->res=$this->objFunc->eliminarPuntoItem($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>