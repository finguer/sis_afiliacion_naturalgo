<?php
/**
*@package pXP
*@file gen-ACTAfiliado.php
*@author  (admin)
*@date 02-04-2015 01:06:18
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/



class ACTAfiliado extends ACTbase{

	function listarAfiliado(){
		$this->objParam->defecto('ordenacion','id_afiliado');

		$this->objParam->defecto('dir_ordenacion','asc');


		if($this->objParam->getParametro('codigo')!=''){
			$this->objParam->addFiltro("afil.codigo = ''".$this->objParam->getParametro('codigo')."''");
		}

		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODAfiliado','listarAfiliado');
		} else{
			$this->objFunc=$this->create('MODAfiliado');

			$this->res=$this->objFunc->listarAfiliado($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}

	function insertarAfiliado(){
		$this->objFunc=$this->create('MODAfiliado');
		if($this->objParam->insertar('id_afiliado')){
			$this->res=$this->objFunc->insertarAfiliado($this->objParam);
		} else{
			$this->res=$this->objFunc->modificarAfiliado($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}

	function eliminarAfiliado(){
			$this->objFunc=$this->create('MODAfiliado');
		$this->res=$this->objFunc->eliminarAfiliado($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}


	function conteoRamas(){


		$this->objFunc=$this->create('MODAfiliado');

		$this->res=$this->objFunc->conteoRamas($this->objParam);

		$this->res->imprimirRespuesta($this->res->generarJson());

	}

    function listarArbolAfilido(){


        if($this->objParam->getParametro('inicio')== 'si'){
            $this->objParam->addFiltro("afil.codigo = ".$this->objParam->getParametro('codigo')."");
        }else if($this->objParam->getParametro('inicio')== 'no'){
            $this->objParam->addFiltro("af.codigo = ''".$this->objParam->getParametro('codigo')."''");

        }


        $this->objFunc=$this->create('MODAfiliado');

        $this->res=$this->objFunc->listarArbolAfilido($this->objParam);

        $this->res->imprimirRespuesta($this->res->generarJson());

    }
    function listarAfiliadoRedPersonal(){


        $this->objFunc=$this->create('MODAfiliado');

        $this->res=$this->objFunc->listarAfiliadoRedPersonal($this->objParam);

        $this->res->imprimirRespuesta($this->res->generarJson());

    }
    function listarAfiliadoOficina(){

        $this->objFunc=$this->create('MODAfiliado');
        $this->res=$this->objFunc->listarAfiliadoOficina($this->objParam);
        $this->res->imprimirRespuesta($this->res->generarJson());

    }
    function cerrarSesionOficinaVirtual()
    {
        session_start() ;
        session_unset();
        session_destroy();
    }


    //funciones para ejectuar en crontab

   /*esta funcion sirve para poder hacer compresion dinamica y eliminar todos los afiliados inactivos 6 meses*/
    function compresionDinamica(){

        $this->objFunc=$this->create('MODAfiliado');
        $this->res=$this->objFunc->compresionDinamica($this->objParam);
        $this->res->imprimirRespuesta($this->res->generarJson());

    }

    function insertarPreRegistro(){

        //g-recaptcha-response
        $url = "https://www.google.com/recaptcha/api/siteverify";
        $privateKey = "6Lf4RegUAAAAABBDHiJIKJlqQ8RNlK20qDjnjTGB";
        $res_captcha = $this->objParam->getParametro('g-recaptcha-response');
        $response = file_get_contents($url . "?secret=" . $privateKey . "&response=" . $res_captcha);
        $data = json_decode($response);
        if (isset($data->success) and $data->success == true) {

            $this->objFunc=$this->create('MODAfiliado');
            $this->res=$this->objFunc->insertarPreRegistro($this->objParam);
            $this->res->imprimirRespuesta($this->res->generarJson());

        }else{
            echo "eres un robot";
        }


    }


    function listarPreRegistroPersona(){
        $this->objParam->defecto('ordenacion','id_persona');

        $this->objParam->defecto('dir_ordenacion','asc');

        $this->objFunc=$this->create('MODAfiliado');

        $this->res=$this->objFunc->listarPreRegistroPersona($this->objParam);
        $this->res->imprimirRespuesta($this->res->generarJson());
    }




}

?>