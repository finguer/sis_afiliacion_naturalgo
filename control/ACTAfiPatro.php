<?php
/**
*@package pXP
*@file gen-ACTAfiPatro.php
*@author  (admin)
*@date 04-04-2015 15:40:52
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTAfiPatro extends ACTbase{    
			
	function listarAfiPatro(){
		$this->objParam->defecto('ordenacion','id_afi_patro');

		$this->objParam->defecto('dir_ordenacion','asc');

		if($this->objParam->getParametro('id_patrocinador')!=''){
			$this->objParam->addFiltro("patro.id_patrocinador = ''".$this->objParam->getParametro('id_patrocinador')."''");
		}

		if($this->objParam->getParametro('id_afiliado_conjunto')!=''){
			$this->objParam->addFiltroOR("patro.id_afiliado = ''".$this->objParam->getParametro('id_afiliado_conjunto')."''");
		}


		if($this->objParam->getParametro('id_afiliado')!=''){
			$this->objParam->addFiltro("patro.id_afiliado = ''".$this->objParam->getParametro('id_afiliado')."''");
		}

		if($this->objParam->getParametro('codigo')!=''){
			$this->objParam->addFiltro("afil.codigo = ''".$this->objParam->getParametro('codigo')."''");
		}



		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODAfiPatro','listarAfiPatro');
		} else{
			$this->objFunc=$this->create('MODAfiPatro');
			
			$this->res=$this->objFunc->listarAfiPatro($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarAfiPatro(){
		$this->objFunc=$this->create('MODAfiPatro');	
		if($this->objParam->insertar('id_afi_patro')){
			$this->res=$this->objFunc->insertarAfiPatro($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarAfiPatro($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarAfiPatro(){
			$this->objFunc=$this->create('MODAfiPatro');	
		$this->res=$this->objFunc->eliminarAfiPatro($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>