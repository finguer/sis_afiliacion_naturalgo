<?php
/**
*@package pXP
*@file gen-ACTBonusproductopatrocinador.php
*@author  (admin)
*@date 06-01-2016 16:58:27
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTBonusproductopatrocinador extends ACTbase{    
			
	function listarBonusproductopatrocinador(){
		$this->objParam->defecto('ordenacion','id_bonus_producto_patrocinador');

		$this->objParam->defecto('dir_ordenacion','asc');
		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODBonusproductopatrocinador','listarBonusproductopatrocinador');
		} else{
			$this->objFunc=$this->create('MODBonusproductopatrocinador');
			
			$this->res=$this->objFunc->listarBonusproductopatrocinador($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarBonusproductopatrocinador(){
		$this->objFunc=$this->create('MODBonusproductopatrocinador');	
		if($this->objParam->insertar('id_bonus_producto_patrocinador')){
			$this->res=$this->objFunc->insertarBonusproductopatrocinador($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarBonusproductopatrocinador($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarBonusproductopatrocinador(){
			$this->objFunc=$this->create('MODBonusproductopatrocinador');	
		$this->res=$this->objFunc->eliminarBonusproductopatrocinador($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>