<?php
/**
*@package pXP
*@file gen-ACTPatroColo.php
*@author  (admin)
*@date 04-04-2015 15:41:13
*@description Clase que recibe los parametros enviados por la vista para mandar a la capa de Modelo
*/

class ACTPatroColo extends ACTbase{    
			
	function listarPatroColo(){
		$this->objParam->defecto('ordenacion','id_patro_colo');

		$this->objParam->defecto('dir_ordenacion','asc');


		if($this->objParam->getParametro('id_colocacion')!=''){
			$this->objParam->addFiltro("colo.id_colocacion = ''".$this->objParam->getParametro('id_colocacion')."''");
		}


		if($this->objParam->getParametro('codigo')!=''){
			$this->objParam->addFiltro("afil.codigo = ''".$this->objParam->getParametro('codigo')."''");
		}

		if($this->objParam->getParametro('conjunto')=='si'){

			if($this->objParam->getParametro('izquierda')!= 'null' && $this->objParam->getParametro('derecha')!= 'null'){

				$this->objParam->addFiltro("colo.id_colocacion = ''".$this->objParam->getParametro('izquierda')."'' or colo.id_colocacion = ''".$this->objParam->getParametro('derecha')."''");

			}else if($this->objParam->getParametro('izquierda')== 'null' && $this->objParam->getParametro('derecha')== 'null'){
				echo "no tiene hijos";
				exit;
			}
			else if($this->objParam->getParametro('izquierda')== 'null'){

				$this->objParam->addFiltro("colo.id_colocacion = ''".$this->objParam->getParametro('derecha')."''");

			}else if($this->objParam->getParametro('derecha')== 'null'){

				$this->objParam->addFiltro("colo.id_colocacion = ''".$this->objParam->getParametro('izquierda')."''");
			}

		}

		/*if($this->objParam->getParametro('izquierda')!=''){
			$this->objParam->addFiltro("colo.izquierda = ''".$this->objParam->getParametro('izquierda')."''");
		}
		if($this->objParam->getParametro('derecha')!=''){
			$this->objParam->addFiltro("colo.derecha = ''".$this->objParam->getParametro('derecha')."''");
		}*/

		if($this->objParam->getParametro('tipoReporte')=='excel_grid' || $this->objParam->getParametro('tipoReporte')=='pdf_grid'){
			$this->objReporte = new Reporte($this->objParam,$this);
			$this->res = $this->objReporte->generarReporteListado('MODPatroColo','listarPatroColo');
		} else{
			$this->objFunc=$this->create('MODPatroColo');
			
			$this->res=$this->objFunc->listarPatroColo($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
				
	function insertarPatroColo(){
		$this->objFunc=$this->create('MODPatroColo');	
		if($this->objParam->insertar('id_patro_colo')){
			$this->res=$this->objFunc->insertarPatroColo($this->objParam);			
		} else{			
			$this->res=$this->objFunc->modificarPatroColo($this->objParam);
		}
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
						
	function eliminarPatroColo(){
			$this->objFunc=$this->create('MODPatroColo');	
		$this->res=$this->objFunc->eliminarPatroColo($this->objParam);
		$this->res->imprimirRespuesta($this->res->generarJson());
	}
			
}

?>