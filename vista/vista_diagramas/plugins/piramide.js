/**
 * Created by f on 11/04/2015.
 */
// ┌────────────────────────────────────────────────────────────────────┐ \\
// │ Favio Figueroa Penarrieta - JavaScript Library                     │ \\
// ├────────────────────────────────────────────────────────────────────┤ \\
// │ Copyright © 2014 Disydes (http://disydes.com)                      │ \\
// ├────────────────────────────────────────────────────────────────────┤ \\
// │ Vista para automatizar cualquier front end basado en jQuery        │ \\
// └────────────────────────────────────────────────────────────────────┘ \\


var piramide = function () {
    "use strict";
    var respuesta;
    var respuesta2;
    var toltip;

    var codigo_padre;
    var codigo_hijo;
    var codigo_nieto;

    var datos_piramide = {
        datos: []
    };

    /* Cadena jSon para crear el formulario con sus respectivos atributos*/

    //alert('llega')


    function iniciar_peticiones() {


        ajax_dyd.peticion_ajax(function (callback) {

            respuesta = callback;
            // console.log(respuesta.datos);


            $.each(respuesta.datos, function (k, v) {
                // console.log(v);
                codigo_padre = v.codigo;

                datos_piramide.datos.push(
                    {
                        id: v.codigo,
                        parent: null,
                        description: "ci : " + v.ci + " Cod: " + v.codigo + "",
                        email: v.correo,
                        groupTitleColor: "#4169e1",
                        image: "demo/images/photos/p.png",
                        itemTitleColor: "#4169e1",
                        phone: "Tel: " + v.telefono1 + " Cel:" + v.celular1 + " ",
                        title: v.desc_person,
                        label: v.desc_person
                    });


                ajax_dyd.x = '../../sis_afiliacion/control/AfiPatro/listarAfiPatro';
                ajax_dyd.p = '{"start":"0","limit":"999","sort":"id_afi_patro","dir":"ASC","id_patrocinador":"' + v.id_afiliado + '"}';
                ajax_dyd.type = 'POST';
                ajax_dyd.url = '../../../../kerp/pxp/lib/lib_control/Intermediario.php';
                ajax_dyd.dataType = 'json';
                ajax_dyd.async = true;


                ajax_dyd.peticion_ajax(function (callback) {
                    //console.log(callback);
                    $.each(callback.datos, function (k, v) {
                        codigo_hijo = v.codigo;
                        if (v.codigo != '' || v.codigo == null) {
                            datos_piramide.datos.push(
                                {
                                    id: v.codigo,
                                    parent: codigo_padre,
                                    description: "ci : " + v.ci + " Cod: " + v.codigo + "",
                                    email: v.correo,
                                    groupTitleColor: "#4169e1",
                                    image: "demo/images/photos/p.png",
                                    itemTitleColor: "#4169e1",
                                    phone: "Tel: " + v.telefono1 + " Cel:" + v.celular1 + " ",
                                    title: v.desc_person,
                                    label: v.desc_person
                                });
                        }


                        //ajax nietos
                        ajax_dyd.x = '../../sis_afiliacion/control/AfiPatro/listarAfiPatro';
                        ajax_dyd.p = '{"start":"0","limit":"999","sort":"id_afi_patro","dir":"ASC","id_patrocinador":"' + v.id_afiliado + '"}';
                        ajax_dyd.type = 'POST';
                        ajax_dyd.url = '../../../../kerp/pxp/lib/lib_control/Intermediario.php';
                        ajax_dyd.dataType = 'json';
                        ajax_dyd.async = true;
                        ajax_dyd.peticion_ajax(function (callback) {
                            $.each(callback.datos, function (k, v) {

                                if (v.codigo != '' || v.codigo == null) {
                                    datos_piramide.datos.push(
                                        {
                                            id: v.codigo,
                                            parent: codigo_hijo,
                                            description: "ci : " + v.ci + " Cod: " + v.codigo + "",
                                            email: v.correo,
                                            groupTitleColor: "#4169e1",
                                            image: "demo/images/photos/p.png",
                                            itemTitleColor: "#4169e1",
                                            phone: "Tel: " + v.telefono1 + " Cel:" + v.celular1 + " ",
                                            title: v.desc_person,
                                            label: v.desc_person
                                        });

                                }
                            });//fin each2


                        }); //fin p3


                    }); //fin each2


                }); // fin p2


            });//fin each1


        });//fin p1

        return datos_piramide;
    }

    var datos_ = iniciar_peticiones();
    console.log(datos_)
    console.log('levame')




   // console.log(datos_piramide);





    return {
        //funcion que inicia
        init: function () {
            //  crearForm();



        }
    };
}();
