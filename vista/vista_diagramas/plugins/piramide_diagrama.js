// ┌────────────────────────────────────────────────────────────────────┐ \\
// │ Favio Figueroa Penarrieta - JavaScript Library                     │
// ├────────────────────────────────────────────────────────────────────┤ \\
// │ Copyright © 2014 Disydes (http://disydes.com)                      │
// ├────────────────────────────────────────────────────────────────────┤ \\
// │ Vista para automatizar cualquier front end basado en jQuery        │ plugin para hacer peticiones ajax
// └────────────────────────────────────────────────────────────────────┘ \\

(function ($) {
    $.tipo = '';
    $.aurl = '';
    piramide_diagrama = {

        respuesta:"",
    respuesta2:"",
    toltip:"",

    codigo_padre:"",
    codigo_hijo:"",
    codigo_nieto:"",
        id_padre:"",
        id_hijo:"",
        id_nieto:"",
        res:"",
        id_afiliado:"",
        consecutivo:0,
        colors : ['#8000FF', '#C8FE2E', '#DBA901','#FE642E', '#01DFD7', '#084B8A'],




        iniciar_peticiones_padre : function () {

             var  datos_piramide = {
                datos: []
            };



            ajax_dyd.x = '../../sis_afiliacion/control/AfiPatro/listarAfiPatro';
            ajax_dyd.p = '{"start":"0","limit":"1","sort":"id_afi_patro","dir":"ASC","codigo":"5910001"}';
            ajax_dyd.type = 'POST';
            ajax_dyd.url = '../../../lib/lib_control/Intermediario.php';

            ajax_dyd.dataType = 'json';
            ajax_dyd.async = false;
            ajax_dyd.peticion_ajax(function (callback) {

                respuesta = callback;

            });//fin p1

            $.each(respuesta.datos, function (k, v) {
                // console.log(v);
               // piramide_diagrama.codigo_padre = v.codigo;
                piramide_diagrama.codigo_padre = 0;
                piramide_diagrama.id_padre = v.id_afiliado;

                datos_piramide.datos.push(
                    {
                        id: v.id_afiliado,
                        parent: null,
                        description: "ci : " + v.ci + " Cod: " + v.codigo + "",
                        email: v.correo,
                        groupTitleColor: piramide_diagrama.colors[0],
                        image: "demo/images/photos/p.png",
                        itemTitleColor: piramide_diagrama.colors[0],
                        phone: "Tel: " + v.telefono1 + " Cel:" + v.celular1 + " ",
                        title: v.desc_person,
                        label: v.desc_person
                    });

            });//fin each1


            this.res = datos_piramide;

            this.iniciar_peticiones_hijos()



        },

        iniciar_peticiones_hijos : function () {


            ajax_dyd.x = '../../sis_afiliacion/control/AfiPatro/listarAfiPatro';
            ajax_dyd.p = '{"start":"0","limit":"999","sort":"id_afi_patro","dir":"ASC","id_patrocinador":"' + this.id_padre + '"}';
            ajax_dyd.type = 'POST';
            ajax_dyd.url = '../../../lib/lib_control/Intermediario.php';
            ajax_dyd.dataType = 'json';
            ajax_dyd.async = false;
            ajax_dyd.peticion_ajax(function (callback) {
                //console.log(callback);
                respuesta = callback;

            }); // fin p2

            $.each(respuesta.datos, function (k, v) {

                piramide_diagrama.consecutivo++;
                piramide_diagrama.codigo_hijo =  piramide_diagrama.consecutivo;
                piramide_diagrama.id_hijo = v.id_afiliado;

                if (v.codigo != '' || v.codigo == null) {

                    piramide_diagrama.res.datos.push(
                     {
                     id: v.id_afiliado,
                     parent: piramide_diagrama.id_padre,
                     description: "ci : " + v.ci + " Cod: " + v.codigo + "",
                     email: v.correo,
                     groupTitleColor: piramide_diagrama.colors[piramide_diagrama.consecutivo],
                     image: "demo/images/photos/p.png",
                     itemTitleColor: piramide_diagrama.colors[piramide_diagrama.consecutivo],
                     phone: "Tel: " + v.telefono1 + " Cel:" + v.celular1 + " ",
                     title: v.desc_person,
                     label: v.desc_person
                     });

                    piramide_diagrama.iniciar_peticiones_nietos(v.id_afiliado);
                }

            }); //fin each2


            //$("#dibujar_diagrama").click();



        },


        iniciar_peticiones_nietos : function (id_afiliado) {


            //ajax nietos
            ajax_dyd.x = '../../sis_afiliacion/control/AfiPatro/listarAfiPatro';
            ajax_dyd.p = '{"start":"0","limit":"999","sort":"id_afi_patro","dir":"ASC","id_patrocinador":"' + id_afiliado + '"}';
            ajax_dyd.type = 'POST';
            ajax_dyd.url = '../../../lib/lib_control/Intermediario.php';
            ajax_dyd.dataType = 'json';
            ajax_dyd.async = false;
            ajax_dyd.peticion_ajax(function (callback) {
                respuesta = callback;

            }); //fin p3

            $.each(respuesta.datos, function (k, v) {


                if (v.codigo != '' || v.codigo == null) {
                    piramide_diagrama.res.datos.push(
                        {
                            id: v.id_afiliado,
                            parent: piramide_diagrama.id_hijo,
                            description: "ci : " + v.ci + " Cod: " + v.codigo + "",
                            email: v.correo,
                            groupTitleColor: piramide_diagrama.colors[piramide_diagrama.codigo_hijo],
                            image: "demo/images/photos/p.png",
                            itemTitleColor: piramide_diagrama.colors[piramide_diagrama.codigo_hijo],
                            phone: "Tel: " + v.telefono1 + " Cel:" + v.celular1 + " ",
                            title: v.desc_person,
                            label: v.desc_person
                        });

                }
            });//fin each2



        }


    };


})
(jQuery);