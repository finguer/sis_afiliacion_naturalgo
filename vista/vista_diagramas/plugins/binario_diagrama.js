// ┌────────────────────────────────────────────────────────────────────┐ \\
// │ Favio Figueroa Penarrieta - JavaScript Library                     │
// ├────────────────────────────────────────────────────────────────────┤ \\
// │ Copyright © 2014 Disydes (http://disydes.com)                      │
// ├────────────────────────────────────────────────────────────────────┤ \\
// │ Vista para automatizar cualquier front end basado en jQuery        │ plugin para hacer peticiones ajax
// └────────────────────────────────────────────────────────────────────┘ \\

(function ($) {
    $.tipo = '';
    $.aurl = '';
    binario_diagrama = {

        respuesta:"",
    respuesta2:"",
    toltip:"",

    codigo_padre:"",
    codigo_hijo:"",
    codigo_nieto:"",
        id_padre:"",
        id_hijo:"",
        id_nieto:"",
        //res:"",
        id_afiliado:"",
        consecutivo:0,
        colors : ['#8000FF', '#8000FF', '#DBA901','#FE642E', '#01DFD7', '#084B8A'],
        res : {
            datos: []
        },
        id_izquierda:"",id_derecha:"",




        iniciar_peticiones_padre : function (codigo_afil) {





            ajax_dyd.x = '../../sis_afiliacion/control/PatroColo/listarPatroColo';
            ajax_dyd.p = '{"start":"0","limit":"1","sort":"id_patro_colo","dir":"ASC","codigo":"'+codigo_afil+'"}';
            ajax_dyd.type = 'POST';
            ajax_dyd.url = '../../../lib/lib_control/Intermediario.php';
            ajax_dyd.dataType = 'json';
            ajax_dyd.async = false;
            ajax_dyd.peticion_ajax(function (callback) {

                respuesta = callback;

            });//fin p1

            $.each(respuesta.datos, function (k, v) {
                // console.log(v);
                binario_diagrama.codigo_padre = v.id_colocacion;
                binario_diagrama.id_izquierda = v.izquierda;
                binario_diagrama.id_derecha = v.derecha;

                binario_diagrama.res.datos.push(
                    {
                        id: v.id_colocacion,
                        parent: null,
                        description: "ci : " + v.ci + " Cod: " + v.codigo + "",
                        email: v.correo,
                        groupTitleColor: binario_diagrama.colors[v.id_colocacion],
                        image: "demo/images/photos/p.png",
                        itemTitleColor: binario_diagrama.colors[v.id_colocacion],
                        phone: "Tel: " + v.telefono1 + " Cel:" + v.celular1 + " ",
                        title: v.desc_person,
                        label: v.desc_person,
                        izquierda: v.izquierda,
                        derecha: v.derecha
                    });

            });//fin each1


           // this.res = datos_binario;
            console.log('variables dinamicas ' , this);

            if(this.id_izquierda != null && this.id_derecha != null){
                this.iniciar_peticiones_hijos()
            }





        },

        iniciar_peticiones_hijos: function () {


            ajax_dyd.x = '../../sis_afiliacion/control/PatroColo/listarPatroColo';
            ajax_dyd.p = '{"start":"0","limit":"5","conjunto":"si","sort":"colocacion","dir":"DESC","izquierda":"' + this.id_izquierda + '","derecha":"' + this.id_derecha+ '"}';
            ajax_dyd.type = 'POST';
            ajax_dyd.url = '../../../lib/lib_control/Intermediario.php';
            ajax_dyd.dataType = 'json';
            ajax_dyd.async = false;
            ajax_dyd.peticion_ajax(function (callback) {
                //console.log(callback);
                respuesta = callback;

            }); // fin p2

            $.each(respuesta.datos, function (k, v) {

                binario_diagrama.consecutivo++;
                binario_diagrama.codigo_hijo =  binario_diagrama.consecutivo;

                if (v.codigo != '' || v.codigo == null) {

                    binario_diagrama.res.datos.push(
                     {
                     id: v.id_colocacion,
                     parent: binario_diagrama.codigo_padre,
                     description: "ci : " + v.ci + " Cod: " + v.codigo + "",
                     email: v.correo,
                     groupTitleColor: binario_diagrama.colors[binario_diagrama.codigo_padre],
                     image: "demo/images/photos/p.png",
                     itemTitleColor: binario_diagrama.colors[binario_diagrama.codigo_padre],
                     phone: "Tel: " + v.telefono1 + " Cel:" + v.celular1 + " ",
                     title: v.desc_person,
                     label: v.desc_person,
                         izquierda: v.izquierda,
                         derecha: v.derecha
                     });


                }

            }); //fin each2


            //$("#dibujar_diagrama").click();



        },


        iniciar_peticiones_nietos : function (id_afiliado) {


            //ajax nietos
            ajax_dyd.x = '../../sis_afiliacion/control/AfiPatro/listarAfiPatro';
            ajax_dyd.p = '{"start":"0","limit":"999","sort":"id_afi_patro","dir":"ASC","id_patrocinador":"' + id_afiliado + '"}';
            ajax_dyd.type = 'POST';
            ajax_dyd.url = '../../../lib/lib_control/Intermediario.php';
            ajax_dyd.dataType = 'json';
            ajax_dyd.async = false;
            ajax_dyd.peticion_ajax(function (callback) {
                respuesta = callback;

            }); //fin p3

            $.each(respuesta.datos, function (k, v) {


                if (v.codigo != '' || v.codigo == null) {
                    binario_diagrama.res.datos.push(
                        {
                            id: binario_diagrama.consecutivo+1,
                            parent: binario_diagrama.codigo_hijo,
                            description: "ci : " + v.ci + " Cod: " + v.codigo + "",
                            email: v.correo,
                            groupTitleColor: binario_diagrama.colors[binario_diagrama.codigo_hijo],
                            image: "demo/images/photos/p.png",
                            itemTitleColor: binario_diagrama.colors[binario_diagrama.codigo_hijo],
                            phone: "Tel: " + v.telefono1 + " Cel:" + v.celular1 + " ",
                            title: v.desc_person,
                            label: v.desc_person
                        });

                }
            });//fin each2



        }


    };


})
(jQuery);