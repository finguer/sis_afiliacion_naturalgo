<?php
/**
*@package pXP
*@file gen-CuentaBancaria.php
*@author  (admin)
*@date 18-08-2019 17:44:41
*@description Archivo con la interfaz de usuario que permite la ejecucion de todas las funcionalidades del sistema
*/

header("content-type: text/javascript; charset=UTF-8");
?>
<script>
Phx.vista.CuentaBancaria=Ext.extend(Phx.gridInterfaz,{

	constructor:function(config){
		this.maestro=config.maestro;
    	//llama al constructor de la clase padre
		Phx.vista.CuentaBancaria.superclass.constructor.call(this,config);
		this.init();
		this.load({params:{start:0, limit:this.tam_pag}})
	},
			
	Atributos:[
		{
			//configuracion del componente
			config:{
					labelSeparator:'',
					inputType:'hidden',
					name: 'id_cuenta_bancaria'
			},
			type:'Field',
			form:true 
		},
        {
            config: {
                name: 'id_afiliado',
                fieldLabel: 'Afiliado',
                allowBlank: false,
                emptyText: 'Elija una opción...',
                store: new Ext.data.JsonStore({
                    url: '../../sis_afiliacion/control/Afiliado/listarAfiliado',
                    id: 'id_afiliado',
                    root: 'datos',
                    sortInfo: {
                        field: 'codigo',
                        direction: 'ASC'

                    },
                    totalProperty: 'total',
                    fields: ['id_afiliado', 'desc_person', 'direccion_principal', 'codigo', 'ci'],
                    remoteSort: true,
                    baseParams: {par_filtro: 'afil.direccion_principal#afil.codigo'}
                }),
                valueField: 'id_afiliado',
                displayField: 'codigo',
                gdisplayField: 'codigo_desc',
                tpl: '<tpl for="."><div class="x-combo-list-item"><p>{desc_person}</p><p>CI:{ci}</p>CODIGO: {codigo} </div></tpl>',

                hiddenName: 'id_afiliado',
                forceSelection: true,
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'remote',
                pageSize: 15,
                queryDelay: 1000,
                anchor: '60%',
                gwidth: 150,
                minChars: 2,
                renderer: function (value, p, record) {
                    return String.format('{0}', record.data['codigo_desc']);
                }
            },
            type: 'ComboBox',
            id_grupo: 0,
            filters: {pfiltro: 'afil.direccion_principal', type: 'string'},
            grid: true,
            form: true
        },
		{
			config:{
				name: 'estado_reg',
				fieldLabel: 'Estado Reg.',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:10
			},
				type:'TextField',
				filters:{pfiltro:'cuban.estado_reg',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},

        {
            config: {
                name: 'tipo_cuenta',
                fieldLabel: 'Tipo de Cuenta',
                allowBlank: true,
                emptyText: 'Tipo de Cuenta...',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: ['AHORRO', 'CORRIENTE'],
                width: 200
            },
            type: 'ComboBox',
            filters:{pfiltro:'cuban.tipo_cuenta',type:'string'},
            id_grupo: 1,
            form: true,
            grid:true

        },



        {
            config: {
                name: 'banco',
                fieldLabel: 'Banco',
                allowBlank: true,
                emptyText: 'Banco...',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: ['BANCO UNION', 'BANCO SOL'],
                width: 200
            },
            type: 'ComboBox',
            filters:{pfiltro:'cuban.banco',type:'string'},
            id_grupo: 1,
            form: true,
            grid:true

        },



		{
			config:{
				name: 'nro_cuenta',
				fieldLabel: 'nro_cuenta',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:255
			},
				type:'TextField',
				filters:{pfiltro:'cuban.nro_cuenta',type:'string'},
				id_grupo:1,
				grid:true,
				form:true
		},
		{
			config:{
				name: 'fecha_reg',
				fieldLabel: 'Fecha creación',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
							format: 'd/m/Y', 
							renderer:function (value,p,record){return value?value.dateFormat('d/m/Y H:i:s'):''}
			},
				type:'DateField',
				filters:{pfiltro:'cuban.fecha_reg',type:'date'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'usuario_ai',
				fieldLabel: 'Funcionaro AI',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:300
			},
				type:'TextField',
				filters:{pfiltro:'cuban.usuario_ai',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'usr_reg',
				fieldLabel: 'Creado por',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'usu1.cuenta',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'id_usuario_ai',
				fieldLabel: 'Creado por',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'cuban.id_usuario_ai',type:'numeric'},
				id_grupo:1,
				grid:false,
				form:false
		},
		{
			config:{
				name: 'fecha_mod',
				fieldLabel: 'Fecha Modif.',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
							format: 'd/m/Y', 
							renderer:function (value,p,record){return value?value.dateFormat('d/m/Y H:i:s'):''}
			},
				type:'DateField',
				filters:{pfiltro:'cuban.fecha_mod',type:'date'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'usr_mod',
				fieldLabel: 'Modificado por',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'usu2.cuenta',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		}
	],
	tam_pag:50,	
	title:'Cuenta Bancaria',
	ActSave:'../../sis_afiliacion/control/CuentaBancaria/insertarCuentaBancaria',
	ActDel:'../../sis_afiliacion/control/CuentaBancaria/eliminarCuentaBancaria',
	ActList:'../../sis_afiliacion/control/CuentaBancaria/listarCuentaBancaria',
	id_store:'id_cuenta_bancaria',
	fields: [
		{name:'id_cuenta_bancaria', type: 'numeric'},
		{name:'id_afiliado', type: 'numeric'},
		{name:'estado_reg', type: 'string'},
		{name:'tipo_cuenta', type: 'string'},
		{name:'banco', type: 'string'},
		{name:'nro_cuenta', type: 'string'},
		{name:'fecha_reg', type: 'date',dateFormat:'Y-m-d H:i:s.u'},
		{name:'usuario_ai', type: 'string'},
		{name:'id_usuario_reg', type: 'numeric'},
		{name:'id_usuario_ai', type: 'numeric'},
		{name:'fecha_mod', type: 'date',dateFormat:'Y-m-d H:i:s.u'},
		{name:'id_usuario_mod', type: 'numeric'},
		{name:'usr_reg', type: 'string'},
		{name:'usr_mod', type: 'string'},
		{name:'codigo_desc', type: 'numeric'},

	],
	sortInfo:{
		field: 'id_cuenta_bancaria',
		direction: 'ASC'
	},
	bdel:true,
	bsave:true
	}
)
</script>
		
		