<?php
/**
*@package pXP
*@file gen-Bonus.php
*@author  (admin)
*@date 11-04-2015 16:33:17
*@description Archivo con la interfaz de usuario que permite la ejecucion de todas las funcionalidades del sistema
*/

header("content-type: text/javascript; charset=UTF-8");
?>
<script>
Phx.vista.Bonus=Ext.extend(Phx.gridInterfaz,{

	constructor:function(config){

		this.initButtons=[this.cmpFieldDirectos];
		this.initButtons=[this.cmpFieldInDirectos];

		this.initButtons=[this.cmbGestion, this.cmbPeriodo];


		this.maestro=config.maestro;
    	//llama al constructor de la clase padre
		Phx.vista.Bonus.superclass.constructor.call(this,config);

		console.log(this.id_afiliado);
		this.init();
		this.loadValoresIniciales();




		this.cmbGestion.on('select', function(combo, record, index){
			this.tmpGestion = record.data.gestion;
			this.cmbPeriodo.enable();
			this.cmbPeriodo.reset();
			this.store.removeAll();
			this.cmbPeriodo.store.baseParams = Ext.apply(this.cmbPeriodo.store.baseParams, {id_gestion: this.cmbGestion.getValue()});
			this.cmbPeriodo.modificado = true;

			/*anio = record.data.gestion;
			 var fecha = new Date(mes+'/'+dia+'/'+anio);

			 this.getComponente('fecha_documento').setMinValue(fecha);
			 this.getComponente('fecha_documento').setMaxValue(fecha_fin);*/


		},this);


		this.cmbPeriodo.on('select', function( combo, record, index){
			this.tmpPeriodo = record.data.periodo;
			this.capturaFiltros();

			/* mes = record.data.periodo;
			 console.log(mes);
			 var fecha = new Date(mes+'/'+dia+'/'+anio);
			 fecha_fin = new Date(mes+'/'+dia+'/'+anio);
			 fecha_fin.setMonth(fecha_fin.getMonth() + 1);
			 fecha_fin.setDate(fecha_fin.getDate() - 1);
			 console.log(fecha);
			 console.log(fecha_fin);

			 this.getComponente('fecha_documento').setMinValue(fecha);
			 this.getComponente('fecha_documento').setMaxValue(fecha_fin);*/



		},this);





			Ext.Ajax.request({
			url: '../../sis_afiliacion/control/Bonus/verSeDebe',
			params: {'id_patrocinador': ''+this.id_afiliado+'','start':'0','limit':'1000',"sort":"id_patrocinador","dir":"ASC","tipo":"directo"},
			success: this.verDebeDirectos,
			failure: this.conexionFailure,
			timeout: this.timeout,
			scope: this
		});

		/*Ext.Ajax.request({
			url: '../../sis_afiliacion/control/Bonus/verSeDebe',
			params: {'id_patrocinador': ''+this.id_afiliado+'','start':'0','limit':'1000',"sort":"id_patrocinador","dir":"ASC","tipo":"directo"},
			success: this.verInDebeDirectos,
			failure: this.conexionFailure,
			timeout: this.timeout,
			scope: this
		});*/


		this.addButton('bonus',{argument: {imprimir: 'pagar'},text:'<i class="fa fa-thumbs-o-up fa-2x"></i> pagar',/*iconCls:'' ,*/disabled:false,handler:this.pagar});


		this.load({params:{start:0, limit:this.tam_pag, id_patrocinador:this.id_afiliado,estado_pago:'debe'}})
	},




		loadValoresIniciales:function(){

			console.log('llega')

		},




		capturaFiltros:function(combo, record, index){
			this.desbloquearOrdenamientoGrid();
			if(this.validarFiltros()){
				this.store.baseParams.id_gestion = this.cmbGestion.getValue();
				this.store.baseParams.id_periodo = this.cmbPeriodo.getValue();
				this.store.baseParams.id_patrocinador = this.maestro.id_afiliado;
				this.load();
			}

		},

		validarFiltros:function(){
			if(this.cmbGestion.validate() && this.cmbPeriodo.validate()){
				return true;
			}
			else{
				return false;
			}
		},
		onButtonAct:function(){
			if(!this.validarFiltros()){
				alert('Especifique los filtros antes')
			}
		},


		cmbGestion: new Ext.form.ComboBox({
			fieldLabel: 'Gestion',
			allowBlank: false,
			emptyText:'Gestion...',
			blankText: 'Año',
			store:new Ext.data.JsonStore(
				{
					url: '../../sis_parametros/control/Gestion/listarGestion',
					id: 'id_gestion',
					root: 'datos',
					sortInfo:{
						field: 'gestion',
						direction: 'DESC'
					},
					totalProperty: 'total',
					fields: ['id_gestion','gestion'],
					// turn on remote sorting
					remoteSort: true,
					baseParams:{par_filtro:'gestion'}
				}),
			valueField: 'id_gestion',
			triggerAction: 'all',
			displayField: 'gestion',
			hiddenName: 'id_gestion',
			mode:'remote',
			pageSize:50,
			queryDelay:500,
			listWidth:'280',
			width:80
		}),


		cmbPeriodo: new Ext.form.ComboBox({
			fieldLabel: 'Periodo',
			allowBlank: false,
			blankText : 'Mes',
			emptyText:'Periodo...',
			store:new Ext.data.JsonStore(
				{
					url: '../../sis_parametros/control/Periodo/listarPeriodo',
					id: 'id_periodo',
					root: 'datos',
					sortInfo:{
						field: 'periodo',
						direction: 'ASC'
					},
					totalProperty: 'total',
					fields: ['id_periodo','periodo','id_gestion'],
					// turn on remote sorting
					remoteSort: true,
					baseParams:{par_filtro:'gestion'}
				}),
			valueField: 'id_periodo',
			triggerAction: 'all',
			displayField: 'periodo',
			hiddenName: 'id_periodo',
			mode:'remote',
			pageSize:50,
			disabled: true,
			queryDelay:500,
			listWidth:'280',
			width:80
		}),

			
	Atributos:[
		{
			//configuracion del componente
			config:{
					labelSeparator:'',
					inputType:'hidden',
					name: 'id_bonus'
			},
			type:'Field',
			form:true 
		},
		{
			//configuracion del componente
			config:{
					labelSeparator:'',
					inputType:'hidden',
					name: 'id_afiliado'
			},
			type:'Field',
			form:true
		},
		{
			//configuracion del componente
			config:{
					labelSeparator:'',
					inputType:'hidden',
					name: 'id_patrocinador'
			},
			type:'Field',
			form:true
		},
		{
			config:{
				name: 'codigo_patrocinador',
				fieldLabel: 'Patrocinador',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:255
			},
				type:'TextField',
				filters:{pfiltro:'patro.codigo',type:'numeric'},
				id_grupo:1,
				grid:true,
				form:false,
            bottom_filter: true,

        },
		{
			config:{
				name: 'estado_pago',
				fieldLabel: 'Estado Pago',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:255
			},
				type:'TextField',
				filters:{pfiltro:'bon.estado_pago',type:'numeric'},
				id_grupo:1,
				grid:true,
				form:false,
            bottom_filter: true,

        },
		{
			config:{
				name: 'codigo_afiliado',
				fieldLabel: 'Afiliado',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:255
			},
				type:'TextField',
				filters:{pfiltro:'afil.codigo',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'desc_persona',
				fieldLabel: 'Persona',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:255
			},
				type:'TextField',
				filters:{pfiltro:'vp.nombre',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'tipo',
				fieldLabel: 'tipo',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:255
			},
				type:'TextField',
				filters:{pfiltro:'bon.tipo',type:'string'},
				id_grupo:1,
				grid:true,
				form:true
		},
		{
			config:{
				name: 'estado_reg',
				fieldLabel: 'Estado Reg.',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:10
			},
				type:'TextField',
				filters:{pfiltro:'bon.estado_reg',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},

		{
			config:{
				name: 'monto',
				fieldLabel: 'monto',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:-5
			},
				type:'NumberField',
				filters:{pfiltro:'bon.monto',type:'numeric'},
				id_grupo:1,
				grid:true,
				form:true
		},
		{
			config:{
				name: 'nivel_from',
				fieldLabel: 'Nivel',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:-5
			},
				type:'NumberField',
				filters:{pfiltro:'bon.nivel_from',type:'numeric'},
				id_grupo:1,
				grid:true,
				form:true
		},
		{
			config:{
				name: 'codigo_afiliado_from',
				fieldLabel: 'Codigo nivel',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:-5
			},
				type:'TextField',
				filters:{pfiltro:'bon.codigo_afiliado_from',type:'numeric'},
				id_grupo:1,
				grid:true,
				form:true
		},
	
		{
			config:{
				name: 'usr_reg',
				fieldLabel: 'Creado por',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'usu1.cuenta',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'usuario_ai',
				fieldLabel: 'Funcionaro AI',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:300
			},
				type:'TextField',
				filters:{pfiltro:'bon.usuario_ai',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'fecha_reg',
				fieldLabel: 'Fecha creación',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
							format: 'd/m/Y', 
							renderer:function (value,p,record){return value?value.dateFormat('d/m/Y H:i:s'):''}
			},
				type:'DateField',
				filters:{pfiltro:'bon.fecha_reg',type:'date'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'id_usuario_ai',
				fieldLabel: 'Fecha creación',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'bon.id_usuario_ai',type:'numeric'},
				id_grupo:1,
				grid:false,
				form:false
		},
		{
			config:{
				name: 'fecha_mod',
				fieldLabel: 'Fecha Modif.',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
							format: 'd/m/Y', 
							renderer:function (value,p,record){return value?value.dateFormat('d/m/Y H:i:s'):''}
			},
				type:'DateField',
				filters:{pfiltro:'bon.fecha_mod',type:'date'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'usr_mod',
				fieldLabel: 'Modificado por',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'usu2.cuenta',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		}
	],
	tam_pag:50,	
	title:'bonus',
	ActSave:'../../sis_afiliacion/control/Bonus/insertarBonus',
	ActDel:'../../sis_afiliacion/control/Bonus/eliminarBonus',
	ActList:'../../sis_afiliacion/control/Bonus/listarBonus',
	id_store:'id_bonus',
	fields: [
		{name:'id_bonus', type: 'numeric'},
		{name:'tipo', type: 'string'},
		{name:'estado_reg', type: 'string'},
		{name:'id_afiliado', type: 'numeric'},
		{name:'monto', type: 'numeric'},
		{name:'id_patrocinador', type: 'numeric'},
		{name:'id_usuario_reg', type: 'numeric'},
		{name:'usuario_ai', type: 'string'},
		{name:'fecha_reg', type: 'date',dateFormat:'Y-m-d H:i:s.u'},
		{name:'id_usuario_ai', type: 'numeric'},
		{name:'fecha_mod', type: 'date',dateFormat:'Y-m-d H:i:s.u'},
		{name:'id_usuario_mod', type: 'numeric'},
		{name:'usr_reg', type: 'string'},
		{name:'usr_mod', type: 'string'},


		{name:'codigo_patrocinador', type: 'numeric'},
		{name:'codigo_afiliado', type: 'numeric'},
		{name:'desc_persona', type: 'string'},
		{name:'estado_pago', type: 'string'},
		{name:'nivel_from', type: 'numeric'},
		{name:'codigo_afiliado_from', type: 'string'},

	],
	sortInfo:{
		field: 'id_bonus',
		direction: 'ASC'
	},


	bdel:false,
	bsave:false,
	badd:false,
	bedit:false,
    preparaMenu: function (tb) {
        // llamada funcion clace padre
        Phx.vista.Bonus.superclass.preparaMenu.call(this, tb)
    },
    onButtonNew: function () {
        Phx.vista.Bonus.superclass.onButtonNew.call(this);
        this.getComponente('id_patrocinador').setValue(this.maestro.id_afiliado);
    },

    onReloadPage: function (m) {
        this.maestro = m;
        console.log(this.maestro);
        this.store.baseParams = {id_patrocinador: this.maestro.id_afiliado};

        if(this.validarFiltros()){
            this.store.baseParams.id_gestion = this.cmbGestion.getValue();
            this.store.baseParams.id_periodo = this.cmbPeriodo.getValue();
        }
        this.load({params: {start: 0, limit: 50}})
    },
	cmpFieldDirectos: new Ext.form.TextField({
	//name: 'directos',
		fieldLabel: 'directos',
	emptyText: 'User',
	allowBlank: false,
	anchor: '80%',
	gwidth: 100,
	maxLength:255
	}),
		cmpFieldInDirectos: new Ext.form.TextField({
			//name: 'directos',
			fieldLabel: 'Indirectos',
			emptyText: 'User',
			allowBlank: false,
			anchor: '80%',
			gwidth: 100,
			maxLength:255
		}),

		pagar:function(){
            var rec = this.sm.getSelected();
			console.log(this.id_afiliado);

			Ext.Ajax.request({
				url: '../../sis_afiliacion/control/Bonus/pagarBonus',
				params: {'id_bonus': rec.json.id_bonus},
				success: this.successSave,
				failure: this.conexionFailure,
				timeout: this.timeout,
				scope: this
			});

		},
		successExport: function (resp) {
			//Phx.CP.loadingHide();
			console.log('resp' , resp)

			//doc.write(texto);
			var objRes = Ext.util.JSON.decode(Ext.util.Format.trim(resp.responseText));

			console.log(objRes)
			//console.log(objRes.ROOT.datos[0].length)

				var texto = objRes;
				ifrm = document.createElement("IFRAME");
				ifrm.name = 'mifr1';
				ifrm.id = 'mifr1';
				document.body.appendChild(ifrm);
				var doc = window.frames['mifr1'].document;
				doc.open();
				doc.write(texto.datos);
				doc.close();

			//this.Cmp.liquidevolu.disable();
			//this.Cmp.liquidevolu.setValue('');
			//this.Cmp.boletos_id.disable();
			//this.Cmp.id_factura.disable();
			//this.megrid.disable();

		},
		/*verDebeDirectos : function (resp) {
			var respuesta = Ext.decode(resp.responseText);
			var sum = respuesta.datos[0].sum;
			this.cmpFieldDirectos.setValue("directos: "+sum);

		},
		verDebeInDirectos : function (resp) {
			var respuesta = Ext.decode(resp.responseText);
			var sum = respuesta.datos[0].sum;
			this.cmpFieldInDirectos.setValue("Indirectos: "+sum);

		},*/


	}
)
</script>
		
		