<?php
/**
 * @package pXP
 * @file gen-AfiliadoPagoPeriodo.php
 * @author  (admin)
 * @date 02-04-2016 16:22:34
 * @description Archivo con la interfaz de usuario que permite la ejecucion de todas las funcionalidades del sistema
 */

header("content-type: text/javascript; charset=UTF-8");
?>
<script>
    Phx.vista.AfiliadoPagoPeriodo = Ext.extend(Phx.gridInterfaz, {

            constructor: function (config) {

                this.initButtons = [this.cmbGestion, this.cmbPeriodo];


                this.maestro = config.maestro;

                this.win_reporte_pago = new Ext.Window(
                    {
                        layout: 'fit',
                        width: 500,
                        height: 250,
                        modal: true,
                        closeAction: 'hide',

                        items: new Ext.FormPanel({
                            labelWidth: 75, // label settings here cascade unless overridden

                            frame: true,
                            // title: 'Factura Manual Concepto',
                            bodyStyle: 'padding:5px 5px 0',
                            width: 339,
                            defaults: {width: 191},
                            // defaultType: 'textfield',

                            items: [this.cmbSucursal, this.cmbFecha, this.cmbFecha_fin],

                            buttons: [{
                                text: 'Save',
                                handler: this.submitVerReporteTotales,

                                scope: this
                            }, {
                                text: 'Cancel'
                            }]
                        })

                    });

                this.win_premiar = new Ext.Window(
                    {
                        layout: 'fit',
                        width: 500,
                        height: 250,
                        modal: true,
                        closeAction: 'hide',

                        items: new Ext.FormPanel({
                            labelWidth: 75, // label settings here cascade unless overridden

                            frame: true,
                            // title: 'Factura Manual Concepto',
                            bodyStyle: 'padding:5px 5px 0',
                            width: 339,
                            defaults: {width: 191},
                            // defaultType: 'textfield',

                            items: [this.cmbDescripcionPremiar],

                            buttons: [{
                                text: 'Save',
                                handler: this.submitPremiar,

                                scope: this
                            }, {
                                text: 'Cancel'
                            }]
                        })

                    });





                    //llama al constructor de la clase padre
                Phx.vista.AfiliadoPagoPeriodo.superclass.constructor.call(this, config);


                this.addButton('pagar', {
                    argument: {imprimir: 'pagar'},
                    text: '<i class="fa fa-thumbs-o-up fa-2x"></i> pagar', /*iconCls:'' ,*/
                    disabled: false,
                    handler: this.pagar
                });

                this.addButton('pagarBonusPaquete', {
                    argument: {imprimir: 'pagarBonusPaquete'},
                    text: '<i class="fa fa-thumbs-o-up fa-2x"></i> pagar Bonus Paquete', /*iconCls:'' ,*/
                    disabled: false,
                    handler: this.pagarBonusPaquete
                });

                //esto genera pagos para todos pero se cambio a crontab para ejecutr automtico cada 1 de cada mes
                /*this.addButton('generarPagos', {
                    argument: {imprimir: 'generarPagos'},
                    text: '<i class="fa fa-thumbs-o-up fa-2x"></i> generarPagos', /!*iconCls:'' ,*!/
                    disabled: true,
                    handler: this.generarPagos
                });*/


                this.addButton('Reporte Total Pago', {
                    argument: {imprimir: 'generar pago'},
                    text: '<i class="fa fa-thumbs-o-up fa-2x"></i> Reporte Total Pago', /*iconCls:'' ,*/
                    disabled: false,
                    handler: this.reporteTotalPago
                });
                this.addButton('Premiar', {
                    argument: {imprimir: 'Premiar'},
                    text: '<i class="fa fa-thumbs-o-up fa-2x"></i> Premiar', /*iconCls:'' ,*/
                    disabled: false,
                    handler: this.premiar
                });



                this.cmbGestion.on('select', function (combo, record, index) {
                    this.tmpGestion = record.data.gestion;
                    this.cmbPeriodo.enable();
                    this.cmbPeriodo.reset();
                    this.store.removeAll();
                    this.cmbPeriodo.store.baseParams = Ext.apply(this.cmbPeriodo.store.baseParams, {id_gestion: this.cmbGestion.getValue()});
                    this.cmbPeriodo.modificado = true;

                    /*anio = record.data.gestion;
                     var fecha = new Date(mes+'/'+dia+'/'+anio);

                     this.getComponente('fecha_documento').setMinValue(fecha);
                     this.getComponente('fecha_documento').setMaxValue(fecha_fin);*/

                    this.getBoton('generarPagos').disable();

                }, this);


                this.cmbPeriodo.on('select', function (combo, record, index) {
                    this.tmpPeriodo = record.data.periodo;
                    this.capturaFiltros();

                    this.getBoton('generarPagos').enable();
                    /* mes = record.data.periodo;
                     console.log(mes);
                     var fecha = new Date(mes+'/'+dia+'/'+anio);
                     fecha_fin = new Date(mes+'/'+dia+'/'+anio);
                     fecha_fin.setMonth(fecha_fin.getMonth() + 1);
                     fecha_fin.setDate(fecha_fin.getDate() - 1);
                     console.log(fecha);
                     console.log(fecha_fin);

                     this.getComponente('fecha_documento').setMinValue(fecha);
                     this.getComponente('fecha_documento').setMaxValue(fecha_fin);*/


                }, this);


                this.init();
                /*this.load({params:{start:0, limit:this.tam_pag}})*/
            },
            cmbDescripcionPremiar: new Ext.form.TextField({

                enableKeyEvents: true,
                name: 'descripcion_premiar',
                id: 'input_descripcion_premiar',
                allowBlank: false,
                fieldLabel: 'Descripcion Premiar',
            }),

            cmbFecha: new Ext.form.DateField({
                name: 'fecha',
                fieldLabel: 'Fecha',
                allowBlank: false,
                disabled: false,
                width: 105,
                format: 'd/m/Y'

            }),
            cmbFecha_fin: new Ext.form.DateField({
                name: 'fecha_fim',
                fieldLabel: 'Fecha fin',
                allowBlank: true,
                disabled: false,
                width: 105,
                format: 'd/m/Y'

            }),

            cmbSucursal: new Ext.form.ComboBox({
                fieldLabel: 'Sucursal',
                allowBlank: false,
                blankText: 'suc',
                emptyText: 'Sucursal...',
                store: new Ext.data.JsonStore(
                    {
                        url: '../../sis_sucursal/control/Sucursal/listarSucursal',
                        id: 'id_sucursal',
                        root: 'datos',
                        sortInfo: {
                            field: 'nombre',
                            direction: 'ASC'
                        },
                        totalProperty: 'total',
                        fields: ['id_sucursal', 'nombre'],
                        // turn on remote sorting
                        remoteSort: true,
                        baseParams: {par_filtro: 'nombre'}
                    }),
                valueField: 'id_sucursal',
                triggerAction: 'all',
                displayField: 'nombre',
                hiddenName: 'id_sucursal',
                mode: 'remote',
                pageSize: 50,
                disabled: false,
                queryDelay: 500,
                listWidth: '280',
                width: 80
            }),

            cmbGestion: new Ext.form.ComboBox({
                fieldLabel: 'Gestion',
                allowBlank: false,
                emptyText: 'Gestion...',
                blankText: 'Año',
                store: new Ext.data.JsonStore(
                    {
                        url: '../../sis_parametros/control/Gestion/listarGestion',
                        id: 'id_gestion',
                        root: 'datos',
                        sortInfo: {
                            field: 'gestion',
                            direction: 'DESC'
                        },
                        totalProperty: 'total',
                        fields: ['id_gestion', 'gestion'],
                        // turn on remote sorting
                        remoteSort: true,
                        baseParams: {par_filtro: 'gestion'}
                    }),
                valueField: 'id_gestion',
                triggerAction: 'all',
                displayField: 'gestion',
                hiddenName: 'id_gestion',
                mode: 'remote',
                pageSize: 50,
                queryDelay: 500,
                listWidth: '280',
                width: 80
            }),


            cmbPeriodo: new Ext.form.ComboBox({
                fieldLabel: 'Periodo',
                allowBlank: false,
                blankText: 'Mes',
                emptyText: 'Periodo...',
                store: new Ext.data.JsonStore(
                    {
                        url: '../../sis_parametros/control/Periodo/listarPeriodo',
                        id: 'id_periodo',
                        root: 'datos',
                        sortInfo: {
                            field: 'periodo',
                            direction: 'ASC'
                        },
                        totalProperty: 'total',
                        fields: ['id_periodo', 'periodo', 'id_gestion'],
                        // turn on remote sorting
                        remoteSort: true,
                        baseParams: {par_filtro: 'gestion'}
                    }),
                valueField: 'id_periodo',
                triggerAction: 'all',
                displayField: 'periodo',
                hiddenName: 'id_periodo',
                mode: 'remote',
                pageSize: 50,
                disabled: true,
                queryDelay: 500,
                listWidth: '280',
                width: 80
            }),

            Atributos: [
                {
                    //configuracion del componente
                    config: {
                        labelSeparator: '',
                        inputType: 'hidden',
                        name: 'id_afiliado_pago_periodo'
                    },
                    type: 'Field',
                    form: true
                },
                {
                    //configuracion del componente
                    config: {
                        labelSeparator: '',
                        inputType: 'hidden',
                        name: 'id_periodo'
                    },
                    type: 'Field',
                    form: true
                },
                {
                    //configuracion del componente
                    config: {
                        labelSeparator: '',
                        inputType: 'hidden',
                        name: 'id_afiliado'
                    },
                    type: 'Field',
                    form: true
                },


                {
                    config: {
                        name: 'nombre_completo2',
                        fieldLabel: 'Nombre Completo',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'person.nombre_completo2', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },

                {
                    config: {
                        name: 'codigo',
                        fieldLabel: 'codigo',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 655362
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afil.codigo', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },


                {
                    config: {
                        name: 'gestion',
                        fieldLabel: 'gestion',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 655362
                    },
                    type: 'NumberField',
                    filters: {pfiltro: 'ges.gestion', type: 'numeric'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },

                {
                    config: {
                        name: 'periodo',
                        fieldLabel: 'Periodo',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'per.periodo', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },


                {
                    config: {
                        name: 'habilitado',
                        fieldLabel: 'habilitado',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afpape.habilitado', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },
                {
                    config: {
                        name: 'premio',
                        fieldLabel: 'premio',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'tpa.premio', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },

                {
                    config: {
                        name: 'afiliados_registrados',
                        fieldLabel: 'afiliados registrados',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afpape.afiliados_registrados', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },


                {
                    config: {
                        name: 'kit_negocios',
                        fieldLabel: 'Kit de Negocios',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 655362
                    },
                    type: 'NumberField',
                    filters: {pfiltro: 'afpape.kit_negocios', type: 'numeric'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },
                {
                    config: {
                        name: 'pago_bonus_registro',
                        fieldLabel: 'Bonus Registro',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 655362
                    },
                    type: 'NumberField',
                    filters: {pfiltro: 'afpape.pago_bonus_registro', type: 'numeric'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },

                {
                    config: {
                        name: 'pago_de_red',
                        fieldLabel: 'Pago de Red',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 655362
                    },
                    type: 'NumberField',
                    filters: {pfiltro: 'afpape.pago_de_red', type: 'numeric'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },


                {
                    config: {
                        name: 'pago_total',
                        fieldLabel: 'pago_total',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 655362
                    },
                    type: 'NumberField',
                    filters: {pfiltro: 'afpape.pago_total', type: 'numeric'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },

                {
                    config: {
                        name: 'sube_nivel',
                        fieldLabel: 'Sube Nivel',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 655362
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afpape.sube_nivel', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },
                {
                    config: {
                        name: 'rango_actual',
                        fieldLabel: 'Rango Actual',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 655362
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afpape.rango_actual', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },
                {
                    config: {
                        name: 'rango_siguiente',
                        fieldLabel: 'Rango Siguiente',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 655362
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afpape.rango_siguiente', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },


                {
                    config: {
                        name: 'estado',
                        fieldLabel: 'estado',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afpape.estado', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },
                {
                    config: {
                        name: 'cv_propio',
                        fieldLabel: 'cv_propio',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 655362
                    },
                    type: 'NumberField',
                    filters: {pfiltro: 'afpape.cv_propio', type: 'numeric'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },

                {
                    config: {
                        name: 'descripcion_pago_red',
                        fieldLabel: 'descripcion_pago_red',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afpape.descripcion_pago_red', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },

                {
                    config: {
                        name: 'pv_propio',
                        fieldLabel: 'pv_propio',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 655362
                    },
                    type: 'NumberField',
                    filters: {pfiltro: 'afpape.pv_propio', type: 'numeric'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },


                {
                    config: {
                        name: 'estado_reg',
                        fieldLabel: 'Estado Reg.',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 10
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afpape.estado_reg', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'id_usuario_ai',
                        fieldLabel: '',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 4
                    },
                    type: 'Field',
                    filters: {pfiltro: 'afpape.id_usuario_ai', type: 'numeric'},
                    id_grupo: 1,
                    grid: false,
                    form: false
                },
                {
                    config: {
                        name: 'fecha_reg',
                        fieldLabel: 'Fecha creación',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        format: 'd/m/Y',
                        renderer: function (value, p, record) {
                            return value ? value.dateFormat('d/m/Y H:i:s') : ''
                        }
                    },
                    type: 'DateField',
                    filters: {pfiltro: 'afpape.fecha_reg', type: 'date'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'usuario_ai',
                        fieldLabel: 'Funcionaro AI',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 300
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afpape.usuario_ai', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'usr_reg',
                        fieldLabel: 'Creado por',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 4
                    },
                    type: 'Field',
                    filters: {pfiltro: 'usu1.cuenta', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'fecha_mod',
                        fieldLabel: 'Fecha Modif.',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        format: 'd/m/Y',
                        renderer: function (value, p, record) {
                            return value ? value.dateFormat('d/m/Y H:i:s') : ''
                        }
                    },
                    type: 'DateField',
                    filters: {pfiltro: 'afpape.fecha_mod', type: 'date'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'usr_mod',
                        fieldLabel: 'Modificado por',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 4
                    },
                    type: 'Field',
                    filters: {pfiltro: 'usu2.cuenta', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                }
            ],
            tam_pag: 50,
            title: 'afiliado pago periodo',
            ActSave: '../../sis_afiliacion/control/AfiliadoPagoPeriodo/insertarAfiliadoPagoPeriodo',
            ActDel: '../../sis_afiliacion/control/AfiliadoPagoPeriodo/eliminarAfiliadoPagoPeriodo',
            ActList: '../../sis_afiliacion/control/AfiliadoPagoPeriodo/listarAfiliadoPagoPeriodo',
            id_store: 'id_afiliado_pago_periodo',
            fields: [
                {name: 'id_afiliado_pago_periodo', type: 'numeric'},
                {name: 'cv_hijos', type: 'numeric'},
                {name: 'id_afiliado', type: 'numeric'},
                {name: 'pago_bonus_registro', type: 'numeric'},
                {name: 'habilitado', type: 'string'},
                {name: 'pago_de_red', type: 'numeric'},
                {name: 'estado', type: 'string'},
                {name: 'cv_propio', type: 'numeric'},
                {name: 'id_periodo', type: 'numeric'},
                {name: 'pago_total', type: 'numeric'},
                {name: 'descripcion_pago_red', type: 'string'},
                {name: 'pv_hijos', type: 'numeric'},
                {name: 'pv_propio', type: 'numeric'},
                {name: 'estado_reg', type: 'string'},
                {name: 'id_usuario_ai', type: 'numeric'},
                {name: 'fecha_reg', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'usuario_ai', type: 'string'},
                {name: 'id_usuario_reg', type: 'numeric'},
                {name: 'fecha_mod', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'id_usuario_mod', type: 'numeric'},
                {name: 'usr_reg', type: 'string'},
                {name: 'usr_mod', type: 'string'},
                'codigo', 'periodo', 'gestion', 'cv_nietos', 'pv_nietos',
                'nombre_completo2', 'sube_nivel','rango_actual', 'rango_siguiente', 'afiliados_registrados',
                'premio','kit_negocios'

            ],
            sortInfo: {
                field: 'id_afiliado_pago_periodo',
                direction: 'DESC'
            },
            bdel: true,
            bsave: true,
            bnew: false,
            bedit: false,
            bdel: false,
            pagar: function () {
                var rec = this.sm.getSelected();

                if(confirm(`estas seguro de pagar este afiliado ${rec.data.codigo}????`)) {

                    Ext.Ajax.request({
                        url: '../../sis_afiliacion/control/AfiliadoPagoPeriodo/PagoAfiliado',
                        params: {id_afiliado_pago_periodo: rec.data.id_afiliado_pago_periodo},
                        success: this.successPagar,
                        failure: this.conexionFailure,
                        timeout: this.timeout,
                        scope: this
                    });
                }

            },
            pagarBonusPaquete: function () {
                var rec = this.sm.getSelected();

                if(confirm(`estas seguro de pagar sus bonus de este afiliado ${rec.data.codigo}????`)) {

                    Ext.Ajax.request({
                        url: '../../sis_afiliacion/control/AfiliadoPagoPeriodo/PagarBonusPaquete',
                        params: {id_afiliado_pago_periodo: rec.data.id_afiliado_pago_periodo},
                        success: this.successPagar,
                        failure: this.conexionFailure,
                        timeout: this.timeout,
                        scope: this
                    });
                }

            },
            successPagar: function () {
                this.load();
            },

            capturaFiltros: function (combo, record, index) {
                this.desbloquearOrdenamientoGrid();


                this.store.baseParams.id_gestion = this.cmbGestion.getValue();
                this.store.baseParams.id_periodo = this.cmbPeriodo.getValue();
                this.load();

            },
            generarPagos: function () {

                Phx.CP.loadingShow();

                Ext.Ajax.request({
                    url: '../../sis_afiliacion/control/AfiliadoPagoPeriodo/generarPagos',
                    params: {id_periodo: this.cmbPeriodo.getValue()},
                    success: this.successPagar,
                    failure: this.conexionFailure,
                    timeout: this.timeout,
                    scope: this
                });

            },
            reporteTotalPago: function () {

                this.win_reporte_pago.show();
            },
            successPremiar: function () {
                Phx.CP.loadingHide();
                this.win_premiar.hide();

                this.load();
            },
            premiar: function () {
                var rec = this.sm.getSelected();

                if (!rec.data.premio) {
                    this.win_premiar.show();

                } else {
                    alert('error puede que no seleccionaste ningun afiliado o ya tenga un premio en este periodo')
                }
            },
            submitPremiar: function () {

                var rec = this.sm.getSelected();
                if (this.cmbDescripcionPremiar.validate() && this.cmbPeriodo.validate() && rec.data.id_afiliado) {

                    Phx.CP.loadingShow();




                    Ext.Ajax.request({
                        url: '../../sis_afiliacion/control/AfiliadoPagoPeriodo/premiar',
                        params: {descripcion: this.cmbDescripcionPremiar.getValue(), id_periodo: this.cmbPeriodo.getValue(), id_afiliado: rec.data.id_afiliado},
                        success: this.successPremiar,
                        failure: this.conexionFailure,
                        timeout: this.timeout,
                        scope: this
                    });



                }


            },
            submitVerReporteTotales: function () {


                if (this.cmbGestion.validate() && this.cmbPeriodo.validate() && this.cmbSucursal.validate()) {

                    Phx.CP.loadingShow();

                    var id_periodo = this.cmbPeriodo.getValue();
                    var id_sucursal = this.cmbSucursal.getValue();
                    var fecha = this.cmbFecha.getValue();
                    var fechaFin = this.cmbFecha_fin.getValue();

                    var rec = this.sm.getSelected();

                    Ext.Ajax.request({
                        url: '../../sis_afiliacion/control/AfiliadoPagoPeriodo/listarSumPagosDetalle',
                        params: {
                            id_sucursal: id_sucursal,
                            id_periodo: id_periodo,
                            fecha: fecha,
                            fecha_fin:fechaFin,

                        },
                        success: this.successOpenReporte,
                        failure: this.conexionFailure,
                        timeout: this.timeout,
                        scope: this
                    });


                }


            },
            successOpenReporte: function (resp) {

                console.log(resp)
                var objRes = Ext.util.JSON.decode(Ext.util.Format.trim(resp.responseText));

                var i = 0;

                var texto = objRes.datos;



                var wnd = window.open("about:blank", "", "_blank");
                wnd.document.write(texto);


                Phx.CP.loadingHide();
            }
        }
    )
</script>
		
		
