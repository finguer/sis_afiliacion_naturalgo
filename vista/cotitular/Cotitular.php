<?php
/**
*@package pXP
*@file gen-Cotitular.php
*@author  (admin)
*@date 02-04-2015 01:19:24
*@description Archivo con la interfaz de usuario que permite la ejecucion de todas las funcionalidades del sistema
*/

header("content-type: text/javascript; charset=UTF-8");
?>
<script>
Phx.vista.Cotitular=Ext.extend(Phx.gridInterfaz,{

	constructor:function(config){
		this.maestro=config.maestro;
    	//llama al constructor de la clase padre
		Phx.vista.Cotitular.superclass.constructor.call(this,config);
		this.init();
		//this.load({params:{start:0, limit:this.tam_pag}})
	},
			
	Atributos:[
		{
			//configuracion del componente
			config:{
					labelSeparator:'',
					inputType:'hidden',
					name: 'id_cotitular'
			},
			type:'Field',
			form:true 
		},
		{
			//configuracion del componente
			config:{
					labelSeparator:'',
					inputType:'hidden',
					name: 'id_persona'
			},
			type:'Field',
			form:true
		},
		{
			//configuracion del componente
			config:{
					labelSeparator:'',
					inputType:'hidden',
					name: 'id_afiliado'
			},
			type:'Field',
			form:true
		},
		{
			config:{
				name: 'nombre',
				fieldLabel: 'nombre',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:250
			},
				type:'TextField',
				filters:{pfiltro:'titu.nombre',type:'string'},
				id_grupo:1,
				grid:true,
				form:true
		},






		{
			config:{
				name: 'domicilio',
				fieldLabel: 'domicilio',
				allowBlank: false,
				anchor: '80%',
				gwidth: 100,
				maxLength:655362
			},
				type:'TextField',
				filters:{pfiltro:'titu.domicilio',type:'string'},
				id_grupo:1,
				grid:true,
				form:true
		},
		{
			config:{
				name: 'apellido1',
				fieldLabel: 'apellido1',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:255
			},
				type:'TextField',
				filters:{pfiltro:'titu.apellido1',type:'string'},
				id_grupo:1,
				grid:true,
				form:true
		},
		{
			config:{
				name: 'apellido2',
				fieldLabel: 'apellido2',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:255
			},
				type:'TextField',
				filters:{pfiltro:'titu.apellido2',type:'string'},
				id_grupo:1,
				grid:true,
				form:true
		},
		{
			config:{
				name: 'relacion',
				fieldLabel: 'relacion',
				allowBlank: false,
				anchor: '80%',
				gwidth: 100,
				maxLength:655362
			},
				type:'TextField',
				filters:{pfiltro:'titu.relacion',type:'string'},
				id_grupo:1,
				grid:true,
				form:true
		},
		{
			config:{
				name: 'estado_reg',
				fieldLabel: 'Estado Reg.',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:10
			},
				type:'TextField',
				filters:{pfiltro:'titu.estado_reg',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'telefono',
				fieldLabel: 'telefono',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:12
			},
				type:'NumberField',
				filters:{pfiltro:'titu.telefono',type:'numeric'},
				id_grupo:1,
				grid:true,
				form:true
		},
		{
			config:{
				name: 'id_usuario_ai',
				fieldLabel: '',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'titu.id_usuario_ai',type:'numeric'},
				id_grupo:1,
				grid:false,
				form:false
		},
		{
			config:{
				name: 'usr_reg',
				fieldLabel: 'Creado por',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'usu1.cuenta',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'usuario_ai',
				fieldLabel: 'Funcionaro AI',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:300
			},
				type:'TextField',
				filters:{pfiltro:'titu.usuario_ai',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'fecha_reg',
				fieldLabel: 'Fecha creación',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
							format: 'd/m/Y', 
							renderer:function (value,p,record){return value?value.dateFormat('d/m/Y H:i:s'):''}
			},
				type:'DateField',
				filters:{pfiltro:'titu.fecha_reg',type:'date'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'fecha_mod',
				fieldLabel: 'Fecha Modif.',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
							format: 'd/m/Y', 
							renderer:function (value,p,record){return value?value.dateFormat('d/m/Y H:i:s'):''}
			},
				type:'DateField',
				filters:{pfiltro:'titu.fecha_mod',type:'date'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'usr_mod',
				fieldLabel: 'Modificado por',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'usu2.cuenta',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		}
	],
	tam_pag:50,	
	title:'Co-Titular',
	ActSave:'../../sis_afiliacion/control/Cotitular/insertarCotitular',
	ActDel:'../../sis_afiliacion/control/Cotitular/eliminarCotitular',
	ActList:'../../sis_afiliacion/control/Cotitular/listarCotitular',
	id_store:'id_cotitular',
	fields: [
		{name:'id_cotitular', type: 'numeric'},
		{name:'nombre', type: 'string'},
		{name:'id_afiliado', type: 'numeric'},
		{name:'domicilio', type: 'string'},
		{name:'apellido1', type: 'string'},
		{name:'apellido2', type: 'string'},
		{name:'relacion', type: 'string'},
		{name:'estado_reg', type: 'string'},
		{name:'telefono', type: 'numeric'},
		{name:'id_usuario_ai', type: 'numeric'},
		{name:'id_usuario_reg', type: 'numeric'},
		{name:'usuario_ai', type: 'string'},
		{name:'fecha_reg', type: 'date',dateFormat:'Y-m-d H:i:s.u'},
		{name:'fecha_mod', type: 'date',dateFormat:'Y-m-d H:i:s.u'},
		{name:'id_usuario_mod', type: 'numeric'},
		{name:'usr_reg', type: 'string'},
		{name:'usr_mod', type: 'string'},
		{name:'desc_person', type: 'string'},

	],
	sortInfo:{
		field: 'id_cotitular',
		direction: 'ASC'
	},
	bdel:true,
	bsave:true,
		preparaMenu:function(tb){
			// llamada funcion clace padre
			Phx.vista.Cotitular.superclass.preparaMenu.call(this,tb)
		},
		onButtonNew:function(){
			Phx.vista.Cotitular.superclass.onButtonNew.call(this);
			this.getComponente('id_afiliado').setValue(this.maestro.id_afiliado);
		},
		onReloadPage:function(m){
			this.maestro=m;
			this.store.baseParams={id_afiliado:this.maestro.id_afiliado};
			this.load({params:{start:0, limit:50}})
		}
	}
)
</script>
		
		