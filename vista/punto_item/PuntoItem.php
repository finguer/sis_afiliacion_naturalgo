<?php
/**
 * @package pXP
 * @file gen-PuntoItem.php
 * @author  (admin)
 * @date 18-12-2015 23:29:48
 * @description Archivo con la interfaz de usuario que permite la ejecucion de todas las funcionalidades del sistema
 */

header("content-type: text/javascript; charset=UTF-8");
?>
<script>
    Phx.vista.PuntoItem = Ext.extend(Phx.gridInterfaz, {

            constructor: function (config) {
                this.maestro = config.maestro;
                //llama al constructor de la clase padre
                Phx.vista.PuntoItem.superclass.constructor.call(this, config);
                this.init();
                this.iniciarEventos();
                this.load({params: {start: 0, limit: this.tam_pag}})
            },
            iniciarEventos: function () {

                this.Cmp.tipo.on('select',function(){


                    this.Cmp.id_item.modificado = true;
                    this.Cmp.id_set.modificado = true;
                    this.Cmp.id_set.setValue();
                    this.Cmp.id_item.setValue();
                    console.log('sad',this)

                    if(this.Cmp.tipo.getValue() == 'SET'){
                        this.Cmp.id_item.hide();
                        this.Cmp.id_set.show();

                    }else{
                        this.Cmp.id_set.hide();
                        this.Cmp.id_item.show();
                    }

                },this);

            },

            Atributos: [
                {
                    //configuracion del componente
                    config: {
                        labelSeparator: '',
                        inputType: 'hidden',
                        name: 'id_punto_item'
                    },
                    type: 'Field',
                    form: true
                },
                {
                    config: {
                        name: 'tipo',
                        fieldLabel: 'Tipo',
                        allowBlank: true,
                        emptyText: 'Tipo...',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: ['SET', 'ITEM'],
                        width: 200
                    },
                    type: 'ComboBox',
                    id_grupo: 1,
                    form: true,
                    grid:true
                },

                {
                    config: {
                        name: 'bonus_patrocinador',
                        fieldLabel: 'Bonus Patrocinador',
                        allowBlank: true,
                        emptyText: 'bonus_patrocinador...',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: ['SI', 'NO'],
                        width: 200
                    },
                    type: 'ComboBox',
                    id_grupo: 1,
                    form: true,
                    grid:true
                },

                {
                    config: {
                        name: 'pv',
                        fieldLabel: 'pv',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                    },
                    type: 'NumberField',
                    filters: {pfiltro: 'punti.pv', type: 'numeric'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },
                {
                    config: {
                        name: 'estado_reg',
                        fieldLabel: 'Estado Reg.',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 10
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'punti.estado_reg', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'cv',
                        fieldLabel: 'cv',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                    },
                    type: 'NumberField',
                    filters: {pfiltro: 'punti.cv', type: 'numeric'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },
                {
                    config: {
                        name: 'id_set',
                        fieldLabel: 'id_set',
                        allowBlank: true,
                        emptyText: 'Elija una opción...',
                        store: new Ext.data.JsonStore({
                            url: '../../sis_venta/control/SetItem/listarSetItem',
                            id: 'id_set_item',
                            root: 'datos',
                            sortInfo: {
                                field: 'nombre',
                                direction: 'ASC'
                            },
                            totalProperty: 'total',
                            fields: ['id_set_item', 'nombre', 'codigo'],
                            remoteSort: true,
                            baseParams: {par_filtro: 'movtip.nombre#movtip.codigo'}
                        }),
                        valueField: 'id_set_item',
                        displayField: 'nombre',
                        gdisplayField: 'desc_set',
                        hiddenName: 'id_set_item',
                        forceSelection: true,
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'remote',
                        pageSize: 15,
                        queryDelay: 1000,
                        anchor: '100%',
                        gwidth: 150,
                        minChars: 2,
                        renderer: function (value, p, record) {
                            return String.format('{0}', record.data['desc_set']);
                        }
                    },
                    type: 'ComboBox',
                    id_grupo: 0,
                    filters: {pfiltro: 'movtip.nombre', type: 'string'},
                    grid: true,
                    form: true
                },
                {
                    config: {
                        name: 'id_item',
                        fieldLabel: 'id_item',
                        allowBlank: true,
                        emptyText: 'Elija una opción...',
                        store: new Ext.data.JsonStore({
                            url: '../../sis_mercado/control/Item/ListarItem',
                            id: 'id_item',
                            root: 'datos',
                            sortInfo: {
                                field: 'nombre',
                                direction: 'ASC'
                            },
                            totalProperty: 'total',
                            fields: ['id_item', 'nombre', 'codigo'],
                            remoteSort: true,
                            baseParams: {par_filtro: 'movtip.nombre#movtip.codigo'}
                        }),
                        valueField: 'id_item',
                        displayField: 'nombre',
                        gdisplayField: 'desc_item',
                        hiddenName: 'id_item',
                        forceSelection: true,
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'remote',
                        pageSize: 15,
                        queryDelay: 1000,
                        anchor: '100%',
                        gwidth: 150,
                        minChars: 2,
                        renderer: function (value, p, record) {
                            return String.format('{0}', record.data['desc_item']);
                        }
                    },
                    type: 'ComboBox',
                    id_grupo: 0,
                    filters: {pfiltro: 'movtip.nombre', type: 'string'},
                    grid: true,
                    form: true
                },

                {
                    config: {
                        name: 'id_bonus_config',
                        fieldLabel: 'id_bonus_config',
                        allowBlank: true,
                        emptyText: 'Elija una opción...',
                        store: new Ext.data.JsonStore({
                            url: '../../sis_afiliacion/control/BonusConfig/listarBonusConfig',
                            id: 'id_bonus_config',
                            root: 'datos',
                            sortInfo: {
                                field: 'bonus',
                                direction: 'ASC'
                            },
                            totalProperty: 'total',
                            fields: ['id_bonus_config', 'bonus'],
                            remoteSort: true,
                            baseParams: {par_filtro: 'boncof.bonus'}
                        }),
                        valueField: 'id_bonus_config',
                        displayField: 'bonus',
                        gdisplayField: 'desc_bonus_config',
                        hiddenName: 'id_bonus_config',
                        forceSelection: true,
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'remote',
                        pageSize: 15,
                        queryDelay: 1000,
                        anchor: '100%',
                        gwidth: 150,
                        minChars: 2,
                        renderer: function (value, p, record) {
                            return String.format('{0}', record.data['desc_bonus_config']);
                        }
                    },
                    type: 'ComboBox',
                    id_grupo: 0,
                    filters: {pfiltro: 'movtip.nombre', type: 'string'},
                    grid: true,
                    form: true
                },
                {
                    config: {
                        name: 'usuario_ai',
                        fieldLabel: 'Funcionaro AI',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 300
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'punti.usuario_ai', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'fecha_reg',
                        fieldLabel: 'Fecha creación',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        format: 'd/m/Y',
                        renderer: function (value, p, record) {
                            return value ? value.dateFormat('d/m/Y H:i:s') : ''
                        }
                    },
                    type: 'DateField',
                    filters: {pfiltro: 'punti.fecha_reg', type: 'date'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'usr_reg',
                        fieldLabel: 'Creado por',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 4
                    },
                    type: 'Field',
                    filters: {pfiltro: 'usu1.cuenta', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'id_usuario_ai',
                        fieldLabel: 'Creado por',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 4
                    },
                    type: 'Field',
                    filters: {pfiltro: 'punti.id_usuario_ai', type: 'numeric'},
                    id_grupo: 1,
                    grid: false,
                    form: false
                },
                {
                    config: {
                        name: 'fecha_mod',
                        fieldLabel: 'Fecha Modif.',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        format: 'd/m/Y',
                        renderer: function (value, p, record) {
                            return value ? value.dateFormat('d/m/Y H:i:s') : ''
                        }
                    },
                    type: 'DateField',
                    filters: {pfiltro: 'punti.fecha_mod', type: 'date'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'usr_mod',
                        fieldLabel: 'Modificado por',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 4
                    },
                    type: 'Field',
                    filters: {pfiltro: 'usu2.cuenta', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                }
            ],
            tam_pag: 50,
            title: 'punto item',
            ActSave: '../../sis_afiliacion/control/PuntoItem/insertarPuntoItem',
            ActDel: '../../sis_afiliacion/control/PuntoItem/eliminarPuntoItem',
            ActList: '../../sis_afiliacion/control/PuntoItem/listarPuntoItem',
            id_store: 'id_punto_item',
            fields: [
                {name: 'id_punto_item', type: 'numeric'},
                {name: 'pv', type: 'numeric'},
                {name: 'estado_reg', type: 'string'},
                {name: 'cv', type: 'numeric'},
                {name: 'id_set', type: 'numeric'},
                {name: 'id_item', type: 'numeric'},
                {name: 'usuario_ai', type: 'string'},
                {name: 'fecha_reg', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'id_usuario_reg', type: 'numeric'},
                {name: 'id_usuario_ai', type: 'numeric'},
                {name: 'fecha_mod', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'id_usuario_mod', type: 'numeric'},
                {name: 'usr_reg', type: 'string'},
                {name: 'usr_mod', type: 'string'},
                {name: 'tipo', type: 'string'},
                {name: 'desc_item', type: 'string'},
                {name: 'desc_set', type: 'string'},
                {name: 'bonus_patrocinador', type: 'string'},
                {name: 'desc_bonus_config', type: 'string'},


            ],
            sortInfo: {
                field: 'id_punto_item',
                direction: 'ASC'
            },
            bdel: true,
            bsave: true
        }
    )
</script>
		
		