<?php
/**
*@package pXP
*@file gen-Rango.php
*@author  (admin)
*@date 14-10-2016 13:55:26
*@description Archivo con la interfaz de usuario que permite la ejecucion de todas las funcionalidades del sistema
*/

header("content-type: text/javascript; charset=UTF-8");
?>
<script>
Phx.vista.Rango=Ext.extend(Phx.gridInterfaz,{

	constructor:function(config){
		this.maestro=config.maestro;
    	//llama al constructor de la clase padre
		Phx.vista.Rango.superclass.constructor.call(this,config);
		this.init();
		this.load({params:{start:0, limit:this.tam_pag}})
	},
			
	Atributos:[
		{
			//configuracion del componente
			config:{
					labelSeparator:'',
					inputType:'hidden',
					name: 'id_rango'
			},
			type:'Field',
			form:true 
		},
		{
			config:{
				name: 'nivel',
				fieldLabel: 'Nivel',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
			type:'NumberField',
			filters:{pfiltro:'ran.nivel',type:'numeric'},
			id_grupo:1,
			grid:true,
			form:true
		},
		{
			config:{
				name: 'nombre',
				fieldLabel: 'Nombre',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:255
			},
			type:'TextField',
			filters:{pfiltro:'ran.nombre',type:'string'},
			id_grupo:1,
			grid:true,
			form:true
		},

		{
			config:{
				name: 'porcentaje_de_pago',
				fieldLabel: 'Porcentaje de Pago',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:655362
			},
				type:'NumberField',
				filters:{pfiltro:'ran.porcentaje_de_pago',type:'numeric'},
				id_grupo:1,
				grid:true,
				form:true
		},
		{
			config:{
				name: 'afiliado_activos',
				fieldLabel: 'Afiliados Activos',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'NumberField',
				filters:{pfiltro:'ran.afiliado_activos',type:'numeric'},
				id_grupo:1,
				grid:true,
				form:true
		},


		{
			config:{
				name: 'volumen_personal',
				fieldLabel: 'Volumen Personal',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,

			},
			type:'NumberField',
			filters:{pfiltro:'ran.volumen_personal',type:'numeric'},
			id_grupo:1,
			grid:true,
			form:true
		},

		{
			config:{
				name: 'volumen_organizacional',
				fieldLabel: 'Volumen Organizacional',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,

			},
				type:'NumberField',
				filters:{pfiltro:'ran.volumen_organizacional',type:'numeric'},
				id_grupo:1,
				grid:true,
				form:true
		},
		{
			config:{
				name: 'volumen_cobro_nivel',
				fieldLabel: 'Volumen Para Cobrar el Nivel',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
			
			},
				type:'NumberField',
				filters:{pfiltro:'ran.volumen_cobro_nivel',type:'numeric'},
				id_grupo:1,
				grid:true,
				form:true
		},
		{
			config: {
				name: 'id_rango_fk',
				fieldLabel: 'rango fk',
				allowBlank: true,
				emptyText: 'Elija una opción...',
				store: new Ext.data.JsonStore({
					url: '../../sis_afiliacion/control/Rango/listarRango',
					id: 'id_rango',
					root: 'datos',
					sortInfo: {
						field: 'nombre',
						direction: 'ASC'
					},
					totalProperty: 'total',
					fields: ['id_rango', 'nombre'],
					remoteSort: true,
					baseParams: {par_filtro: 'movtip.nombre'}
				}),
				valueField: 'id_rango',
				displayField: 'nombre',
				gdisplayField: 'desc_rango_fk',
				hiddenName: 'id_rango',
				forceSelection: true,
				typeAhead: false,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'remote',
				pageSize: 15,
				queryDelay: 1000,
				anchor: '100%',
				gwidth: 150,
				minChars: 2,
				renderer : function(value, p, record) {
					return String.format('{0}', record.data['desc_rango_fk']);
				}
			},
			type: 'ComboBox',
			id_grupo: 0,
			filters: {pfiltro: 'movtip.nombre',type: 'string'},
			grid: true,
			form: true
		},


		{
			config:{
				name: 'periodos_acumulados',
				fieldLabel: 'Periodos Acumulados',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'NumberField',
				filters:{pfiltro:'ran.periodos_acumulados',type:'numeric'},
				id_grupo:1,
				grid:true,
				form:true
		},

		{
			config:{
				name: 'usuario_ai',
				fieldLabel: 'Funcionaro AI',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:300
			},
				type:'TextField',
				filters:{pfiltro:'ran.usuario_ai',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'fecha_reg',
				fieldLabel: 'Fecha creación',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
							format: 'd/m/Y', 
							renderer:function (value,p,record){return value?value.dateFormat('d/m/Y H:i:s'):''}
			},
				type:'DateField',
				filters:{pfiltro:'ran.fecha_reg',type:'date'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'usr_reg',
				fieldLabel: 'Creado por',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'usu1.cuenta',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'id_usuario_ai',
				fieldLabel: 'Creado por',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'ran.id_usuario_ai',type:'numeric'},
				id_grupo:1,
				grid:false,
				form:false
		},
		{
			config:{
				name: 'fecha_mod',
				fieldLabel: 'Fecha Modif.',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
							format: 'd/m/Y', 
							renderer:function (value,p,record){return value?value.dateFormat('d/m/Y H:i:s'):''}
			},
				type:'DateField',
				filters:{pfiltro:'ran.fecha_mod',type:'date'},
				id_grupo:1,
				grid:true,
				form:false
		},
		{
			config:{
				name: 'estado_reg',
				fieldLabel: 'Estado Reg.',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:10
			},
			type:'TextField',
			filters:{pfiltro:'ran.estado_reg',type:'string'},
			id_grupo:1,
			grid:true,
			form:false
		},

		{
			config:{
				name: 'usr_mod',
				fieldLabel: 'Modificado por',
				allowBlank: true,
				anchor: '80%',
				gwidth: 100,
				maxLength:4
			},
				type:'Field',
				filters:{pfiltro:'usu2.cuenta',type:'string'},
				id_grupo:1,
				grid:true,
				form:false
		}
	],
	tam_pag:50,	
	title:'rango',
	ActSave:'../../sis_afiliacion/control/Rango/insertarRango',
	ActDel:'../../sis_afiliacion/control/Rango/eliminarRango',
	ActList:'../../sis_afiliacion/control/Rango/listarRango',
	id_store:'id_rango',
	fields: [
		{name:'id_rango', type: 'numeric'},
		{name:'porcentaje_de_pago', type: 'numeric'},
		{name:'afiliado_activos', type: 'numeric'},
		{name:'estado_reg', type: 'string'},
		{name:'volumen_organizacional', type: 'numeric'},
		{name:'volumen_cobro_nivel', type: 'numeric'},
		{name:'id_rango_fk', type: 'numeric'},
		{name:'volumen_personal', type: 'numeric'},
		{name:'nombre', type: 'string'},
		{name:'periodos_acumulados', type: 'numeric'},
		{name:'nivel', type: 'numeric'},
		{name:'usuario_ai', type: 'string'},
		{name:'fecha_reg', type: 'date',dateFormat:'Y-m-d H:i:s.u'},
		{name:'id_usuario_reg', type: 'numeric'},
		{name:'id_usuario_ai', type: 'numeric'},
		{name:'fecha_mod', type: 'date',dateFormat:'Y-m-d H:i:s.u'},
		{name:'id_usuario_mod', type: 'numeric'},
		{name:'usr_reg', type: 'string'},
		{name:'usr_mod', type: 'string'},
		{name:'desc_rango_fk', type: 'string'},

	],
	sortInfo:{
		field: 'id_rango',
		direction: 'ASC'
	},
	bdel:true,
	bsave:true
	}
)
</script>
		
		