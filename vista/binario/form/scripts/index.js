(function(window, document, undefined) {
  'use strict';
  var responsiveNav = window.responsiveNav;
  // Preloader
  $(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(500).fadeOut('slow');
  });

  // One page nav
  $('#main-nav').onePageNav({
    currentClass: 'active',
    scrollSpeed: 800,
    easing: 'easeOutQuart'
  });

  // Sticky
  $('#sticky').sticky({
    topSpacing: 0
  });

  // Responsive nav
  if($('.nav-collapse').length > 0) {
    responsiveNav('.nav-collapse', {
      label: '<span class="icon-th-menu"></span>',
      jsClass: 'javascript'
    });
  }

  // Back to top button functionality
  $(window).on('scroll', function() {
    var btn = $('.go-top');
    if($(this).scrollTop() > 200) {
      btn.show();
    } else {
      btn.hide();
    }
  });
  $('.go-top').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop: 0}, 700);
  });

  // jQuery niceScroll
  $('html').niceScroll({
    zindex: '100000',
    cursorwidth: '7px'
  });
})(this, this.document);
