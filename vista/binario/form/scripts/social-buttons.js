(function(window, document, undefined) {
  'use strict';
  // Preloader
  $(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(500).fadeOut('slow');
  });

  // Social buttons
  $('.btn').on('click', function(e) {
    e.preventDefault();
  });

  // Tooltips on icons
  $('.btn').tooltip({
    position: {
      my: 'center bottom-7',
      at: 'center top'
    },
    tooltipClass: 'arrow-cb'
  });
})(this, this.document);
