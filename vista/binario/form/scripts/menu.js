(function(window, document, undefined) {
  'use strict';
  // Preloader
  $(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(500).fadeOut('slow');
  });

  // jQueryUI menu
  $('#basic-menu').menu({
    icons: {
      submenu: 'icon-arrow-right9'
    },
    disabled: false,
    position: {
      my: 'left+2 top-1',
      at: 'right top'
    }
  });
})(this, this.document);
