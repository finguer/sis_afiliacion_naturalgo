(function(window, document, undefined) {
  'use strict';
  // Preloader
  $(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(500).fadeOut('slow');
  });

  // nice file inputs
  $('input[type=file]').nicefileinput();

  // jQuery niceScroll
  $('html').niceScroll({
    zindex: '100000',
    cursorwidth: '7px'
  });
})(this, this.document);
