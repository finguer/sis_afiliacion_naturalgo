(function(window, document, undefined) {
  'use strict';
  // Preloader
  $(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(500).fadeOut('slow');
  });
  // Basic datepickers
  $('#basic-datepicker-1').datepicker({
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>'
  });

  $('#basic-datepicker-2').datepicker({
    showButtonPanel: true,
    showOtherMonths: true,
    selectOtherMonths: true,
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>'
  });

  // Datepicker with month & year menus
  $('#datepicker-month-and-year-1').datepicker({
    showButtonPanel: false,
    changeMonth: true,
    changeYear: true,
    showOtherMonths: true,
    selectOtherMonths: true,
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>'
  });

  $('#datepicker-month-and-year-2').datepicker({
    showButtonPanel: true,
    changeMonth: true,
    changeYear: true,
    showOtherMonths: true,
    selectOtherMonths: true,
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>'
  });

  // Display multiple months
  $('#multiple-months').datepicker({
    numberOfMonths: 3,
    showOtherMonths: true,
    showButtonPanel: true,
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>'
  });

  // Inline calendar
  $('#datepicker-inline-1').datepicker({
    showOtherMonths: true,
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>'
  });
  $('#datepicker-inline-2').datepicker({
    showOtherMonths: true,
    changeMonth: true,
    changeYear: true,
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>'
  });

  // Date formats
  $('#datepicker-date-formats').datepicker({
    showButtonPanel: false,
    showOtherMonths: false,
    selectOtherMonths: true,
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>'
  });

  $('#date-format').on('change', function() {
    $('#datepicker-date-formats').datepicker('option', 'dateFormat', $(this).val());
  });

  // Input group
  $('#datepicker-icon').datepicker({
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>'
  });

  // Icon trigger
  $('#datepicker-trigger').datepicker({
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>',
    showButtonPanel: true,
    showWeek: true
  });

  $('#icon-trigger').on('click', function() {
    $('#datepicker-trigger').datepicker('show');
  });

  // Populate alternate field
  $('#datepicker-alternate-1').datepicker({
    altField: '#alternate',
    altFormat: 'DD, d MM, yy',
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>'
  });

  // Restrict date range
  $('#datepicker-restrict').datepicker({
    minDate: -20,
    maxDate: '+1M +10D',
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>'
  });

  // Select a date range
  $('#datepicker-from').datepicker({
    defaultDate: '+1w',
    onClose: function(selectedDate) {
      $('#datepicker-to').datepicker('option', 'minDate', selectedDate);
    },
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>'
  });

  $('#datepicker-to').datepicker({
    defaultDate: '+1w',
    onClose: function(selectedDate) {
      $('#datepicker-from').datepicker('option', 'maxDate', selectedDate);
    },
    prevText: '<i class="icon-arrow-left10"></i>',
    nextText: '<i class="icon-uniE91B"></i>'
  });

  // jQuery niceScroll
  $('html').niceScroll({
    zindex: '100000',
    cursorwidth: '7px'
  });
})(this, this.document);
