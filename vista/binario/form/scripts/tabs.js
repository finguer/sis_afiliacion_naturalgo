(function(window, document, undefined) {
  'use strict';
  // Preloader
  $(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(500).fadeOut('slow');
  });

  // jQueryUI tabs
  var tabs = $('#tabs-example-1').tabs({
    collapsible: true,
    event: 'click'
  });

  tabs.find('.ui-tabs-nav').sortable({
    axis: 'x',
    stop: function() {
      tabs.tabs('refresh');
    }
  });

  // jQueryUI vertical tabs
  $('#tabs-vertical-1').tabs().addClass('ui-tabs-vertical');
})(this, this.document);
