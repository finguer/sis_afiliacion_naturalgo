(function(window, document, undefined) {
  'use strict';
  // Preloader
  $(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(500).fadeOut('slow');
  });

  // NICE FILE INPUT
  $('input[type=file]').nicefileinput();
})(this, this.document);
