(function(window, document, undefined) {
  'use strict';
  // Preloader
  $(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(500).fadeOut('slow');
  });

  // Basic spinner
  var spinner = $('#basic-spinner').spinner({
    icons: {
      up: 'icon-arrow-up8',
      down: 'icon-arrow-down9'
    }
  });

  $('#toggle-de').on('click', function() {
    if(spinner.spinner('option', 'disabled')) {
      spinner.spinner('enable');
    } else {
      spinner.spinner('disable');
    }
  });

  $('#get-value').on('click', function() {
    console.log(spinner.spinner('value'));
  });

  $('#set-value').on('click', function() {
    spinner.spinner('value', 5);
  });

  // Currency
  $('#spinner-currency').spinner({
    min: 5,
    max: 5000,
    step: 25,
    start: 1000,
    icons: {
      up: 'icon-arrow-up8',
      down: 'icon-arrow-down9'
    }
  });

  // Decimal spinner
  $('#spinner-decimal').spinner({
    min: 5.00,
    max: 99.99,
    step: 0.01,
    numberFormat: 'n',
    icons: {
      up: 'icon-arrow-up8',
      down: 'icon-arrow-down9'
    }
  });

  // Overflow spinner
  $('#spinner-overflow').spinner({
    spin: function(event, ui) {
      if (ui.value > 10) {
        $( this ).spinner('value', -10);
        return false;
      } else if (ui.value < -10) {
        $( this ).spinner('value', 10);
        return false;
      }
    },
    icons: {
      up: 'icon-arrow-up8',
      down: 'icon-arrow-down9'
    }
  });
})(this, this.document);
