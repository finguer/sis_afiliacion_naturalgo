(function(window, document, undefined) {
  'use strict';
  // Preloader
  $(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(500).fadeOut('slow');
  });

  // Helper function for the validate plugin
  function appendLoader(name) {
    var $validatedInput = $('[name="' + name + '"]'),
        $inputFormGroup = $validatedInput.parents('.form-group'),
        $html = '<span class="image-loader icon-right"><img src="../images/arrows.gif" alt="Image loader"></span>';
    $inputFormGroup.addClass('has-icon-right').append($html);
    return false;
  }

  function removeLoader(name) {
    var $validatedInput = $('[name="' + name + '"]'),
        $inputFormGroup = $validatedInput.parents('.form-group');
    $inputFormGroup.removeClass('has-icon-right').find('.image-loader').remove();
    return false;
  }

  function closeAlert() {
    $('.alert-dismissible').find('.close').on('click', function() {
      $(this).parent().fadeOut();
    });
  }

  // Rules for the validate plugin
  var ruleList = {
    username: {
      required: true,
      rangelength: [2, 32],
      alphanumeric: true,
      remote: {
        url: 'username-checker.php',
        type: 'post',
        data: {
          username: function() {
            return $('[name="username"]').val();
          }
        },
        beforeSend: function() {
          appendLoader('username');
        },
        complete: function() {
          removeLoader('username');
        }
      }
    },
    email: {
      required: true,
      email: true,
      remote: {
        url: 'email-checker.php',
        type: 'post',
        data: {
          email: function() {
            return $('[name="email"]').val();
          }
        },
        beforeSend: function () {
          appendLoader('email');
        },
        complete: function() {
          removeLoader('email');
        }
      }
    },
    password: {
      required: true,
      minlength: 8
    },
    'retype_password': {
      required: true,
      equalTo: '#password'
    },
    'first_name': {
      required: true,
      minlength: 2
    },
    'last_name': {
      required: true,
      minlength: 2
    },
    gender: {
      required: true
    },
    terms: {
      required: true
    },
    captcha: {
      required: true,
      remote: {
        url: 'captcha-checker.php',
        type: 'post',
        data: {
          captcha: function() {
            return $('[name="captcha"]').val();
          }
        }
      }
    }
  };

  // Messages for the validate plugin
  var messages = {
    username: {
      required: 'The username is required.',
      rangelength: $.validator.format('Username must have between {0} and {1} characters.'),
      alphanumeric: 'Username must contain only letters, digits and underscores.',
      remote: 'The username already exists please choose another username.'
    },
    email: {
      required: 'The email address is required.',
      email: 'The email address must be valid.',
      remote: 'The email already exists please choose another email.'
    },
    password: {
      required: 'The password is required.',
      minlength: $.validator.format('The password must have at least {0} characters.')
    },
    'retype_password': {
      required: 'Retype password is required.',
      equalTo: 'Please enter the same password again.'
    },
    'first_name': {
      required: 'The first name is required.',
      minlength: $.validator.format('The first name must have at least {0} characters.')
    },
    'last_name': {
      required: 'The last name is required.',
      minlength: $.validator.format('The first name must have at least {0} characters.')
    },
    gender: {
      required: 'The gender is required.'
    },
    terms: {
      required: 'You must agree with the terms and conditions.'
    },
    captcha: {
      required: 'The captcha is required.',
      remote: 'The captcha was wrong. Please try again.'
    }
  };

  var $registerForm = $('#register-form');

  $registerForm.validate({
    success: function(label) {
      label.parents('.form-group').addClass('has-success');
      label.remove();
    },
    highlight: function(elem) {
      var formGroup = $(elem).parents('.form-group');
      if(formGroup.hasClass('has-success')) {
        formGroup.removeClass('has-success').addClass('has-error');
      } else {
        formGroup.addClass('has-error');
      }
    },
    unhighlight: function(elem) {
      $(elem).parents('.form-group').removeClass('has-error');
    },
    errorPlacement: function(error, elem) {
      $(elem).parents('.form-group').append(error).fadeIn();
    },
    errorElement: 'span',
    errorClass: 'help-block',
    rules: ruleList,
    messages: messages,
    submitHandler: function(form) {
      var $form = $(form),
          $submitButton = $form.find('#submit');
      $form.ajaxSubmit({
        data: {
          ajax: 'yes'
        },
        dataType: 'json',
        beforeSend: function() {
          $form.find('#submit').val('Loading...');
        },
        success: function(data) {
          var $submitMessageDiv;
          if(data.errorSend) {
            $submitMessageDiv = '<div id="submit-message" class="col-md-12"><div style="margin-top: 20px;" class="alert alert-danger alert-dismissible has-alert-icon"><span class="alert-icon icon-exclamation-circle"></span> <strong>Well done!</strong> ' + data.errorSend + ' <button type="button" class="close"><span class="icon-cancel5"></span><span class="sr-only"> Close</span></button></div></div>';
            $('#message-container').append($submitMessageDiv);
            $submitButton.val('Register');
            closeAlert();
          } else if(data.errorRegistration) {
            $submitMessageDiv = '<div id="submit-message" class="col-md-12"><div style="margin-top: 20px;" class="alert alert-danger alert-dismissible has-alert-icon"><span class="alert-icon icon-exclamation-circle"></span> <strong>Well done!</strong> ' + data.errorRegistration + ' <button type="button" class="close"><span class="icon-cancel5"></span><span class="sr-only"> Close</span></button></div></div>';
            $('#message-container').append($submitMessageDiv);
            $submitButton.val('Register');
            closeAlert();
          } else if(data.success) {
            $submitMessageDiv = '<div id="submit-message" class="col-md-12"><div style="margin-top: 20px;" class="alert alert-success alert-dismissible has-alert-icon"><span class="alert-icon icon-checkmark4"></span> <strong>Well done!</strong> ' + data.success + ' <a href="../examples/login.html" title="Log in" class="alert-link">Log in</a> or return to the <a href="../index_.html" title="Home" class="alert-link">home page</a>.<button type="button" class="close"><span class="icon-cancel5"></span><span class="sr-only"> Close</span></button></div></div>';
            $('#message-container').append($submitMessageDiv);
            $submitButton.val('Register');
            $registerForm.slideUp(300);
            closeAlert();
          }
        }
      });
    }
  });

  // Captcha reload
  var $captchaImage = $('.captcha-image');
  $captchaImage.on('click', function() {
    $(this).attr('src', 'captcha.php?n=' + Math.random());
    return false;
  });

  // Add captcha tooltip
  $captchaImage.tooltip({
    show: {
      duration: 300,
      effect: 'fadeIn'
    },
    hide: {
      duration: 300,
      effect: 'fadeOut'
    },
    position: {
      my: 'center bottom-10',
      at: 'center top'
    },
    tooltipClass: 'arrow-cb',
    track: false,
    items: '[data-tooltip]',
    content: function() {
      return $(this).attr('data-tooltip');
    }
  });

  // Show captcha tooltip on focus, hide on blur
  $('#captcha').on({
    focus: function() {
      $captchaImage.tooltip('open');
    },
    blur: function() {
      $captchaImage.tooltip('close');
    }
  });
})(this, this.document);
