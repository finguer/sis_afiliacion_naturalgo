var imprimirPagoPeriodo = ({pagoPeriodo, bonusPatrocinioTotales, bonusPatrocinioDetalle, habilitado_a_cobrar_paquete}) => {

    console.log('pagoPeriodo', pagoPeriodo)


    const {
        generaciones_cv,
        generaciones_pv,
        generaciones_cv_monto,
        generaciones_count_afiliados_100pts_pv,
        generaciones_count_afiliados_activos,
        porcentajes_pagados,
        monto_multiplicar,
        tipo_moneda
    } = pagoPeriodo;
    let bodyGen = '';

    if(porcentajes_pagados) {
        const countNivelesDisponible = porcentajes_pagados.length;

        if(generaciones_cv !== null) {

            bodyGen = `
                            <tr>
                            <td></td>
                         ${
                porcentajes_pagados.map((pp, indexPp) => `<td>${indexPp + 1}Gen (${pp}%)</td>`).join('')
            }
                            </tr>
                            <tr>
                            <td>CV</td>
                         ${
                generaciones_cv.map((gcv, i) => (i < countNivelesDisponible) && `<td>${gcv} cv</td>`).join('')
            }
                            </tr>
                            <tr>
                            <td>(CV * %) * ${monto_multiplicar}</td>
                         ${
                generaciones_cv_monto.map((gcvm, i) => (i < countNivelesDisponible) && `<td>${gcvm} ${tipo_moneda}</td>`).join('')
            }
                            </tr>
                            <tr>
                            <td>PV</td>
                         ${
                generaciones_pv.map((gpv, i) => (i < countNivelesDisponible) && `<td>${gpv} pv</td>`).join('')
            }
                            </tr>
                            `;

        }
        console.log('bodyGen',bodyGen)
    }



    let bonusPatrocinio = '';
    bonusPatrocinioTotales && bonusPatrocinioTotales.forEach((bpt) => {

        if(habilitado_a_cobrar_paquete && habilitado_a_cobrar_paquete.find(hacp => hacp.bonus === bpt.tipo) ) {
            bonusPatrocinio += `<tr>
<td>${bpt.tipo}</td>
<td>${bpt.sum}</td>
</tr>                               `;
        }



    });
    let bonusPatrocinioDetalleHtml = '';
    bonusPatrocinioDetalleHtml += `<tr>
<th>Bono Patrocinio</th>
<th>Monto</th>
<th>Afiliado Directo</th>
<th>Desde el Nivel</th>
<th>Desde el Codigo</th>
</tr>`;
    bonusPatrocinioDetalle && bonusPatrocinioDetalle.forEach((bpd) => {

        //if(habilitado_a_cobrar_paquete && habilitado_a_cobrar_paquete.find(hacp => hacp.bonus === bpt.tipo) ) {
        bonusPatrocinioDetalleHtml += `<tr>
<td>${bpd.tipo}</td>
<td>${bpd.monto}</td>
<td>${bpd.codigo_afiliado_hijo_directo}</td>
<td>${bpd.nivel_from}</td>
<td>${bpd.codigo_from}</td>
</tr>                               `;
       // }



    });


    const html =
        `<!doctype html>
    <html lang="en"><head>
    <meta charset="utf-8">
    <title>Example 1</title>
<style>
    .clearfix:after {
    content: "";
    display: table;
    clear: both;
}

    a {
    color: #5D6975;
    text-decoration: underline;
}

    body {
    position: relative;
    width: 21cm;
    height: 29.7cm;
    margin: 0 auto;
    color: #001028;
    background: #FFFFFF;
    font-family: Arial, sans-serif;
    font-size: 12px;
    font-family: Arial;
}

    header {
    padding: 10px 0;
    margin-bottom: 30px;
}

    #logo {
    text-align: center;
    margin-bottom: 10px;
}

    #logo img {
    width: 90px;
}

    h1 {
    border-top: 1px solid  #5D6975;
    border-bottom: 1px solid  #5D6975;
    color: #5D6975;
    font-size: 2.4em;
    line-height: 1.4em;
    font-weight: normal;
    text-align: center;
    margin: 0 0 20px 0;
    background: url(dimension.png);
}

    #project {
    float: left;
}

    #project span {
    color: #5D6975;
    text-align: right;
    width: 52px;
    margin-right: 10px;
    display: inline-block;
    font-size: 0.8em;
}

    #company {
    float: right;
    text-align: right;
}

    #project div,
    #company div {
    white-space: nowrap;
}

    table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px;
}

    table tr:nth-child(2n-1) td {
    background: #F5F5F5;
}

    table th,
    table td {
    text-align: center;
}

    table th {
    padding: 5px 20px;
    color: #5D6975;
    border-bottom: 1px solid #C1CED9;
    white-space: nowrap;
    font-weight: normal;
}

    table .service,
    table .desc {
    text-align: left;
}

    table td {
    padding: 20px;
    text-align: right;
}

    table td.service,
    table td.desc {
    vertical-align: top;
}

    table td.unit,
    table td.qty,
    table td.total {
    font-size: 1.2em;
}

    table td.grand {
    border-top: 1px solid #5D6975;;
}

    #notices .notice {
    color: #5D6975;
    font-size: 1.2em;
}

    footer {
    color: #5D6975;
    width: 100%;
    height: 30px;
    position: absolute;
    bottom: 0;
    border-top: 1px solid #C1CED9;
    padding: 8px 0;
    text-align: center;
}
</style>
</head>
<body>
<header class="clearfix">
<div id="logo">
<!--  <img src="logo.png">-->
</div>

<h1>RED</h1>
<div id="company" style="float: right;" class="clearfix">
<div>GONET</div>
<div>Tipo de Moneda: ${pagoPeriodo.tipo_moneda}</div>
<div>Multiplicador: ${pagoPeriodo.monto_multiplicar}</div>
<div>Niveles a cobrar: ${pagoPeriodo.niveles_cobrados}</div>
<div>Pago red: ${pagoPeriodo.pago_de_red}</div>
<div>Porcetajes pagados: ${pagoPeriodo.porcentajes_pagados}</div>


</div>
<div id="project" style="float: left;">
<div><span>Proceso de Pago</span></div>
<div><span>Codigo Afil </span> ${pagoPeriodo.codigo}</div>
<div><span>Gestion</span> ${pagoPeriodo.gestion}</div>
<div><span>Periodo</span> ${pagoPeriodo.periodo}/12.</div>
<div><span>Habilitado</span>${pagoPeriodo.habilitado}</div>
<div><span>Red Activos</span>${pagoPeriodo.afiliados_activos}</div>
<div><span>Registrados</span>${pagoPeriodo.afiliados_registrados}</div>

<div><span>Nombre</span> ${pagoPeriodo.nombre_completo2} </div>
<div><span>Estado</span> ${pagoPeriodo.estado} </div>

</div>
</header>
<main>
<div>

<table>
    <theader>
    <tr>
    <th>PAGO RED</th>
    <th>BONUS PAGADO</th>
    <th>BONUS NO PAGADO</th>
    <th>BONUS MULTINIVEL</th>
    <th>TOTAL GANADO</th>
    <th>TOTAL A PAGAR</th>
    </tr>
    </theader>
    <tbody>
    <tr>
    <td>
    ${pagoPeriodo.pago_de_red}
    </td>
    <td>
        ${pagoPeriodo.monto_bonus_ya_pagado}
    </td>
    <td>
        ${pagoPeriodo.monto_bonus_no_pagado}
    </td>
    <td>
        ${pagoPeriodo.monto_bonus_multinivel}
    </td>
    <td>
     ${pagoPeriodo.total_ganado}
    </td>
    <td style="font-size:20pt; color: green;">
    <b>        ${pagoPeriodo.pago_total}</b>
    </td>
    </tr>
    </tbody>
    </table>
    
    
    <table>
    <theader>
    <tr>
    <th>PV PROPIO</th>
    <th>PV ACUMULADO</th>
    <th>SUBE NIVEL</th>
    </tr>
    </theader>
    <tbody>
    <tr>
    <td>
    ${pagoPeriodo.pv_propio}
    </td>
    <td>
    ${pagoPeriodo.pv_acumulado}
    </td>
   
    <td style="font-size:20pt;">
    <b>${pagoPeriodo.sube_nivel}</b>
    </td>
    </tr>
    </tbody>
    </table>
<table>
<tbody>
${bodyGen}
</tbody>
</table>

Bonus Patrocinios
<table>
${bonusPatrocinio}
</table>

Bonus Patrocinios Detalle
<table>
${bonusPatrocinioDetalleHtml}
</table>


</div>
</main>
<!--<footer>
PROFORMA - KAE ELECTRONICS    </footer>-->

</body></html>`;


    return html;

}

