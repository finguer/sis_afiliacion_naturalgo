<?php
/**
 * @package pXP
 * @file gen-Afiliado.php
 * @author  (admin)
 * @date 02-04-2015 01:06:18
 * @description Archivo con la interfaz de usuario que permite la ejecucion de todas las funcionalidades del sistema
 */

header("content-type: text/javascript; charset=UTF-8");
?>
<script src="../../../sis_afiliacion/vista/afiliado/ReportePagoPeriodo.js?v=3"></script>

<script>
    Phx.vista.Afiliado = Ext.extend(Phx.gridInterfaz, {

            constructor: function (config) {
                this.maestro = config.maestro;

                console.log('config',config)
                console.log('this',this)
                console.log('Phx',Phx)
                console.log('Phx.vista',Phx.vista)

                this.win_pago = new Ext.Window(
                    {
                        layout: 'fit',
                        width: 500,
                        height: 250,
                        modal: true,
                        closeAction: 'hide',

                        items: new Ext.FormPanel({
                            labelWidth: 75, // label settings here cascade unless overridden

                            frame: true,
                            // title: 'Factura Manual Concepto',
                            bodyStyle: 'padding:5px 5px 0',
                            width: 339,
                            defaults: {width: 191},
                            // defaultType: 'textfield',

                            items: [this.cmbGestion, this.cmbPeriodo],

                            buttons: [{
                                text: 'Save',
                                handler: this.submitPagar,

                                scope: this
                            }, {
                                text: 'Cancel'
                            }]
                        })

                    });


                this.cmbGestion.on('select', function (combo, record, index) {
                    this.tmpGestion = record.data.gestion;
                    this.cmbPeriodo.enable();
                    this.cmbPeriodo.reset();
                    this.cmbPeriodo.store.baseParams = Ext.apply(this.cmbPeriodo.store.baseParams, {id_gestion: this.cmbGestion.getValue()});
                    this.cmbPeriodo.modificado = true;


                }, this);

                this.win = new Ext.Window({
                    layout: 'fit',
                    width: 200,
                    height: 100,
                    closeAction: 'hide',
                    title: 'Vista Previa de Notas de credito y debito',
                    preventBodyReset: true,
                    html: '<h1>This should be the way you expect it!</h1>',
                    buttons: [
                        {
                            text: '<i class="fa fa-check"></i> Aceptar',
                            handler: this.guardar,
                            scope: this
                        }, {
                            text: '<i class="fa fa-times"></i> Cancelar',
                            handler: this.closeWin,
                            scope: this
                        }]
                });


                //llama al constructor de la clase padre
                Phx.vista.Afiliado.superclass.constructor.call(this, config);
                this.init();
                this.iniciarEventos();


                this.addButton('generar pago', {
                    argument: {imprimir: 'generar pago'},
                    text: '<i class="fa fa-thumbs-o-up fa-2x"></i> generar pago', /*iconCls:'' ,*/
                    disabled: false,
                    handler: this.pagar
                });


                this.store.baseParams.solo_registrados_por_usuario = 'si';

                this.load({params: {start: 0, limit: this.tam_pag, solo_registrados_por_usuario: 'si'}})
            },

            cmbGestion: new Ext.form.ComboBox({
                fieldLabel: 'Gestion',
                allowBlank: false,
                emptyText: 'Gestion...',
                blankText: 'Año',
                store: new Ext.data.JsonStore(
                    {
                        url: '../../sis_parametros/control/Gestion/listarGestion',
                        id: 'id_gestion',
                        root: 'datos',
                        sortInfo: {
                            field: 'gestion',
                            direction: 'DESC'
                        },
                        totalProperty: 'total',
                        fields: ['id_gestion', 'gestion'],
                        // turn on remote sorting
                        remoteSort: true,
                        baseParams: {par_filtro: 'gestion'}
                    }),
                valueField: 'id_gestion',
                triggerAction: 'all',
                displayField: 'gestion',
                hiddenName: 'id_gestion',
                mode: 'remote',
                pageSize: 50,
                queryDelay: 500,
                listWidth: '280',
                width: 80
            }),


            cmbPeriodo: new Ext.form.ComboBox({
                fieldLabel: 'Periodo',
                allowBlank: false,
                blankText: 'Mes',
                emptyText: 'Periodo...',
                store: new Ext.data.JsonStore(
                    {
                        url: '../../sis_parametros/control/Periodo/listarPeriodo',
                        id: 'id_periodo',
                        root: 'datos',
                        sortInfo: {
                            field: 'periodo',
                            direction: 'ASC'
                        },
                        totalProperty: 'total',
                        fields: ['id_periodo', 'periodo', 'id_gestion'],
                        // turn on remote sorting
                        remoteSort: true,
                        baseParams: {par_filtro: 'gestion'}
                    }),
                valueField: 'id_periodo',
                triggerAction: 'all',
                displayField: 'periodo',
                hiddenName: 'id_periodo',
                mode: 'remote',
                pageSize: 50,
                disabled: true,
                queryDelay: 500,
                listWidth: '280',
                width: 80
            }),


            iniciarEventos: function () {

                /*this.Cmp.pais.on('select', function (rec) {
                    var pais = this.Cmp.pais.getValue();
                    if (pais == 'BOLIVIA') {

                        this.Cmp.codigo.setValue('591');
                    } else if (pais == 'PERU') {
                        this.Cmp.codigo.setValue('051');
                    }
                }, this);*/

                this.Cmp.id_persona_pre_registro.hide();


                this.Cmp.id_patrocinador.on('select', function (rec) {


                }, this);


                this.Cmp.id_set_item.on('select', function (rec, d) {

                    console.log(d.data.precio);
                    this.Cmp.monto_registro.setValue(d.data.precio);

                }, this);

                this.Cmp.pre_registro.on('select', function (rec, d) {

                    var preRegistro = this.Cmp.pre_registro.getValue();
                    if (preRegistro === 'si') {
                        this.Cmp.nombre.hide();
                        this.Cmp.ap_paterno.hide();
                        this.Cmp.ap_materno.hide();
                        this.Cmp.ci.hide();
                        this.Cmp.telefono1.hide();
                        this.Cmp.celular1.hide();
                        this.Cmp.correo.hide();
                        this.Cmp.id_persona_pre_registro.show();

                    } else {
                        this.Cmp.nombre.show();
                        this.Cmp.ap_paterno.show();
                        this.Cmp.ap_materno.show();
                        this.Cmp.ci.show();
                        this.Cmp.telefono1.show();
                        this.Cmp.celular1.show();
                        this.Cmp.correo.show();
                        this.Cmp.id_persona_pre_registro.hide();


                    }


                }, this);

                this.Cmp.id_persona_pre_registro.on('select', function (rec, d) {
                    console.log(rec)
                    console.log(d.data.id_patrocinador)
                    console.log(d.data.codigo_patrocinador)
                    console.log(this.Cmp.id_persona_pre_registro)

                    this.Cmp.id_patrocinador.enable();
                    this.Cmp.id_patrocinador.reset();
                    this.Cmp.id_patrocinador.store.setBaseParam('codigo', d.data.codigo_patrocinador);
                    this.Cmp.id_patrocinador.modificado = true;
                    this.Cmp.id_patrocinador.store.load({
                        params: {start: 0, limit: this.tam_pag},
                        callback: function (r) {
                            console.log('r', r)
                            this.Cmp.id_patrocinador.setValue(r[0].data.id_afiliado);

                            this.Cmp.id_patrocinador.fireEvent('select', this.Cmp.id_patrocinador, this.Cmp.id_patrocinador.store.getById(r[0].data.id_afiliado));

                        }, scope: this
                    });


                }, this);
            },


            Atributos: [
                {
                    //configuracion del componente
                    config: {
                        labelSeparator: '',
                        inputType: 'hidden',
                        name: 'id_afiliado'
                    },
                    type: 'Field',
                    form: true
                },

                {
                    config: {
                        name: 'pre_registro',
                        fieldLabel: 'Es pre registrado?',
                        allowBlank: false,
                        emptyText: 'Es pre registrado?',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: ['si', 'no'],
                        width: 200
                    },
                    type: 'ComboBox',
                    //filters: {pfiltro: 'item.tiene_numero_serie', type: 'string'},
                    id_grupo: 1,
                    form: true,
                    //grid: true
                },


                /*para personas registradas desde la oficina virtual*/
                {
                    config: {
                        name: 'id_persona_pre_registro',
                        fieldLabel: 'Persona',
                        allowBlank: true,
                        emptyText: 'Persona...',
                        store: new Ext.data.JsonStore({

                            url: '../../sis_afiliacion/control/Afiliado/listarPreRegistroPersona',
                            id: 'id_persona',
                            root: 'datos',
                            sortInfo: {
                                field: 'nombre_completo2',
                                direction: 'ASC'
                            },
                            totalProperty: 'total',
                            fields: ['id_persona', 'id_patrocinador', 'nombre_completo1', 'ci', 'codigo_patrocinador'],
                            // turn on remote sorting
                            remoteSort: true,
                            baseParams: {par_filtro: 'vper.nombre_completo1#per.ci'}
                        }),
                        valueField: 'id_persona',
                        displayField: 'nombre_completo1',
                        gdisplayField: 'desc_person',//mapea al store del grid
                        tpl: '<tpl for="."><div class="x-combo-list-item"><p>{nombre_completo1}</p><p>CI:{ci}</p> </div></tpl>',
                        hiddenName: 'id_persona',
                        forceSelection: true,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'remote',
                        pageSize: 10,
                        queryDelay: 1000,
                        width: 250,
                        gwidth: 280,
                        minChars: 2,
                        turl: '../../../sis_seguridad/vista/persona/Persona.php',
                        ttitle: 'Personas',
                        // tconfig:{width:1800,height:500},
                        tdata: {},
                        tcls: 'persona',
                        pid: this.idContenedor,

                        renderer: function (value, p, record) {
                            return String.format('{0}', record.data['desc_person']);
                        }
                    },
                    type: 'TrigguerCombo',
                    bottom_filter: true,
                    id_grupo: 0,
                    filters: {
                        pfiltro: 'nombre_completo1',
                        type: 'string'
                    },

                    grid: false,
                    form: true
                },

                {
                    //configuracion del componente
                    config: {
                        labelSeparator: '',
                        inputType: 'hidden',
                        name: 'id_persona'
                    },
                    type: 'Field',
                    form: true
                },


                {
                    config: {
                        name: 'desc_person',
                        fieldLabel: 'Persona',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'per.desc_person', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },

                {
                    config: {
                        name: 'nombre',
                        fieldLabel: 'Nombre',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'per.nombre', type: 'string'},
                    id_grupo: 1,
                    grid: false,
                    form: true
                },

                {
                    config: {
                        name: 'ap_paterno',
                        fieldLabel: 'Primer Apellido',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'per.apellido_paterno', type: 'string'},
                    id_grupo: 1,
                    grid: false,
                    form: true
                },

                {
                    config: {
                        name: 'ap_materno',
                        fieldLabel: 'Segundo Apellido(opcional)',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'per.ap_materno', type: 'string'},
                    id_grupo: 1,
                    grid: false,
                    form: true
                },


                {
                    config: {
                        name: 'ci',
                        fieldLabel: 'Carnet de Identidad',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'per.ci', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },


                {
                    config: {
                        fieldLabel: "Telefono",
                        gwidth: 120,
                        name: 'telefono1',
                        allowBlank: true,
                        maxLength: 15,
                        minLength: 5,
                        anchor: '100%'
                    },
                    type: 'TextField',
                    filters: {type: 'string'},
                    id_grupo: 0,
                    grid: false,
                    form: true
                },
                {
                    config: {
                        fieldLabel: "Celular",
                        gwidth: 120,
                        name: 'celular1',
                        allowBlank: true,
                        maxLength: 15,
                        minLength: 5,
                        anchor: '100%'
                    },
                    type: 'TextField',
                    filters: {type: 'string'},
                    id_grupo: 0,
                    grid: false,
                    form: true
                },

                {
                    config: {
                        fieldLabel: "Correo",
                        gwidth: 150,
                        name: 'correo',
                        allowBlank: true,
                        vtype: 'email',
                        maxLength: 100,
                        minLength: 5,
                        anchor: '100%'
                    },
                    type: 'TextField',
                    filters: {type: 'string'},
                    id_grupo: 0,
                    grid: false,
                    form: true
                },

                {
                    config: {
                        name: 'id_punto_venta',
                        hiddenName: 'id_punto_venta',
                        fieldLabel: 'Punto de Venta',
                        typeAhead: false,
                        forceSelection: false,
                        allowBlank: false,
                        emptyText: 'Sucuesales...',
                        store: new Ext.data.JsonStore({
                            url: '../../sis_sucursal/control/PuntoVenta/listarPuntoVenta',
                            id: 'id_punto_venta',
                            root: 'datos',
                            sortInfo: {
                                field: 'id_punto_venta',
                                direction: 'ASC'

                            },
                            totalProperty: 'total',
                            fields: ['id_punto_venta', 'id_sucursal', 'desc_sucursal', 'nombre'],
                            remoteSort: true,
                            baseParams: {
                                empleado_sucursal: 'si',
                                par_filtro: 'emsu.nombre_sucursal#emsu.nombre_sucursal'
                            }
                        }),


                        valueField: 'id_punto_venta',
                        displayField: 'nombre',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'remote',
                        pageSize: 20,
                        queryDelay: 200,
                        listWidth: 280,
                        minChars: 2,
                        width: '80%',
                        tpl: '<tpl for="."><div class="x-combo-list-item"><p>{desc_sucursal}</p><p>PV:{nombre}</p></div></tpl>',
                    },
                    type: 'ComboBox',
                    id_grupo: 1,
                    form: true
                },

                {
                    config: {
                        name: 'pais',
                        fieldLabel: 'Pais',
                        allowBlank: true,
                        emptyText: 'Pais...',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: ['BOLIVIA', 'PERU'],
                        width: 200
                    },
                    type: 'ComboBox',
                    id_grupo: 1,
                    form: true
                },


                {
                    config: {
                        name: 'codigo',
                        fieldLabel: 'codigo',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        disabled: false,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afil.codigo', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false

                },

//                {
//                    config: {
//                        name: 'id_persona',
//                        fieldLabel: 'Persona',
//                        allowBlank: false,
//                        emptyText: 'Persona...',
//                        store: new Ext.data.JsonStore({
//
//                            url: '../../sis_seguridad/control/Persona/listarPersona',
//                            id: 'id_persona',
//                            root: 'datos',
//                            sortInfo: {
//                                field: 'nombre_completo1',
//                                direction: 'ASC'
//                            },
//                            totalProperty: 'total',
//                            fields: ['id_persona', 'nombre_completo1', 'ci'],
//                            // turn on remote sorting
//                            remoteSort: true,
//                            baseParams: {par_filtro: 'p.nombre_completo1#p.ci'}
//                        }),
//                        valueField: 'id_persona',
//                        displayField: 'nombre_completo1',
//                        gdisplayField: 'desc_person',//mapea al store del grid
//                        tpl: '<tpl for="."><div class="x-combo-list-item"><p>{nombre_completo1}</p><p>CI:{ci}</p> </div></tpl>',
//                        hiddenName: 'id_persona',
//                        forceSelection: true,
//                        typeAhead: true,
//                        triggerAction: 'all',
//                        lazyRender: true,
//                        mode: 'remote',
//                        pageSize: 10,
//                        queryDelay: 1000,
//                        width: 250,
//                        gwidth: 280,
//                        minChars: 2,
//                        turl: '../../../sis_seguridad/vista/persona/Persona.php',
//                        ttitle: 'Personas',
//                        // tconfig:{width:1800,height:500},
//                        tdata: {},
//                        tcls: 'persona',
//                        pid: this.idContenedor,
//
//                        renderer: function (value, p, record) {
//                            return String.format('{0}', record.data['desc_person']);
//                        }
//                    },
//                    type: 'TrigguerCombo',
//                    id_grupo: 0,
//                    filters: {
//                        pfiltro: 'nombre_completo1',
//                        type: 'string'
//                    },
//
//                    grid: true,
//                    form: true
//                },
                {
                    config: {
                        name: 'direccion_principal',
                        fieldLabel: 'direccion principal',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afil.direccion_principal', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },
                {
                    config: {
                        name: 'lugar_nacimiento',
                        fieldLabel: 'lugar nacimiento',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afil.lugar_nacimiento', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },
                {
                    config: {
                        name: 'estado_reg',
                        fieldLabel: 'Estado Reg.',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 10
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afil.estado_reg', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'fecha_nacimiento',
                        fieldLabel: 'fecha nacimiento',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        format: 'd/m/Y',
                        renderer: function (value, p, record) {
                            return value ? value.dateFormat('d/m/Y') : ''
                        }
                    },
                    type: 'DateField',
                    filters: {pfiltro: 'afil.fecha_nacimiento', type: 'date'},
                    id_grupo: 1,
                    grid: true,
                    form: true
                },


                {
                    config: {
                        name: 'id_patrocinador',
                        fieldLabel: 'Patrocinador',
                        allowBlank: false,
                        emptyText: 'Elija una opción...',
                        store: new Ext.data.JsonStore({
                            url: '../../sis_afiliacion/control/Afiliado/listarAfiliado',
                            id: 'id_afiliado',
                            root: 'datos',
                            sortInfo: {
                                field: 'codigo',
                                direction: 'ASC'

                            },
                            totalProperty: 'total',
                            fields: ['id_afiliado', 'desc_person', 'direccion_principal', 'codigo', 'ci'],
                            remoteSort: true,
                            baseParams: {par_filtro: 'afil.direccion_principal#afil.codigo'}
                        }),
                        valueField: 'id_afiliado',
                        displayField: 'codigo',
                        gdisplayField: 'codigo_patrocinador',
                        tpl: '<tpl for="."><div class="x-combo-list-item"><p>{desc_person}</p><p>CI:{ci}</p>CODIGO: {codigo} </div></tpl>',

                        hiddenName: 'id_afiliado',
                        forceSelection: true,
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'remote',
                        pageSize: 15,
                        queryDelay: 1000,
                        anchor: '60%',
                        gwidth: 150,
                        minChars: 2,
                        renderer: function (value, p, record) {
                            return String.format('{0}', record.data['codigo_patrocinador']);
                        }
                    },
                    type: 'ComboBox',
                    id_grupo: 0,
                    filters: {pfiltro: 'afil.direccion_principal', type: 'string'},
                    grid: true,
                    form: true
                },


                {
                    config: {
                        name: 'id_set_item',
                        fieldLabel: 'paquete',
                        allowBlank: false,
                        emptyText: 'Elija una opción...',
                        store: new Ext.data.JsonStore({
                            url: '../../sis_venta/control/SetItem/listarSetItem',
                            id: 'id_set_item',
                            root: 'datos',
                            sortInfo: {
                                field: 'nombre',
                                direction: 'ASC'

                            },
                            totalProperty: 'total',
                            fields: ['id_set_item', 'nombre', 'descripcion', 'precio'],
                            remoteSort: true,
                            baseParams: {par_filtro: 'kitp.nombre#kitp.descripcion'}
                        }),
                        valueField: 'id_set_item',
                        displayField: 'nombre',
                        gdisplayField: 'desc_set',
                        tpl: '<tpl for="."><div class="x-combo-list-item"><p>{nombre}</p><p>descripcion: {descripcion} </div></tpl>',

                        hiddenName: 'id_set_item',
                        forceSelection: true,
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'remote',
                        pageSize: 15,
                        queryDelay: 1000,
                        anchor: '60%',
                        gwidth: 150,
                        minChars: 2,
                        renderer: function (value, p, record) {
                            return String.format('{0}', record.data['desc_set']);
                        }
                    },
                    type: 'ComboBox',
                    id_grupo: 0,
                    filters: {pfiltro: 'kitp.nombre', type: 'string'},
                    grid: true,
                    form: true
                },


                {
                    config: {
                        name: 'monto_registro',
                        fieldLabel: 'Monto',
                        allowBlank: true,
                        anchor: '30%',
                        gwidth: 100,
                        disabled: false,
                        maxLength: 255
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afil.monto_registro', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: true

                },


                {
                    config: {
                        name: 'id_usuario_ai',
                        fieldLabel: '',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 4
                    },
                    type: 'Field',
                    filters: {pfiltro: 'afil.id_usuario_ai', type: 'numeric'},
                    id_grupo: 1,
                    grid: false,
                    form: false
                },
                {
                    config: {
                        name: 'usr_reg',
                        fieldLabel: 'Creado por',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 4
                    },
                    type: 'Field',
                    filters: {pfiltro: 'usu1.cuenta', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'fecha_reg',
                        fieldLabel: 'Fecha creación',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        format: 'd/m/Y',
                        renderer: function (value, p, record) {
                            return value ? value.dateFormat('d/m/Y H:i:s') : ''
                        }
                    },
                    type: 'DateField',
                    filters: {pfiltro: 'afil.fecha_reg', type: 'date'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'usuario_ai',
                        fieldLabel: 'Funcionaro AI',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 300
                    },
                    type: 'TextField',
                    filters: {pfiltro: 'afil.usuario_ai', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'usr_mod',
                        fieldLabel: 'Modificado por',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        maxLength: 4
                    },
                    type: 'Field',
                    filters: {pfiltro: 'usu2.cuenta', type: 'string'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'fecha_mod',
                        fieldLabel: 'Fecha Modif.',
                        allowBlank: true,
                        anchor: '80%',
                        gwidth: 100,
                        format: 'd/m/Y',
                        renderer: function (value, p, record) {
                            return value ? value.dateFormat('d/m/Y H:i:s') : ''
                        }
                    },
                    type: 'DateField',
                    filters: {pfiltro: 'afil.fecha_mod', type: 'date'},
                    id_grupo: 1,
                    grid: true,
                    form: false
                },
                {
                    config: {
                        name: 'id_moneda',
                        origen: 'MONEDA',
                        fieldLabel: 'Moneda',
                        gdisplayField: 'moneda',//mapea al store del grid
                        gwidth: 200,
                        allowBlank: false,
                        renderer: function (value, p, record) {
                            return String.format('{0} - {1}', record.data['codigo_moneda'], record.data['moneda']);
                        }
                    },
                    type: 'ComboRec',
                    id_grupo: 0,
                    filters: {
                        pfiltro: 'mon.codigo#mon.moneda',
                        type: 'string'
                    },

                    grid: true,
                    form: true
                }
            ],
            tam_pag: 50,
            title: 'Afiliado',
            ActSave: '../../sis_afiliacion/control/Afiliado/insertarAfiliado',
            ActDel: '../../sis_afiliacion/control/Afiliado/eliminarAfiliado',
            ActList: '../../sis_afiliacion/control/Afiliado/listarAfiliado',
            id_store: 'id_afiliado',
            fields: [
                {name: 'id_afiliado', type: 'numeric'},
                {name: 'direccion_principal', type: 'string'},
                {name: 'id_persona', type: 'numeric'},
                {name: 'lugar_nacimiento', type: 'string'},
                {name: 'estado_reg', type: 'string'},
                {name: 'fecha_nacimiento', type: 'date', dateFormat: 'Y-m-d'},
                {name: 'id_usuario_ai', type: 'numeric'},
                {name: 'id_usuario_reg', type: 'numeric'},
                {name: 'fecha_reg', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'usuario_ai', type: 'string'},
                {name: 'id_usuario_mod', type: 'numeric'},
                {name: 'fecha_mod', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'usr_reg', type: 'string'},
                {name: 'usr_mod', type: 'string'},
                {name: 'desc_person', type: 'string'},
                {name: 'ci', type: 'string'},
                {name: 'desc_patrocinador', type: 'string'},
                'codigo_patrocinador',
                'codigo_colocacion',
                'codigo',
                {name: 'codigo', type: 'numeric'},
                {name: 'id_patrocinador', type: 'numeric'},
                {name: 'id_colocacion', type: 'numeric'},
                'monto_registro',
                'modalidad_de_registro',
                'colocacion',
                'colo_posi'
            ],
            sortInfo: {
                field: 'id_afiliado',
                direction: 'ASC'
            },
            bdel: false,
            bedit: false,
            bnew: false,
            bsave: true,
             ...((Phx.CP.config_ini.id_usuario == 1 || Phx.CP.config_ini.id_usuario == 3 || Phx.CP.config_ini.id_usuario == 2 || Phx.CP.config_ini.id_usuario == 28375 || Phx.CP.config_ini.id_usuario == 12299 ) && {tabeast:[
            {
                url:'../../../sis_afiliacion/vista/bonus/Bonus.php',
                title:'Bonus',
                width:600,
                cls:'Bonus'
            }]}),
            pagar: function () {

                this.win_pago.show();
            },
            submitPagar: function () {


                if (this.cmbGestion.validate() && this.cmbPeriodo.validate()) {

                    Phx.CP.loadingShow();

                    var id_periodo = this.cmbPeriodo.getValue();

                    var rec = this.sm.getSelected();

                    Ext.Ajax.request({
                        url: '../../sis_afiliacion/control/AfiliadoPagoPeriodo/generarPagoAfiliado',
                        params: {
                            id_afiliado: rec.data.id_afiliado,
                            id_periodo: id_periodo,
                            limit: 1,
                            start: 0,
                            impresion: 'si'
                        },
                        success: this.successPagar,
                        failure: this.conexionFailure,
                        timeout: this.timeout,
                        scope: this
                    });


                }


            },
            successPagar: function (resp) {

                console.log(resp)
                var objRes = Ext.util.JSON.decode(Ext.util.Format.trim(resp.responseText));
                console.log('objRes',objRes)
                const  json = JSON.parse(objRes.json);
                console.log('json',json)
                const {
                    pago_periodo,
                    bonus_patrocinio_detalle,
                    bonus_patrocinio_totales,
                    codigo,
                    desc_person,
                    habilitado_a_cobrar_paquete

                } = json[0];
                console.log('pago_periodo',pago_periodo[0])
                const resHtml = imprimirPagoPeriodo({
                    pagoPeriodo: pago_periodo[0],
                    bonusPatrocinioTotales: bonus_patrocinio_totales,
                    bonusPatrocinioDetalle: bonus_patrocinio_detalle,
                    habilitado_a_cobrar_paquete: habilitado_a_cobrar_paquete
                });
                console.log('resHtml',resHtml)



                var i = 0;

                var texto = objRes.datos;


                var wnd = window.open("about:blank", "", "_blank");
                wnd.document.write(resHtml);


                Phx.CP.loadingHide();
            },
            successSave: function (resp) {

                console.log(resp)
                var objRes = Ext.util.JSON.decode(Ext.util.Format.trim(resp.responseText));

                var i = 0;
                console.log(objRes);
                console.log(objRes.datos);
                console.log(objRes.ROOT.datos.json_res);
                console.log(Ext.util.JSON.decode(Ext.util.Format.trim(objRes.ROOT.datos.json_res)));
                var datosAfiliadoRegistrado = Ext.util.JSON.decode(Ext.util.Format.trim(objRes.ROOT.datos.json_res));
                var jsonAfiliadoRegistrado = datosAfiliadoRegistrado[0].afiliado_registrado[0];


                var texto = '<!DOCTYPE html>\n<html lang="en">\n <head>\n    <meta charset="UTF-8">\n    <title>Afiliacion</title>\n</head>\n<body>\n<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fafbfc" bgcolor="#fafbfc"\n       align="center">\n    <thead>\n    <tr>\n        <td align="center">\n            <div style="text-align: center; display: block; background-color: #282828;">\n                <center><img src="http://naturalgo.org/content/tea3/images/naturalgoLogo.png" align="middle" width="150"\n                             style="display: block;"/></center>\n            </div>\n            <div>\n                <br><br>\n                <span style="font-size:12px;font-family:\'Helvetica Neue\',Helvetica,Arial,sans-serif;color:rgb(153,153,153)"></span>\n                <span style="font-size:12px;font-family:\'Helvetica Neue\',Helvetica,Arial,sans-serif;color:rgb(153,153,153)">¡Enhorabuena! te has afiliado</span>\n\n            </div>\n        </td>\n    </tr>\n    </thead>\n    <tbody>\n    <tr>\n        <td align="center">\n            <table width="100%" border="0" cellpadding="0" cellspacing="0"\n                   style="border-collapse:collapse;border-spacing:0;width:90%;color:rgb(51,51,51);font-size:12px;font-family:\'Helvetica Neue\',Helvetica,Arial,sans-serif">\n                <tbody>\n                <tr height="24" style="background-color:#F5F5F5">\n                    <td colspan="2"\n                        style="padding-left:10px;border-top-left-radius:3px;border-bottom-left-radius:3px">\n                        <span style="font-size:14px;font-weight:500">Datos de la Afiliación</span>\n                    </td>\n                    <td align="right"\n                        style="padding-right:20px;border-top-right-radius:3px;border-bottom-right-radius:3px">\n                        <span style="color:#999999;font-size:10px;white-space:nowrap">Detalle</span>\n                    </td>\n                </tr>\n                <tr height="90">\n                    <td style="margin:0;height:60px;">Afiliado</td>\n                    <td width="40%" style="line-height:15px;" align="left">\n                        <span style="font-weight:600">' + jsonAfiliadoRegistrado.nombre_completo2 + '</span><br>\n                        <span style="font-weight:600; font-size: 20px;">' + jsonAfiliadoRegistrado.codigo + '</span><br>\n                        <span style="color:rgb(153,153,153)">Patrocinador: ' + jsonAfiliadoRegistrado.codigo_patrocinador + '</span><br>\n                        <span style="color:rgb(153,153,153)">' + jsonAfiliadoRegistrado.nombre_set + '</span><br>\n                        <span style="font-size:10px"></span>\n                    </td>\n                    <td width="40%" align="right" style="padding:0 20px 0 0;">\n                        <table>\n                            <tr>\n                                <td style="text-align: left; padding-right: 20px; font-weight:200;white-space:nowrap">\n                                    Datos Persona\n                                </td>\n                                <td style="text-align: right; font-weight:600;white-space:nowrap"></td>\n                            </tr>\n                            <tr>\n                                <td style="text-align: left; font-weight:200;white-space:nowrap">Nombre Completo:</td>\n                                <td style="text-align: right; font-weight:600;white-space:nowrap">' + jsonAfiliadoRegistrado.nombre_completo2 + '</td>\n                            </tr>\n                            <tr>\n                                <td style="text-align: left; font-weight:200;white-space:nowrap">CI</td>\n                                <td style="text-align: right; font-weight:600;white-space:nowrap">' + jsonAfiliadoRegistrado.ci + '</td>\n                            </tr>\n                            <tr>\n                                <td style="text-align: left; font-weight:200;white-space:nowrap">Celular</td>\n                                <td style="text-align: right; font-weight:600;white-space:nowrap">' + jsonAfiliadoRegistrado.celular1 + '</td>\n                            </tr>\n                            <tr>\n                                <td style="text-align: left; font-weight:200;white-space:nowrap">Telefono</td>\n                                <td style="text-align: right; font-weight:600;white-space:nowrap">' + jsonAfiliadoRegistrado.telefono1 + '</td>\n                            </tr>\n                            <tr>\n                                <td style="text-align: left; font-weight:200;white-space:nowrap">Correo</td>\n                                <td style="text-align: right; font-weight:600;white-space:nowrap">' + jsonAfiliadoRegistrado.correo + '</td>\n                            </tr>\n                            <tr height="1">\n                                <td height="1" colspan="3"\n                                    style="padding:0 10px 0 10px">\n                                    <div style="line-height:2px;min-height:2px;background-color:rgb(238,238,238)"></div>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="text-align: left; font-weight:600;white-space:nowrap">Patrocinador</td>\n                                <td style="text-align: right; font-weight:600;white-space:nowrap">' + jsonAfiliadoRegistrado.codigo_patrocinador + '\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="text-align: left; font-weight:200;white-space:nowrap">Nombre Patrocinador\n                                </td>\n                                <td style="text-align: right; font-weight:600;white-space:nowrap">' + jsonAfiliadoRegistrado.nombre_patrocinador + '\n                                </td>\n                            </tr>\n                            <tr height="1">\n                                <td height="1" colspan="3"\n                                    style="padding:0 10px 0 10px">\n                                    <div style="line-height:3px;min-height:3px;background-color:rgb(238,238,238)"></div>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="text-align: left; font-weight:600;white-space:nowrap">Nro de Venta</td>\n                                <td style="text-align: right; font-weight:600;white-space:nowrap">' + jsonAfiliadoRegistrado.nro_venta + '</td>\n                            </tr>\n                            <tr>\n                                <td style="text-align: left; font-weight:600;white-space:nowrap">Paquete Registro</td>\n                                <td style="text-align: right; font-weight:600;white-space:nowrap">' + jsonAfiliadoRegistrado.nombre_set + '</td>\n                            </tr>\n                            <tr height="1">\n                                <td height="1" colspan="3"\n                                    style="padding:0 10px 0 10px">\n                                    <div style="line-height:3px;min-height:3px;background-color:rgb(238,238,238)"></div>\n                                </td>\n                            </tr>\n                                                      \n                        </table>\n                    </td>\n                </tr>\n                <tr>\n                    <td colspan="3">\n                        <div>\n                            <table width="100%">\n                                <tr height="1">\n                                    <td height="1" colspan="3"\n                                        style="padding:0 10px 0 10px">\n                                        <div style="line-height:1px;min-height:1px;background-color:rgb(238,238,238)"></div>\n                                    </td>\n                                </tr>\n                                                   <tr height="1">\n                                    <td height="1" colspan="3"\n                                        style="padding:0 10px 0 10px">\n                                        <div style="line-height:1px;min-height:1px;background-color:rgb(238,238,238)"></div>\n                                    </td>\n                                </tr>\n                            </table>\n                        </div>\n                    </td>\n                </tr>\n                </tbody>\n            </table>\n        </td>\n    </tr>\n    <tr>\n        <td>\n            <div style="background-color:#2D4D89; height: auto; width: 100%; color: #fff;font-family: \'Roboto\', sans-serif;font-weight: 100; font-size: 12px;">\n                \n\n\n            </div>\n        </td>\n    </tr>\n\n    </tbody>\n</table>\n</body>\n</html>';


                var wnd = window.open("about:blank", "", "_blank");
                wnd.document.write(texto);

                this.window.hide();

                this.reload();

                Phx.CP.loadingHide();
            },


        }
    )
</script>
		
